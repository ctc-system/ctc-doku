---
layout: szenario
title:  "Starter-Set erweitert um einen Schattenbahnhof"
chapter: 1.2
date:   2023-03-08
author: Klaus Gungl
---
Die [Gleisanlage des Starterkits]({% link _starterkit/Einleitung_Ausblick.md %}) wird um einen Schattenbahnhof erweitert. 
Es werden zusätzliche Weichen, Balisen und Signale eingebaut. 
Die dafür notwendigen Module gilt es einzubauen.
Der Gleisplan wird für den kommenden Zugbetrieb in Blöcke aufgeteilt.

Die Erweiterungen ergeben den folgenden Gleisplan:

![Gleisbild Starterkit Erweiterung Schattenbahnhof]({% link assets/images/Starterkits/Szenarien/Schattenbahnhof/Szenario_Schattenbahnhof.jpg %})

Weitere Beschreibung folgt in Kürze.



