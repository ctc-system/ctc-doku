---
layout: szenario
title:  "Starter-Set erweitert um einen Sackbahnhof"
chapter: 1.1
date:   2023-03-08
author: Klaus Gungl
---
Die [Gleisanlage des Starterkits]({% link _starterkit/Einleitung_Ausblick.md %}) wird um einen Sackbahnhof erweitert. 
Es wird ein Weiche, zusätzliche Balisen und Signale eingebaut. 
Die dafür notwendigen Module gilt es einzubauen.
Der Gleisplan wird für den kommenden Zugbetrieb in Blöcke aufgeteilt.

Die Erweiterungen ergeben den folgenden Gleisplan:

![Gleisbild für Starterkit Modellbahnanlage erweitert 1]({% link assets/images/Starterkits/Szenarien/Sackbahnhof/Szenario_Sackbahnhof.jpg %})

Weitere Beschreibung folgt in Kürze.



