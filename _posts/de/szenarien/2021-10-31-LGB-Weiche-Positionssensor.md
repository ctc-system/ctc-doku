---
layout: post
title:  "LGB Weiche mit Positionssensor"
date:   2021-10-31
categories: CTC-Module 
tags: Weiche Weichenmodul Gartenbahn LGB Sensor Weichenstellung
author: Peter Rudolph
---
Dieser Artikel wurde verschoben zur Einbauanleitung ["Anschluss LGB-Weiche mit Sensor"]({% link _anschliessen/Weiche-LGB-Sensor.md %})