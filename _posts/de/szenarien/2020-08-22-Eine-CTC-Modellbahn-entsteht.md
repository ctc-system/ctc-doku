---
layout: post
title:  "Eine CTC-Modellbahn entsteht"
date:   2020-08-22
categories: Modellbahn
tags: Einsteiger Lok Signal Weiche Automatik
author: Peter Rudolph
---
**Hinweis: Dieser Artikel wurde am 05.02.2021 soweit angepasst, dass er zur neuen Version 3 von CTC passt.
Screenshots die sich nur optisch geändert haben wurden belassen.**

In diesem Artikel stelle ich Ihnen vor, wie eine CTC-Modellbahn entsteht.
Dabei kommen wir bei allem vorbei, was CTC ausmacht: Von den CTC-Lok- und CTC-Weichenmodulen bis zu den Infrarot-Sensoren und einfacher Automatik ist alles dabei. 
Das einzige, das wir vernachlässigen, ist die Modell-Landschaft.

## Planung und Stückliste 

![Demoanlage]({% link assets/images/demoanlage/Demoanlage-1.jpg %})

Unsere Modellbahn soll ein einfaches Gleisoval mit einem Ausweichgleis werden.
Wir nutzen Märklin C-Gleise (Spur H0), weil ich mich als alter Märklinist damit am besten auskenne.
Es spricht aber nichts dagegen diese Demoanlage auch auf Basis eines Zweileiter-Systems aufzubauen.
Aufgrund der Größe der Lokmodule ist allerdings kleiner als Spur TT (noch) nicht möglich. 

![Stückliste Demoanlage]({% link assets/images/demoanlage/Demoanlage-Stueckliste.jpg %})

**Hinweis:** Auf dem Foto sind schon ein paar Bauteile zu sehen, die wir erst für den zweiten Teil benötigen. 

Folgendes brauchen wir für die Modellbahn an sich:
* 10x gebogenes Gleis R1, 30°
* 2x gebogenes Gleis R2, 30°
* 12x gerades Gleis 188,3 mm
* 4x gerades Gleis 77,5 mm
* 1 Paar Bogenweichen inkl. elektrischem Antrieb
* 2x Flügelsignal
* 2x Lokomotive
* 1x Netzteil mit Anschlusskabel für C-Gleis

Und folgendes kommt hinzu, damit es eine CTC-Modellbahn wird:
* 1x [CTC-Router]({% link _data_sheet/CTC-Router.md %})
* 2x [CTC-Weichenmodul]({% link _data_sheet/CTC-Weichenmodul-H0.md %})
* 2x [CTC-Lokmodul]({% link de/docu/CTC-Lokmodule.md %})
* 4x [CTC-IR-Balise]({% link _data_sheet/CTC-IR-Sender.md %}): 20x IR-LED, 4x 1 kOhm Widerstände, 2 Lochrasterplatinen 50 mm x 100 mm, Kabel, 6x 2er-Pfostenleiste 2,54 mm
* Kabel und Stecker (2x 3er und 2x 4er Buchsenleiste 2,54 mm) zum Anschluss der Signale und CTC-IR-Balisen an die CTC-Weichenmodule
  
## Vorbereitung  

### IR-Balisen in die Gleise einbauen

Wie die IR-Balisen in Gleis eingebaut werden, können Sie im Artikel [CTC-IR-Balise]({% link _data_sheet/CTC-IR-Sender.md %}) nachlesen.

### Loks umbauen

Den Umbau einer Märklin E103 hat Klaus Gungl hier ausführlich beschrieben: [Lok-Umbau]({% link _posts/de/szenarien/2020-09-09-Umbau-E103-Ma.md %})  

### CTC-Weichenmodule ergänzen

Damit wir unsere zwei Signale und vier der sechs IR-Balisen an unsere beiden CTC-Weichenmodule anschließen können, müssen an diese noch ein paar Kabel angelötet werden:

![CTC-Weichenmodul]({% link assets/images/demoanlage/Demoanlage-Weichenmodul.jpg %})

Anschließend werden die CTC-Weichenmodule in die beiden Bogenweichen eingebaut:

![Bogenweiche von unten]({% link assets/images/demoanlage/Demoanlage-Weiche-unten.jpg %})
 
## Modellbahn aufbauen

Der Aufbau der Modellbahn ist schnell erledigt:

1. Stromversorgung an einem beliebigen Gleis einstecken.

1. Gleise zusammenzustecken.

1. Jeweils die zwei IR-Balisen und das zugehörige Signal mit den CTC-Weichenmodulen verbinden.
Dabei wird das vordere Gleis mit Dem CTC-Modul der rechten und das hintere Gleis mit dem CTC-Modul der linken Weiche verbunden.
![Demoanlage]({% link assets/images/demoanlage/Demoanlage-Signal-und-IR.jpg %})

## Modellbahn konfigurieren

Nun stecken wir den CTC-Router ein und warten bis dessen WLAN-Leuchte dauerhaft brennt.
Dann stecken wir die Modellbahn ein und öffnen die App.
Nach wenigen Sekunden zeigt die App die Loks und Weichen, aber natürlich noch kein Gleisbild an:

![Demoanlage]({% link assets/images/demoanlage/Demoanlage-App-1.png %})

**Hinweis 1:** Bezüglich der CTC-Module gehen wir davon aus, dass Sie uns beim Kauf der Module die Zugangsdaten Ihres Modellbahn-WLANs mitgeteilt haben.
Wenn nicht sehen Sie ncoh keine Loks und auch keien Weichen und müssen diese jetzt mit der CTC-App ins Modellbahn-WLAN einbuchen.
Wie das geht, steht in der [CTC-App Doku]({% link de/docu/CTC-App-Doku.html %}). 

**Hinweis 2:** Viele der folgenden Schritte mit der CTC-App sind übrigens in der [CTC-App Doku]({% link de/docu/CTC-App-Doku.html %}) ausführlich beschrieben. 

### CTC-Weichenmodule konfigurieren

In diesem Schritt legen wir fest, dass an den CTC-Weichenmodulen jeweils eine Weiche, ein Signal und zwei IR-Balisen angeschlossen sind.
Dabei bekommen alle Weichen und Signale einen sprechenden Namen.

Herauszufinden welche Weiche nun wo eingebaut ist, geht am einfachsten, indem wir sie in der Weichenansicht der CTC-App schalten.
Dann klicken wir das Stift-Symbol neben der Weiche an, um sie zu konfigurieren.
Als erstes ändern wir den Namen des CTC-Moduls:

![Demoanlage]({% link assets/images/demoanlage/Demoanlage-Weiche-Name.png %})

Dann gehen wir in die Config:
 
![Demoanlage]({% link assets/images/demoanlage/Demoanlage-Weichenmodul.jpg %})

Dort gehen wir auf den Stift neben der Weiche und ändern ihren Namen.

Anschließend legen wir die Signale an, indem wir sie aus dem Produktkatalog "universell-signale.xml" auswählen.

![Demoanlage]({% link assets/images/demoanlage/Demoanlage-Signal-Cfg.png %})

Dann geben wir noch an, mit welche Pins mit welchen Signalanschlüssen verbunden sind. 

Nun zeigt die CTC-App bei den Weichen die richtigen Namen und vor allem auch die Signale an.
  
![Demoanlage]({% link assets/images/demoanlage/Demoanlage-App-2.png %})

Bleibt noch zu testen, ob Signale und Weichen richtig herum schalten. Wenn nicht, dann
* können wir bei den Signalen am einfachsten den Stecker um 180° verdrehen.
* müssen wir bei den Weichen den zu schaltenden Pin in der Config anpassen.

Der Name der IR-Balisen setzt sich aus zwei Zeichen für den Gleisabschnitt und ein weiteres für die Nummerierung im Uhrzeigersinn zusammen.
Außerdem tragen wir bei den IR-Balisen den Abstand in Zentimeter zur vorhergehenden IR-Balise im selben Gleisabschnitt ein.    

### Gleisplan erstellen

Da wir unsere Modellbahn in der Mitte teilbar machen wollen, erstellen wir zwei Gleispläne.
Diese werden in linken und rechten Weichen gespeichert.

So wird ein Gleisplan erstellt: 
1. In der CTC-App den Konfigurator öffnen und dort das CTC-Weichenmodul wählen, in dem das Gleisbild gespeichert werden soll.
1. Den Button "Config" klicken und dann "Gleisplan ändern"
1. Den Gleisplan zeichnen.
1. Die Weichen, Signale und IR-Balisen zuordnen.
1. Den Gleisplan aufs Modul hochladen.

![Demoanlage]({% link assets/images/demoanlage/Demoanlage-Gleisplan-Edit.png %})

### Modellbahn erstellen

In diesem Schritt werden die beiden Gleispläne zu einer Modellbahn zusammengefügt.
Erst dann werden sie im Gleisbildstellwerk auch angezeigt.

![Demoanlage]({% link assets/images/demoanlage/Demoanlage-Modellbahn-Edit.png %})

## Erster Testbetrieb

Nachdem alles konfiguriert ist starten wir die CTC-App noch mal neu, um uns zu vergewissern, dass alles was wir sehen auch aus den CTC-Modulen kommt.

Nun können wir im Gleisbild die Weichen und Signale schalten sowie die Loks steuern.

![Demoanlage]({% link assets/images/demoanlage/Demoanlage-App-3.png %})

Wenn die Lok über einen Sensor fährt wird diese im Gleisbild an der entsprechenden Stelle angezeigt. 
Außerdem sehen wir den Namen des überfahrenen Sensors im Panel der Lok.
Überfährt die Lok den zweiten Sensor eines Gleisabschnitts, so wird außerdem die Geschwindigkeit der Lok zwischen den beiden Sensoren in cm/s angezeigt. 

## Automatikbetrieb

Für den Automatikbetrieb müssen wir nun nur noch die Sensoren mit entsprechenden Kommandos verknüpfen.
Wie das geht erfahren Sie im Artikel [Automatisiert fahren]({% link _posts/de/szenarien/2020-09-01-Automatisiert-fahren.md %})