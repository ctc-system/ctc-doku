---
layout: post
title:  "Umsteigen von Digital"
date:   2020-06-19
categories: Umsteigen
tags: Digital Lok Weiche Signal Umsteiger
author: Peter Rudolph
---
Der Umstieg von einer digitalen Steuerung auf CTC lohnt sich vor allem, wenn die Modellbahn noch weiter wachsen soll.
Ein weiterer Grund für die Umstellung auf CTC können die IR-Balisen sein, die deutliche Vorteile bei der Positionserkennung von Loks und der Automatik-Steuerung bieten.

## Was braucht man für CTC?

* Eine Stromversorgung: Das wird erst mal die Digital-Zentrale bzw. der Booster sein.
* Ein [WLAN-Access-Point]({% link de/docu/WLAN-Access-Point.md %}): Für die ersten Versuche tut es auch das private WLAN.
  Ab ca. 20 Modulen sollte es dann aber schon ein eigenes WLAN für die Modellbahn sein.
* Für jede Lokomotive ein [CTC-Lokmodul]({% link de/docu/CTC-Lokmodule.md %}).
* Für die Weichen und Signale entweder je ein [CTC-Weichenmodul]({% link _data_sheet/CTC-Weichenmodul-H0.md %}) oder ein [CTC-Multi-I/O-Board]({% link _data_sheet/CTC-Multi-IO-Board.md %}) für bis zu 4 Weichen/Signale.

Der Parallelbetrieb von digitalen Loks/Weichen/Signalen und CTC-Loks/Weichen/Signalen ist jederzeit möglich.
Die Steuerung erfolgt dann aber getrennt voneinander über die Digital-Zentrale und die CTC-App.
Eine Verbindung beider Welten in einer Steuerung wird erst möglich, wenn Modellbahn-Steuerungsprogramme auch CTC einbinden können.

## Was passiert mit meinen Digital-Dekodern?

Die Digital-Dekoder werden durch entsprechende CTC-Module ersetzt.

Für Lokomotiven mit ausgefeilten Sound-Dekodern ist es auch möglich, das CTC-Lokmodul zwischen Gleis und Sound-Dekoder zu setzen.
Das setzt allerdings voraus, dass die Lok über genügend Platz für Digital-Dekoder und CTC-Lokmodul verfügt.
Vor allem in Dampfloks ist dies oft nicht der Fall.

## Wie fange ich an?

Am einfachsten lassen Sie sich von uns ein [Starter-Set]({% link _data_sheet/CTC-Starter-Sets.md %}) kommen, um sich so erst mal mit der neuen Welt von CTC vertraut zu machen.
Dabei können Sie entweder mit einer Weiche, einer Lok oder mit beidem beginnen.

In jedem Fall bekommen Sie einen CTC-Router, die CTC-Module und die CTC-App.
Mehr brauchen Sie nicht, denn die Stromversorgung übernimmt Ihre bestehende Digital-Zentrale bzw. der Booster.
Die CTC-Lok kann gleichzeitig mit Digital-Loks auf demselben Gleis fahren.
Die Weichen können mit Digital-Strom oder einer separaten Stromquelle versorgt werden.

## Und wie geht es weiter?

Dazu gibt es diverse Möglichkeiten:
* Sie rüsten nach und nach alle Loks, Weichen und Signale auf CTC um und erreichen irgendwann eine rein mit CTC gesteuerte Modellbahn.
* Sie rüsten nur Weichen, Signale und Rückmelder auf CTC um und fahren Ihre Loks nach wie vor digital.
* Sie rüsten nur Ihre Loks auf CTC um und bauen ggf. CTC-IR-Balisen ins Gleis ein, schalten aber nach wie vor digital.

Eine Unterstützung von CTC durch Modellbahnsteuerungs-Software wird kommen und dann nahezu beliebige Kombinationen aus klassisch Digital und CTC ermöglichen.
Aber wir werden alles dran setzen, Sie von der rein mit CTC betriebenen Modellbahn zu überzeugen.

## Was passiert mit meinen Rückmeldern?

Hier wird es viel einfacher mit CTC: Der Rückmeldebus entfällt komplett.
Jeweils vier Sensoren können an das CTC-Multi-I/O-Board angeschlossen werden.

Allerdings lohnt es sich, einen Blick auf die [CTC-IR-Balisen]({% link _data_sheet/CTC-IR-Sender.md %}) zu werfen.
Jeweils zwei von ihnen können an ein CTC-Weichenmodul oder CTC-Multi-I/O-Board angeschlossen werden.
Sie ermöglichen es, dass die Lok ihre Position ermitteln und ggf. auch einen Befehl (z.B. Signal Halt in 70cm Entfernung) empfangen kann.
Mithilfe der CTC-IR-Balisen ist eine einfache Automatisierung ganz ohne Eingriff der App möglich.

In allen Fällen werden die genannten Rückmelde-Informationen zusammen mit den Status-Meldungen der CTC-Module über das WLAN übertragen.

Mehr zur Automatisierung erfahren Sie im Artikel [Automatisiert fahren]({% link _posts/de/szenarien/2020-09-01-Automatisiert-fahren.md %}).

