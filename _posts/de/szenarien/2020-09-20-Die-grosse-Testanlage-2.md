---
layout: post
title:  "Die große Testanlage - Teil 2"
date:   2020-09-20
categories: Modellbahn
tags: Anlage Lok Signal Weiche Automatik
author: Peter Rudolph
---
Fortsetzung von [Teil 1]({% link _posts/de/szenarien/2020-09-07-Die-grosse-Testanlage.md %}). 

Die letzten zwei Wochen waren geprägt von Planung der Signale für den Hauptbahnhof und von Tüfteleien rund um Märklins C-Gleis.

## Das entnehmbare C-Gleis

Das hat zwar mit CTC nichts zu tun, möchten wir aber niemandem vorenthalten: 
So schön das C-Gleis für Teppichbahner ist so ärgerlich ist es für die feste Installation:
Es ist ruckzuck zusammengesteckt, aber wenn man es wieder trennen will, muss man die halbe Anlage zerlegen.
Peters Vater hat schon vor 20 Jahren drüber geflucht und sich vom Märklin-Support auslachen lassen, als er dort anrief und ein entnehmbares C-Gleis anregte.

Nun ja das entnehmbare C-Gleis gibt es nahc wie vor nicht, lässt sich aber einfach herstellen:
1. An einem Ende die Kunstofflasche und sonstigen Nasen absägen
1. Mit einer Zange die Anschlusslaschen komplett herausoperieren.
1. Das Gleisende nach unten hin leicht schräg abfeilen.
1. Dort wo vom anderen Gleis der Überstand kommt noch ein bisschen eine Nut reinfeilen.

![Testanlage am 07.09.2020]({% link assets/images/testanlage/C-Gleis-entnehmbar.jpg %})

Die seitliche Führung ist immer noch erstaunlich gut und angeschraubt passt das Gleis perfekt.
Mit einem Messer lässt es sich jederzeit leicht nach oben rauskippen:  

![Testanlage am 07.09.2020]({% link assets/images/testanlage/C-Gleis-entnehmen.jpg %})

## Lichtsignale

Nach längerem Tüfteln haben wir uns entschlossen die Ausfahrsignale nicht mit an die CTC-Weichenmodule anzuschließen.
Ausschlaggebend dafür war unser eigener Werbespruch "Sensationelle Reduktion der Verkabelung".

So erhält jedes Ausfahrsignale ein eigenes CTC-Multi-I/O-Board, an das dann auch nur die IR-Balise direkt vor dem Ausfahrsignale angeschlossen wird.
Die andere IR-Balise wird am CTC-Multi-I/O-Board für das Ausfahrsignale am anderen Ende des Gleises (für die andere Fahrtrichtung) angeschlossen.
Da die Ausfahrsignale mit 6 LEDs ausgestattet sind, reichen die 8 Anschlüsse des IO-Borads.

Wir platzieren das Lichtsignal ca. 20 cm von der IR-Balise entfernt.
Für den seitlichen Abstand nutzen wir Märklins Oberleitungslehre.

![Testanlage am 07.09.2020]({% link assets/images/testanlage/Lichtsignale-setzen.jpg %})

Die CTC-Multi-I/O-Boards kommen von vorne oder hinten gut zugänglich an den Rand des Modellbahnsegments: 

![Testanlage am 07.09.2020]({% link assets/images/testanlage/IO-Board-Anschluss-Lichtsignal.jpg %})

Das erste Ausfahrsignale wurde erfolgreich angeschlossen, konfiguriert und mit der CTC-App getestet.

**Fortsetzung folgt...** 
