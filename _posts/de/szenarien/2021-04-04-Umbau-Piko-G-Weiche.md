---
layout: post
title:  "Umbau Piko G Weiche"
date:   2021-04-04
categories: CTC-Module Umsteigen
tags: Weiche Weichenmodul Umbau Gartenbahn Piko
author: Peter Rudolph
---
In diesem Artikel zeige ich Ihnen, wie der Piko G Weichenantrieb (Nr.35271) inkl. Weichenlaterne (Nr. 35266) mit einem [CTC-Weichenmodul-G]({% link _data_sheet/CTC-Weichenmodul-G.md %}) ausgerüstet wird.

**Wichtig:** Diese Dokumentation entstand noch vor unserer Festlegung bzgl. [Kabelfarben]({% link _app_doku/A40-kabelfarben.md %}).
**Die Kabelfarben auf den Fotos entsprechen somit nicht dieser Festlegung!**

Hier erst mal der fertig umgebaute Weichenantrieb:

![Weiche Piko G fertig umgebaut]({% link assets/images/Umbau-Weiche-Piko-G/Umbau-Piko-G-fertig.jpg %})
                       
Den Deckel erhalten Sie von uns natürlich in Schwarz.
Für diese Anleitung habe ich den grauen Prototypen verwendet.

Als Erstes wird die Weichenlaterne nach der, dem Produkt beiliegenden Anleitung, angebaut. 
Am Ende der Anschlusskabel der Weichenlaterne befinden sich kurz vor dem Stecker eine Diode und ein Widerstand.
Der Sinn der Diode erschließt sich mir nicht.
Der Widerstand ist essenziell, denn die Laterne enthält eine LED, die ohne Widerstand durchbrennen würde.
Beim Kürzen der Kabel müssen wir also den Widerstand beibehalten, der bei mir im schwarzen Kabel enthalten war.

Bevor wir die Weichenlaterne anschließen, kümmern wir uns um den Weichenantrieb.
Dazu werden die beiden kurzen Kabel (grün und blau, rund 4,5 cm lang) durch die dafür vorgesehenen Löcher geschoben und von oben angeschraubt.
Anschließend ziehen wir an beiden Kabeln kräftig um sicherzugehen, dass wir sie auch gut angeschraubt haben.
Ich hatte auch die Abdeckung der Dichtung entfernt, was sich aber dann als unötige herausstellte:

![Weiche Piko G Antrieb anschließen]({% link assets/images/Umbau-Weiche-Piko-G/Umbau-Piko-G-Antrieb.jpg %})

Warum Piko für den Pluspol ein schwarzes Kabel verwendet, das man üblicherweise für den Minuspol verwendet, ist mir ein Rätsel? 
Wir schließen den Pluspol der Weichenlaterne (schwarzes Kabel mit Widerstand) an den Ausgang HB-3 und den Minuspol an GND des CTC-Weichenmodul-G an.
Im folgenden Bild ist das CTC-Weichenmodul-G komplett verdrahtet und der zum Lieferumfang gehörende Deckel liegt links schon bereit. 

![Weiche Piko G CTC-Modul verdrahtet]({% link assets/images/Umbau-Weiche-Piko-G/Umbau-Piko-G-verdrahtet.jpg %})
   
Spätestens jetzt müssen wir die vier Schrauben entfernen, die später vom Deckel verdeckt sind.
Sie werden durch entsprechend längere, ebenfalls mitgelieferte Schrauben ersetzt.
Nun setzen wir das CTC-Weichenmodul-G in den Deckel ein und führen die Kabel durch die dafür vorgesehenen Aussparungen:

![Weiche Piko G Deckel montieren Schritt 1]({% link assets/images/Umbau-Weiche-Piko-G/Umbau-Piko-G-Deckel-1.jpg %})

Dann setzen wir den Deckel an das Gehäuse des Weichenantriebs und klappen ihn vorsichtig herunter.
Dabei müssen wir vor allem darauf achten, dass die Kabel in Ihren Aussparungen verbleiben:

![Weiche Piko G Deckel montieren Schritt 2]({% link assets/images/Umbau-Weiche-Piko-G/Umbau-Piko-G-Deckel-2.jpg %})

Schließlich schieben wir den Deckel in Position:

![Weiche Piko G Deckel montieren Schritt 3]({% link assets/images/Umbau-Weiche-Piko-G/Umbau-Piko-G-Deckel-3.jpg %})

Dann noch die vier Schrauben mit Gefühl hineindrehen (Plastikgewinde!) und fertig.
Im Bild rechts sieht man die übrig gebliebenen Kabelenden und die Diode der Weichenlaterne. 

![Weiche Piko G Deckel montieren fertig]({% link assets/images/Umbau-Weiche-Piko-G/Umbau-Piko-G-Deckel-4.jpg %})
  




