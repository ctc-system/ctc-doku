---
layout: post
title:  "Umbau LGB Weiche"
date:   2021-04-04
categories: CTC-Module Umsteigen
tags: Weiche Weichenmodul Umbau Gartenbahn LGB
author: Peter Rudolph
---
In diesem Artikel zeige ich Ihnen, wie der LGB Weichenantrieb (Nr.12010) inkl. Weichenlaterne (Nr. 12040) mit einem [CTC-Weichenmodul-G]({% link _data_sheet/CTC-Weichenmodul-G.md %}) ausgerüstet wird.

**Wichtig:** Diese Dokumentation entstand noch vor unserer Festlegung bzgl. [Kabelfarben]({% link _app_doku/A40-kabelfarben.md %}). 
**Die Kabelfarben auf den Fotos entsprechen somit nicht dieser Festlegung!** 

Hier, zu Beginn der fertig umgebaute Weichenantrieb:

![Weiche LGB fertig umgebaut]({% link assets/images/Umbau-Weiche-LGB/Umbau-Weiche-LGB-fertig.jpg %})

Als Erstes wird die Weichenlaterne nach der, dem Produkt beiliegenden Anleitung angebaut.
Als nächstes werden die Deckel von Weiche und Laterne entfernt.
Im folgenden Bild habe ich rechts schon den Einbaurahmen und das CTC-Weichenmodul-G bereitgelegt:

![Weiche LGB Bauteile]({% link assets/images/Umbau-Weiche-LGB/Umbau-Weiche-LGB-Bauteile.jpg %})

Anschließend wird der Weichenantrieb so weit angehoben, dass die Schraubklemmen zugänglich sind.
Dann werden die kurzen, an HB-1 und HB-2 angelöteten Kabel mit den Schraubklemmen des Weichenantriebs verbunden:

![Weiche LGB Antrieb angeschlossen]({% link assets/images/Umbau-Weiche-LGB/Umbau-Weiche-LGB-Antrieb.jpg %})

Des weiteren werden die Kabel zum Anschluss der Weichenlaterne (HB-3 und GND, grau und schwarz) durch die Löcher des Weichenantriebs gefädelt:

![Weiche LGB Kabel für Laterne]({% link assets/images/Umbau-Weiche-LGB/Umbau-Weiche-LGB-Laternenkabel.jpg %})

Beim Wiedereinsetzen des Antriebs muss darauf geachtet werden, dass kein Kabel abgequetscht wird!

Das CTC-Modul in den Einbaurahmen einführen und die Gleisanschlüsse (Gleis-A und Gleis-B, braun und gelb) auf der anderen Seite des Gehäuses wieder herausführen.
Dafür wurde in die entnehmbare Blende ein 3 mm Loch gebohrt.

![Weiche LGB Stromanschluss]({% link assets/images/Umbau-Weiche-LGB/Umbau-Weiche-LGB-Gleisanschluss.jpg %})

Nun schrauben wir die Kabel (grou und schwarz) an die Schraubklemmen der Weichenlaterne an.
Da es sich noch um eine Glühbirne handelt, müssen wir uns keine Gedanken um die Polarität machen:

![Weiche LGB Weichenlaterne anschließne]({% link assets/images/Umbau-Weiche-LGB/Umbau-Weiche-LGB-Laterne.jpg %})

Dann setzen wir den Deckel auf die Weichenlaterne.
Dabei muss darauf geachtet werden, dass der Führungszapfen – im dafür vorgesehenen Loch – landet.
Im Bild rechts liegen schon der Deckel und die von CTC mitgelieferten längeren Schrauben bereit:

![Weiche LGB Weichenlaterne anschließne]({% link assets/images/Umbau-Weiche-LGB/Umbau-Weiche-LGB-Deckel.jpg %})

Abschließend bringen wir den Einbaurahmen in Position, setzen den Deckel darauf und verschrauben diesen mit den längeren Schrauben. 


