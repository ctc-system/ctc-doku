---
layout: post
title:  "Pendelzug in einer Vitrine"
date:   2022-11-13
categories: Automatisierung
tags: App Software IR-Balise 
author: Peter Rudolph
---
Aus einem Messe-Gespräch unter Ausstellern in Bauma ist die ganz simple Lösung für eine Automatisierung entstanden, die hier beschrieben wird.

Die [Joe Schwarz Vitrinenmanufaktur](https://joeschwarz.ch/) stellt Vitrinen für Modellbahner und Sammler her:

![Vitrine Joe Schwarz]({% link assets/images/Vitrine-JS/Vitrine-Joe-Schwarz.jpg %})

Der Wunsch von Joe Schwarz war, dass sich in der auf der Messe gezeigten Vitrine auch etwas bewegt.
Dafür in eine große Digitalzentrale zu investieren, erschien ihm aber preislich nicht angemessen.

Mit CTC lässt sich solch ein Pendelzugbetrieb einfach und kostengünstig lösen.
Dazu werden lediglich folgende Bauteile benötigt:
* 2x CTC-Weichenmodul
* 1x CTC-Lokmodul, im konkreten Fall das CTC-Lokmodul-PluX22 mit einem ESU Einbauadapter
* 1x IR-Empfänger für die Lok
* 4x Bausatz IR-Balise
* Stromversorgung fürs Gleis - die war im konkreten Fall schon vorhanden (Märklin MobileStation)
* WLAN-Router - wird im konkreten Fall über einen CTC-Router bereitgestellt

Der Gleisplan besteht aus einer Strecke, die eingleisig beginnt, in der Mitte zweigleisig weiter geht und am anderen Ende wieder eingleisig aufhört:

![Gleisplan Vitrine]({% link assets/images/Vitrine-JS/Gleisplan-Vitrine.png %})

Das Konzept sieht 2 IR-Balisen an den jeweiligen Wendepunkten vor, sowie je eine Balise in den Parallelgleisen in der Mitte.
Die Weichen werden zu Beginn so gestellt, dass der Zug auf der Hinfahrt das eine und der Rückfahrt das andere Gleis der Parallelstrecke nutzt.
Wie es sich für eine schweizer Bahn gehört fährt der Zug jeweils links entlang.

Da nun keinerlei Schaltlogik benötigt wird, können alle Balisen statisch konfiguriert werden.
Man muss nur eine Lok aufs Gleis setzten, bei dieser die Richtung gemäß Uhrzeigersinn einstellen und den Knopf für Automatikbetrieb drücken.
Und schon fährt die Lok so lange hin und her bis man sie durch erneuten Klick auf den Automatik-Button anhält oder den Strom abschaltet.

Die Installation ist denkbar einfach.
Als Erstes werden in die beiden Weichen je ein CTC-Weichenmodul eingebaut.
Dann werden die vier IR-Balisen in die Gleise eingebaut und mit den beiden Weichenmodulen verbunden.

Anschließend kann mit der Konfiguration begonnen werden.
Die beiden Balisen an den Endpunkten erhalten im Parameter "cmd" den Lok-Befehl "Rückfahrt" ("R" bzw. "r") und im Parameter "cmdDist" die Wartezeit in Sekunden:

![Wende-Balise]({% link assets/images/Vitrine-JS/Wende-Balise-Parameter.png %})

Die Balisen in der Mitte erhalten im Parameter "cmd" den Lok-Befehl "Bremsen auf Minimalgeschwindigkeit" ("M" bzw. "m") und im Parameter "cmdDist" die Distanz in Zentimeter (etwa 5 cm weniger als die Entfernung zur Wende-Balise):

![Brems-Balise]({% link assets/images/Vitrine-JS/Brems-Balise-Parameter.png %})
  
**Hinweis:** Die zugehörige Doku findet sich
* für die Konfiguration der Weichen und IR-Balisen im [Kapitel 4.1 "Config - Produkte anschließen"]({% link _app_doku/041-produkte-anschliessen.md %}) 
* für die Schaltbefehle im [Kapitel 4.5 "Config - Skript bearbeiten"]({% link _app_doku/045-skript-bearbeiten.md %})
* Für das Bearbeiten des Gleisplans [Kapitel 6.1 "Gleisplan bearbeiten"]({% link _app_doku/061-gleisplan-bearbeiten.md %}) 


