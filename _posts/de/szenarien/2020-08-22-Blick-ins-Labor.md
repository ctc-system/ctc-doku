---
layout: post
title:  "Blick ins Labor"
date:   2020-08-22
categories: Blick-ins-Labor
tags: Lokmodul
author: Peter Rudolph
---
![Lokmodul]({% link assets/images/Blick-ins-Labor.jpg %})

Wir waren sehr fleißig in den letzen Monaten und haben viele Ideen bis zur Prototypenreife gebracht.
Mit diesem Artikel wollen wir Ihnen einen kleinen Blick hinter die Kulissen erlauben, damit Sie wissen, was Sie in den nächsten Monaten an CTC-Neuigkeiten erwartet.

## DCC-Adapter

Ab sofort können alle unsere Lokmodule mit einer alternativen Firmware bestückt werden, die am Motorausgang DCC spricht.

![Lokmodul]({% link assets/images/S-3-6-Lokmodul-DCC.jpg %})

Damit ist es möglich, ein CTC-Lokmodul zwischen Schiene und DCC-Dekoder zu klemmen.
So kann die CTC-App per Wifi Ihre DCC-Lok mit vollem Funktionsumfang fernsteuern – natürlich auch den Sound.

Wie man sieht, ist in unserer Testlok S 3/6 von Roco (Art.Nr. 69370) das Standard CTC-Lokmodul noch ein bisschen groß, aber schon ein 21mtc-Modul würde unter dem Lokdach kaum noch auffallen. 

![Lokmodul]({% link assets/images/S-3-6-Umbau-DCC.jpg %})

Wer möchte, kann nun auch einen Akku in die Lok einbauen und auf die Stromversorgung über die Schiene ganz verzichten.
Für den einen oder anderen Gartenbahner sicher eine interessante Entwicklung – spätestens, wenn zur Gartenbahnsaison 2021 auch unsere Lok-Module für Gatenbahnloks (höhere Strombelastung) erscheinen.   

## Neues Lokmodul – das Feature-Monster 

Zehn Schaltausgänge, zwei Servo-Anschlüsse und ein Anschluss für einen zweiten Motortreiber dürften für fast alle Wünsche ausreichen.
Und das Ganze in derselben Bauform wie das erste CTC-Lokmodul!

![Lokmodul]({% link assets/images/VT137-Lokmodul-2.jpg %})

Unser Testzug ist ein SVT137 "Hamburger" von Märklin (Art.Nr. 37770). 
Auf den Sound haben wir gerne verzichtet, dafür können wir nun das Licht vorne und hinten rot und gelb, beide Führerstände, sowie die beiden Fahrgasträume alle einzeln an und ausschalten.
Uns es sind noch nicht mal alle Schaltausgänge verbraucht.
 
**TODO Bild Lok** 

## Lokmodul mit 21mtc Anschluss

Einen ersten Prototyp gab es schon lange, aber dann hatten andere Dinge Priorität.
Nun ist es endlich so weit: Ein voll funktionsfähiger Prototyp mit 21mtc Stecke ist fertig und auch schon auf dem Weg zur Presse.
Es wird also auch bald einen Artikel darüber geben. 

![Lokmodul]({% link assets/images/BR247-Lokmodul-21mtc.jpg %})

Bis auf Sound kann das CTC-21mtc-Lokmodul alles, was der 21mtc Standard hergibt und verfügt darüber hinaus noch über den Anschluss für unseren IR-Empfänger.
Somit steht dem ungetrübten CTC-Spaß mit entsprechend ausgerüsteten Loks nichts mehr im Wege. 

## CTC für die Gartenbahn

...geht bei nicht so stromhungrigen Loks auch mit dem aktuellen CTC-Lokdekoder, z. B. für useren Adler:

![Lokmodul]({% link assets/images/Adler-Lokmodul.jpg %})

Und für die großen Loks und die Weichen kommen passende CTC-Module zur Gartenbahnsaison 2021 – versprochen!

Mit der Gartenbahn hat vor acht Jahren übrigens alles angefangen – für die, auf Lochrasterplatinen aufgebauten Prototypen war in den Gartenbahnloks einfach mehr Platz:

![Lokmodul]({% link assets/images/RhB-Krokodil-XBee.jpg %})


