---
layout: post
title:  "Umbau C-Gleis Weiche"
date:   2021-05-05
categories: CTC-Module Umsteigen
tags: Weiche Weichenmodul Umbau Märklin C-Gleis
author: Klaus Gungl
---
Um eine Weiche, im Rahmen einer CTC-gesteuerten Bahn schalten zu können, muss diese an ein CTC-Weichenmodul oder ein CTC-IO-Modul angeschlossen werden.
Im Folgenden wird eine Märklin C-Gleis Weiche verwendet.
Sie hat zwei Magnetspulen als Antrieb.
Der Antrieb einer Weiche mit Servomotoren wird hier nicht betrachtet.

**Wichtig:** Diese Dokumentation entstand noch vor unserer Festlegung bzgl. [Kabelfarben]({% link _app_doku/A40-kabelfarben.md %}).
**Die Kabelfarben auf den Fotos entsprechen somit nicht dieser Festlegung!**

Das Datenblatt des CTC-Weichenmoduls ist hier: [CTC-Weichenmodul]({% link _data_sheet/CTC-Weichenmodul-H0.md %}).
Bei der Bestellung eines CTC-Weichenmoduls ist es notwendig anzugeben, welche Funktionen des CTC-Weichenmoduls verwendet werden.
Damit kann die entsprechende Verkabelung werksseitig am CTC-Weichenmodul angelötet werden.

Mögliche werksseitige Anschlüsse gehen aus dem Datenblatt hervor:
* Gleis-A, Gleis-B: Stromversorgung.
  Hier wird bei Verwendung von Märklin C-Gleisen ein Kabel mit konfigurierten Steckern angebracht, welches an die Gleisanschlüsse gesteckt wird.
* W1-Grün, W1-VBB, W1-Rot: Ansteuerung der Magnetspulen der Weiche.
  Hier wird bei Verwendung von Märklin C-Gleisen ein Kabel mit konfiguriertem Stecker angebracht, sodass nur der Stecker in den Weichenantrieb gesteckt werden muss.
* W2-Grün, W2-VBB, W2-Rot: Ansteuerung der Magnetspulen einer optionalen zweiten Weiche oder eines zweiten Weichenantriebs, wie beispielsweise bei einer Dreiwegweiche.
  Hier wird bei Verwendung von Märklin C-Gleisen bei einer Dreiwegweiche ein Kabel mit konfiguriertem Stecker angebracht, sodass nur der Stecker in den Weichenantrieb gesteckt werden muss.
  Es können mit diesen Anschlüssen auch die Magnetspulen eines Signals betrieben werden, wie es beispielsweise beim Starter-Set der Fall ist.
* IR1-GND, IR1-Vcc, IR2-GND, IR2-Vcc: Ansteuerungen von Infrarotsendedioden.
  IRx-GND wird an die Kathode und Irx-Vcc an die Anode angeschlossen.

Die werksseitig angelöteten Kabel dürfen auf keinen Fall mechanischen Belastungen ausgesetzt werden, da sich sonst die Lötanschlüsse auf der Platine lösen!

In der folgenden Abbildung ist ein Modul kurz vor dem Einbau in eine Märklin Weiche C-Gleis gezeigt.
Es ist wie folgt verdrahtet:
* 2 Stecker für Spannungsversorgung.
  Diese werden direkt an das Märklin C-Gleis gesteckt.
* Stecker für Weichenantrieb.
  Dieser Stecker wird direkt in den Weichenantrieb gesteckt.
* Stecker für 2 IR-Balisen.
  Es können hier 2 IR-Balisen angeschlossen werden, Farbkodierung beachten!
* Noch zu löten: Kabel mit Stecker für Flügelsignal.
  Im Falle des Starter-Sets wird hier ein Flügelsignal angeschlossen.
  Es kann alternativ auch ein zweiter Weichenantrieb angeschlossen werden.

![Weichenmodul bedrahtet, beschriftet]({% link assets/images/Umbau-C-Weiche/Weichenmodul_bedrahtet_beschriftet.jpg %})

Für den weiteren Einbau ist zu erwähnen:  
* Die Weiche von Märklin hat bereits vorgefertigt kleine Kunststoffnippel als Montagehilfe.
* Das CTC-Weichenmodul hat passende Bohrungen für die Kunststoffnippel.
* Das CTC-Weichenmodul ist in einem Bereich mit Isolierband versehen, welches Kurzschlüsse mit den Metallteilen der Weiche verhindert.

Bei der Steckverbindung des CTC-Weichenmoduls für den Weichenantrieb ist zu beachten, dass der Stecker in der Buchse mechanisch vertauschungssicher ist.
Vorsicht ist deswegen geboten, weil es ein relativ kleiner Stecker ist, daher mit „roher Gewalt“ auch falsch herum eingesteckt werden kann.
Im Bild ist ein Ausschnitt des CTC-Weichenmoduls und der Stecker für die Weiche des Märklin C-Gleises zu sehen.

![Stecker Weichenantrieb]({% link assets/images/Umbau-C-Weiche/Stecker_Weichenantrieb.jpg %})

Das CTC-Weichenmodul wird an die vorgesehene Stelle von unten in die Weiche eingesteckt. Die 2 Stecker der Spannungsversorgung und der Stecker des Weichenantriebs ebenfalls.
Siehe dazu Abb. 3: Weiche_Weichenmodul_beschriftet.jpg.
Wenn die Weiche jetzt in die Gleisanlage eingebaut wird oder die Spannungsversorgung separat angeschlossen wird, kann mit der Konfiguration des CTC-Weichenmoduls begonnen werden.

![Weiche mit Weichenmodul beschriftet]({% link assets/images/Umbau-C-Weiche/Weiche_Weichenmodul_beschriftet.jpg %})

Die Konfiguration eines CTC-Weichenmoduls ist im [Kapitel 4.1 der Bedienungsanleitung]({% link _app_doku/041-produkte-anschliessen.md %}) beschrieben.

## Einbaubilder zweite Serie
   
Die folgenden Bilder zeigen den Einbau der zweiten, ab Juli 2021 ausgelieferten Serie des CTC-Weichenmoduls in die verschiedenen C-Gleis-Weichen:

![Weiche links]({% link assets/images/Umbau-C-Weiche/Weiche-links.jpg %})

![Weiche rechts]({% link assets/images/Umbau-C-Weiche/Weiche-rechts.jpg %})

![Bogenweiche links]({% link assets/images/Umbau-C-Weiche/Bogenweiche-links.jpg %})

![Bogenweiche rechts]({% link assets/images/Umbau-C-Weiche/Bogenweiche-rechts.jpg %})
