---
layout: post
title:  "Automatisierung im Garten - Teil 3"
date:   2022-08-12
categories: Automatisierung
tags: App Software Gartenbahn NFC-Balise Halteschild
author: Peter Rudolph
---
Diese Artikel-Serie beschreibt die Konfiguration der Automatisierung am Beispiel einer Gartenbahn.

Im dritten Teil ergänzen wir unseren Bahnhof durch Halteschilder.
                       
## Halteschild anlegen

Wir suchen uns ein CTC-Modul in der Nähe aus, z.B. jenes welches die Weiche zum zugehörigen Abstellgleis schaltet.
Dort gehen wir in die Config und klicken auf den Button Hinzufügen eines Produkts.
Wir wählen den Katalog "universell-schilder.xml" und dort das "Halteschild" in passender Richtung:

![Halteschild anlegen]({% link assets/images/Auto-Garten/Config-Halteschild-1.png %})

Dann klicken wir auf "Übernehmen".

Im Config-Dialog klicken wir auf "Hochladen".

## Halteschild im Gleisplan platzieren
                                             
Im zugehörigen Gleisplan platzieren wir ein Ampel-Gleisstück in der passenden Richtung:

![Halteschild im Gleisplan 1]({% link assets/images/Auto-Garten/Gleisplan-Halteschild-1.png %})

Dann wählen wir aus der Action-Gruppe "Signal-Tower" das neu angelegte Schild und klicken im Gleisplan auf die gerade platzierte Ampel.
Das Ampel-Symbol wird durch das H-Symbol ersetzt:

![Halteschild im Gleisplan 2]({% link assets/images/Auto-Garten/Gleisplan-Halteschild-2.png %})
                         
Anschließend klicken wir auf "Hochladen". 
             
## Halteschild mit NFC-Balise verknüpfen 

Um das Halteschild mit der NFC-Balise zu verknüpfen, öffnen wir den Dialog "Automatisierung bearbeiten" des Gleisbilds, in dem wir die NFC-Balise platziert haben.
Dann suchen wir unter "Produkte und Aktionen" die NFC-Balise und wählen dort den Trigger aus.

Rechts von "Auslöser" klicken wir den Button "Auswählen".
Die Action-Gruppe "SignalTower" sollte bereits gewählt sein.
In der unteren Liste klicken wir unser Halteschild an:  

![Trigger Halteschild an Balise 1]({% link assets/images/Auto-Garten/Config-Halt-Trigger-1.png %})

Dann klicken wir auf übernehmen und unser Automation-Config sieht wie folgt aus:

![Trigger Halteschild an Balise 2]({% link assets/images/Auto-Garten/Config-Halt-Trigger-2.png %})

Anschließen klicken wir auf "Hochladen".

Kurz darauf erscheint unser Halteschild im Gleisbild.

Wenn immer eine Lok im Automatik-Modus über die mit dem Halteschild verknüpfte Balise fährt, hält sie an.
Eine manuell gesteuerte Lok ignoriert das Halteschild. 

---

[Zurück zu Teil 1]({% link _posts/de/szenarien/2022-08-12-Automatisierung-im-Garten-1.md %}) | [Zurück zu Teil 2]({% link _posts/de/szenarien/2022-08-12-Automatisierung-im-Garten-2.md %}) | **Teil 3** | [Weiter mit Teil 4]({% link _posts/de/szenarien/2022-08-12-Automatisierung-im-Garten-4.md %}) | [Vor zu Teil 5]({% link _posts/de/szenarien/2022-08-21-Automatisierung-im-Garten-5.md %}) 