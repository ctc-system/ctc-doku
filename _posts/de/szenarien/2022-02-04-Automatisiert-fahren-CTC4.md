---
layout: post
title:  "Automatisiert fahren mit CTC 4"
date:   2022-02-04
categories: Automatisierung
tags: Lok Signal Automatik
author: Peter Rudolph
---
Mit Version 4 wurden die Möglichkeiten zum automatischen Betrieb der Modellbahn mit CTC stark erweitert.
Dieser Artikel ersetzt den [alten Artikel von 2020]({% link _posts/de/szenarien/2020-09-01-Automatisiert-fahren.md %}). 

Die Art, wie Automatisierung bei CTC funktioniert, unterscheidet sich deutlich vom Konzept klassischer Digitalsteuerungen. 
Bei der Konzeption der Automatisierung von CTC haben wir uns am Betrieb der echten Bahn ([Zugsicherungssystem ETCS](https://de.wikipedia.org/wiki/European_Train_Control_System)) orientiert und großzügig Gebrauch von unserem technologischen Vorsprung gegenüber der klassischen Digitalsteuerung gemacht.
Die Basis dazu bilden:
* Unterteilung der Modellbahn in Blöcke.
* Signale, die die Einfahrt in bzw. Ausfahrt aus einem Block erlauben.
* Im Uhrzeigersinn durchnummerierte Balisen, sodass eine Lok nach Passieren der zweiten Balise weiß, in welche Richtung sie fährt.
* Lokomotiven die mithilfe von ID-Empfängern "sehen" wie das nächste Signal gestellt ist und selbständig geeignet reagieren können.
* Fahrstraßen, die mehrere Weichen und Signale auf einmal stellen, um die Fahrt von einem Block zu einem anderen ermöglichen.
* Fahrpläne, die an die Lok übermittelt und von dieser selbständig abgearbeitet werden.

![ctc-app-doku-bild]({% link assets/images/ctc-app-doku/Screenshot_PC_Automatisierung.png %})

Grundlage für Automatisierung bei CTC sind Balisen im Gleis und entsprechende Empfänger in der Lok.
Balisen gibt es für CTC auf Basis von [Infrarot (IR)]({% link _data_sheet/CTC-IR-Sender.md %}) und von [NFC (RFID)]({% link _data_sheet/CTC-NFC-Reader.md %}).
Wenn nicht gesondert erwähnt, gibt es für die folgenden Betrachtungen keinen Unterschied zwischen IR und NFC.

Darüber hinaus ist es wichtig, dass die Sensoren aller am Automatikbetrieb teilnehmenden [Loks kalibriert]({% link _app_doku/051-sensor-kalibrieren.md %}) wurden.
Dann ist die Lok nämlich in der Lage, ihre tatsächliche Geschwindigkeit und Position zu ermitteln und so punktgenau anzuhalten.

Im Unterschied zur klassischen Sensorik mit Schaltgleisen, Lichtschranken oder Reed-Relais weiß die CTC-App nicht nur, dass irgendetwas vorbeigekommen ist, sondern genau welche Lok.
Genauer gesagt weiß die Lok wo sie ist und übermittelt dies an die CTC-App.

Darüber hinaus wird die Lok, wie beim großen Vorbild [ETCS](https://de.wikipedia.org/wiki/European_Train_Control_System), von der Balise mit Informationen zur Strecke versorgt und kann selbstständig ihre Geschwindigkeit anpassen, anhalten und auch wieder losfahren.
Die simpelste Automatik, nämlich das Anhalten vor einem roten Signal, lässt sich somit in CTC ganz ohne Eingriff durch die App lösen.

## Automatisch anhalten vor einem roten Signal

Eine mit einem Signal verknüpfte Balise teilt einer passierenden Lok mit, in welcher Distanz sich das Signal befindet und was dieses anzeigt.
Diese Verknüpfung zwischen Signal und Balise stellen wir in der Konfiguration des CTC-Moduls ein, an dem die IR-Balise angeschlossen ist.
Bei einer NFC-Balise erfolgt die Konfiguration über den Dialog "Automatisierung bearbeiten". 
                                                         
Anhand der von der Balise übertragenen ID wissen Lok und CTC-App, in welchem Block sich die Lok gerade befindet.
Wurde dem Block ein Ausfahrsignal zugeordnet, so informiert die CTC-App eine im Block haltende Lok, sobald das Signal die Fahrt freigibt.
Die Lok kann dann ihre Fahrt selbständig fortsetzen. 

### Signal und Balise verknüpfen

Um ein Signal und eine IR-Balise zu verknüpfen, öffnen wir die Konfiguration des CTC-Moduls, an dem die IR-Balise angeschlossen ist.
Das geht am einfachsten durch Rechtsklick auf der IR-Balise im Gleisbild.

Die Verknüpfung von Signal und NFC-Balise funktioniert nahezu identisch.
Hier starten wir aber mit dem CTC-Modul, auf dem der Gleisplan mit der NFC-Balise gespeichert ist.
Im Config-Dialog des CTC-Moduls klicken wir auf "Automatisierung bearbeiten". 

Wie die Verknüpfung angelegt wird erfahren Sie in der Bedienungsanleitung im [Kapitel 4.3 - Aktionen verknüpfen]({% link _app_doku/043-aktionen-verknuepfen.md %}). 

Nachdem wir die Verknüpfung gespeichert (hochgeladen) haben können wir probieren, ob unsere Konfiguration funktioniert:
Sobald eine Lok die entsprechende Balise vor einem roten Signal überfährt, beginnt sie zu bremsen.

Mit einer zweiten Balise können wir das Anhalten so genau einstellen, dass die Lok über der Balise zum Stehen kommt.
Dabei wird über die erst Balise das Kommando "Minimalgeschwindigeit" mit einer Distanz bis kurz vor der zweiten Balise ausgesendet. 
Die Lok bremst dann ab und "kriecht" die letzten Zentimeter bis sie das Signal der zweiten Balise empfängt.

## Blocksteuerung

Das Prinzip der Blocksteuerung dient der Verhinderung von Unfällen beim Bahnbetrieb.
Dabei wird die gesamte Bahnstrecke in Blöcke unterteilt.
Mithilfe von Signalen werden Blöcke freigegeben oder gesperrt.
Befindet sich ein Zug in einem Block, so ist der Block hinter dem Zug gesperrt, um ein Auffahren des Folgezugs zu verhindern. 

Bei CTC lässt sich dieses Prinzip wie folgt realisieren:
* Unterteilen Sie Ihre Modellbahn in Blöcke, die über eine ID aus zwei Buchstaben/Ziffern (z.B. "AB")identifiziert werden.
  Dabei hat jeder Block genau einen Anfang und ein Ende.
  Weichen sind immer außerhalb von Blöcken.
* Platzieren Sie am Anfang und Ende jedes Blocks eine Balise, deren ID mit der ID des Blocks beginnt, z.B. "AB1" und "AB2"
* Platzieren Sie ein Ausfahrsignal am Ende des Blocks und geben Sie diesem im Gleisplan als PositionsID die ID des Blocks gefolgt von "+" falls es am Block-Ende im Uhrzeigersinn steht, sonst "-".
* Verknüpfen Sie die Balise mit dem Signal.
* Legen Sie an jedem Block je eine Fahrstraße (s.u.) für jede gewünschte Verbindung zu einem anderen Block an.  

## Fahrstraßen

Fahrstraßen dienen dazu eine ganze Reihe von Weichen zu schalten, um so die Fahrt von einem Block zu einem anderen zu ermöglichen.
Nachdem alle Weichen gestellt sind, wird das zugehörige Ausfahrsignal des Start-Blocks auf Grün gestellt.
Eine vor diesem Signal wartende Lok fährt dann automatisch los.
Damit die Lok im End-Block anhält, muss natürlich auch das entsprechende Ausfahrsignal den End-Blocks auf Rot gestellt sein.

## Geschwindigkeit regeln

Die Signale der echten Bahn kennen nicht nur die Stellungen für Halt und Frei, sondern auch Frei mit reduzierter Geschwindigkeit.
Dies wird in der Regel dazu genutzt, um dem Lokführer anzuzeigen, dass die folgende Weiche auf "Abbiegen" steht und deshalb mit verminderter Geschwindigkeit zu fahren ist.
Die CTC-Loks kennen diese Signalisierung ebenfalls und natürlich kann die Balise diese Information auch übertragen.

Darüber hinaus gibt es bei der echten Bahn und auch bei CTC Schilder, die die zulässige Geschwindigkeit begrenzen.
Für einen Prellbock können Sie das Schild "Schutzhalt" verwenden - das sind die roten Tafeln im Gleisbild. 

## Fahraufträge

Fahraufträge erlauben es nun nahezu beliebig komplexe Abläufe in Kombination mit zu passierende Balisen zu definieren.
Dabei kann für jede aufgeführte Balise festgelegt werden, was zu passieren hat, wenn die Lok diese bestimmte Balise passiert.

Beim Ausführen des Fahrauftrags werden zuerst alle zu passierenden Balisen als Fahrplan an die Lok geschickt.
Der Fahrplan wird an die Lok geschick, die in dem Block steht, der zur als erste im Fahrauftrag genannten Balise gehört. 
Dann werden die Weichen (idealerweise die Fahrstraßen) geschaltet.
 
## Gute Planung ist das Wichtigste 

Da im Automatikbetrieb viele Züge unterwegs sind und Sie sich ja eben nicht auf deren Kontrolle konzentrieren wollen, ist eine gute Planung unverzichtbar:
* Beim Schalten von Fahrstraßen sollte der letzte Schritt immer das Öffnen eines Signals sein.
* Überlegen Sie welche Blöcke in welche Richtung durchfahren werden sollen.
* Bedenken Sie beim Platzieren von Balisen wie weit die Empfänger in der Lok von deren Anfang/Ende entfernt sind. 
  Eventuell brauchen sie zusätzliche Balisen je nachdem, ob ein Zug vorwärts oder rückwärts in den Block einfährt. 
 
Die CTC-App wartet übrigens bei jedem Schaltvorgang bis der Empfang vom CTC-Modul bestätigt wurde, bevor der nächste Befehl gesendet wird.   
