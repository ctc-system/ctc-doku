---
layout: post
title:  "CTC Version 4"
date:   2022-05-30
categories: CTC-Allgemein
tags: App Software
author: Peter Rudolph
---
Mit CTC Version 4 kommen unsere IR-Sender bzw. NFC-Tags so richtig in Fahrt!
Automatikbetrieb und Zugsicherung nach dem Vorbild der echten Bahn ([Zugsicherungssystem ETCS](https://de.wikipedia.org/wiki/European_Train_Control_System)) werden damit zum Kinderspiel.

Am Pfingstwochenende 2022 haben wir in Burgdorf (CH) die neue Version 4 von CTC vorgestellt:

![Messe Burgdorf 20222]({% link assets/images/Messe-Burgdorf-2022.png %})
                
**Die neue Version hat den Messe-Stress gut gemeistert und kann im [Download-Bereich]({% link de/download.md %}) heruntergeladen bzw. über die App-Stores bezogen werden.**

Außer den neuen Funktionalitäten gab es natürlich jede Menge Fehlerkorrekturen in CTC-App und -Firmware.
Und wie bisher: CTC ist auch in Version 4 als kostenloses Update für alle bisher verkauften CTC-Module verfügbar. 
   
## Automatisierung und Zugsicherung

Der Großteil der neuen Funktionalitäten in CTC 4 dreht sich rund um Automatisierung und Zugsicherung.

Die ursprünglich Idee, nur ein paar einfache Automatisierungsfunktionen für Pendelzugbetrieb und Bahnhofshalt einzubauen, wurde schnell verworfen, nachdem die ersten Loks frontal zusammengestoßen waren.
Ohne zusätzliche Funktonen zur Zugsicherung erschien uns die Automatisierung zu schwer zu beherrschen.

![Fahrstrasse B4 nach B3 im Gleisbild]({% link assets/images/ctc-app-doku/Fahrauftrag-B4-A3.png %})

Somit bekommt CTC mit Version 4 alles was eine moderne, vorbildgerechte Zugsteuerung braucht:
* Blöcke
* Fahrstraßen
* Fahrpläne für die Lok
 
In der gesamten Doku wurde der Begriff "ID-Sender" durch "Balise" ersetzt und entsprechend "IR-Sender" durch "IR-Balise" sowie "NFC-Tag" durch "NFC-Balise". 
                                                                                    
Die von Balisen ausgesendeten Kommandos wurden stark erweitert und berücksichtigen nun die Fahrtrichtung der Lok.

In der Bedienungsanleitung finden Sie im neunen [Kapitel 7 - Automatisierung]({% link _app_doku/070-automatisierung.md %}) ausführliche Erklärungen.   

### NEU: Blocksteuerung

Jede Balise wird automatisch einem Block zugeordnet, identifiziert durch die ersten beiden Zeichen der 3-Zeichen-ID der Balise.

* Blöcke können im Gleisbild platziert werden und zeigen dort ihren Zustand (frei, belegt, reserviert) an.
* Jedem Block können zwei Signale für seien beiden Enden als Ausfahrsignal zugeordnet werden.
* Geht ein Ausfahrsignal auf Grün, so fährt die in dem Block haltende Lok wieder los.

### NEU: Fahrstraßen
    
Eine Fahrstraße stellt die Verbindung zwischen zwei Blöcken her, indem alle dazwischen liegenden Weichen passend gestellt werden.

Eine Fahrstraße reserviert den Zielblock und alle zu schaltenden Weichen und Signale bevor diese geschaltet werden.
Diese Weichen und Signale können erst weider geschaltet werden, wenn die Lok den Zielblock der Fahrstraße erreicht hat. 
          
### NEU: Fahrpläne

Ein Fahrplan ergänzt eine oder mehrere Fahrstraßen-Befehle um die von der Lok zu passierenden Balisen.
Für jede Balise kann ein Kommando hinterlegt werden.
Die Liste der Balisen wird als elektronischer Fahrplan an die Lok übergeben und von dieser selbständig abgearbeitet. 

### Lokomotiven

* Die Lok kennt nun 9 verschiedene richtungsabhängige Befehle
* Lok erkennt und meldet Fahrtrichtung.
* Die Fahrtrichtung kann gesetzt werden - für Automatik muss sie gesetzt werden.
* Befehle für die Gegenrichtung werden ignoriert.
* Die Lok-Steuerung zeigt den (Automatik-) Status der Lok an.
* Ein Nothalt-Button bringt den Lok-Motor in max. 0,5 Sekunden zum Stehen.

## Mehrmotorige Lokomotiven

* Steuerung überarbeitet, z.B. Max Speed

## Gruppen

Jede Funktionalität (Action) ist einer Gruppe zugeordnet. 
* Gruppen können nun frei definiert werden.
* Die Zuordnung vom Actions zu Gruppen ist änderbar.
* Die Schaltpult-Anzeige erlaubt nun den Wechsel zwischen Gruppen.

## Betriebssicherheit

* Es wird bei jedem Schaltbefehl geprüft, ob dieser angekommen ist und er wird ggf. neu gesendet.
* Bei Skripten wartete die CTC-App bis der Empfang eines Schaltbefehls bestätigt wurde, bevor sie mit dem nächsten Schritt weiter macht.
* Nothalt-Knöpfe auf der Lok-Steuerung für die angezeigte Lok und auf dem Schaltpult für alle Loks.

## Weichen und IO-Board

* Neuer Zustand wird sofort nach Ausführen des Befehls zurückgemeldet, statt wie bisher erst mit nächstem Status.

## CTC-App

* Die Farbe der Lok-Anzeige im Gleisbild wurde angepasst.
* Die Signal-Icons wurden überarbeitet (Kontraste verbessert).
* In der Lok-Steuerung wird der Status des Automatikbetriebs mit einem Icon angezeigt.
* Unterstützung von Kreuzungen mit 4 Zuständen inkl. passender Icons.
* Anzeige des aktuell von einer Balise ausgesendeten Kommandos.
* Auswahl und Anzeige des aktuellen Fahrplans inkl. start/stop/setzen der Modell-Zeit.

## Config

* Im Aktionen-Tab können nun Parameter hinzugefügt, entfernt und geändert werden.
* Dialog "Automatisierung bearbeiten" ergänzt um Blöcke, Fahrstraßen und Fahraufträge.
* Neuer Dialog "Fahrplan bearbeiten".
* Neue Auswahlboxen für viele Felder, an denen bisher nur ein Textfeld vorhanden war.
* Diverse Ergänzungen in den Produktkatalogen.
       
## Z21 WLAN Protokoll

Mit CTC 4 wird nach und nach eine volle Unterstützung des Z21 WLAN Protokolls angestrebt.
In dieser ersten Version können an die Z21 Zentrale angeschlossene Loks gesteuert werden.

## Migration CTC 3 nach CTC 4

Beim Wechsel von CTC 3 nach CTC 4 muss folgendes angepasst werden:
* Alle mit CTC 3 angelegten Balisen sollten gelöscht und neu angelegt werden.

## Behobene Fehler
            
Außer vielen kleinen Fehlern wurden folgende wichtige Fehler behoben: 
* Memory Leak mit NFC-Tags behoben: Sobald NFC-Tags im Gleisbild platziert und eine 3-Zeichen-ID zugeornet war, stürzte allem das CTC-Lokmodul-H0a nach wenigen Minuten ab.
  Dieser Fehler existiert seit CTC-App Version 3.07.