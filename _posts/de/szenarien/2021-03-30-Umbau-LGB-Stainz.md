---
layout: post
title:  "Umbau LGB Stainz 2020 und 2021"
date:   2021-03-30
categories: CTC-Module Umsteigen
tags: Lok Lokmodul Umbau Gartenbahn
author: Peter Rudolph
---
![Stainz auf Rollenprüfstand]({% link assets/images/Umbau-Stainz/Stainz-Rollenpruefstand.jpg %})

Diese Dokumentation beschreibt den Umbau einer alten LGB Lokomotive von analog auf CTC.
Genau genommen wurden zwei Stainz-Modelle (2020 und 2021) umgebaut.
Dazu habe ich (Peter Rudolph) mich per Video-Konferenz mit unserem Kunden Gregor Denndörfer getroffen und wir haben meine 2020 und seine 2021 umgebaut.
An dieser Stelle auch herzlichen Dank an ihn für die Bilder der 2021.

**Wichtig:** Diese Dokumentation entstand noch vor unserer Festlegung bzgl. [Kabelfarben]({% link _app_doku/A40-kabelfarben.md %}).
**Die Kabelfarben auf den Fotos entsprechen somit nicht dieser Festlegung!**

Für diese kleinen LGB-Loks brauchen wir noch keine speziellen Gartenbahn-Module.
Das [CTC-Lokmodul-H0a]({% link _data_sheet/CTC-Lokmodul-H0a.md %}) reicht völlig aus.

Bei den alten LGB-Loks besteht grundsätzlich das Problem, dass mindestens ein Pol des Motors im Motorblock fest mit der Stromaufnahme verbunden ist.
So war es auch bei unseren beiden.

Als Erstes muss die Lok so weit zerlegt werden, dass man an den Motorblock heran kommt, um diesen zu öffnen.
Wie man eine Stainz zerlegt, ist viel beschrieben im Internet.
Leider gibt es aber auch sehr viele Varianten der Stainz, sodass uns einiges an Rätselraten blieb, bis wir tatsächlich den Motorblock vor uns hatten:

![Umbau Stainz - Motorblock ausgebaut]({% link assets/images/Umbau-Stainz/Stainz-Motorblock-ausgebaut.jpg %})

So sieht der Motorblock der 2021 aus:

![Umbau Stainz 2021 - Motorblock ausgebaut]({% link assets/images/Umbau-Stainz/Stainz-2021-Motorblock-ausgebaut.jpg %})

Folgende Links haben uns geholfen:
* [Modell-Land.de](https://forum.modell-land.de/loks-lokomotiven-f2/demontage-einer-lgb-stainz-lok-t4472.html)
* [Massoth.de](https://www.massoth.de/wp-content/uploads/2020/04/LGB-2x212-Stainz_DE_massoth_umbaubericht.pdf)

Das waren die wesentlichen Erkenntnisse:
* Führerhaus und Kessel sind in der Mitte des Rahmens von unten angeschraubt und können somit erst entfernt werden, wenn der Motorblock ausgebaut ist.
* Der Motorblock sitzt sehr stramm.
  Er kann herausgenommen werden, nachdem der Schornstein herausgeschraubt, die beiden Kupplungen entfernt und das Gestänge vor dem Führerhaus losgeschraubt und ausgehängt wurden.  

Bei der 2020 musste das Gestänge entfernt und auf einer Seite die Räder abgenommen werden, um den, aus zwei Halbschalen bestehnden Motorblock, öffnen zu können:

![Umbau Stainz - Motorblock offen]({% link assets/images/Umbau-Stainz/Stainz-Motorblock-offen.jpg %})

Bei der 2021 hat der Motorblock einen Deckel und einen Boden, sodass dort das Gestänge nicht abmontiert werden muss.

![Umbau Stainz 2021 - Motorblock offen]({% link assets/images/Umbau-Stainz/Stainz-2021-Motorblock-offen.jpg %})

Bei der 2020 sind die Motorkontakte als Blechstreifen in den beiden Halbschalen ausgeführt.
Diese wurden durch Hin- und Herbiegen herausgebrochen.
Im folgenden Bild habe ich in der unteren Halbschale den Blechstreifen schon entfernt und in der oberen gerade mit dem Biegen angefangen:

![Umbau Stainz - Motorkontakte entfernen]({% link assets/images/Umbau-Stainz/Stainz-Motorkontakte-entfernen.jpg %})

Bei der 2021 sind die Motorkontakte dicke Messingstifte und es war auch nur eine Seite mit der Stromaufnahme verbunden.
Hier wurde, von den Messingstiften genau so viel entfernt, dass sie noch halten, aber keine Verbindung zum Motor mehr herstellen konnten.

![Umbau Stainz 2021 - Motorkontakte entfernen]({% link assets/images/Umbau-Stainz/Stainz-2021-Motorkontakte-entfernen.jpg %})

Nun wurden bei beiden Varianten (der Motor selbst ist identisch) Kabel an die Kontaktstifte angelötet.
Dann wurden Löcher für die Kabelführung nach oben ins Gehäuse gebohrt.
Dabei ist darauf zu achten, dass diese oben an einer Stelle herauskommen, an der sie beim späteren Einbau nicht abgequetscht werden.
Das folgende Bild zeigt wieder die 2020:

![Umbau Stainz - Kabel an Motor angelötet]({% link assets/images/Umbau-Stainz/Stainz-Motor-Kabel-angeloetet.jpg %})

Und hier die 2021:

![Umbau Stainz 2021 - Kabel an Motor angelötet]({% link assets/images/Umbau-Stainz/Stainz-2021-Motor-Kabel-angeloetet.jpg %})

Beim folgenden Zusammenbau ist bei der 2020 darauf zu achten, dass die Stifte auf den Rädern synchron laufen.
Am besten testet man mit der untersten Stange, ob sich die Räder noch drehen lassen.
Ich habe meinen Motorblock zweimal wieder auseinander genommen, bis das Gestänge richtig lief.

Da mich die, der Stromanschluss von Blechstreifen auf Schrauben in der Grundplatte nicht überzeugen konnte, habe ich auch hier Kabel angelötet:

![Umbau Stainz - Kabel am Motorblock]({% link assets/images/Umbau-Stainz/Stainz-Kabel-Motorblock.jpg %})

Spätestes jetzt sollte man prüfen, ob der Motor noch läuft und alle Gleiskontakte funktionieren. 

Nun konnte der Motorblock wieder in die Grundplatte eingesetzt und das [CTC-Lokmodul-H0a]({% link _data_sheet/CTC-Lokmodul-H0a.md %}) angeschlossen werden.
Auf die Schraube in der Mitte von Gehäuse und Kessel haben wir bewusst verzichtet.

![Umbau Stainz - CTC-Modul angeschlossen]({% link assets/images/Umbau-Stainz/Stainz-CTC-Modul.jpg %})

Im folgenden Bild sind bereits alle Kabel ans CTC-Modul angeschlossen:

![Umbau Stainz - CTC komplett verkabelt]({% link assets/images/Umbau-Stainz/Stainz-CTC-verkabelt.jpg %})

Die letzte Herausforderung war, die Verkabelung so unterzubringen, dass beim Zusammenbau nichts abgequetscht wird.
Ich habe dazu die Kesselblende im Führerhaus, unten ein bisschen abgezwickt und die Kabel teilweise mit der Heißklebepistole fixiert:

![Umbau Stainz - Kabel fixiert]({% link assets/images/Umbau-Stainz/Stainz-Kabel-fixiert.jpg %})

Das CTC-Modul habe ich im Wasserkasten rechts vor dem Führerhaus untergebracht.
Beim Zusammenbau des Gehäuses hatte ich den Deckel des Wasserkastens entfernt und das CTC-Modul nach oben herausgezogen, um zu verhindern, dass ich die Kabel abquetsche.

Nach 8 Stunden Umbau ist es endlich geschafft:
Das Einleitungsbild dieses Artikels zeigt, die fertig umgebaute und mit CTC funktionierende Stainz auf dem Rollenprüfstand.
Wer genau ins Führerhaus schaut, sieht den Stützkondensator, der eigentlich in den Kohlebehälter hätte sollen.
Leider hatte ich die Kabel etwas zu kurz gemacht und es war schon spät am Abend - vielleicht richte ich das ein anderes Mal?! 




          
