---
layout: post
title:  "Automatisierung im Garten - Teil 4"
date:   2022-08-12
categories: Automatisierung
tags: App Software Gartenbahn NFC-Balise Block Fahrstrasse
author: Peter Rudolph
---
Diese Artikel-Serie beschreibt die Konfiguration der Automatisierung am Beispiel einer Gartenbahn.

Im vierten Teil ergänzen wir unseren Bahnhof durch Blöcke und Fahrstrassen.
                  
**Wichtig:** Blöcke werden bei CTC immer durch genau 2 Zeichen (Buchstaben, Ziffern) identifiziert.
Die Zuordnung zwischen Balisen und Blöcke erfolg ausschließlich über diesen Namen, d.h. **alle Balisen in einem Block müssen mit den zwei Buchstaben des Blocknamens beginnen**. 
         
## Block im Gleisplan anlegen
          
Sobald wir die erste Balisen eines Blocks angelegt haben existiert auch der Block.
In diesem Fall müssen weird den Block gar nicht anlegen, sondern können ihn direkt im Gleisbild platzieren.
Dazu legen wir an der gewünschten Stelle einen Sensor mit der in Uhrzeigersinn richtigen Richtung an:

![Gleisplan Block anlegen 1]({% link assets/images/Auto-Garten/Gleisplan-Block-1.png %})

Dann wählen wir den Block aus und klicken auf die Stelle, wo wir gerade den Sensor angelegt haben: 

![Gleisplan Block anlegen 2]({% link assets/images/Auto-Garten/Gleisplan-Block-2.png %})

**Wichtig:** Der kleine Pfeil am Block muss im Uhrzeigersinn zeigen.

## Block in der Config anlegen

Gibt es noch keine Balise für den Block so können wir in auch von Hand anlegen.
Dazu öffnen wir "Automatisierung bearbeiten" eines CTC-Moduls, am besten das Modul auf dessen Gleisplan der Block später platziert werden soll.
Dor klicken wir auf Plus-Button neben "Produkte und Aktionen".

![Config Block anlegen]({% link assets/images/Auto-Garten/Config-Block.png %})

Wir vergeben einen Namen (**genau 2 Zeichen**), wählen "Block" und klicken auf "Übernehmen".
            
## Blöcke im Gleisbild

![Blöcke im Gleisbild]({% link assets/images/Auto-Garten/Gleisbild-Bloecke.png %})

## Fahrstrasse anlegen
                      
Eine Fahrstrasse ist die Verbindung zwischen zwei Blöcken.
Sie wird immer bei dem Block angelegt, an dem die Fahrstrasse beginnt.
Dazu öffnen wir den Dialog "Automatisierung bearbeiten" des CTC-Moduls, in dem der Start-Block definiert ist.
In der Regel ist das dasselbe CTC-Modul, auf dem auch der Gleisplan abgelegt ist, der den Block zeigt.

Dort klicken wir auf den Block und dann auf den Plus-Button rechts von "Produkte und Aktionen":

![Config Fahrstraße 1]({% link assets/images/Auto-Garten/Config-Fahrstrasse-1.png %})

Auf der neu angelegten Fahrstraße wählen wir die Skript-Zeile, die mit "Wechsel auf" beginnt.
Dort klicken wir auf den Button unter "End-Block" und wählen das Ende der Fahrstraße aus: 

![Config Fahrstraße 2]({% link assets/images/Auto-Garten/Config-Fahrstrasse-2.png %})

Dan klicken wir auf den Button "Hinzufügen" um den ersten Schaltbefehl hinzuzufügen:

![Config Fahrstraße 3]({% link assets/images/Auto-Garten/Config-Fahrstrasse-3.png %})

Wir wählen "call" und klicken "Übernehmen".
Das Skript-Fenster wechselt auf den neu angelegten "call".
Dann klicken wir auf den Button unter "Führe Aktion aus" um die zu schaltenden Weiche zu wählen: 

![Config Fahrstraße 4]({% link assets/images/Auto-Garten/Config-Fahrstrasse-4.png %})
                                                                 
Wir markieren ggf. die Aktionen-Gruppe, dann die gewünschte Weiche und klicken auf "Übernehmen".
Schließlich legen wird über die Auswahl bei "Parameter" noch fest in welche Richtung die Weiche schalten soll: 

![Config Fahrstraße 5]({% link assets/images/Auto-Garten/Config-Fahrstrasse-5.png %})

So sieht die fertig konfigurierte Fahrstrasse aus:

![Config Fahrstraße 6]({% link assets/images/Auto-Garten/Config-Fahrstrasse-6.png %})
                     
ZUm Schluss klicken wir ncoh auf "Hochladen".

## Fahrstrasse schalten
                
Im Schaltpult finden wir nun eine Action-Gruppe "Fahrstrasse", mit der wir unsere Fahrstrassen schalten können.
Nach Klick auf den Button wird dieser farbig:

![Fahrstraße schalten]({% link assets/images/Auto-Garten/Fahrstrasse-schalten.png %})
         
Beim Schalten der Fahrstraße werden zuerst alle Weichen und der Zielblock reserviert und dann werden die Weichen geschaltet.
Die Reservierung bleibt bestehen bis eine Lok den Zielblock erreicht hat.
Die reservierten Weichen werden orange eingefärbt, der reservierte Block gelb:

![Fahrstraße im Gleisbild]({% link assets/images/Auto-Garten/Gleisbild-Fahrstrasse.png %})
                          
Durch Rechtsklick auf dem Zielblock kann die Fahrstraße zurückgenommen werden, ohne dass eine Lok den Zielblock erreichen muss:  

## Fahrstrasse zurücknehmen

![Fahrstraße zurücknehmen]({% link assets/images/Auto-Garten/Fahrstrasse-loeschen.png %})

---

[Zurück zu Teil 1]({% link _posts/de/szenarien/2022-08-12-Automatisierung-im-Garten-1.md %}) | [Zurück zu Teil 2]({% link _posts/de/szenarien/2022-08-12-Automatisierung-im-Garten-2.md %}) | [Zurück zu Teil 3]({% link _posts/de/szenarien/2022-08-12-Automatisierung-im-Garten-3.md %}) | **Teil 4** | [Weiter mit Teil 5]({% link _posts/de/szenarien/2022-08-21-Automatisierung-im-Garten-5.md %})