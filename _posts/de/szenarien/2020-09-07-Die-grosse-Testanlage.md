---
layout: post
title:  "Die große Testanlage"
date:   2020-09-07
categories: Modellbahn
tags: Anlage Lok Signal Weiche Automatik
author: Peter Rudolph
---
CTC ist etwas neues – keine kleine Evolution sondern eine Revolution.
Kein Wunder also, dass man uns derzeit viel skeptische Fragen stellt.
Wir haben also noch einiges zu erklären bzw. zu beweisen.

Mit diesem Artikel starten wir eine Serie über die große Testanlage unseres Chefentwicklers.
Sie wurde vor vielen Jahren angefangen und dann erst mal geparkt, um CTC zu entwickeln.
So sieht sie heute, am 7. September 2020 aus:

![Testanlage am 07.09.2020]({% link assets/images/testanlage/Testanlage-2020-09-07.jpg %})

## Technische Daten

* Fläche 6,5 m², Länge 4,60 m, Tiefe bis 1,90 m
* 7 Ebenen: Bergstrecke, Hauptebene, Tiefbahnhof, S-Bahn und drei Ebenen Schattenbahnhof
* 2 Gleiswendeln zu den Schattenbahnhofsebenen
* Gleismaterial im sichtbaren Bereich Märklin K-Gleis, Bahnof und S-Bahn Märklin C-Gleis und Schattenbahnhof Märklin M-Gleis
* Im Endausbau rund 60 Weichen, 60 Lichtsignale und mindestens genauso viele CTC-IR-Balisen
* Rund 40 Loks vom uralten Analog-Schatz bis zu Neuerwerbungen werden am Ende mit CTC-Lokmodulen unterwegs sein
* Auch das Car-System soll eines Tages mit CTC laufen.      

Bereits mit CTC-Weichenmodul ausgestattet sind 19 C-Gleis-Weichen. 
Im ersten Lasttests hatten wir noch weitere 30 CTC-Weichenmodule und 5 umgebaute Loks ohne Probleme in Betrieb. 
Die Analysen zeigten, dass eine Steigerung auf bis 250 Module insgesamt, kein Problem darstellen dürfte.

**Bis auf das Car-System kann CTC heute schon alles benötigt - es ist also nur eine Frage der verfügbaren Freizeit, bis die Umstellung komplett ist.**

## Planung

Das letzte Wochenende war vor allem der Planung gewidmet.

Als Erstes soll der Tiefbahnhof nach Stuttgart 21 Vorbild funktional vervollständigt werden.
Die Weichen haben schon ein CTC-Modul, aber die IR-Balisen und Lichtsignale fehlen noch.

## Lichtsignale

Märklin verbaut in seinen aktuellen Lichtsignalen leider immer einen Dekoder, den wir zwar ansteuern können, der aber keinen Mehrwert für CTC bringt.
Beim Anblick des Preises war dann schnell klar, dass es keine Märklin-Signale werden.
Für all diejenigen, die schon Märklin-Signale besitzen und auf CTC umsteigen wollen, haben wir trotzdem zwei Märklin-Signale beschafft und werden deren Ansteuerung in einem separaten Artikel besprechen.

Für die Demoanlage wurden nun Viessmann-Signale für den sichtbaren Bereich und Völkner Selbstbau-Signale für den unsichtbaren Bereich bestellt.
Auf Viessmann's Multiplex haben wir ebenfalls aus Kostengründen verzichtet.   

Solch ein Viessmann-Signal hat bis zu 10 einzeln ansteuerbare LEDs.
Das ist selbst für unser CTC-Multi-I/O-Board zu viel.
Also wurde kurzerhand ein schon im Hinterkopf existierendes neuen Modul geschaffen: Der CTC-IO-Extender.
Bis zu 7 solcher Module können an ein beliebiges CTC-Modul über den I2C-Bus angeschlossen werden und bieten je 16 zusätzliche Schaltausgänge.

In ca. zwei Wochen werden die Prototyp-Platinen eintreffen und müssen dann nur noch bestückt werden.
Bis dahin können wir schon mal die IR-Balisen fertigen und die Kabel in den richtigen Längen anlöten.

**[Fortsetzung in Teil 2]({% link _posts/de/szenarien/2020-09-20-Die-grosse-Testanlage-2.md %})** 