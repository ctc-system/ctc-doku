---
layout: post
title:  "Automatisierung im Garten - Teil 1"
date:   2022-08-12
categories: Automatisierung
tags: App Software Gartenbahn NFC-Balise
author: Peter Rudolph
---
Diese Artikel-Serie beschreibt die Konfiguration der Automatisierung am Beispiel einer Gartenbahn.
Das beginnt mit der Platzierung der NFC-Balisen und endet mit einer vollständigen Automatisierung durch einen Fahrplan.
 
Im ersten Teil planen wir die Unterteilung der Modellbahn in Blöcke.
Dann platzieren wir die Balisen auf dem Gleisbild. 

**Hinweis:** Die hier beschriebenen Funktionalitäten setzen **mindestens** die **CTC-App Version 4.04** und zugehörige Firmware voraus.

## Planung: Blöcke und Balisen                       
                       
Im [Kapitel 7.1 - Automatisierung: Blöcke]({% link _app_doku/071-bloecke.md %}) der Bedienungsanleitung findet sich eine Einführung zu Blöcken.

Wir unterteilen den gesamten Gleisplan unserer Modellbahnanlage in einzelne Blöcke (Gleisabschnitte).
Weichen (Abzweige) dürfen nur zwischen Blöcken existieren, niemals innerhalb eines Blocks.
                            
Der Hauptteil unserer Gartenbahn sieht wie folgt aus. 
Ganz schließt sich noch die über mehrere Wendeln in den Abstellbahnhof in der Garage führende Strecke an.

![Gleisplan mit Blöcken]({% link assets/images/Auto-Garten/Gleisplan-Gesamt.png %})

Wir vergeben für jeden Bahnhof einen Großbuchstaben. 
Unsere Anlage verfügt über drei Bahnhöfe, die die Buchstaben "S", "T" und "U" erhalten.

Innerhalb des Bahnhofs nummerieren wir die Gleise durch, beginnend mit den Hauptgleisen, gefolgt von den Abstellgleisen.
Nach den Ziffern 1 bis 9 kommen dann bei Bedarf die Kleinbuchstaben a bis z.
Unser Bahnhof "S" hat die Gleise 1 bis 3 für den Personenverkehr und die Abstellgleise 4 bis 6. 

Blöcke zwischen den Bahnhöfen beginnen mit dem Buchstaben des nächstgelegenen Bahnhofs gefolgt von einem weiteren Großbuchstaben.
                       
Die Balisen innerhalb eines Blocks werden im Uhrzeigersinn durchnummeriert.
Das Gleis 1 unseres Hauptbahnhofs "S" hat zwei Balisen, die somit die Positions-IDs "S11" und "S12" erhalten.

Jedes Abstellgleis erhält mindestens eine Balise vor dem Prellbock, sodass eine Lok, die diese Balise liest, noch rechtzeitig vor dem Prellbock anhalten kann.
Bei einem langen Abstellgleis platzieren wir eine weitere Balise in der Nähe der Weiche, sodass eine Lok, die über dieser Balise zum Stehen kommt, bereits vollständig im Abstellgleis steht.

Vor jedem Signal oder Halteschild wird ebenfalls eine Balise platziert, sodass eine mti dem NFC-Reader über dieser Balise angehaltene Lok noch genügend Abstand zum Signal hat.
In genügend Abstand vor dieser Balise wird eine weitere Balise platziert, die einer ankommenden Lok das Signal ankündigen kann, sodass diese bis kurz vor der zweiten Balise ihre Minimalgeschwindigkeit erreicht und dann exakt über der zwiten Balise zujm Stehen klommt  

Signale oder Schilder müssen übrigens nicht unbedingt sichtbar auf der Modellbahn existieren.
Sie können auch nur im Gleisbild vorhanden sein und trotzdem zur Zugbeeinflussung genutzt werden.
Nur die Balisen müssen real existieren.

## NFC-Balisen platzieren
                 
Nachdem wir alle Balisen an den geplanten Stellen auf das Gleis gesteckt haben, fahren wir mit dem NFC-Reader einer Lok über die nächstbeste Balise.
Direkt unter dem Bild der Lok erscheint hinter "Last ID" die eindeutige Nummer der NFC-Balise:

![Anzeige Balise in Loksteuerung]({% link assets/images/Auto-Garten/Neue-Balise-Lok.png %})

Dann öffnen wir den Gleisplan, in dem wir die Balise platzieren wollen.
Dort klicken auf die Action-Gruppe Balise und markieren in der unteren Liste die gerade mit der Lok gelesene NFC-Balise.
In der Regel sollte es die letzte in der Liste sein.
Dann klicken wir im Gleisbild auf die Stelle, an der wir die Balise platzieren wollen.
Die Stelle wird mit einem orangen Rechteck markiert und das Gleisstück wird blau:

![Balise im Gleisplan platzieren]({% link assets/images/Auto-Garten/Neue-Balise-Gleisplan.png %})

Als Letztes tragen wir die dreistellige Positions-ID in das (hier blau umrahmte) Eingabefeld ein.

Diese Schritte können wir gleich für mehrere NFC-Balisen wiederholen.
Den Gleisplan können wir geöffnet lassen während wir mit der Lok zur nächsten Balise fahren.

Wenn wir mit den Balisen dieses Gleisplans fertig sind klicken wir auf "Hochladen".
    
---

**Teil 1** | [Weiter mit Teil 2]({% link _posts/de/szenarien/2022-08-12-Automatisierung-im-Garten-2.md %}) | [Vor zu Teil 3]({% link _posts/de/szenarien/2022-08-12-Automatisierung-im-Garten-3.md %}) | [Vor zu Teil 4]({% link _posts/de/szenarien/2022-08-12-Automatisierung-im-Garten-4.md %}) | [Vor zu Teil 5]({% link _posts/de/szenarien/2022-08-21-Automatisierung-im-Garten-5.md %}) 