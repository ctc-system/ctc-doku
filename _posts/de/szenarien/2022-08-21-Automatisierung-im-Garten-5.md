---
layout: post
title:  "Automatisierung im Garten - Teil 5"
date:   2022-08-21
categories: Automatisierung
tags: App Software Gartenbahn NFC-Balise Fahrauftrag Fahrplan
author: Peter Rudolph
---
Diese Artikel-Serie beschreibt die Konfiguration der Automatisierung am Beispiel einer Gartenbahn.

Im fünften Teil legen wir Fahraufträge an und erstellen auf deren Basis einen Fahrplan.
           
In unserem Fahrplan soll die im Abstellgleis 6 stehende Lok auf Gleis 1 im Bahnhof S bereitgestellt werden.
Anschließend soll sie eine Runde auf der Anlage drehen udn dabei im Bahnhof T auf Gleis 1 einen Zwischenstopp einlegen.
Nach Ankunft auf Gleis 1 im Bahnhof S soll die Lok wieder in Gleis 6 abgestellt werden.

## Fahrauftrag anlegen

Zum Anlegen eines Fahrauftrags öffnen wir die "Automatisierung Config", wählen den obersten Eintrag von "Produkte und Aktionen".
Dann klicken wir rechts daneben auf den Plus-Button:

![Fahrauftrag anlegen 1]({% link assets/images/Auto-Garten/Fahrauftrag-anlegen-1.png %})

Unter dem neu angelegten Fahrauftrag wählen wir die Funktion (Fahrauftrag).
Dann wählen wir im Skript den obersten Knoten und klicken rechts davon auf "Hinzufügen".
Es wird eine neue Fahrt "Fahrt new" angelegt:

![Fahrauftrag anlegen 2]({% link assets/images/Auto-Garten/Fahrauftrag-anlegen-2.png %})
                                                                                
Dann geben wir der Fahrt einen Kommando-Buchstaben, einen passenden Namen und setzen dessen Ziel-Block:

![Fahrauftrag anlegen 3]({% link assets/images/Auto-Garten/Fahrauftrag-anlegen-3.png %})

In unserem banalen Fall besteht der Fahrauftrag aus einer einzigen Fahrstraße:

![Fahrauftrag anlegen 4]({% link assets/images/Auto-Garten/Fahrauftrag-anlegen-4.png %})

Anschließend legen wir die weiteren Fahraufträge an:
* Von Bahnhof S Gleis (Block S1) nach Bahnhof T Gleis 1 (Block T1)
* Von Bahnhof T Gleis 1 (Block T1) nach Bahnhof S Gleis 1 (Block S1)
* Abstellen der Lok in Abstellgleis 6: Von Block S1 nach Block S6.
                     
## Fahrplan erstellen

Hhier geht es ion Kürze weiter. 
Bis dahin hilft die Bedienungsanleitung [Kapitel 7.4 "Automatisierung: Fahrpläne"]({% link _app_doku/074-fahrplane.md %}) weiter

---

[Zurück zu Teil 1]({% link _posts/de/szenarien/2022-08-12-Automatisierung-im-Garten-1.md %}) | [Zurück zu Teil 2]({% link _posts/de/szenarien/2022-08-12-Automatisierung-im-Garten-2.md %}) | [Zurück zu Teil 3]({% link _posts/de/szenarien/2022-08-12-Automatisierung-im-Garten-3.md %}) | [Zurück zu Teil 4]({% link _posts/de/szenarien/2022-08-12-Automatisierung-im-Garten-4.md %}) | **Teil 5** 
