---
layout: post
title:  "Anschluss Fleischmann-Drehscheibe"
date:   2023-09-12
categories: Anschließen
tags: Umbau Drehscheibe 
author: Peter Rudolph
---
Die Fleischmann-Drehscheibe 6651 kann an das [CTC-Weichenmodul-G]({% link _data_sheet/CTC-Weichenmodul-G.md %}) angeschlossen und somit von der CTC-App gesteuert werden.
Vermutlich gilt das hier vorgestellte Anschlussprinzip auch für viele andere (nicht digitale) Drehscheiben.

![Drehscheibe mit CTC-Modul]({% link assets/images/Drehscheibe/Drehscheibe-1.jpg %})
   
Die Anschlüsse der Drehscheibe bestehen aus einem 3-adrigen Kabel (rot/gelb/grau) zum Bewegen der Drehscheibe und einem 2-adrigen Kabel (2x gelb) für die Stromversorgung des Gleises auf der Drehbühne.
Die Adern gelb und rot sind der Motoranschluss. 
Das graue Kabel dient zum Starten der Drehbewegung.

Wir schließen alle drei Adern gelb/rot/grau an HB-Anschlüsse des CTC-Weichenmodul-G an, hier:
* gelb an HB-1
* rot an HB-2
* grau an HB-3

Dann verbinden wir die Anschlüsse Gleis-A und Gleis-B des CTC-Weichenmodul-G mit einer geeigneten Spannungsversorgung (laut Datenblatt 12 bis 14 Volt).
Für unseren Messestand war ich ein kleines bisschen mutig und nutze die dort vorhandene 15 Volt Versorgung.

Nachdem das CTC-Weichenmodul-G in unser Modellbahn-WLAN eingebucht ist, öffnen wir den Config-Dialog (Stift-Symbol) und klicken auf "Config ändern".
Dort wählen wir aus dem Produktkatalog "universell-sonstiges.xml" die Drehscheibe aus und verbinden die Pins wie im folgenden Screenshot zu sehen:

![Drehscheibe Config]({% link assets/images/Drehscheibe/Drehscheibe-Config.png %})

Die Drehscheibe taucht dann im "Schaltpult" auf:

![Drehscheibe Schaltpult]({% link assets/images/Drehscheibe/Drehscheibe-Schaltpult.png %})
