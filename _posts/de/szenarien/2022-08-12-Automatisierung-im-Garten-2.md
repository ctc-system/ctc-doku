---
layout: post
title:  "Automatisierung im Garten - Teil 2"
date:   2022-08-12
categories: Automatisierung
tags: App Software Gartenbahn NFC-Balise Stoppschild Zugsicherung
author: Peter Rudolph
---
Diese Artikel-Serie beschreibt die Konfiguration der Automatisierung am Beispiel einer Gartenbahn.

Im zweiten Teil beschäftigen wir uns mit der Zugsicherung durch Stop-Balisen vor Prellböcken.

## Zugsicherung: Balisen vor Prellböcken
                
Als Erstes kümmern wir uns um die Balisen vor Prellböcken.
Sie sorgen dafür, dass unsere Loks immer rechtzeitig vor dem Prellbock zum Stehen kommen.
Dabei müssen wir natürlich bedenken wie weit der NFC-Reader in den einzelnen Loks vom Anfang bzw. Ende der Lok entfernt ist, und dass die Lok auch bei einer Vollbremsung nicht unbedingt sofort steht. 

### Stoppschild (Sh2) anlegen

Wir suchen uns ein CTC-Modul in der Nähe aus, z.B. jenes welches die Weiche zum zugehörigen Abstellgleis schaltet.
Dort gehen wir in die Config und klicken auf den Button Hinzufügen eines Produkts.
Wir wählen den Katalog "universell-schilder.xml":

![Config Schilder]({% link assets/images/Auto-Garten/Config-Schilder.png %})
                                                  
**Hinweis:** Auf [Stellwerke.de](http://www.stellwerke.de/index.html) findet sich eine umfangreiche Beschreibung von bei der Bahn üblichen Schildern und deren Kurzbezeichnungen.

Dort vergeben wir einen passenden Namen und wählen "Halteschild" in der richtigen Richtung (rechts steht für "im Uhrzeigersinn"):

![Auswahl Schild Prellbock]({% link assets/images/Auto-Garten/Config-Schild-Prellbock.png %})

Das neue Schild wurde übernommen:

![Schild Prellbock in der Config]({% link assets/images/Auto-Garten/Config-Schild-Prellbock-2.png %})
                                 
Nun können wir die Config hochladen und anschließend im Gleisbild die Position des Schilds setzen.

### Stoppschild im Gleisbild platzieren

Dazu öffnen das Gleisbild, in dem der Prellbock platziert werden soll und setzen ein Signal (Ampel) an die betreffende Stelle.

![Prellbock im Gleisplan 1]({% link assets/images/Auto-Garten/Gleisplan-Prellbock-1.png %})

Dann wählen wir das Schild am Prellbock aus:

![Prellbock im Gleisplan 2]({% link assets/images/Auto-Garten/Gleisplan-Prellbock-2.png %})

Durch Klick auf das neu platzierte Signal wird das Prellbock-Schild zugeordnet.
Das Symbol ändert sich zu dem roten Rechteck mit weißem Rand, wie es für Sh2 definiert ist:

![Prellbock im Gleisplan 3]({% link assets/images/Auto-Garten/Gleisplan-Prellbock-3.png %})

Dann müssen wir noch das Stoppschild und die NFC-Balise miteinander verknüpfen.

### Stoppschild und NFC-Balise verknüpfen 

Dazu klicken wir in der Schaltkasten-Konfiguration des CTC-Moduls, in dem auch der Gleisplan enthalten ist, auf "Automatisierung ändern".
Dort suchen wir unter "Aktionen" die betreffende NFC-Balise und wählen den bereits vorhandenen Trigger:

![Config Trigger Prellbock 1]({% link assets/images/Auto-Garten/Config-Prellbock-Trigger-1.png %})
                                                                                                   
Wir klicken auf den Button "Auslöser" und wählen das Schild am Prellbock als Auslöser:

![Config Trigger Prellbock 2]({% link assets/images/Auto-Garten/Config-Prellbock-Trigger-2.png %})

Der Prellbock wurde als Auslöser übernommen:

![Config Trigger Prellbock 3]({% link assets/images/Auto-Garten/Config-Prellbock-Trigger-3.png %})
                                            
Wir laden die Config hoch und die Balise vor dem Prellbock wird aktiv.
Die rote Markierung in Fahrtrichtung rechts vom Gleis weist auf das Stop-Schild voraus hin: 

![Gleisbild Anzeige Prellbock-Balise]({% link assets/images/Auto-Garten/Gleisbild-Anzeige-Prellbock.png %})

**Hinweis:** Wenn die Anzeige an der Balise auf der falschen Seite erscheint, haben Sie die Balise falsch rum im Gleisbild platziert.
Achten Sie beim Platzieren der Balise auf den kleinen grauen Pfeil in der Mitte des Symbols: Er muss im Uhrzeigersinn zeigen.
                          
## Zustand der Balise überprüfen

Durch Klick auf die Balise im Gleisbild erhalten wir jederzeit Detail-Informationen zur Balise:

![Gleisbild Detailinfo Balise]({% link assets/images/Auto-Garten/Gleisbild-Info-Balise.png %})
               
--- 

[Zurück zu Teil 1]({% link _posts/de/szenarien/2022-08-12-Automatisierung-im-Garten-1.md %}) | **Teil 2** | [Weiter mit Teil 3]({% link _posts/de/szenarien/2022-08-12-Automatisierung-im-Garten-3.md %}) | [Vor zu Teil 4]({% link _posts/de/szenarien/2022-08-12-Automatisierung-im-Garten-4.md %}) | [Vor zu Teil 5]({% link _posts/de/szenarien/2022-08-21-Automatisierung-im-Garten-5.md %}) 