---
layout: post
title:  "Automatisiert fahren"
date:   2020-09-01
categories: Automatisierung
tags: Lok Signal Automatik
author: Peter Rudolph
---
**Dieser Artikel ist mit Version 4 von CTC veraltet.
Den [Nachfolgeartikel finden sie hier]({% link _posts/de/szenarien/2022-02-04-Automatisiert-fahren-CTC4.md %}).**

**Hinweis: Dieser Artikel wurde am 05.02.2021 soweit angepasst, dass er zur Version 3 von CTC passt.**

Dieser Artikel ist die Fortsetzung von [Eine CTC-Modellbahn entsteht]({% link _posts/de/szenarien/2020-08-22-Eine-CTC-Modellbahn-entsteht.md %}) und beschäftigt sich mit der Automatisierung dieser Modellbahn.

Die CTC-App bietet grundsätzlich nur einige einfache Automatisierungen.
Wer hier aufs Ganze gehen will, muss sich noch ein bisschen gedulden, bis wir eine Schnittstelle zu Modellbahn-Steuerungs-Software wie iTrain geschaffen haben.

Grundlage für Automatisierung sind immer geeignete Sensoren.
Mit [CTC-IR-Balisen]({% link _data_sheet/CTC-IR-Sender.md %}) im Gleis und entsprechenden Empfängern in der Lok ist CTC hier bestens gerüstet.
Denn im Unterschied zur klassischen Sensorik mit Schaltgleisen, Lichtschranken oder Reed-Relais weiß die CTC-App nicht nur, dass irgend etwas vorbeigekommen ist, sondern genau welche Lok.

## Automatisch anhalten vor einem roten Signal

Die simpelste Automatik lässt sich in CTC ganz ohne Eingriff durch die App lösen.
Dabei wird der Lok über die IR-Balise mitgeteilt, dass und in welcher Distanz sie anhalten soll.
Die notwendige Verknüpfung zwischen Signal und IR-Balise stellen wir in der Konfiguration des CTC-Weichenmoduls ein, an dem die IR-Balise angeschlossen ist.

Dazu suchen wir uns im Konfigurator das betreffende CTC-Weichenmodul und klicken dort auf das Stift-Symbol.
Es öffnet sich der Dialog "Schaltkasten bearbeiten".
Dort fügen wir über den Produktkatalog die IR-Balise ein und wechseln dann auf den Reiter "Aktionen".
    
Dort selektieren wir bei "angeschlossene Produkte" den Trigger unterhalb der IR-Balise.
Es erscheinen die "Details der Aktion".
Dort klicken wir auf den Button rechts von "Auslöser" und wählen das zugehörige Signal aus:

![Signal bearbeiten]({% link assets/images/demoanlage/Demoanlage-Signal-Edit.png %})

Die notwendigen Skripte und Befehle wurden bei der Übernahme aus dem Produktkatalog bereits angelegt.
Wir müssen nur noch in den Skripten die korrekte Distanz bis zum Haltepunkt eintragen:  

![Signal-Aktionen bearbeiten]({% link assets/images/demoanlage/Demoanlage-Signal-Edit-Aktionen.png %})

Nachdem wir auf "Hochladen" geklickt haben, können wir probieren, ob unsere Konfiguration funktioniert:
Sobald eine Lok die entsprechende IR-Balisen vor einem roten Signal überfährt, beginnt sie zu bremsen.
Spätestens beim Überfahren der zweiten IR-Balise (mit cmdDist = 0) hält die Lok an.

Wenn wir die Bremsparameter unserer Lok gut genug eingestellt haben, können wir uns die zweite IR-Balise sogar sparen.

## Schalten beim Überfahren einer IR-Balise

Wollen wir beim Überfahren einer IR-Balise eine Weiche oder Signal schalten, so muss das die CTC-App übernehmen.
Wie das geht ist im Kapitel [Aktionen verknüpfen]({% link _app_doku/043-aktionen-verknuepfen.md %}) der Bedienungsanleitung beschrieben.

Zu einem späteren Zeitpunkt werden wir im Rahmen eines ausführlichen Beispiels noch einmal detaillierter auf diese Thematik eingehen.
 