---
layout: post
title:  "CTC Version 3"
date:   2021-01-31
categories: CTC-Allgemein
tags: App Software
author: Peter Rudolph
---
**Eineinhalb Jahre Marktpräsenz, die Erfahrungen mit den ersten CTC-Kunden und viele Gespräche mit Interessenten sind in diese große Überarbeitung der CTC-Software geflossen!**

Die Neuerungen im Kurzüberblick:
* Wesentlich vereinfachte und gleichzeitig leistungsfähigere Konfiguration.
* Modul übergreifende Verkettung von Sensoren und Aktionen.
* Vollständige Integration von DCC-Dekodern.
                                   
## Konfiguration
         
Vor allem für Einsteiger haben wir uns etwas einfallen lassen:
Der CTC-App liegt ein Produkt-Katalog bei, den wir ständig erweitern.

![CTC v3 Config-Dialog]({% link assets/images/CTC3-Produktkatalog.png %}) 

Möchten Sie nun, z. B. eine Weiche an ein CTC-Multi-I/O-Board anschließen, so öffnen Sie einfach den Produktkatalog, wählen die passende Weiche, sowie die Anschlüsse aus und schon können Sie die Weiche schalten!  

![CTC v3 Config-Dialog]({% link assets/images/CTC3-Config.png %})

<BR>

Wem das nicht genügt, der kann ab sofort bis ins kleinste Detail die Konfiguration seiner CTC-Module anpassen.
Siehe dazu auch den nächsten Abschnitt "Automatisierung".

![CTC v3 Config-Dialog]({% link assets/images/CTC3-Config-Trigger.png %})

## Automatisierung

Sie können nun beliebige Verknüpfungen zwischen Sensoren und Aktionen anlegen.
Für einen ersten Eindruck, hier ein paar Beispiele aus dem schier unendlichen Spektrum der Möglichkeiten:
* Schalten einer Weiche, wenn eine andere Weiche geschaltet wird.
* Setzen eines Signals auf rot, wenn ein Schaltgleis überfahren wird.
* Abspielen einer Ansage, wenn ein bestimmter Zug in einen Bahnhof einfährt (ausgelöst durch die Lok, wenn Sie eine IR-Balise überfährt).
* Auslösen der Hupe einer Lok, wenn sie auf einen Tunnel zufährt (Kommando von der IR-Balise).
                         
Wir sind gespannt, welche Ideen Ihnen alle kommen, wenn Sie sich mit CTC Version 3 beschäftigen – schreiben Sie uns!

## Integration von DCC

Vor allem, um geliebte Sound-Dekoder auch weiter nutzen zu können, wurden wir immer wieder darum gebeten, eine Lösung anzubieten.

Ab sofort können alle CTC-Lokmodule über Ihren Motor-Ausgang ein DCC-Signal erzeugen.
Das CTC-Modul verhält sich, wie eine Mini-Zentrale, die Sie zwischen Gleis und DCC-Dekoder einbauen können.
Einziger Wermutstropfen: in der Lok sollte genügend Platz für das CTC-Modul und den DCC-Dekoder vorhanden sein.

Unser CTC-Lokmodul-H0d hat sogar einen separaten Anschluss für DCC.
Damit kann der Motor über CTC gesteuert und parallel der DCC-Dekoder angesprochen werden.
              
DCC-Weichen oder sonstige DCC-Dekoder können ebenfalls mit Hilfe der Lokmodule angesteuert werden.

## Selber machen

Auch die innere Struktur der CTC-Firmware wurde deutlich vereinfacht.
Wer sich an die Programmierung mit Arduino heran traut, kann jetzt all das realisieren, was CTC (noch) nicht kann und dieses mit der CTC-App steuern.

Wir sind gespannt, was hier alles entstehen kann.
Und unterstützen Sie gerne bei Ihren Projekten.
