---
layout: post
title:  "Umsteigen von Analog"
date:   2020-06-19
categories: Umsteigen
tags: Analog Lok Weiche Signal Umsteiger
author: Peter Rudolph
---
Wer heute noch analog fährt, kann mit Hilfe von CTC das Zeitalter der digitalen Modellbahn überspringen. 
Dabei gestaltet sich der Umstieg viel leichter und konstengünstiger als zu einem Digital-System.

## Was braucht man dazu?

* Eine Stromversorgung: Das kann aber zumindest vorerst auch der alte Modellbahn-Trafo sein.
* Ein [WLAN-Access-Point]({% link de/docu/WLAN-Access-Point.md %}): Für die ersten Versuche tut es auch das private WLAN.
  Ab ca. 20 Modulen sollte es dann aber schon ein eigenes WLAN für die Modellbahn sein.
* Für jede Lokomotive ein [CTC-Lokmodul]({% link de/docu/CTC-Lokmodule.md %})
* Für Weichen und Signale mit Doppelspulenantrieb (3 Kabel, Standard bei H0) entweder das [CTC-Weichenmodul]({% link _data_sheet/CTC-Weichenmodul-H0.md %}) oder das [CTC-Multi-I/O-Board]({% link _data_sheet/CTC-Multi-IO-Board.md %}) für bis zu 4 Weichen/Signale.
* Für Weichen und Signale mit Motor-Antrieb (2 Kabel, Standard bei Spur-G) das [CTC-Weichenmodul-G]({% link _data_sheet/CTC-Weichenmodul-G.md %}). 
* Für Weichen und Signale mit Servo-Antrieb das [CTC-Weichenmodul-G]({% link _data_sheet/CTC-Weichenmodul-G.md %}) oder das [CTC-Multi-I/O-Board]({% link _data_sheet/CTC-Multi-IO-Board.md %}).

Der Parallelbetrieb von analogen Loks/Weichen/Signalen und CTC-Loks/Weichen/Signalen ist jederzeit möglich.
Die Steuerung erfolgt dann aber getrennt voneinander über Fahr-/Schaltpult und die CTC-App.
Für den Gleisabschnitt, auf dem die CTC-Lok unterwegs ist, muss das Fahrpult auf Vollgas gestellt werden.

## Was passiert mit Loks mit Digital-Dekoder?

Die Digital-Dekoder werden durch entsprechende CTC-Lokmodule ersetzt.

## Wie fange ich an?

Am einfachsten lassen Sie sich von uns ein [Starter-Set]({% link _data_sheet/CTC-Starter-Sets.md %}) kommen, um sich so erst mal mit der neuen Welt von CTC vertraut zu machen.
Dabei können Sie entweder mit einer Weiche, einer Lok oder mit beidem beginnen.

In jedem Fall bekommen Sie einen CTC-Router, die CTC-Module und die CTC-App.
Mehr brauchen Sie nicht, denn die Stromversorgung übernimmt Ihr bestehendes Fahrpult.
Die CTC-Lok kann gleichzeitig mit Analog-Loks auf derselben Anlage fahren, braucht aber einen eigenen, voll aufgedrehten Fahrpult.
Die Weichen können vom Lichtanschluss des Fahrpults oder einer separaten Stromquelle versorgt werden.

## Und wie geht es weiter?

Dazu gibt es diverse Möglichkeiten:
* Sie rüsten nach und nach alle Loks, Weichen und Signale auf CTC um und erreichen irgendwann eine rein mit CTC gesteuerte Modellbahn.
* Sie rüsten nur Weichen, Signale und Rückmelder auf CTC um und fahren Ihre Loks nach wie vor analog.
* Sie rüsten nur Ihre Loks auf CTC um und bauen ggf. CTC-IR-Balisen ins Gleis ein, schalten aber nach wie vor analog.

Wenn Sie nicht gerade Museumsreife Loks haben, die Sie im Originalzustand erhalten wollen, spricht aus unserer Sicht nichts dafür bei Analog zu bleiben.

## Was passiert mit meinen Rückmeldern?

Hier wird es viel einfacher mit CTC: Komplexe Verdrahtungen oder proprietäre Elektronik entfallen komplett.
Jeweils vier klassische Sensoren können an das CTC-Multi-I/O-Board angeschlossen werden.

Allerdings lohnt es sich, einen Blick auf die [CTC-IR-Balise]({% link _data_sheet/CTC-IR-Sender.md %}) zu werfen.
Jeweils zwei von ihnen können an ein CTC-Weichenmodul oder CTC-Multi-I/O-Board angeschlossen werden.
Sie ermöglichen es, dass die Lok ihre Position ermitteln und ggf. auch einen Befehl (z.B. Signal Halt in 70cm Entfernung) empfangen kann.
Mithilfe der CTC-IR-Balisen ist eine einfache Automatisierung ganz ohne Eingriff der App möglich.

In allen Fällen werden die genannten Rückmelde-Informationen zusammen mit den Status-Meldungen der CTC-Module über das WLAN übertragen.

Mehr zur Automatisierung erfahren Sie im Artikel [Automatisiert fahren]({% link _posts/de/szenarien/2020-09-01-Automatisiert-fahren.md %}).
