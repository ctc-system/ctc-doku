---
layout: starterkit
title:  "Einleitung und Ausblick"
chapter: 1
date:   2024-09-26
author: Klaus Gungl
author2: Walter Kammermann
---

## Einleitung

Ein Starter-Set von CTC ermöglicht in Verbindung mit einem Ergänzungs-Set den Aufbau einer kleinen Gleisanlage, 
mit welcher die wesentlichen Fähigkeiten und Möglichkeiten von CTC getestet und verwendet werden können.
Obwohl es sich hier "nur" um ein Starter-Set handelt, wird die Anlage so aufgebaut,
dass sie erweiterbar ist. 
Diese Erweiterungen werden im Laufe ergänzender Dokumentation beschrieben. 
Der Aufbau des Starter-Sets berücksichtigt die Erweiterungen bereits bei den vergebenen Namen, 
an den betreffenden Stellen wird darauf hingewiesen. 


In dieser Dokumentation wird das Starter-Set plus Ergänzungs-Set für das Märklin C-Gleis beschrieben.
Die Module gilt es in die Lokomotiven und Weichen einzubauen und zu konfigurieren.
Sollte ein Starter-Set mit bereits eingebauten CTC-Lokmodulen und -Weichenmodulen verwendet werden,
können die Schritte zum Einbau übersprungen werden.
Zusätzlich werden zwei Formsignale mit Magnetantrieb verwendet.
Die folgenden Themen werden für den Anfang behandelt:
* Module (Loks, Weichen) einbauen und elektrisch anschließen (Kapitel 2),
* Aufbau der Anlage (Kapitel 3),
* Weichenmodule für den Betrieb der Weichen und der Formsignale konfigurieren (Kapitel 4),
* Erstellen des Gleisbildes (Kapitel 5), und
* erste Inbetriebnahme der Anlage (Kapitel 6).

Die aufzubauende Gleisanlage ist ähnlich der im Artikel
[Eine CTC-Modellbahn entsteht]({% link _posts/de/szenarien/2020-08-22-Eine-CTC-Modellbahn-entsteht.md %}) beschriebenen.
Die zugehörige Anwendungssoftware CTC-App ist sowohl unter Windows, Linux und Mac installierbar und lauffähig.
Zu beachten sind mögliche Einschränkungen bezüglich der Betriebssysteme.
So können die unterschiedlichen Distributionen von Linux durchaus divergentes Verhalten zeigen.
Nähere Information zu den unterstützten Versionen finden sich in der
[Bedienungsanleitung Kapitel 2]({% link _app_doku/020-installation-der-app.md %}).

Der Grundriss der Gleisanlage besteht aus einem Oval mit einem Bahnhof als Haltestelle.
Für den Zugbetrieb wird angenommen, dass eine Lokomotive im Uhrzeigersinn und eine weitere 
Lokomotive entgegen dem Uhrzeigersinn fährt.
Mit der CTC-App werden die Weichen geschaltet sowie die Loks gefahren.


![Gleisbild für Starter-Set Modellbahnanlage]({% link assets/images/Starterkits/Gleisoval_Basis.jpg %})

Nachdem zum ersten Mal die Loks auf der Anlage gefahren sind, kann der Ausbau der Anlage in Angriff genommen werden:
* IR-Balisen (früher "IR-Sender" genannt) in die Gleise einbauen und für den Betrieb konfigurieren (Kapitel 7),
* Gleisplan um IR-Balisen ergänzen (Kapitel 7),
* Sensoren der Loks kalibrieren (Kapitel 9), und
* Fahrbetrieb mit Signalen und IR-Balisen durchführen (Kapitel 10).


Es wird die folgende Funktionalität implementiert: wenn ein Signal auf "Halt" steht, 
wird die Lok langsamer, fährt bis zur IR-Balise vor dem Signal und hält an.
Die Funktionalität wird um die Balisen erweitert. 

Für die Bezeichnungen der Balisen, Signale und Weichen werden Namen gewählt, 
welche für den Blockbetrieb und die Zugautomatisierung sinnvoll oder zum Teil
sogar notwendig sind (genauere Erklärung siehe "Ausblick").

![Gleisbild für Starter-Set Modellbahnanlage mit IR-Balisen]({% link assets/images/Starterkits/Gleisoval_Basis_Signal_Weichen_IRSender_Loks.jpg %})

Darauf folgend wird der automatisierte Zugbetrieb eingeführt. 
Mit dieser Konfiguration wird für eine Lok ein kleiner "Fahrplan" erstellt:

* Einfahrt in den Bahnhofsbereich mit immer langsamer fahrender Lok (Kapitel 10).
* Langsame Fahrt in den Bahnhof und anhalten (Kapitel 10).
* Nach der vordefinierten Haltezeit setzt die Lok ihre Fahrt fort, 
   fährt eine Runde und fährt wieder in den Bahnhofsbereich ein (Kapitel 10).

## Ausblick

Für den weiteren Ausbau der Anlage wird das Starter-Set erweitert:
* Mit einer Weiche und weiteren Schienen wird ein "Sackbahnhof" ermöglicht. 
* Mit weiteren IR-Balisen wird der Blockbetrieb erweitert.
* Einige Blöcke erhalten Formsignale. 
* Für die IR-Balisen, die Formsignale und die Weiche sind entsprechende CTC-Module erforderlich. 

Mithilfe der genannten Erweiterungen wird der Fahrbetrieb 
auf den Sackbahnhof erweitert.
Die folgende Abbildung zeigt das schematische Gleisbild in einer sehr umfangreichen Ausbaustufe.
Welche der einzelnen Balisen, Signale, Weichen und Gleise in den jeweiligen Aufbaubeispielen verwendet werden,
wird jeweils angegeben.

![Gleisbild für Starter-Set Modellbahnanlage erweitert 1]({% link assets/images/Starterkits/Gleisoval_Erweiterung_1.jpg %})

Für die Bezeichnungen der Gleise und Blöcke, wie schon für die Balisen, Signale und Weichen, werden
Namen gewählt, welche für den Blockbetrieb und die Zugautomatisierung sinnvoll oder zum Teil
sogar notwendig sind. 
1. Es gibt zwei Bahnhofsbereiche, diese werden mit "D" und "E" bezeichnet. Daraus ergeben sich folgende Blöcke:
  * Block D1 ist der "Durchfahrbereich" von "Bahnhofbereich D".
  * Block D2 ist der "Haltebereich" von "Bahnhofsbereich D".
  * Block E1 ist der "Haltebereich" von "Bahnhofbereich E", dieser Bahnhof ist als Sackbahnhof ausgeführt.
  * Block E2 ist der "Durchfahrbereich" von "Bahnhofsbereich E".
1. Block DE verbindet Block D mit Block E. 
   Die Bezeichnung nimmt die Fahrtrichtung einer Lok im Uhrzeigersinn an.
1. Block ED verbindet Block E mit Block D.
   Die Bezeichnung nimmt die Fahrtrichtung einer Lok im Uhrzeigersinn an.
1. Balisen werden im Uhrzeigersinn durchnummeriert und einem Block zugeordnet.
  * Balisen D21 und D22 gehören zu Block D2 und stehen in engem Zusammenhang mit der Stellung 
    des zugehörigen Signals Demo-S-D2-li. 
  * Balisen D11 und D12 gehören zu Block D1 und stehen in engem Zusammenhang mit der Stellung 
    des zugehörigen Signals Demo-S-D1-re.
  * Balisen DE1 und DE2 gehören zu Block DE und stehen in engem Zusammenhang mit der Stellung 
    des zugehörigen Signals Demo-S-Ein-D-li.
  * Balisen ED1 und ED2 gehören zu Block ED und stehen in engem Zusammenhang mit der Stellung 
    des zugehörigen Signals Demo-S-Ein-D-re.
  * Balisen E11 und E12 gehören zu Block E1. Balise E12 steht in engem Zusammenhang mit der Stellung 
    des zugehörigen Signals Demo-S-E1.
1. Signale sind wie Balisen Teil eines Blocks. Signale können als "Ausfahrsignale aus einem Block raus" 
   betrachtet werden oder als "Einfahrsignale in den nächsten Block" (jeweils in Richtung des fahrenden Zuges).
   * Signal "Demo-S-D2-li" ist ein Ausfahrsignal für Block D2, Lok BR212-FW fährt im Uhrzeigersinn 
     und später im Pendelzugbetrieb.
   * Signal "Demo-S-D1-re" ist ein Ausfahrsignal für Block D1, Lok BR365 fährt gegen den Uhrzeigersinn.
