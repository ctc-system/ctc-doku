---
layout: starterkit
title:  "Erste Inbetriebnahme der Anlage"
chapter: 6.1
date:   2022-08-29
author: Klaus Gungl
categories: Starterkit
tags: Erste_Inbetriebnahme
---

Im Folgenden wird davon ausgegangen, dass die CTC-Anlage gemäß den Beschreibungen für das Starter-Set aufgebaut
und konfiguriert ist.

Bei der Einschaltsequenz der CTC-Komponenten ist zu beachten, dass zuerst der WLAN-Router eingeschaltet und betriebsbereit sein muss.
Anschließend wird die Eisenbahnanlage eingeschaltet.

Die CTC-App findet das PI-Rail WLAN, und verbindet sich mit den CTC-Modulen.
Die Konfiguration der Module wird ausgelesen.
Im gewählten Beispiel des Starterkits hat die CTC-App eine Lokomotive (BR365), zwei Weichen und zwei Formsignale gefunden.
Die Konfiguration der Lokomotive wurde ausgelesen, daraus ergibt sich das jeweilige Bild der Lokomotive, 
deren Name und weiter Konfigurationsinformation, welche später betrachtet werden.
Der Gleisplan - hier „Starterkit_1“ genannt - ist in einem der konfigurierten Weichenmodule (Demo-W-rechts) 
gespeichert und wurde ebenfalls ausgelesen.
Darin ist auch die Information über die Formsignale enthalten.

Nachdem die CTC-App gestartet wurde, zeigt das Anfangsbild das Fenster "Module":

![Startbild Loks Gleisplan]({% link assets/images/Starterkits/Inbetriebnahme1_Startbild_Module_Gleisplan_1Lok.jpg %})

Das Fenster mit den Reitern zur Auswahl „Module, Loks, Steuerung, Schaltpult und Sensoren“ ermöglicht 
den Zugriff auf die einzelnen Funktionsgruppen:
* Module: Heir erschein die Liste aller gefunden Module und deren wichtigsten Kenngrößen. 
  Von hier aus kann auch direkt deren Konfiguration aufgerufen werden. 
* Loks: Hier erscheinen alle gefunden Lokomotiven, dabei ist die grau hinterlegt Lokomotive die ausgewählte Lokomotive (siehe auch „Steuerung“)
* Steuerung: Hier erscheint das Steuerungsfenster der ausgewählten Lokomotive (siehe „Loks“).
* Schaltpult: Hier werden die ansteuerbaren Module des Gleisbildes angezeigt, die Schaltfunktion ausüben, also Weichen, Signale, etc. Im Gleisbild sind außerdem die IR-Balisen eingezeichnet.
* Fahrpläne: Hier werden die Fahrpläne für den automatisierten Zugbetrieb bearbeitet und aufgerufen.
* Sensoren: Hier sind alle Sensoren gezeigt.
  In diesem Fall hat jede Lokomotive einen Infrarotsensor.
  Die Sensoren empfangen die Mitteilungen der im Gleis verbauten IR-Balisen.

Über die Menüleiste wird mit "Ansicht" und "Zeige linke Tafel" eine weitere Bedienungstafel geöffnet.
Die Fenster der Tafeln können so verschoben werden, dass sie gut sichtbar sind.
Die Auswahl von "Steuerung" für die linke Tafel und ""Schaltpult" für die rechte Tafel ergibt:

![Steuerung BR365 Geschwindigkeit]({% link assets/images/Starterkits/Inbetriebnahme1_Steuerung_Schaltpult.jpg %})

<br/>
Der Reiter „Steuerung“ bietet die Kontrolle über die ausgewählte Lokomotive (bei Auswahlreiter "Loks") an:

![Steuerung BR365]({% link assets/images/Starterkits/Inbetriebnahme1_Steuerung_BR365.jpg %})


<br/>
Der Reiter "Schaltpult" gibt Zugriff auf Weichen und Signale (und optional weitere Antriebe falls vorhanden).
Die Schaltfunktionen werden mit den Einzelschaltflächen folgendermaßen ausgeführt:

![Schaltpult Weiche und Signal schalten]({% link assets/images/Starterkits/Inbetriebnahme1_Schalten_mit_Schaltpult.jpg %})

<br/>

Weichen und Signale können alternativ auch direkt im Gleisplan geschaltet werden:

![Gleiplan Weiche und Signal schalten]({% link assets/images/Starterkits/Inbetriebnahme1_Schalten_mit_Gleisplan.jpg %})


<br/>

Im ersten Betrieb der Anlage kann die Lok BR365 nun fahren, die Steuerung der Weichen erlaubt die Einfahrt in das
Bahnhofsgleis oder die Fahrt auf dem Überholgleis.
Auch die Signale können geschaltet werden. 
Erkennbar ist, dass die Lok aber auf die Stellung der Signale nicht reagiert, der Zug muss im 
Fenster "Steuerung" manuell angehalten werden oder die Weiterfahrt angestoßen werden.

Weitere Konfigurationsschritte sind notwendig, damit die Lok vor einem auf "Halt" stehendem Signal langsamer wird
und zum Stehen kommt.
Dafür werden die IR-Balisen eingesetzt.
