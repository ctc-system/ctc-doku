---
layout: starterkit
title:  "Aufbau der Anlage"
chapter: 3
categories: Starterkit
tags: Anlage Aufbau
date:   2023-02-03
author: Klaus Gungl
---
Dieses Kapitel beschreibt, wie
* die Gleisanlage aufgebaut wird
* die Formsignale und die IR-Balisen an die Weichenmodule angeschlossen werden.

###  Starter-Set Aufbau der Gleisanlage

Der Grundriss der Gleisanlage besteht aus einem Oval mit einer Haltestelle,
so wie bei einem kleinen Bahnhof.
Die Anlage wird aufgebaut und an die konfigurierten Weichenmodule werden die Formsignale und IR-Balisen angeschlossen.
Für das hier beschriebene CTC-Starter-Set werden zwei CTC-Lokmodule und zwei CTC-Weichenmodule benötigt.
In weiteren Kapiteln wird beschrieben, wie mit der CTC-App das Gleisbild "am PC" aufgebaut wird.
Anschließend wird die Anlage "in Betrieb" genommen, es findet Zugbetrieb statt.
Für den Zugbetrieb wird angenommen, dass eine Lokomotive im Uhrzeigersinn und eine andere Lokomotive entgegen dem Uhrzeigersinn fährt.

Schematisch ist diese Anlage in der folgenden Abbildung dargestellt.

![Gleisoval Basis Signal, Weichen, IR-Balisen]({% link assets/images/Starterkits/Basis/Gleisoval_Signale_Balisen_Weichen_Loks.jpg %})


Für das aufzubauende Gleisoval mit Ausweich- beziehungsweise Bahnhofsstrecke wird folgendes
Weichen- und Schienenmaterial verwendet:
* Bogenweiche links Artikel-Nr. 24771 ausgerüstet mit Elektroantrieb Artikel-Nr. 74491
* Bogenweiche rechts Artikel-Nr. 24671 ausgerüstet mit Elektroantrieb Artikel-Nr. 74491
* 10x gebogenes Gleis R1, 30°
* 2x gebogenes Gleis R2, 30°
* 12x gerades Gleis 188,3 mm, davon 4 umgerüstet mit CTC-IR-Balisen

Sicht der Anlage von oben, wenn sie mit den Weichen und Gleisen zusammengebaut 
und die Lokomotiven darauf gestellt wurden:

![Gleisanlage aufgebaut]({% link assets/images/Starterkits/Gleisanlage_aufgebaut.jpg %})

Die Formsignale und IR-Balisen werden als nächstes angeschlossen. 

### Formsignale anschließen

Die in dieser Beschreibung vorgesehenen Formsignale haben einen Magnetantrieb.
Das verwendete CTC-Weichenmodule hat einen Anschluss "W1", 
dieser wird für den Weichenantrieb verwendet und ist bereits angeschlossen.
Der Anschluss "W2" wird für das Formsignal verwendet.
Der dazugehörige Steckverbinder ist als drei-poliger linearer Buchsenverbinder ausgeführt.
An der mittleren Buchse liegt die Betriebsspannung an, die beiden äußeren Anschlüsse sind die Schaltfunktionen.
Der Magnetantrieb des Formsignals ist entsprechend anzuschließen.

| Bezeichnung am Modul | Funktion des Ausgangs           | Farbe des Kabels |  Farbe des Schrumpfschlauchs / Stecker | Position an der Busche | Funktion beim Formsignal | 
|----------------------|---------------------------------|------------------| --------------------|------------------------|---------------------------------|
| W2-Rot               | Schalten-1                      | blau             |  rot                |  Links                 | Magnet für Schaltzustand Halten |
| W2-VBB               | Spannungsversorung des Antriebs | gelb             |  gelb               |  Mitte                 | Spannungsversorgung             |
| W2-Grün              | Schalten-2                      | blau             |  grün               |  Rechts                | Magnet für Schaltzustand Fahrt |

<br/>



### IR-Balisen anschliessen

Jedes der CTC-Weichenmodule steuert beim Starter-Set zwei IR-Balisen (Infra-Rot-Balisen) an. 
Der Anschluss von IR-Balisen an das Weichenmodul wird gemäß folgernder Tabelle vorgenommen:

| Bezeichnung am Modul | Funktion des Ausgangs  | Farbe des Schrumpfschlauchs | Position an der Buchse | Funktion bei der IR-Balise  | 
|----------------------|------------------------|-----------------------------|------------------------|-----------------------------|
| IR1-GND              | IR-Balise 1 Minuspol   | grün                        |  Mitte-Links           | IR-Balise 1 Dioden Kathode  |
| IR1-VCC              | IR-Balise 1 Pluspol    | rot                         |  Links                 | IR-Balise 1 Dioden Anode    |
| IR2-VCC              | IR-Balise 2 Pluspol    | rot                         |  Rechts                | IR-Balise 2 Dioden Anode    |
| IR2-GND              | IR-Balise 2 Minuspol   | grün                        |  Mitte-Rechts          | IR-Balise 2 Dioden Kathode  |

<br/>

Weitere Informationen zum Aufbau und Anschluss von IR-Balisen an das Weichenmodul sind hier zu finden:

[CTC-IR-Balise]({% link _data_sheet/CTC-IR-Sender.md %}).

---
Die Gleisanlage ist jetzt aufgebaut und die Weichenmodule können konfiguriert werden.
