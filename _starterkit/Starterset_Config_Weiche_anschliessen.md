---
layout: starterkit
title:  "Konfiguration der Weichenmodule: Weiche anschließen"
chapter: 4.2
categories: Starterkit
tags: Weichenmodul
date:   2023-02-19
author: Klaus Gungl
---


Dieser Text beschreibt die Konfiguration der Weichenmodule für das Starter-Set.
Es wird angenommen:
* die Gleisanlage ist aufgebaut,
* die Weichenmodule sind in die Weichen eingebaut und der Spulenantrieb der Weichen ist verdrahtet,
* die Signale sind an die Weichenmodule verdrahtet,
* die IR-Balisen sind an die Weichenmodule verdrahtet,
* die Stromversorgung ist vollständig,
* die Weichenmodule wurden zurückgesetzt,
* die Weichenantriebe sind verdrahtet und
* die Formsignale sind verdrahtet.
 

Zur Erinnerung: Die Verdrahtung für jedes der Weichenmodule ist wie folgt ausgeführt:

![Verdrahtung Weiche, Signal, IR-Balisen]({% link assets/images/Starterkits/Verdrahtung_Weiche_Signal_IRSender.jpg %})

### Produkt Weiche (magnetisch) "Demo-W-rechts" anschließen

Um den Magnetantrieb der Weiche an das Weichenmodul anzuschließen (softwaretechnisch zu verbinden), wird die
Konfiguration aufgerufen:
1. Im Reiter "Module" in der Zeile von "Demo-W-rechts" auf das "Edit"-symbol klicken.
1. Es öffnet sich das Fenster "Schaltkasten bearbeiten" des Weichenmoduls.
1. Auf das Feld "Config ändern" klicken.
1. Es öffnet sich das Fenster "Schaltkasten-Konfiguration bearbeiten".

![Schaltkasten-Konfiguration bearbeiten]({% link assets/images/Starterkits/Konfiguration_Weiche/Schaltkasten_Konfig_bearbeiten.jpg %})

An die Anschlüsse "W1-gruen" und "W1-rot" des Moduls "Demo-W-rechts" wird als Produkt eine Weiche angeschlossen.
Bei der nun folgenden Auswahl des anzuschließenden Produkts ist zu beachten,
dass dies eine Weiche ist, welche in Fahrtrichtung des Abzweigs nach rechts 
abbiegt.

Im Fenster "Schaltkasten-Konfiguration bearbeiten" wird im Bereich "Angeschlossene Produkte" auf "+" geklickt 
um die Liste der Produktkataloge zu öffnen und einen Katalog auszuwählen.

1. Der Katalog "universell-weichen.xml" wird ausgewählt und 
1. mit Klick auf "Übernehmen" zur weiteren Arbeit geöffnet.

![Produktkataloge Auswahl]({% link assets/images/Starterkits/Katalog-auswaehlen-universell-weichen.jpg %})

Der Katalog "universell-weichen" ist ausgewählt. 
Aus diesem Katalog wird ein Name vergeben und es wird eine Weiche ausgewählt:

1. Im Fenster "Produkt auswählen" wird als Produktname "Demo-W-rechts" eintragen.
1. Als Produkt wird "Weiche (magnetisch)" ausgewählt.
1. Bei Produkt-Konfiguration wird "Weiche-Rechts" ausgewählt.
1. Mit Klick auf "Übernehmen" wird die Auswahl bestätigt.

![Produkt Anschluss]({% link assets/images/Starterkits/Konfiguration_Weiche/Konfiguration-Weichenmodul-Produkt_auswaehlen.jpg %})

Das CTC-Weichenmodul ist bereits in der Weiche einbaut, und W1 mit dem magnetischen Weichenantrieb verdrahtet.
Jetzt gilt es "die logische Verdrahtung" der Steuerung vorzunehmen.
Als Voraussetzung dazu ist es nötig zu wissen, welcher Pin / Port des Weichenmoduls die Weiche auf "abbiegen" 
beziehungsweise "gerade" schaltet.
Es wird wie folgt vorgegangen:

* Im Feld "Pins, Ports und Erweiterungen" den Ausgang "W1-gruen" auswählen.
* Auf das Feld "Test" klickten. Die Weiche schaltet. Annahme: die Weiche schaltet auf "gerade"
* Im Feld "Pins, Ports und Erweiterungen" den Ausgang "W1-rot" auswählen.
* Auf das Feld "Test" klickten. Die Weiche schaltet. Annahme: die Weiche schaltet auf "abbiegen. "

Jetzt werden die im Fenster "Anschlüsse" genannten Pin-Namen ("gerade" und "abbiegen") 
des Produkts ("Demo-Weiche-rechts-conn") mit den
im Fenster "Pins, Ports und Erweiterungen" genannten Anschlüssen verbunden:
1. Klick auf "W1-gruen: Output pin #16 (Low Side)" wählt den Pin des Weichenmoduls aus.
1. Klick auf "gerade" bei "Anschlüsse und Parameter" wählt den Pin-Name "gerade" des Produkts aus.
1. Klick auf "Verbinden" stellt die logische Verbindung her zwischen "W1-gruen" und "gerade" her.

![Konfiguration Weichenmodul Weiche gerade]({% link assets/images/Starterkits/Konfiguration_Weiche/Konfig_Weichenmodul_Weiche_rechts_anschliessen.jpg %})  

Ebenso wird "W1-rot: Output pin #17 (Low Side)" mit "abbiegen" verbunden.

1. Klick auf "W1-rot: Output pin #17 (Low Side)" wählt den Pin des Weichenmoduls aus.
1. Klick auf "abbiegen" bei "Anschlüsse und Parameter" wählt den Pin-Name "abbiegen" des Produkts aus.
1. Klick auf "Verbinden" stellt die logische Verbindung zwischen "W1-rot" und "abbiegen" her.


Das Ergebnis der Anschlussarbeit des Weichenmoduls an den magnetischen Weichenantrieb sieht in der CTC-App so aus:

![Konfiguration Weichenmodul Weiche angeschlossen]({% link assets/images/Starterkits/Konfiguration-Weichenmodul-Weiche_angeschlossen.jpg %})

Im Fenster "Schaltkasten-Konfiguration bearbeiten" wird die Aktion abgeschlossen, indem mit Klick auf "Hochladen"
die Konfiguration in dem Weichenmodul "Demo-Weiche-rechts" abgespeichert wird.

### Produkt Weiche (magnetisch) "Demo-Weiche-links" anschließen

Für die zweite Weiche wird ähnlich wie oben beschrieben vorgegangen.
Mit "Edit" von "Demo-Weiche-links" wird die Konfiguration aufgerufen.

* Anders als bei "Demo-Weiche-rechts" ist für "Demo-Weiche-links" beim "Produkt anschließen" die
Produkt-Konfiguration "Weiche-links" zu wählen.
  
Die Schaltverbindungen werden wie oben beschrieben getestet und hergestellt.
Im Fenster "Schaltkasten-Konfiguration bearbeiten" wird die Aktion abgeschlossen, indem mit Klick auf "Hochladen"
die Konfiguration in dem Weichenmodul "Demo-Weiche-links" abgespeichert wird.







