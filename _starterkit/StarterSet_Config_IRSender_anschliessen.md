---
layout: starterkit
title:  "Konfiguration Weichenmodul: IR-Balisen anschliessen"
chapter: 7.1
categories: Starterkit
tags: Weichenmodul
date:   2023-02-14
author: Klaus Gungl
---

Dieser Text beschreibt die Konfiguration der Weichenmodule für das Starter-Set.
Bei der Wortwahl ist zu beachten:
* "Verdrahten" bezeichnet den elektrischen Anschluss, also die Kabel, Leitungen, Steckverbinder.
* "Anschluss" oder "anschliessen" bezeichnet den softwaretechnischen Anschluss, 
  also die Konfiguration des Moduls, an welches die entsprechende Komponente 
  (Weiche mit Spulenantrieb, Formsignal mit Spulenantrieb, IR-Balisen)
  verdrahtet ist.

Es wird angenommen:
* die Gleisanlage ist aufgebaut,
* die Weichenmodule sind in die Weichen eingebaut und der Spulenantrieb der Weichen ist angeschlossen,  
* die Signale sind an die Weichenmodule verdrahtet,
* die IR-Balisen sind an die Weichenmodule verdrahtet,
* die Stromversorgung ist vollständig,
* die Weichenmodule wurden zurückgesetzt,
* die Weichenantriebe sind verdrahtet und
* die Formsignale sind verdrahtet.

Wie bereits ganz an Anfang gezeigt, ist dies die Gleisanlage:

![Gleisbild für Starter-Set Modellbahnanlage]({% link assets/images/Starterkits/Gleisoval_Basis_Signal_Weichen_IRSender_Loks.jpg %})


Zur Erinnerung: Die Verdrahtung für jedes der Weichenmodule ist wie folgt ausgeführt:

![Verdrahtung Weiche, Signal, IR-Balisen]({% link assets/images/Starterkits/Verdrahtung_Weiche_Signal_IRSender.jpg %})

Angewendet auf die Gleisanlage bedeutet das für die Verdrahtung:
* An das Modul der Weiche "Demo-W-Links" sind das Formsignal "Demo-S-D2-li" 
  sowie die Balisen "D21" und "D22" verdrahtet.
* An das Modul der Weiche "Demo-W-Rechts" sind das Formsignal "Demo-S-D1-re"
  sowie die Balisen "D11" und "D12" verdrahtet.

Die "Verdrahtungen" der IR-Balisen werden jetzt "angeschlossen". 

### Produkt IR-Balise anschließen

Im Fenster "Module" wird in der Zeile der ID "Demo-Weiche-links" das "Edit"-Symbol angeklickt, 
es öffnet sich das Fenster "Schaltkasten bearbeiten". 
Im Fenster "Schaltkasten bearbeiten" öffnet sich durch Klick auf die Schaltfläche "Config ändern" das Fenster
"Schaltkasten-Konfiguration bearbeiten":

![Config Balise hinzufügen]({% link assets/images/Starterkits/Konfiguration_IRSender/Config-IR_Balise_hinzu.png %})

Hier wird im Bereich "Angeschlossene Produkte" auf "+" (siehe Mauszeiger) geklickt, um die Liste der Produktkataloge zu öffnen.
1. Der Katalog "universell-sensoren.xml" wird ausgewählt und
1. mit Klick auf "Übernehmen" zur weiteren Arbeit geöffnet.

![Konfiguration Weichenmodul Produkt_anschliessen Formsignal]({% link assets/images/Starterkits/Konfiguration_IRSender/Config-IR_Katalog_Sensoren.png %})


Der Katalog "universell-sensoren" ist ausgewählt.
Aus diesem Katalog wird die "IR-Balise" ausgewählt und dafür ein Name vergeben:

1. Bei "Name" wird "D11" eingetragen
2. Als Produkt wird "IR-Balise" ausgewählt und
3. Mit Klick auf "Übernehmen" ist die IR-Balise zum Anschluss bereit.

![Konfiguration Weichenmodul Produkt_anschliessen Formsignal]({% link assets/images/Starterkits/Konfiguration_IRSender/Config-IR_Balise_anschliessen.png %})


Nach Klick auf "Übernehmen" ist man zurück beim Fenster "Schaltkasten-Konfiguration bearbeiten".

Die erste IR-Balise D11 wird angeschlossen:
1. Bei "Pins, Ports und Erweiterungen" wird "Port: IRPort-1 (SerialModulated)" ausgewählt, 
1. bei "Angeschlossene Produkte" wird D11-Conn ausgewählt,
1. bei "Anschlüsse und Parameter" wird "IRPort-1" ausgewählt und
1. mit "Verbinden" wird der Anschluss übernommen.

![Konfiguration Weichenmodul IR-Balise verbinden]({% link assets/images/Starterkits/Konfiguration-Weichenmodul-IR-Sender-verbinden.jpg %})

Ebenso wird mit dem zweiten Infrarot-Port "IRPort-2 vorgegangen: 
1. Produktkatalog "universell-sensoren.xml" mit Doppelklick öffnen,
1. Produkt IR-Balise auswählen, 
1. "D12" als Name eintragen und übernehmen.
1. Im Fenster "Schaltkasten-Konfiguration bearbeiten" bei "Pins, Ports und Erweiterungen" wird "Port: IRPort-2 (SerialModulated)" ausgewählt,
1. bei "Angeschlossene Produkte" wird D12-Conn ausgewählt,
1. bei "Anschlüsse und Parameter" wird "IRPort-2" ausgewählt und
1. mit "Verbinden" wird der Anschluss übernommen.

Das erste Weichenmodul hat den Namen "Demo-Weiche-rechts" ist fertig verbunden und daran sind angeschlossen:
* ein magnetischer Weichenantrieb,
* ein magnetischer Signalantrieb,
* ein IR-Balise D11 und
* ein IR-Balise D12.

![Konfiguration Weichenmodul Weiche]({% link assets/images/Starterkits/IO_Config_Weiche_Signal_IR_angeschlossen.png %})







