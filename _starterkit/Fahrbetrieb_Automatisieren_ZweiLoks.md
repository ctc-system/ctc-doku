---
layout: starterkit
title:  "Fahrbetrieb Automatisieren mit zwei Loks"
chapter: 10.4
date:   2023-03-24
author: Klaus Gungl
---
Beim [Fahrbetrieb automatisieren mit einer Lok]({% link _starterkit/Fahrbetrieb_Automatisieren_EineLok.md %})
wurde eine statische Konfiguration verwendet.  
Im jetzt beschriebenen Scenario ist das anders. Daraus ergeben sich Änderungen in den Konfigurationen.
Der automatisierte Zugbetrieb ist wie folgt:
1. Der Zug mit der Lok BR212-FW steht am Bahnhof in Block D2.
1. Signal Demo-S-D2-li schaltet auf "Fahren".
1. BR212-FW fährt los, beginnt eine Runde im Uhrzeigersinn zu fahren.
1. Signal Demo-S-D2-li schaltet auf "Halten".
1. BR212-FW fährt eine Runde **im Uhrzeigersinn** und fährt in den Bahnhof Block D2 ein,
1. wird langsamer und
1. kommt zum Stehen.
1. Die Weichen schalten um, so daß BR365 eine Runde fahren kann.
1. Signal Demo-S-D2-li schaltet auf "Fahren".
1. BR365 fährt los, beginnt eine Runde **entgegen den Uhrzeigersinn** zu fahren.
1. Signal Demo-S-D1-re schaltet auf "Halten".
1. BR365 fährt eine Runde gegen den Uhrzeigersinn und fährt in den Bahnhof Block D1 ein,
1. wird langsamer und
1. kommt zum Stehen.
1. Die Weichen schalten um, so daß BR212-FW eine Runde im Uhrzeigersinn fahren kann.
1. Signal Demo-S-D2-li schaltet auf "Fahren".
   
Dieser Fahrbetrieb wiederholt sich.

Für Lok BR212-FW ist das Scenario sehr ähnlich wie bei 
[Fahrbetrieb automatisieren mit einer Lok, Variante 2]({% link _starterkit/Automat_Fahren_1Lok_V2.md %}).

Die Lok BR212-FW hält am Bahnhofsgleis, Signal Demo-S-D2-li schaltet auf "Fahren",
BR212-FW fährt los, beginnt eine Runde im Uhrzeigersinn zu fahren,
Signal Demo-S-D2-li schaltet auf "Halten",
BR212-FW fährt eine Runde im Uhrzeigersinn und fährt in den Bahnhof ein,
wird langsamer und
kommt zum Stehen:

![Automatisierter Fahrbetrieb mit zwei Loks Start ]({% link assets/images/Starterkits/Automatisierter_Zugbetrieb/Automat_Zugbetrieb_EinZug_V2_1a.jpg %})


![Automatisierter Fahrbetrieb mit zwei Loks Start ]({% link assets/images/Starterkits/Automatisierter_Zugbetrieb/Automat_Zugbetrieb_EinZug_V2_3a.jpg %})

![Automatisierter Fahrbetrieb mit zwei Loks Start ]({% link assets/images/Starterkits/Automatisierter_Zugbetrieb/Automat_Zugbetrieb_EinZug_V2_4a.jpg %})


Jetzt ist BR365 dran: Die Weichen werden geschaltet, so daß BR365 eine Runde gegen den Uhrzeigersinn drehen kann:

![Automatisierter Fahrbetrieb mit zwei Loks Start ]({% link assets/images/Starterkits/Automatisierter_Zugbetrieb/Automat_Zugbetrieb_ZweiZug_2a.jpg %})


BR365 fährt los, beginnt eine Runde gegen den Uhrzeigersinn zu fahren, Signal Demo-S-D2-li schaltet auf "Halten":


![Automatisierter Fahrbetrieb mit zwei Loks Start ]({% link assets/images/Starterkits/Automatisierter_Zugbetrieb/Automat_Zugbetrieb_ZweiZug_3a.jpg %})


BR365 fährt in den Bahnhof ein, wird langsamer: 

![Automatisierter Fahrbetrieb mit zwei Loks Start ]({% link assets/images/Starterkits/Automatisierter_Zugbetrieb/Automat_Zugbetrieb_ZweiZug_4a.jpg %})

Die Lok BR365 kommt bei Balise D11 zum Stehen.

Diese notwendigen Informationen und Kommandos werden konfiguriert.

Weitere Beschreibung folgt in Kürze.



