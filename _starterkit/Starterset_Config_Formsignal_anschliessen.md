---
layout: starterkit
title:  "Konfiguration der Weichenmodule: Formsignal anschließen"
chapter: 4.3
categories: Starterkit
tags: Weichenmodul
date:   2021-07-14
author: Klaus Gungl
---

Dieser Text beschreibt die Konfiguration der Weichenmodule für das Starter-Set.
Es wird angenommen:
* die Gleisanlage ist aufgebaut,
* die Weichenantriebe sind verdrahtet,
* die Weichenmodule sind zurückgesetzt und die Namen wurden vergeben (Demo-Weiche-links, Demo-Weiche-rechts),
* die Signale sind an die Weichenmodule verdrahtet,
* die IR-Balisen sind an die Weichenmodule verdrahtet (die IR-Balisen werden aber vorerst noch nicht benötigt) und
* die Stromversorgung ist vollständig.

Zur Erinnerung: Die Verdrahtung für jedes der Weichenmodule ist wie folgt ausgeführt:

![Verdrahtung Weiche, Signal, IR-Balisen]({% link assets/images/Starterkits/Verdrahtung_Weiche_Signal_IRSender.jpg %})

### Produkt Formsignal (magnetisch) an Demo-Weiche-rechts anschließen

Beim Magnetantrieb für das Formsignal ist ganz ähnlich vorzugehen, wie beim Magnetantrieb der Weiche.

Um den Magnetantrieb des Formsignals an das Weichenmodul anzuschließen (softwaretechnisch zu verbinden), wird die
Konfiguration aufgerufen:
1. Im Reiter "Module" in der Zeile von "Demo-Weiche-rechts" auf das "Edit"-symbol klicken.
1. Es öffnet sich das Fenster "Schaltkasten bearbeiten" der Demo-Weiche-rechts.
1. Auf das Feld "Config ändern" klicken.
1. Es öffnet sich das Fenster "Schaltkasten-Konfiguration bearbeiten".

Im Fenster "Schaltkasten-Konfiguration bearbeiten" wird im Bereich "Angeschlossene Produkte" auf "+" geklickt,
um die Liste der Produktkataloge zu öffnen.
1. Der Katalog "universell-signale.xml" wird ausgewählt und 
1. mit Klick auf "Übernehmen" zur weiteren Arbeit geöffnet.

![Katalog Auswählen Universell Signale]({% link assets/images/Starterkits/Katalog-auswaehlen-universell-signale.jpg %})


Der Katalog "universell-signale" ist ausgewählt.
Aus diesem Katalog wird ein Name vergeben und es wird ein Signal ausgewählt.
Die Angabe "rechts" bzw "Links" bezieht sich auf die Fahrrichtung (Uhrzeigersinn) für die das Signal gelten soll ("rechts" heißt "im Uhrzeigersinn"):  

1. Bei "Name" wird "Demo-S-Gleis-A" eingetragen
1. Als Produkt wird "Formsignal (magnetisch)" ausgewählt
1. Als Produkt-Konfiguration wird "Formsignal rechts" gewählt und
1. mit Klick auf "Übernehmen" ist das Formsignal zum Anschluss bereit.

![Konfiguration der Weichenmodule: Produkt_anschliessen Formsignal]({% link assets/images/Starterkits/Produkt-auswaehlen-Formsignal-(magnetisch).jpg %})

Mit Klick auf "Übernehmen" ist man zurück beim Fenster "Schaltkasten-Konfiguration bearbeiten".
  
Wie beim Anschluss der Weichenansteuerung wird "W2-gruen: Output pin #27 (Low Side)" 
bzw. "W2-rot: Output pin #32 (Low Side)" ausgewählt und mit Klick auf "Test" auf Funktion geprüft.
Annahme: Bei "W2-gruen" schaltet das Formsignal auf "frei Fahrt" und bei "W2-rot" schaltet das Signal auf "halten"
Für den Anschluss des Formsignals wird ähnlich wie bei dem magnetischen Weichenantrieb verfahren:
1. Bei "Ports" wird "W2-gruen: Output pin #27 (Low Side)" ausgewählt,
1. bei "Anschlüsse" wird "gruen" ausgewählt und
1. "Verbinden" anklicken.

![Konfiguration Weichenmodul Produkt_verbinden Formsignal]({% link assets/images/Starterkits/Konfiguration-Weichenmodul-Produkt_verbinden-Formsignal.jpg %})

So wie der Pin "W2-gruen" mit Anschluss "gruen" verbunden wurde, wird auch Pin "W2-rot" mit Anschluss "rot" verbunden.
Das Formsignal ist wie auch bereits der Weichenantrieb fertig angeschlossen:

![Konfiguration Weichenmodul Weiche Formsignal verbunden]({% link assets/images/Starterkits/Konfiguration-Weichenmodul-Weiche-Signal-verbunden.jpg %})

Im Fenster "Schaltkasten-Konfiguration bearbeiten" wird die Aktion abgeschlossen, indem mit Klick auf "Hochladen"
die Konfiguration in dem Weichenmodul "Demo-Weiche-rechts" abgespeichert wird.

### Produkt Formsignal (magnetisch) an Demo-Weiche-links anschließen

Für das Formsignal, welches an Demo-Weiche-links angeschlossen ist, wird ähnlich wie oben beschrieben vorgegangen.
Mit "Edit" von "Demo-Weiche-links" wird die Konfiguration aufgerufen. 
Im weiteren Verlauf wird für dieses Signal der Name "Demo-S-Gleis-B" vergeben.
Im Fenster "Schaltkasten-Konfiguration bearbeiten" wird die Aktion abgeschlossen, indem mit Klick auf "Hochladen"
die Konfiguration in dem Weichenmodul "Demo-Weiche-links" abgespeichert wird.

---
Die Weichenmodule sind konfiguriert, die Weichen und Formsignal schalten.












