---
layout: starterkit
title:  "Fahrbetrieb Automatisieren mit einer Lok"
chapter: 10.1
date:   2023-04-04
author: Klaus Gungl
---
Das hier verwendete Scenario für den automatisierten Zugbetrieb ist wie folgt:
1. Der Zug mit der Lok BR212-FW fährt in den Bahnhof ein,  
1. wird langsamer und 
1. kommt zum Stehen.
1. Der Zug steht für eine bestimmte Zeit am Bahnhof.
1. Nach der Haltezeit fährt der Zug weiter, fährt eine Runde im Uhrzeigersinn und fährt wieder in den Bahnhof ein.

Dieser Fahrbetrieb wiederholt sich. 

Folgende Voraussetzungen sind für den automatisierten Fahrbetrieb mit einer Lok (in diesem Beispiel BR212-FW)
in diesem Scenario zu erfüllen.
Es wird angenommen:

1. Der Gleisplan ist fertig, die Balisen sind an die Module in den Weichen angeschlossen.
1. Die Haltezeit von BR212-FW am Bahnhof soll 5 Sekunden sein.
1. Die Weichen sind so gestellt, dass BR212-FW eine Runde drehen kann und wieder in den Bahnhof einfährt.
1. Die Signale Demo-S-D2-li und Demo-S-D1-re werden nicht verwendet, sie sind auf "Fahren" gestellt.
1. BR212-FW wird für den automatisierten Zugbetrieb verwendet, BR365 nimmt nicht teil. 
1. Die Distanz von Balise D21 zu Balise D22 beträgt bei dem hier vorgenommenem Aufbau 43 cm. 
   Für das Kommando, welches nachher von D21 an BR212-FW geschickt wird, 
   wird ein etwas geringerer Abstand (35 cm) festgelegt, 
   damit die Lok ihre Minimalgeschwindigkeit sicher erreicht hat, wenn sie bei Balise D22 ankommt. 

Bevor es weitergeht, ist noch eine kurze Begriffserklärung zu Lok **Halt**, Lok **Stop** und Lok **Rückfahrt** sinnvoll.
Die genannten Begriffe werden in Anlehnung an die Kommandos von der Balise wie folgt verwendet:
* Mit **Halt** wird ein unbedingtes und **zeitlich nicht begrenztes** Anhalten der Lok bezeichnet. 
  Solange eine Lok **Halt** von einer Balise empfängt, fährt sie nicht los.
* Mit **Stop** wird ein unbedingtes aber **im Kommando kommuniziertes zeitlich begrenztes** Anhalten der Lok bezeichnet. 
  Nach Ablauf der im Kommando **Stop** enthaltenen Wartezeit fährt die Lok **in die gleiche Richtung** weiter.  
* Mit **Rückfahrt** wird ein unbedingtes aber **im Kommando kommuniziertes zeitlich begrenztes** Anhalten der Lok bezeichnet. 
  Nach Ablauf der im Kommando **Stop** enthaltenen Wartezeit fährt die Lok **in entgegengesetzter Richtung** weiter.
  Dies entspricht dem Pendelzugbetrieb. 
  
Die Beschreibung der Kommandos, welche von einer Balise zur Lok geschickt werden können, 
ist in der Bedienungsanleitung der CTC-App im Kapitel 
[Config - Skript bearbeiten]({% link _app_doku/045-skript-bearbeiten.md %})
(Abschnitt "Kommandos für Loks / Signale") 
zu finden.
Diese Kommandos werden nachher bei der Konfiguration verwendet. 

Der detailliertere Ablauf des Scenarios ist im Folgenden beschrieben. 
BR212-FW fährt in das Bahnhofsgleis ein: 

![Automat Fahren Lok fährt in Bahnhof ein ]({% link assets/images/Starterkits/Automatisierter_Zugbetrieb/Automat_Zugbetrieb_EinZug_4a.jpg %})

Bei der Einfahrt in das Bahnhofsgleis fährt BR212-FW über Balise D21 und 
empfängt die folgenden Informationen:
1. Positions-ID D21: "Ich bin D21"
1. Abstand zur Vorgänger-Balise im selben Block (da es keine Vorgänger-Balise in Block gibt, ist dieser Wert 0).
1. Kommando: "Bremse ab auf Minimalgeschwindigkeit in 35 cm"

Die Lok BR212-FW wird langsamer und fährt mit Minimalgeschwindigkeit weiter ...

![Automat Fahren Lok fährt in Bahnhof ein ]({% link assets/images/Starterkits/Automatisierter_Zugbetrieb/Automat_Zugbetrieb_EinZug_5a.jpg %})

... bis zur Balise D22:

![Automat Fahrbetrieb Lok Start ]({% link assets/images/Starterkits/Automatisierter_Zugbetrieb/Automat_Zugbetrieb_EinZug_6a.jpg %})

Von Balise D22 bekommt BR212-FW folgenden Informationen:
1. Positions-ID D22: "Ich bin D22"
1. Abstand zur Vorgänger-Balise im selben Block ist 35 cm.
1. Kommando: "Stop sofort (in Distanz 0) für die Zeit 5 Sekunden". (Implizit ist damit verbunden,
   dass die Lok nach der angegebenen Wartezeit weiter fährt, sofern sie im Automatikmodus ist.)

Die Lok wartet selbstständig die von der Balise im Kommando übertragene Wartezeit:

![Automat Fahrbetrieb Lok dreht weitere Runde ]({% link assets/images/Starterkits/Automatisierter_Zugbetrieb/Automat_Zugbetrieb_EinZug_7a.jpg %})

Nach der im Kommando "Stop" übertragenen Wartezeit fährt BR212-FW weiter:

![Automat Fahrbetrieb Lok dreht weitere Runde ]({% link assets/images/Starterkits/Automatisierter_Zugbetrieb/Automat_Zugbetrieb_EinZug_8a.jpg %})


Diese notwendigen Informationen und Kommandos werden konfiguriert. 



Weiter geht es mit dem [Fahrbetrieb automatisieren mit einer Lok, Variante 2]({% link _starterkit/Automat_Fahren_1Lok_V2.md %}).

oder 

mit dem [Fahrbetrieb automatisieren mit zwei Loks]({% link _starterkit/Fahrbetrieb_Automatisieren_ZweiLoks.md %}).


     
---


