---
layout: starterkit
title:  "Fahrbetrieb Automatisieren mit Pendelzugbetrieb"
chapter: 10.3
date:   2023-03-08
author: Klaus Gungl
---
Die Lok BR212-FW wird für den Pendelzugbetrieb verwendet. 
Die Lok BR365 wird nicht benötigt, sie wird von der Anlage entfernt. 


Das hier verwendete Scenario für den automatisierten Zugbetrieb ist wie folgt:
1. Der Zug mit der Lok BR212-FW steht am Bahnhof im Block D2,
1. BR212-FW fährt entgegen dem Uhrzeigersinn los, 
![Gleisbild für Starter-Set Modellbahnanlage erweitert 1]({% link assets/images/Starterkits/Automatisierter_Zugbetrieb/Automat_Pendel_BR212-FW_2A.png %})
1. fährt entgegen dem Uhrzeigersinn eine Runde und fährt in den Bahnhof Block D1 ein,   
1. wird ab Balise D12 langsamer und 
1. kommt bei Balise D11 zum Stehen.
1. Der Zug steht für eine bestimmte Pausenzeit am Bahnhof im Block D1 bei Balise D11.
1. Nach Ablauf der Pausenzeit fährt BR212-FW im Uhrzeigersinn los, 
![Gleisbild für Starter-Set Modellbahnanlage erweitert 1]({% link assets/images/Starterkits/Automatisierter_Zugbetrieb/Automat_Pendel_BR212-FW_2B.png %})
1. fährt eine Runde im Uhrzeigersinn und fährt in den Bahnhof im Block D2 ein,
1. wird ab Balise D21 langsamer und 
1. kommt bei Balise D22 zum Stehen.
1. Der Zug steht für eine bestimmte Pausenzeit (nicht identisch mit der Pausezeit von Block D1) 
   am Bahnhof im Block D2 bei Balise D22.
1. Nach Ablauf der Pausenzeit fährt BR212-FW wieder entgegen dem Uhrzeigersinn los.

Dieser Fahrbetrieb wiederholt sich. 


Folgende Voraussetzungen sind für den automatisierten Fahrbetrieb mit einer Lok in diesem Scenario zu erfüllen und 
folgende Informationen stehen zur Verfügung:

1. Der Gleisplan ist fertig, die Balisen sind an die Module in den Weichen angeschlossen.
1. Die Haltezeit von BR212-FW am Bahnhof im Block D2 soll 5 Sekunden sein.
1. Die Weiche Demo_W_Links ist so gestellt, dass BR212-FW **entgegen dem Uhrzeigersinn** eine Runde dreht und 
   in den Bahnhof **Block D1** einfährt.
1. Die Weiche Demo_W_Rechts ist so gestellt, dass BR212-FW **im Uhrzeigersinn** eine Runde dreht und 
   in den Bahnhof **Block D2** einfährt.
1. Die Signale Demo-S-D2-li und Demo-S-D1-re werden nicht verwendet.
1. BR212-FW wird für den automatisierten Zugbetrieb verwendet, BR365 ist nicht Teil des Betriebs. 
1. Die Distanz von Balise D21 zu Balise D22 beträgt bei dem hier vorgenommenem Aufbau 43 cm. 
   Für das Kommando, welches nachher von D21 an BR212-FW geschickt wird, wird ein etwas geringerer Abstand (35 cm) festgelegt, 
   damit die Lok ihre Minimalgeschwindigkeit sicher erreicht hat, wenn sie bei Balise D22 ankommt. 
1. Die Distanz von Balise D12 zu Balise D11 beträgt bei dem hier vorgenommenem Aufbau 43 cm. 
   Für das Kommando, welches nachher von D12 an BR212-FW geschickt wird, wird ein etwas geringerer Abstand (35 cm) festgelegt, 
   damit die Lok ihre Minimalgeschwindigkeit sicher erreicht hat, wenn sie bei Balise D11 ankommt. 

Lok **Halt**, Lok **Stop** und Lok **Rückfahrt**:

Im Folgenden werden die Begriffe **Halt**, **Stop** und **Rückfahrt** in Anlehnung 
an die Kommandos von der Balise wie folgt verwendet:
* Mit **Halt** wird ein unbedingtes und **zeitlich nicht begrenztes** Anhalten der Lok bezeichnet. 
  Solange eine Lok **Halt** von einer Balise empfängt, fährt sie nicht los.
* Mit **Stop** wird ein unbedingtes aber **im Kommando kommuniziertes zeitlich begrenztes** Anhalten der Lok bezeichnet. 
  Nach Ablauf der im Kommando **Stop** enthaltenen Wartezeit fährt die Lok **in die gleiche Richtung** weiter.  
* Mit **Rückfahrt** wird ein unbedingtes aber **im Kommando kommuniziertes zeitlich begrenztes** Anhalten der Lok bezeichnet. 
  Nach Ablauf der im Kommando **Stop** enthaltenen Wartezeit fährt die Lok **in entgegengestzter Richtung** weiter.
  Dies entspricht dem Pendelzugbetrieb. 
  
Die Beschreibung der Kommandos, welche von einer Balise zur Lok geschickt werden können, 
ist in der Bedienungsanleitung der CTC-App im Kapitel 
[Config - Skript bearbeiten]({% link _app_doku/045-skript-bearbeiten.md %})
(Abschnitt "Kommandos für Loks / Signale") 
zu finden.
Diese Kommandos werden nachher bei der Konfiguration verwendet. 


Weitere Beschreibung folgt in Kürze.


