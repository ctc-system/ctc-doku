---
layout: starterkit
title:  "Starterkit mit Modulen eingebaut in Loks und Weichen"
chapter: 6
categories: Starterset
tags: Weichenmodul Lokmodul
date:   2021-05-05
author: Klaus Gungl
---
Dieses Starter-Set besteht aus Modulen mit "Starter-Set“-Konfiguration welche fertig in Lokomotiven und Weichen sind.

CTC Teile mit Konfiguration:
* 2 x Lokomotive mit eingebautem und konfiguriertem [CTC-Lokmodul-H0a]({% link _data_sheet/CTC-Lokmodul-H0a.md %}): Motorkonfiguration und ein Licht.
* 2 x Weiche mit eingebautem und konfiguriertem [CTC-Weichenmodul]({% link _data_sheet/CTC-Weichenmodul-H0.md %}). Konfiguration: Weiche, Signal, 2x IR-Balise
* 2 x Signal
* 4 x [CTC-IR-Balise]({% link _data_sheet/CTC-IR-Sender.md %}) eingebaut in vier Gleise
* 1 x [CTC-Router]({% link _data_sheet/CTC-Router.md %})

Weitere notwendige Teile
* Spannungsversorgung / Netzteil
* 2 x Flügelsignal
* Schienen

## Starter-Set mit konfigurierten CTC-Modulen eingebaut in Lokomotiven und Weichen

Im Folgenden wird der Aufbau und die Inbetriebnahme einer Modellbahn mit CTC Steuerung beschrieben.
Es wird davon ausgegangen, dass ein Starter-Set mit CTC-Modulen eingebaut in Loks und Weichen vorhanden ist.

Die Details zum Weichenmodul sind hier zu finden: [CTC-Weichenmodul]({% link _data_sheet/CTC-Weichenmodul-H0.md %}).

An jedes der Weichenmodule wird wie in der folgenden Skizze angeschlossen:
* an Gleis-A/B die Stromversorgung für das Weichenmodul,
* an W1 der Weichenantrieb,
* an W2 der Antrieb für das Signal
* an IR1 eine im Gleis eingebaute IR-Balise, hier mit DA1 bezeichnet und
* an IR2 eine im Gleis eingebaute IR-Balise, hier mit DA2 bezeichnet.

![Verdrahtung Weiche, Signal, IR-Balise]({% link assets/images/Starterkits/Verdrahtung_Weiche_Signal_IRSender.jpg %})

Die Gleisanlage wird gemäß dieser Skizze aufgebaut.

Die fertig aufgebaute Anlage mit den Lokomotiven sieht so aus:

![Verdrahtung Weiche, Signal, IR-Balise]({% link assets/images/Starterkits/Gleisanlage_aufgebaut.jpg %})
           
---

Weiter geht es mit dem [Erstellen des Gleisbildes]({% link _starterkit/Starterkit-Gleisplan-erstellen.md %})