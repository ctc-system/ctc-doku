---
layout: starterkit
title:  "Konfiguration der Weichenmodule: Erste Schritte"
chapter: 4.1
categories: Starterkit
tags: Weichenmodul Rücksetzen
date:   2021-07-14
author: Klaus Gungl
---
## Konfiguration der CTC-Weichenmodule rücksetzen

Um von jeglicher Voreinstellung und vorheriger Konfiguration unabhängig zu sein,
werden die CTC-Weichenmodule in den Ursprungszustand zurückgesetzt.
Dies gilt auch für CTC-Multi-IO-Module, obwohl diese hier im Starter-Set nicht verwendet werden.

**Hinweis: Wenn immer Sie sich nicht hundertprozentig sicher sind, wie ein Modul konfiguriert ist, sollten Sie es zurücksetzten, bevor sie etwas an dieses Modul anschließen!
Von CTC neu gelieferte Module erhalten Sie immer in zurückgesetztem Zustand, d.h. bei diesen müssen Sie nichts unternehmen.**

Das Zurücksetzen der CTC-Module hätte bereits erfolgen sollen, hier nochmal zum entsprechendem Kapitel:

[Einbau der Weichenmodule und Lokmodule]({% link _starterkit/Starterkit-Moduleinbau.md %}).


## Erste Schritte zur Konfiguration der CTC-Weichenmodule

Dieser Text beschreibt die ersten Schritte der Konfiguration der Weichenmodule für das Starter-Set "Lok und Weiche".
Es wird angenommen: 
* die Gleisanlage ist aufgebaut,
* die Weichenmodule sind in die Weichen eingebaut und die Magnetantriebe angeschlossen,
* die Signale sind an die Weichenmodule verdrahtet (werden aber erst später konfiguriert),
* die IR-Balisen sind an die Weichenmodule verdrahtet (werden aber erst später konfiguriert) und
* die Stromversorgung ist vollständig. 

Die Verdrahtung der Signale und der IR-Balisen für jedes der Weichenmodule ist wie folgt ausgeführt:

![Verdrahtung Weiche, Signal, IR-Balisen]({% link assets/images/Starterkits/Verdrahtung_Weiche_Signal_IRSender.jpg %})

Die Verdrahtung ist durchgeführt, jetzt gilt es dem jeweiligen CTC-Weichenmodul mitzuteilen,
welche "Produkte" (Weichenantrieb, Formsignal und IR-Balisen) an welchem Port des Moduls angeschlossen sind 
und wie sie angesteuert werden. 
Wenn im Folgenden von "Anschlüssen" die Rede ist, so sind damit **NICHT** elektrische Leitungen gemeint,
sondern Verbindungen auf Softwareebene.

Folgende Schritte werden im Einzelnen beschrieben und durchgeführt:

1. Aus dem Produktkatalog werden "Produkte" entnommen. Für jedes Weichenmodul werden aus dem Produktkatalog
   * eine Weiche, 
   * ein Formsignal und 
   * zwei IR-Balisen entnommen.
1. Diese Produkte haben "Anschlüsse" die eine bestimmte Ansteuerung benötigen, diese Information ist bereits im
   Produktkatalog enthalten und wird von der CTC-App verwendet, um auf korrekten Anschluss zu prüfen:
   * Der magnetische Antrieb der Weiche und der magnetische Antrieb des Formsignals brauchen einen kurzen Puls.
   * Die IR-Balisen werden mit einem pulsweiten moduliertem Signal angesteuert.
1. Die Anschlüsse der Produkte werden mit den Pins des Weichenmoduls verbunden.
   Dabei wird die Konfiguration des Weichenmoduls entsprechend den Notwendigkeiten für die 
   Ansteuerung des jeweiligen Produktes implizit gesetzt.


## Erste Schritte der Konfiguration

Im nächsten Schritt müssen den Weichenmodulen die elektrischen Verbindungen bekannt gegeben werden.
An jedem Weichenmodul sind angeschlossen:
* der Magnetantrieb der Weiche
* der Magnetantrieb für ein Flügel- bzw Form-Signal
* zwei IR-Balisen, der eine ganz nahe am Signal und der andere weiter davor.

Das rechte Weichenmodul wird konfiguriert.

Klick in der CTC-App auf "Einstellungen" - "Konfigurator" öffnet das Fenster "Liste aller Module".

![Einstellungen - Konfigurator - Liste aller Module]({% link assets/images/Starterkits/Einstellungen-Konfigurator-Liste_aller_Module-Edit.jpg %})

In der Tabelle sind unter "Typ" zwei Module des Typs "SwitchBox".
Dies sind die beiden Weichenmodule, welche zu konfiguriert sind.
Beim Rücksetzen wurden schon die neuen Namen vergeben, diese sind der Spalte "ID" zu entnehmen.

Alternativ erreicht man das Fenster "Schaltkasten bearbeiten" direkt von der CTC-App beim Reiter "Module":

![CTC-App-Schaltkasten_bearbeiten]({% link assets/images/Starterkits/CTC-App-Module-Edit.jpg %})

Mit Klick in der Spalte "Edit" in der entsprechenden Zeile der "SwitchBox"-Liste wird "Demo-W-rechts" ausgewählt.
Es öffnet sich das Fenster "Schaltkasten bearbeiten".

![Schaltkasten_bearbeiten]({% link assets/images/Starterkits/Konfiguration_Weiche/Konfiguration-Weichenmodul-Schaltkasten_bearbeiten.png %})

Mit Klick auf "Config ändern" öffnet sich das Konfigurationsfenster "Schaltkasten-Konfiguration bearbeiten".
Der Anschluss und die Funktion des magnetischen Weichenantriebs werden wie folgt geprüft:

1. Im Feld "Pins, Ports und Erweiterungen" den Ausgang "W1-gruen" auswählen.
1. Auf das Feld "Test" klicken. Die Weiche schaltet.
1. Im Feld "Pins, Ports und Erweiterungen" den Ausgang "W1-rot" auswählen.
1. Auf das Feld "Test" klicken. Die Weiche schaltet.

![Schaltkasten-Konfiguration_bearbeiten_Weiche_pruefen]({% link assets/images/Starterkits/Konfiguration_Weiche/Konfiguration_bearbeiten_Weichenantrieb_pruefen.jpg %})

Jetzt hat die Weiche mindestens einmal geschaltet und es ist bekannt, welcher Anschluss die Weiche
auf "gerade" und welcher auf "abbiegen" schaltet.
Diese Information wird für den korrekten Anschluss der Weiche gebraucht.


