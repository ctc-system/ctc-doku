---
layout: starterkit
title:  "Konfiguration Weichenmodul: IR-Balisen mit Aktion belegen"
chapter: 7.2
categories: Starterkit
tags: Weichenmodul
date:   2021-07-14
author: Klaus Gungl
---


Dieser Text beschreibt bei Konfiguration der Weichenmodule wie der IR-Balisen mit einer Aktion belegt wird.
Es wird angenommen: 
* die Gleisanlage ist aufgebaut,
* die Signale sind an die Weichenmodule verdrahtet,
* die IR-Balisen sind an die Weichenmodule verdrahtet,
* die Stromversorgung ist vollständig,
* die Weichenmodule wurden zurückgesetzt,
* der Weichenantrieb ist angeschlossen und
* die IR-Balisen sind angeschlossen.

Zur Erinnerung: Die Verdrahtung für jedes der Weichenmodule ist wie folgt ausgeführt:

![Verdrahtung Weiche, Signal, IR-Balisen]({% link assets/images/Starterkits/Verdrahtung_Weiche_Signal_IRSender.jpg %})

Bis hierher ist erledigt:
1. Die Verdrahtung ist durchgeführt und
1. die "Produkte" (Weichenantrieb, Formsignal und IR-Balisen) sind an ihren jeweiligen Ports / Pins des Modules angeschlossen. 



### IR-Balise mit Aktion belegen

#### Grundidee

Die Grundidee bei der folgenden Konfiguration ist:
Wenn sich eine Lokomotive einem Signal nähert soll sie Information bekommen, 
ob das Signal auf "Halten" oder "Fahren" steht und wie weit die Lokomotive von dem Signal entfernt ist.
Über die in den Schienen eingebauten IR-Balisen wird dies an die Lokomotive geschickt.
* Bei "Fahren" fährt die Lokomotive unverändert weiter.
* Bei "Halten" soll die Lokomotive langsamer werden und vor dem Signal anhalten.

Hier nochmal der schematische Aufbau der Anlage:

![Gleisoval Basis Signal, Weichen, IR-Balisen]({% link assets/images/Starterkits/Gleisoval_Basis_Signal_Weichen_IRSender_Loks.jpg %})

In Fahrtrichtung der Lokomotiven sind vor jedem Signal 2 IR-Balisen eingebaut.
Als Beispiel für einen Ablauf sei angenommen, die Rangierlokomotive BR365 fährt in Richtung Signal "Demo-S-D1-re" und das Signal steht auf "Halten".
Es ergibt sich folgender Ablauf:
1. Durchfahrt über "Demo-W-Links".
1. Fahrt über IR-Balise D12.
1. Die Information, die von D12 gesendet wird, hat folgende Qualität: 
"Hier ist IR-Balise D12, die Entfernung zum kommenden Signal is 80 cm, das Signal steht auf "Halten". ".
1. Die Lokomotive reduziert langsam die Geschwindigkeit, sodass sie in 80 cm zum Stehen käme.
1. Da die Entfernung zwischen D11 und D12 weniger als 80 cm ist, wird D11 mit schon deutlich verringerter Geschwindigkeit überfahren.
1. Fahrt über IR-Balise D11.
1. Die Information, die von D11 gesendet wird, hat folgende Qualität:
"Hier ist IR-Balise D11, die Entfernung zum kommenden Signal is 0 cm, das Signal steht auf "Halten" ". 
1. Die Lokomotive reduziert sofort ihre Geschwindigkeit und kommt zum Stehen. 

#### Implementierung

Im Folgenden wird das Modul in "Demo-W-rechts" für die IR-Balisen D11 und D12 entsprechend konfiguriert.

Beim Anschluss der IR-Balisen hat die CTC-App intern bereits einiges dafür angelegt.

Klick auf "Einstellungen" und "Konfigurator" öffnet den Konfigurator. 
Klick auf den Bleistift in der SwitchBox Zeile mit der ID "Demo-W-rechts" öffnet das Fenster "Schaltkasten-Konfiguration bearbeiten".
1. Mit Klick auf den Reiter "Aktionen" öffnet sich das entsprechende Fenster.
1. Mit Klick auf das DREIECK VOR "IR-Balise: D11-Conn" öffnet sich weitere Auswahl.
1. Mit Klick auf das Feld "Trigger: D11-Trigger" öffnet sich bei "Details der Aktion" weitere Auswahl.
1. Mit Klick auf das Feld "Auswählen" öffnet sich das Fenster "Auslöser wählen".

![Konfiguration Aktion IR-Balise]({% link assets/images/Starterkits/Konfiguration_Aktion_IR-Sender-Details_Auswaehlen_1.jpg %})

Weiter geht es mit dem Auslöser wählen:
1. Bei "Aktionen-Gruppe" wird "SignalTower" ausgewählt,
1. bei "Aktion" wird "Demo-S-D1-re" gewählt,
1. mit Klick auf "Übernehmen" wird das Fenster wieder geschlossen.

![Konfiguration Aktion IR-Balise]({% link assets/images/Starterkits/Konfiguration_IRSender/Config-IR_Trigger_auswaehlen.png %})

Für die Balise direkt vor dem Signal sind die Details der Aktion bereits passend konfiguriert:
Es wird in allen Fällen ('*') als Kommando der Zustand des Signals unverändert übermittelt (param[cmd]='?').

![Konfiguration Aktion IR-Balise]({% link assets/images/Starterkits/Konfiguration_IRSender/Config-IR_Trigger_D11.png %})

Die IR-Balise D12 wird fast gleich wie D11 konfiguriert.
Allerdings soll dort bei rotem Signal nicht sofort gehalten, sondern auf Minimalgeschwindigkeit abgebremst werden, und zwar über die Distanz bis zur Balise D11.

1. Mit Klick auf das DREIECK VOR "IR-Balise: D12-Conn" öffnet sich weitere Auswahl.
1. Mit Klick auf das Feld "Trigger: D12-Trigger" öffnet sich bei "Details der Aktion" weitere Auswahl.
1. Mit Klick auf das Feld "Auswählen" öffnet sich das Fenster "Auslöser wählen".
   1. Bei "Aktionen-Gruppe" wird "SignalTower" ausgewählt,
   1. bei "Aktion" wird "Demo-S-D1-re" gewählt,
   1. mit Klick auf "Übernehmen" wird das Fenster wieder geschlossen.
1. Im Bereich "Details der Aktion": Mit Klick auf das DREIECK VOR "D12-Trigger" öffnet sich weitere Auswahl.
1. Im Bereich "Details der Aktion": Mit Klick auf das DREIECK VOR "Wenn 'Demo-S-D1-re' == '*' öffnet sich weitere Auswahl
1. Mit Klick auf die Zeile "Wenn 'Demo-S-D1-re' == '*'" werden rechts die Parameter des Wenn-Kommandos angezeigt.  

![Konfiguration Aktion IR-Balise]({% link assets/images/Starterkits/Konfiguration_IRSender/Config-IR_Trigger_D12_wenn.png %})
   
Es soll zuerst der Sonderfall "Signal rot" behandelt werden.
Welcher Buchstabe diesem Zustand zugeordnet ist kann in der Bedienungsanleitung im [Kapitel 4.5 "Config - Skript bearbeiten"]({% link _app_doku/045-skript-bearbeiten.md %}) nachgelesen werden.

Alternativ kann man natürlich auch einfach die Config des Signals anschauen.
1. Mit Klick auf das DREIECK VOR "Formsignal magnettisch: Demo-S-D1-re-conn" öffnet sich weitere Auswahl.
1. Mit Klick auf "Funktion (Stop): Demo-S-D1-re" werden die Details der Aktion angezeigt.
1. Klick auf das Skript "Wechsel auf Hp0_Bar ('h')" zeigt die Details zum Zustand mit dem Buchstaben 'h' an.  
   Das Symbol rechts neben dem Skript-Fenster spricht mehr als tausend Worte.

![Signalzustand 'h']({% link assets/images/Starterkits/Konfiguration_IRSender/Config-IR_Signal_Zustand_h.png %})
   
Zurück zum Trigger kann nun der Zustand 'h' beim Wenn-Befehl eingetragen werden.
1. Klick auf "Trigger: D12-Trigger" wechselt zurück zum Trigger der Balise D12.
2. Mit Klick auf die Zeile "Wenn 'Demo-S-D1-re' == '*'" werden rechts die Parameter des Wenn-Kommandos angezeigt.
3. Klick in das Feld unter dem Entfernen-Button erlaubt das Eingeben des Buchstabens 'h'.
4. Im Feld darunter wird ein zur Bedingung passender Name, z.B. "stop" eingeben.

![Trigger D12 wenn 'h']({% link assets/images/Starterkits/Konfiguration_IRSender/Config-IR_Trigger_D12_wenn_h.png %})

Dann muss noch angegeben werden was bei 'h' passieren soll:
1. Mit Klick auf "param[cmd] = '?'" werden rechts die Parameter des Kommandos "param" angezeigt.
1. Klick auf den Button neben '?' öffnet die Auswahl des Kommandos.

![Trigger Auswahl Kommando 'm']({% link assets/images/Starterkits/Konfiguration_IRSender/Config-IR_Trigger_Kommando.png %})

In allen anderen Fällen soll wieder Zustand des Signals übernommen werden.
Dazu muss ein neuer Wenn-Befehl für beliebige Zustände hinzugefügt werden.
1. Klick auf die oberste Zeile im Skript-Bereich ("D12-Trigger").
2. Dann klick auf "Hinzufügen"

![Trigger neues wenn]({% link assets/images/Starterkits/Konfiguration_IRSender/Config-IR_Trigger_D12_wenn_sonst.png %})

1. Als Bedingung für den Wenn-Befehl das Zeichen Stern ('*') eintragen.
2. Im Feld darunter einen zur Bedingung passenden Name eintragen, z.B. "sonst".
3. Die Skript-Position darunter von 0 auf 1 ändern. 

![Trigger neues wenn]({% link assets/images/Starterkits/Konfiguration_IRSender/Config-IR_Trigger_D12_wenn_sonst-2.png %})
 
Dann noch das Kommando für den neuen Wenn-Befehl hinzufügen.
1. Klick auf "Hinzufügen" - es öffnet sich das Fenster "Neuen Befehl auswählen".
2. Den Befehl "setChar" anklicken.
3. Dann auf "Übernehmen" klicken.

![Trigger neues wenn]({% link assets/images/Starterkits/Konfiguration_IRSender/Config-IR_Trigger_D12_sonst_Befehl.png %})
 
Die Zeile "param[?] = '?" ist ausgewählt.
Nun noch Eintragen, dass "cmd" auf den Zustand des Signals übernehmen soll ('?'):
1. Aus der Liste unter"Entfernen" den Wert "cmd" auswählen.
2. Den Button links unter der Auswahlliste klicken - es öffnet sich das Fenster "Befehl auswählen".
3. Die Zeile '?' auswählen.
4. Auf "Übernehmen" klicken

![Trigger neues wenn]({% link assets/images/Starterkits/Konfiguration_IRSender/Config-IR_Trigger_D12_sonst_Befehl-2.png %})
                                                                                                                       
Das Skript für den Trigger der Balise D12 ist nun fertig:

![Trigger neues wenn]({% link assets/images/Starterkits/Konfiguration_IRSender/Config-IR_Trigger_D12_fertig.png %})

Was noch fehlt, ist die Distanz für das Abbremsen.
Da diese unabhängig vom Zustand des Signals ist, wird sie fest eingestellt, anstatt sie im Skript zu verändern.
1. Klick auf die Zeile "cmdDist" unter "Anschlüsse und Parameter des Produkts".
2. Klick auf das Stift-Symbol rechts neben der ausgewählten Zeile.

![Trigger neues wenn]({% link assets/images/Starterkits/Konfiguration_IRSender/Config-IR_Trigger_D12_cmdDist.png %})
     
Nun ist auch die Balise D12 komplett fertig konfiguriert:

![Trigger neues wenn]({% link assets/images/Starterkits/Konfiguration_IRSender/Config-IR_Trigger_D12_komplett.png %})

Durch Klick auf "Hochladen" werden die Änderungen an den Triggern der beiden Balisen D11 und D12 abgeschlossen.


### Zweites Weichenmodul konfigurieren

Das zweite Weichenmodul wird von der Vorgehensweise genauso konfiguriert wie das erste Weichenmodul.
Es hat genau die gleichen Anschlüsse: 
einen Weichenantrieb, ein Formsignal mit Magnetantrieb und zwei IR-Balisen.
Zu beachten ist bei der Produktauswahl, dass es eine Weiche ist, die nach links abbiegt.

Die vergebenen Namen sind:
* Modul: Demo-W-Links
* Signal: Demo-S-D2-li
* IR-Balisen D22 (stop) und D21 (abbremsen)

Die Konfiguration der Weichenmodule ist abgeschlossen. 






