---
layout: starterkit
title:  "Gleisplan erstellen"
chapter: 5
categories: Starterkit
tags: Gleisplan
date:   2022-09-28
author: Klaus Gungl
---

Bei CTC wird eine Hierarchie von Modellbahn und Gleisplan angenommen: 
Eine Modellbahn besteht aus einem oder mehreren Gleisplänen.

Sowohl Gleisplan (beziehungsweise Gleispläne), als auch Modellbahn werden in einem der Module abgelegt. 
Das bedeutet, dass der Gleisplan als Teil der in einem Modul abgelegten Konfiguration angesehen werden kann.
Hier bei dem Starter-Set werden Gleisplan und Modellbahn im Weichenmodul "Demo-W-rechts" abgelegt.
Zum Erstellen und Bearbeiten des Gleisplanes bei diesem Starter-Set wird die Konfiguration des Moduls geöffnet.
Dazu gibt es die beiden Möglichkeiten:

1. Im Hauptmenü wird "Einstellungen" - "Konfigurator" ausgewählt. 
   Es öffnet sich das neue Fenster "Liste aller Module".
   In der Zeile mit der ID "Demo-W-rechts" wird in der Spalte "Edit" auf den Stift geklickt, 
   es öffnet sich das Fenster "Schaltkasten bearbeiten".
1. Im Hauptmenü wird der Reiter "Module" ausgewählt.
   In der Zeile mit der ID "Demo-W-rechts" wird in der Spalte "Edit" auf den Stift geklickt,
   es öffnet sich das Fenster "Schaltkasten bearbeiten".   

Mit Klick auf die Schaltfläche "Gleisplan ändern" öffnet sich das Fenster "Gleisplan bearbeiten".
Mit Klick auf die Schaltfläche "Neuer Gleisplan" wird der Name des neuen Gleisplans eingegeben und mit "Anlegen" übernommen. 
Als Name für den neuen Gleisplan wird "Starterkit" eingegeben. 

![Gleisplan bearbeiten]({% link assets/images/Starterkits/Gleisplan-Neuer_Gleisplan.jpg %})

Der Gleisplan hat jetzt den neuen Namen "Starterkit". 
Der Gleisplan hat eine "Default-Größe" von 10 x 10 Elementen.
Für den Gleisplan des Starterkits werden 6 x 11 Elemente benötigt.
Die Werte bei "Spalten" und "Zeilen" sind entsprechend anzupassen.

![Gleisplan bearbeiten-Zeilen-Spalten]({% link assets/images/Starterkits/Gleisplan_bearbeiten_Zeilen_Spalten.jpg %})



Einzelne Gleiselemente und deren Orientierung werden ausgewählt. 

![Gleisplan bearbeiten_Weiche_Orientierung_Auswahl]({% link assets/images/Starterkits/Gleisplan_bearbeiten_Auswahl_Gleis_Orientierung.jpg %})

Hier im Beispiel wird ein Gleisbogen positioniert:
1. als Orientierung "nach Links" selektiert,
1. als Gleis wird der "Gleisbogen" selektiert, und
1. im Gleisplan per Mausklick positioniert.

![Gleisplan bearbeiten_Weiche_Orientierung_Ablegen]({% link assets/images/Starterkits/Gleisplan_bearbeiten_Auswahl_Gleis_Orientierung_Ablegen.jpg %})

Auf diese Art und Weise wird der Gleisplan mit weiteren Gleiselementen, den Weichen und den Formsignalen erstellt.
Die IR-Balisen werden im Gleisplan nicht eingefügt, dies wird später nachgeholt.
Ein Mausklick auf "Hochladen" speichert den erstellten Gleisplan im Modul "Demo-Weiche-rechts".

![Gleisplan bearbeiten_Weiche_Orientierung_Ablegen]({% link assets/images/Starterkits/Gleisplan_bearbeiten_Gleise_fertig.jpg %})

### Weichen und Formsignale im Gleisplan zuordnen

Der Gleisplan ist erstellt, als Nächstes werden die Weichen und Formsignale gemäß der folgenden Tabelle zugeordnet:

| ID                   | Element im Gleisplan                            |
|----------------------|-------------------------------------------------|
| Demo-W-links         | linke Weiche im Gleisbild (Demo-W-links)   |
| Demo-W-rechts        | rechte Weiche im Gleisbild (Demo-W-rechts) |
| Demo-S-Gleis-B       | Signal links im Gleisbild  (Demo_S_Gleis_B)     |
| Demo-S-Gleis-A       | Signal rechts im Gleisbild  (Demo_S_Gleis_A)    |


<br/>
Die "Demo-W-links" wird wie folgt zugeordnet:
1. Im Feld "Action-Gruppe" wird "SignalTower" ausgewählt.
1. In der Spalte "ID" wird "Demo-W-links" ausgewählt.
1. Der Bleistift wird ausgewählt.
1. Die linke Weiche im Gleisbild wird ausgewählt.
1. Bei "Details der Aktion" ist zu sehen, dass bei "Name" der Weichenname "Demo-W-links" eingetragen wurde.

![Gleisplan Weiche links zuordnen]({% link assets/images/Starterkits/Gleisplan_Weiche_links_zuordnen.jpg %})

Mit der rechten Weiche wird analog vorgegangen:
1. Im Feld "Action-Gruppe" wird "SignalTower" ausgewählt.
1. In der Spalte "ID" wird "Demo-W-rechts" ausgewählt.
1. Der Bleistift wird ausgewählt.
1. Die rechte Weiche im Gleisbild wird ausgewählt.
1. Bei "Details der Aktion" ist zu sehen, dass bei "Name" die Bezeichnung der Weiche "Demo-W-rechts" eingetragen wurde.

<br/>

Das rechte Signal "Demo-S-Gleis-A" im Gleisabschnitt "Block-A" wird ganz ähnlich wie bei den Weichen wie folgt zugeordnet::
1. Im Feld "Action-Gruppe" wird "SignalTower" ausgewählt.
1. In der Spalte "ID" wird "Demo-S-Gleis-A" ausgewählt.
1. Der Bleistift wird ausgewählt.
1. Das rechte Signal im Gleisbild wird ausgewählt.
1. Im Gleisbild ist zu sehen, wie sich das Symbol geändert hat.
1. Bei "Details der Aktion" ist zu sehen, dass bei "Name" der Formsignalname "Demo-S-Gleis-A" eingetragen wurde.

![Gleisplan Signal rechts zuordnen]({% link assets/images/Starterkits/Gleisplan_Demo_S_Gleis_A_zuordnen.jpg %})

Mit dem linken Signal "Demo-S-Gleis-B" im Gleisabschnitt "Block-B" wird analog vorgegangen:
1. Im Feld "Action-Gruppe" wird "SignalTower" ausgewählt.
1. In der Spalte "ID" wird "Demo-S-Gleis-B" ausgewählt.
1. Der Bleistift wird ausgewählt.
1. Das linke Signal im Gleisbild wird ausgewählt. Im Gleisbild ist zu sehen, wie sich das Symbol geändert hat.
1. Bei "Details der Aktion" ist zu sehen, dass bei "Name" der Formsignalname "Demo-S-Gleis-B" eingetragen wurde.
1. Der Gleisplan ist jetzt für die ersten Fahrversuche fertig konfiguriert und wird mit 
Klick auf "Hochladen" im Weichenmodul "Demo-Weiche-rechts" gespeichert. 

![Gleisplan Signal links zuordnen]({% link assets/images/Starterkits/Gleisplan_Demo_S_Gleis_B_zuordnen.jpg %})


Als letzten Schritt gilt es, den Gleisplan in die Modellbahn zu integrieren.
Dazu wird wieder die Konfiguration von "Demo-Weiche-rechts" geöffnet.
Mit Klick auf die Schaltfläche "Modellbahn ändern" öffnet sich das Fenster "Modellbahn bearbeiten."
Mit Klick auf "Neuer Gleisabschnitt" wird ein neuer Name vergeben.
Bei "Verfügbare Panels" wird mit Klick auf die Schaltfläche "Pfeil" das Panel mit dem Namen Starterkit übernommen und positioniert.
Mit Klick auf die Schaltfläche "Hochladen" wird die neue Modellbahn im Gleismodul gespeichert.

