---
layout: starterkit
title: "Weichen- und Lokmodule zurücksetzen und einbauen, IR-Balisen einbauen"
chapter: 2
categories: Starterkit
tags: Weichenmodul Lokmodul IR-Balise Umbau
date:   2023-02-02
author: Klaus Gungl
---
Dieses Kapitel behandelt die folgenden Themen:
* Weichenmodule zurücksetzen,
* Weichenmodule in die Märklin C-Gleis Weichen einbauen,
* Lokmodule einbauen, und
* IR Balisen in das Märklin C-Gleis einbauen.

### Grundsätzliches

**Wenn immer Sie sich nicht hundertprozentig sicher sind, 
wie ein Modul konfiguriert ist, sollten Sie es zurücksetzten, bevor sie etwas an dieses Modul anschließen!**

Immer - und hier sei darauf hingewiesen: IMMER - gilt: bevor ein CTC-Weichenmodul oder ein CTC-IO-Modul 
irgendwo eingebaut und verdrahtet wird, muss es zurückgesetzt sein. 
"Zurückgesetzt" heißt in diesem Fall, dass es eine "leere" Konfiguration haben muss.

Damit werden unbeabsichtigte, unerwünschte und möglicherweise schädliche Steuer- und Schaltzustände vermieden.
Diese könnten beispielsweise von einer früheren anderen Konfiguration und Verdrahtung herstammen, 
die damals bei der entsprechenden Anlage Sinn ergeben hat, jetzt aber nicht mehr erforderlich 
oder gewünscht und vielleicht sogar schädlich ist. 
Eine unpassende Konfiguration kann zur Zerstörung von CTC-Modulen oder anderen Schaltelementen führen. 
                                                                                   
**Hinweis: Von CTC neu gelieferte Module erhalten Sie immer in zurückgesetztem Zustand, d.h. bei diesen müssen Sie nichts unternehmen.** 

### Konfiguration der Weichenmodule zurücksetzen

Aus Gründen der Einfachheit wird empfohlen, das Zurücksetzen für jedes CTC-Weichenmodul einzeln vorzunehmen.
CTC-Weichenmodule und CTC-IO-Module werden vor dem Einbau und der Verdrahtung der Gleisanlage wie folgt zurückgesetzt:

1. Das Modul, welches zurückgesetzt werden soll, wird mit der Betriebsspannung versorgt.
1. Die CTC-App wird gestartet.
1. Im Startfenster erscheint unter dem Reiter "Modul-Liste" die Liste aller Module.
   Da nur ein CTC-Weichenmodul mit der Betriebsspannung versorgt wird, enthält die Liste nur einen Eintrag.
1. In der Zeile des Moduls, welches zurückgesetzt werden soll, wird auf das Stiftsymbol in der Spalte Edit klickt.

![Module: ein Modul zurücksetzen]({% link assets/images/Starterkits/Liste_aller_Module_erstes_Modul_zuruecksetzen.jpg %})

Es öffnet sich das Fenster "Schaltkasten".
Das Feld "Config ersetzen" anklicken.

![Config ersetzen Button]({% link assets/images/Starterkits/Konfiguration_Weiche/Config_ersetzen.png %})

Es öffnet sich das Fenster "Config auswählen":

![Config auswählen]({% link assets/images/Starterkits/Konfiguration_Weiche/Config_ersetzen_auswaehlen.png %})

1. Dort wird die Konfiguration "standard" ausgewählt
2. Dann wird auf "Übernehmen" geklickt.

Es folgt ein Hinweis mit Informationen zur ausgewählten Konfiguration:

![Config ersetzen neuer Name]({% link assets/images/Starterkits/Konfiguration_Weiche/Config_ersetzen_name.png %})

1. Dem zu löschenden Modul wird ein neuer Name gegeben. 
   Im Beispiel wird dies zunächst für das CTC-Weichenmodul gemacht, welches in die linke Weiche eingebaut wird.
   Es wird der Name "Demo-W-links" vergeben.
2. Den Button "Hochladen" anklicken.

Die alte Konfiguration wird gelöscht und das erfolgreiche Hochladen auf das Modul mit dem bisherigen Modulnamen bestätigt:

![Config ersetzen erfolgreich]({% link assets/images/Starterkits/Konfiguration_Weiche/Config_ersetzen_ok.png %})
                                                                                          
Nun wird in Modulliste bereits der neu vergebene Name angezeigt: 

![Modul-Liste nach Config ersetzen]({% link assets/images/Starterkits/Konfiguration_Weiche/Config_ersetzen_fertig.png %})

Das zweite Weichenmodul wird ebenso zurückgesetzt:
1. Die Stromversorgung wird ausgeschaltet.
1. Die zweite Weiche angeschlossen. 
1. Danach wird die Stromversorgung wieder eingeschaltet.
1. Die CTC-App wird neu gestartet.
1. Das zweite CTC-Weichenmodul wird ebenso wie das Erste zurückgesetzt. 
   Da es das Modul für die rechte Weiche ist, wird als Name "Demo-W-rechts" vergeben.   
1. Beide CTC-Weichenmodule sind jetzt auf einen bekannten und definierten Ausgangszustand zurückgesetzt 
   und haben einen neuen eindeutigen Namen.
  
Beide Weichenmodule sind zurückgesetzt und an die Spannungsversorgung angeschlossen. 
Die CTC-App wird neu gestartet und zeigt die neu vergebenen Namen:

![Liste der Module beide Modul zurückgesetzt neuer Name]({% link assets/images/Starterkits/Konfiguration_Weiche/Config_ersetzen_beide_fertig.png %})

Zu beachten ist, dass das Zurücksetzten der Konfiguration, den in einen Modul gespeicherten Gleisplan nicht löscht. 
Falls ein Gleisplan in einem Modul gespeichert ist, bliebt dieser also beim Zurücksetzen erhalten.  

###  Weichenmodule einbauen

Die CTC-Weichenmodule werden bei diesem Starter-Set für folgende Funktionen verwendet:
* die Ansteuerung für die Elektromagnete der Weichen,
* die Ansteuerung eines Flügelsignals mit Magnetspulenantrieb (zu beachten: NICHT Lichtsignal, sondern mechanisches Signal) und
* zwei IR-Balisen.

Jede Weiche hat ihr eigenes CTC-Weichenmodul.

Die CTC-Weichenmodule sind in die Weichen einzubauen.
Zu beachten sind die Modulnamen, die beim Zurücksetzen der Weichenmodule vergeben werden: 
* Das Modul "Demo-W-links" wird in die Weiche eingebaut, welche im Gleisplan links verwendet wird und eine
"Links-Abbiege-Weiche" ist.
* Das Modul "Demo-W-rechts" wird in die Weiche eingebaut, welche im Gleisplan rechts verwendet wird und eine
  "Rechts-Abbiege-Weiche" ist.

Weitere Informationen zum Schalten mit CTC-Modulen sind hier zu finden:

[Übersicht zum Schalten mit CTC]({% link de/docu/CTC-Schaltmodule.md %}).

Die Anleitung zum Einbau beziehungsweise den Umbau von Weichen für das C-Gleis ist hier:

[Umbau Märklin C-Gleis Weiche]({% link _posts/de/szenarien/2021-05-05-Umbau-C-Weiche.md %})


###  Lokmodule einbauen

Die CTC-Module für die Lokomotiven erfüllen folgende Funktionen:
* die Motorsteuerung der Lokomotive,
* Licht in Fahrtrichtung der Lokomotive und Rücklicht,
* Infrarotempfang und
* Ansteuerung des Mechanismus zum Entkuppeln (falls in der Lokomotive vorhanden).

Jede Lokomotive hat ihr eigenes CTC-Lokmodul.

Die CTC-Module für die Lokomotiven sind in die Lokomotiven einzubauen. 
Links dazu sind hier:

[Übersicht zu CTC-Lokmodulen]({% link de/docu/CTC-Lokmodule.md %}).

###  Einbau der IR-Balisen

Die IR-Balisen sind in die Gleise einzubauen.
Die Anleitung dazu ist hier:

[CTC-IR-Balise]({% link _data_sheet/CTC-IR-Sender.md %}).


---
Die einzelnen Module sind eingebaut, die Gleisanlage kann aufgebaut werden.

