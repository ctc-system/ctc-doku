---
layout: starterkit
title:  "Fahrbetrieb Automatisieren mit einer Lok, zweite Version"
chapter: 10.2
date:   2023-04-04
author: Klaus Gungl
---
Das jetzt beschriebene Scenario für den automatisierten Zugbetrieb erscheint auf den ersten Blick 
dem Scenario
[Fahrbetrieb Automatisieren mit einer Lok]({% link _starterkit/Fahrbetrieb_Automatisieren_EineLok.md %})
sehr ähnlich, es kommt aber etwas mehr Dynamik ins Spiel: 
* Das Signal Demo-S-D2-li wird eingesetzt,
* die Kommandos, welche von den Balisen D21 und D22 gesendet werden,
  sind nicht immer gleich, sondern abhängig vom Signalstand Demo-S-D2-li ("Halt" oder "Fahren") und 
* es werden Zeitgeber ("Timer") verwendet.

Das Scenario:

1. BR212-FW fährt eine Runde im Uhrzeigersinn und fährt in den Bahnhof ein,   
1. wird langsamer und 
1. kommt zum Stehen.
1. Der Zug steht für eine bestimmte Zeit am Bahnhof.
1. Nach der Haltezeit schaltet Signal Demo-S-D2-li auf "Fahren".
1. BR212-FW fährt los. 
1. Signal Demo-S-D2-li schaltet nach einer gewissen Zeit zurück auf "Halt".

Dieser Fahrbetrieb wiederholt sich. 

Folgende Voraussetzungen sind für den automatisierten Fahrbetrieb zu erfüllen und 
folgende Informationen stehen zur Verfügung:

1. Der Gleisplan ist fertig, die Balisen sind an die Module in den Weichen angeschlossen.
1. Die Haltezeit von BR212-FW am Bahnhof soll 5 Sekunden sein.
1. Die Weichen sind so gestellt, dass BR212-FW eine Runde drehen kann und wieder in den Bahnhof einfährt.
1. Das Signal Demo-S-D2-li wird verwendet.
1. Das Signal Demo-S-D1-re wird nicht verwendet.
1. Die Distanz von Balise D21 zu Balise D22 beträgt bei dem hier vorgenommenem Aufbau 43 cm. 
   Für das Kommando, welches nachher von D21 an BR212-FW geschickt wird, wird ein etwas geringerer Abstand (35 cm) festgelegt, 
   damit die Lok ihre Minimalgeschwindigkeit sicher erreicht hat, wenn sie bei Balise D22 ankommt. 

Die Beschreibung der Kommandos, welche von einer Balise zur Lok geschickt werden können,
ist in der Bedienungsanleitung der CTC-App im Kapitel
[Config - Skript bearbeiten]({% link _app_doku/045-skript-bearbeiten.md %})
(Abschnitt "Kommandos für Loks / Signale")
zu finden.
Diese Kommandos werden nachher bei der Konfiguration verwendet.

Der detailliertere Ablauf des Scenarios ist im Folgenden beschrieben.
BR212-FW fährt in das Bahnhofsgleis ein, das Signal Demo_S_D2_li steht auf "Halt":


![Automat Fahren Lok fährt in Bahnhof ein ]({% link assets/images/Starterkits/Automatisierter_Zugbetrieb/Automat_Zugbetrieb_EinZug_V2_4a.jpg %})

Bei der Einfahrt in das Bahnhofsgleis fährt BR212-FW über Balise D21 und
empfängt die folgenden Informationen:
1. Positions-ID D21: "Ich bin D21"
1. Abstand zur Vorgänger-Balise im selben Block (da es keine Vorgänger-Balise in Block gibt, ist dieser Wert 0).
1. Kommando: "Bremse ab auf Minimalgeschwindigkeit in 35 cm"

Die Lok BR212-FW wird langsamer und fährt mit Minimalgeschwindigkeit weiter ...

![Automat Fahren Lok fährt in Bahnhof ein ]({% link assets/images/Starterkits/Automatisierter_Zugbetrieb/Automat_Zugbetrieb_EinZug_V2_5a.jpg %})

... bis zur Balise D22:

![Automat Fahrbetrieb Lok Start ]({% link assets/images/Starterkits/Automatisierter_Zugbetrieb/Automat_Zugbetrieb_EinZug_V2_6a.jpg %})

Von Balise D22 bekommt BR212-FW folgenden Informationen:
1. Positions-ID D22: "Ich bin D22"
1. Abstand zur Vorgänger-Balise im selben Block ist 35 cm.
1. Kommando: "Halt in Distanz 0".

Die Lok meldet an die App, dass sie über die Balise D22 gefahren ist.
Diese Meldung wird als "Trigger" verwendet, um den ersten Timer zu starten.
Der erste Timer bestimmt die Aufenthaltszeit der Lok am Bahnhof.

![Automat Fahren Lok Start ]({% link assets/images/Starterkits/Automatisierter_Zugbetrieb/Automat_Zugbetrieb_EinZug_V2_7a.jpg %})

Wenn der erste Timer abgelaufen ist, wird das Signal auf "Fahrt" gestellt.
Dies führt dazu, dass die von Balise D22 gesendete Mitteilung nicht mehr "Halt in Distanz Null" ist,
sondern "Fahren" als Kommando geschickt wird.
Das Signal Demo-S-D2-li schaltet auf "Fahren", BR212-FW kann losfahren und den Bahnhof verlassen:

![Automat Fahren Lok fährt los ]({% link assets/images/Starterkits/Automatisierter_Zugbetrieb/Automat_Zugbetrieb_EinZug_V2_8a.jpg %})

Das Signal Demo-S-D2-li schaltet nach kurzer Zeit (hierfür wird ein weiterer Timer konfiguriert)
zurück auf "Halt", BR212-FW fährt eine Runde:


![Automat Fahren Lok dreht Runde ]({% link assets/images/Starterkits/Automatisierter_Zugbetrieb/Automat_Zugbetrieb_EinZug_V2_9a.jpg %})

Wieder BR212-FW fährt in das Bahnhofsgleis ein: 

![Automat Fahren Lok fährt in Bahnhof ein ]({% link assets/images/Starterkits/Automatisierter_Zugbetrieb/Automat_Zugbetrieb_EinZug_V2_4a.jpg %})


Die für diese Scenario notwendigen Informationen und Kommandos werden jetzt konfiguriert. 

Die entsprechende Anleitung folgt in Kürze.


