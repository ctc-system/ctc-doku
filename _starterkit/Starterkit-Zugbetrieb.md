---
layout: starterkit
title:  "Zugbetrieb mit BR365 und E103"
chapter: 8
categories: Starterkit
tags: Zugbetrieb
date:   2021-06-09
author: Klaus Gungl
---
Zunächst wird die BR365 in Betrieb genommen.
Im Reiter „Loks“ wird die BR365 angeklickt, anschließend wird der Reiter „Steuerung“ ausgewählt.
Die gesetzten Voreinstellungen sind in hier zu sehen:

![App Start Steuerung BR365]({% link assets/images/Starterkits/CTC-Zugbetrieb_Start_Steuerung_BR365.jpg %})

Fahren mit der BR365:
* Die Geschwindigkeit wird über die +/- Knöpfe oder mit dem Schieberegler im Reiter „Steuerung“ vorgewählt.
* Die beiden Weichen „Demo-W-Links“ und „Demo-W-Rechts“ werden für die BR365 gestellt: 
  Ein Klick auf die Weiche im Gleisbild unter der Steuerung der Lok ändert die Weichenstellung.
* Das Signal „Demo-S-D1-re“ vor der rechten Weiche wird auf „grün“ gestellt.
* Nach Klick auf den Fahrtrichtungsschalter nach rechts fährt die Lokomotive BR365 gegen den Uhrzeigersinn los, 
  fährt am Signal „Demo-S-D1-re“ vorbei, fährt über die Weiche „Demo-W-Rechts“ und in die Schleife.
* Durch Klick auf das Signal „Demo-S-D1-re“ schaltet das Signal wieder auf Rot um. 
  Jetzt sendet die IR-Balisen D12 "Bremsen auf Minimalgeschwindigkeit in 80 cm" und D11 die Mitteilung „Halt, Signal steht auf Rot“.
* BR365 empfängt diese Mitteilung über den in der Lokomotive eingebauten IR-Empfänger 
  (gelistet unter dem Reiter „Sensoren“) beim Überfahren der IR-Balise.
* BR365 hält vor dem Signal „Demo-S-D1-re“ an.

Wie in der folgenden Abbildung zu sehen ist, kann mit „Ansicht“ - „Zeige linke Tafel“ auch die zweite Lokomotive eingeblendet werden.

![CTC-App Steuerung Steuerung]({% link assets/images/Starterkits/Zugbetrieb_Start_Steuerung_BR365_E103.png %})

Jetzt wird ein Runde mit der E103 gefahren:
* Mit Klick auf „Ansicht“ - „Zeige linke Tafel“ wird eine weitere Tafel/Fenster geöffnet.
  Die Fenstergröße wird neu justiert und der Reiter angewählt,
* Klick auf einen der beiden großen Pfeile neben dem Bild der Lok BR365 wählt die nächste Lok aus, in diesem Fall die E103.
* Am Schieberegler im Reiter „Steuerung“ wird die gewünschte Geschwindigkeit vorgewählt.
* Die beiden Weichen „Demo-W-Links“ und „Demo-W-Rechts“ werden für die E103 gestellt: 
  Ein Klick auf die Weiche im Gleisbild unter der Steuerung der Lok ändert die Weichenstellung.
* Das Signal „Demo-S-D2-li“ vor der linken Weiche wird auf „grün“ gestellt.
* Nach Klick auf den Fahrtrichtungsschalter nach links fährt die Lokomotive E103 im Uhrzeigersinn los, 
  fährt am Signal „Demo-S-D2-li“ vorbei, fährt über die Weiche „Demo-W-Links“ und in die große Schleife.
* Durch Klick auf das Signal „Demo-S-D2-li“ schaltet das Signal wieder auf Rot um.
  Jetzt sendet die IR-Balisen D21 "Bremsen auf Minimalgeschwindigkeit in 80 cm" und D22 „Halt, Signal steht auf Rot“.
* E103 dreht ihre Runde und kommt zurück, überfährt zuerst D21 und kommt bei D22 an. 
  Empfängt die Mitteilung „Signal steht auf Rot“ über den in der Lokomotive eingebauten 
  IR-Empfänger (gelistet unter dem Reiter „Sensoren“) 
  beim Überfahren der IR-Balise.
* E103 hält vor dem Signal „Demo-S-D2-li“ an. 

             
---

Ziel ist es ausserdem, dass die Lokomotiven zielgerichtet vor einem auf "Halt" stehendem Signal anhalten.
Dazu müssen die Sensoren der Lokomotiven kalibriert werden.
