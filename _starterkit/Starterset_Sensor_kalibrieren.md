---
layout: starterkit
title:  "Sensor Kalibrieren"
chapter: 9
categories: Starterkit
tags: Sensor_Kalibrieren
date:   2021-07-19
author: Klaus Gungl
---
## Grundidee

Wünschenswert ist, dass eine Lokomotive zielgenau vor einem auf „Halt“ stehendem Signal zum Stehen kommt. 
Bei der Gleisanlage des Starter-Sets sind vor jedem Signal zwei IR-Balisen in den Gleisen eingebaut. 
Fährt eine Lokomotive auf das Signal zu und überfährt eine der IR-Balisen, so empfängt sie folgende
Information:

* die Kennung der IR-Balisen,
* ob Signal auf „Halt“ oder „Freie Fahrt“ und
* die Entfernung IR-Balise zum Signal.

Letztere Information kann das Lokmodul verwenden, um genau vor einem auf „Halt“ stehendem Signal anzuhalten. 
Voraussetzung dafür ist, dass der Motor präzise genug ansteuert werden kann. 
Um die dafür notwendigen Ansteuerparameter zu erhalten, ist die Kalibrierung des Sensors für die Motoransteuerung notwendig. 
Die CTC-App unterstützt diesen Ablauf, der mit "Sensor kalibrieren" bezeichnet wird.

Im Folgenden werden die Einzelschritte beschrieben. 
Es ist unbedingt erforderlich sich nach der beschriebenen Vorgehensweise zu richten.

## Voraussetzungen

Um den Sensor einer Lokomotive zu kalibrieren, wird eine Strecke zum Messen benötigt. Dafür wird eine Messstrecke festgelegt:

* Ein festgelegter Start: "ab hier geht es mit dem Messen los",
* ein festgelegtes Ende: "jetzt ist das Ende der Messstrecke erreicht" und
* die Distanz zwischen Start und Ende.

Die dafür notwendigen Definitionen werden in der Konfiguration des CTC-Moduls gespeichert, 
welches die IR-Balise ansteuert und in welcher die Daten der Modellbahn gespeichert sind. 
Als Start und Ende der Messstrecke wird jeweils eine IR-Balise verwendet . 
Die Distanz der IR-Balise geht aus der Modellbahn hervor. 
Fährt die Lokomotive diese Messstrecke mehrfach in verschiedenen Geschwindigkeiten ab, 
können daraus die Ansteuerungsparameter für den Motor der Lokomotive errechnet werden.

![Fahrstrecke Motor kalibrieren]({% link assets/images/Starterkits/Gleisoval_Messung_Motor_kalibrieren.jpg %})


Zu beachten ist:

* Konvention: Die Bezeichnung der IR-Balisen haben einen laufenden Index im Uhrzeigersinn.
* Konvention: Zum Einmessen wird die Gleisanlage im Uhrzeigersinn befahren.

Eine notwendige Voraussetzung für das Kalibrieren des Sensors  der Motorsteuerung einer Lokomotive ist, 
dass eine möglichst aktuelle Firmware im Lokmodul geladen ist. Dazu:

* Konfiguration des entsprechenden Lokmoduls öffnen,
* auf "Firmware laden" klicken, es öffnet sich das Auswahlfenster,
* Firmware auswählen und mit Klick auf „Open“ bestätigen,
* die ausgewählte Firmware wird zu dem Lokmodul übertragen.

## Messstrecke definieren

Aus dem Bild der Gleisanlage geht hervor:
* die Messung fängt bei D21 an,
* die Messung hört bei D22 auf und
* die Länge der Messstrecke ist die zurückgelegte Strecke von D21 nach D22.

Wo die Messung anfängt und aufhört, wird in der Modellbahn definiert. Wie lang die Messstrecke ist, 
wird in der zugehörigen Konfiguration der zweiten IR-Balisen gespeichert. 

### Start und Ende der Messstrecke

Der Start und das Ende der Messstrecke wird in der Modellbahn definiert. 
Diese ist im Modul "Demo-Weiche-rechts" gespeichert: 
1. In der CTC-App auf den Reiter "Module" klicken. 
1. Klick auf "Edit" des Moduls "Demo-Weiche-rechts" öffnet das Fenster "Schaltkasten bearbeiten"

![CTC-App Sensor kalibrieren 1]({% link assets/images/Starterkits/Sensor_Kalibrieren_1.jpg %})

* Im Fenster "Schaltkasten bearbeiten" - Klick auf "Modellbahn ändern".
    
![CTC-App Sensor kalibrieren 1]({% link assets/images/Starterkits/Sensor_Kalibrieren_3.jpg %})

* Es öffnet sich das Fenster "Modellbahn bearbeiten" - Bei "Messstrecken" auf "Neu" klicken.

![CTC-App Sensor kalibrieren 1]({% link assets/images/Starterkits/Sensor_Kalibrieren_4.jpg %})

* Den Namen der Messstrecke eingeben, in diesem Fall "Bahnhof", mit Klick auf "Anlegen" übernehmen.  

![CTC-App Sensor kalibrieren 1]({% link assets/images/Starterkits/Sensor_Kalibrieren_5.jpg %})


* Zurück im Fenster "Modellbahn bearbeiten" wird "Start setzen" angeklickt. 

![CTC-App Sensor kalibrieren 1]({% link assets/images/Starterkits/Sensor_Kalibrieren_Modellbahn_Start_setzen.jpg %})


* Es öffnet sich das Fenster "Start-Message".
 "ID-Sender" auswählen, "D21" bei "Aktion" auswählen, "Übernehmen" anklicken.


![CTC-App Sensor kalibrieren 1]({% link assets/images/Starterkits/Sensor_Kalibrieren_Start_setzen.jpg %})


* Zurück im Fenster "Modellbahn bearbeiten" wird "Ende setzen" angeklickt.

![CTC-App Sensor kalibrieren 1]({% link assets/images/Starterkits/Sensor_Kalibrieren_Modellbahn_Ende_setzen.jpg %})

* Es öffnet sich das Fenster "End-Message". 
 In diesem Fenster "ID-Sender" auswählen, "D22" bei "Aktion" auswählen, "Übernehmen" anklicken.

![CTC-App Sensor kalibrieren 1]({% link assets/images/Starterkits/Sensor_Kalibrieren_Ende_setzen.jpg %})


In der Modellbahn sind jetzt Start und Ende der Messstrecke definiert. 
In der aktuellen Version der App soll und kann nur eine Messstrecke definiert werden, 
mehrere Messstrecken sind nicht möglich.
Zurück im Fenster "Modellbahn bearbeiten" - "Hochladen" anklicken, dann "Schließen".

![CTC-App Sensor kalibrieren 1]({% link assets/images/Starterkits/Sensor_Kalibrieren_Modellbahn_hochladen.jpg %})


### Länge der Messstrecke 
Es gilt folgende Information zu übermitteln: "Wenn die Lokomotive über D21 und anschließend über D22 fährt, 
dann hat sie die Distanz von D21 nach D22 zurückgelegt und zwar XXX Centimeter. 
Anders ausgedrückt: "D22 ist 'xxx' Centimeter von D21 entfernt."
Die Länge der Messstrecke wird in der Konfiguration des entsprechenden IR-Balisen definiert.
Hier im Starter-Set sind die IR-Balisen D21 (Start) und D22 (Ende) an "Demo-Weiche-links" angeschlossen.
Diese Längeninformation gilt es in dem Modul "Demo-Weiche-links" zu speichern:
1. In der CTC-App auf den Reiter "Module" klicken.
1. Auswahl "Edit" des Moduls "Demo-Weiche-links" öffnet das Fenster "Schaltkasten-Konfiguration bearbeiten" der Demo-Weiche-links.


![CTC-App Sensor kalibrieren 1]({% link assets/images/Starterkits/Sensor_Kalibrieren_Distanz_1.jpg %})
 
Auswahl "Config ändern" öffnet des Fenster "Schaltkasten-Konfiguration bearbeiten". 


1. Im Bereiche "Angeschlossene Produkte" wird "D22-Conn" ausgewählt.
1. Im Bereiche "Anschlüsse und Parameter" die Zeile "dist" ausgewählt.
1. Den Editierstift anklicken.
1. Jetzt wird die korrekte Distanz (in cm) von D21 nach D22 eintragen. Hier in diesem Aufbau wird angenommen, 
dass die Entfernung der beiden IR-Balisen 43 cm ist. 
1. Mit "Übernehmen" wird die Eingabe abgeschlossen und man ist zurück im Fenster "Schaltkasten-Konfiguration bearbeiten".
1. "Hochladen" anklicken.   


![CTC-App Sensor kalibrieren 1]({% link assets/images/Starterkits/Sensor_Kalibrieren_Distanz_dist_eingeben.jpg %})

## Sensor Kalibrierung ablaufen lassen

So vorbereitet, kann das Kalibrieren des Sensors einer Lokomotive erfolgen. 
Die CTC-App führt dazu eine Reihe von Messungen durch, wobei die Messstrecke von der 
gewählten Lokomotive mit unterschiedlichen Geschwindigkeiten durchfahren wird.
Die Anfangsgeschwindigkeit muss manuell vom Anwender gewählt werden.
Den eigentlichen Messzyklus führt die CTC-App selbstständig durch, 
es ist kein weiteres Eingreifen erforderlich.
Die Messstrecke wird mehrfach durchfahren, wobei jeweils nach drei Runden die Geschwindigkeit erhöht wird. 
Der Fortschritt der Messungen kann in der Grafik der aufgenommenen Messwerte verfolgt werden.

Im Hauptfenster CTC-App wird die Fahrstrecke der Lokomotive vorbereitet:
1. "Demo-Weiche-links" wird auf "Abbiegen gestellt".
1. Das Signal "Demo-S-Gleis-B" des Bahnhofgleises wird auf "freie Fahrt" gestellt.
1. "Demo-Weiche-rechts" wird auf "Abbiegen gestellt".
1. Die Lokomotive E103 auswählen und in der Zeile auf "Edit" klicken. 


![CTC-App Sensor kalibrieren 1]({% link assets/images/Starterkits/Sensor_Kalibrieren_Fahrstrecke_vorbereitet.jpg %})


Es öffnet sich das Fenster zur Konfiguration der Lokomotive, dort auf "Sensor kalibrieren" klicken.

1. Der Messzyklus deer CTC-App sollte mit der langsamsten möglichen Fahrgeschwindigkeit der Lokomotive starten.
Dieser muss vom Anwender eingestellt werden.
Typischerweise sollte dieser Wert zwischen 10 und 20 Prozent liegen. 
Bei der hier eingesetzten Lokomotive ist es ausnahmsweise ein deutlich höherer Wert, es muss 35% eingestellt werden.
1. Mit Klick auf "Fahren/Halten" fährt die Lok los.
Die Lokomotive soll einige Zeit fahren, das gibt Gelegenheit, die eingestellte minimal Fahrgeschwindigkeit zu prüfen
   und bei Bedarf anders zu setzen.
1. Mit Klick auf "Kalibrierung starten" wird der Messzyklus gestartet und selbstständig durchgeführt.


![CTC-App Sensor kalibrieren 1]({% link assets/images/Starterkits/Sensor_Kalibrieren_Starten.jpg %})

Sobald die Lokomotive drei mal über die Messstrecke gefahren ist, wird von der App selbstständig 
die Fahrgeschwindigkeit um 10%-Punkte hochgesetzt. 
Wieder wird die Messstrecke drei mal überfahren.
Die App verfährt weiter so, bis die 80% Marke für die Geschwindigkeit überschritten ist.
Im Fenster "Motor-Sensor kalibrieren" kann der Fortschritt der Messwerterfassung und der Auswertung beobachtet werden.

Wenn der Messzyklus vollständig ist, hält die Lokomotive an und mit 
Klick auf "Motor Config hochladen" werden die von der App errechneten Kalibrierungswerte in das Lok-Modul übertragen
und stehen für den Fahrbetrieb zur Verfügung.

![CTC-App Sensor kalibrieren 1]({% link assets/images/Starterkits/Sensor_Kalibrieren_Fertig_Hochladen.jpg %})
