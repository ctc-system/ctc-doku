---
layout: doku
title: "Config - Skript bearbeiten"
chapter: 4.5
---
Skripte gibt es in Funktionen, Triggern und Timern.
Sie legen eine Folge von einfachen Befehlen fest, die ausgeführt werden, wenn die Aktion einen bestimmten Zustand einnimmt.

Skripte setzen sich aus folgenden einfachen Befehlen zusammen:
* pin[]: Setze einen Ausgangspin auf einen bestimmten Wert.
* param[]: Setze einen Parameter auf einen bestimmten Wert.
* pause(): Warte einige Millisekunden.
* call(): Rufe eine andere Aktion auf demselben CTC-Modul auf, z.B. einen Timer.
* wenn (): Führe die Befehle unter dem "wenn" nur aus, wenn ein Parameter einen bestimmten Wert hat.

Im Skrtipt für einen Fahrauftrag gibt es zusätzlich folgenden Befehl
* on XXX : Die Lok soll die Balise XXX passieren und ggf. dabei ein Kommando ausführen - siehe [Kapitel 7.3 - Fahraufträge]({% link _app_doku/073-fahrauftraege.md %})    

Wann sich der Zustand einer Aktion ändert, hängt von der Art der Aktion ab:
* Funktionen und Wert-Regler werden vom Benutzer der CTC-App geändert, indem er einen entsprechenden Button drückt oder einen Schieberegler verstellt.
* Timer werden einmalig nach einer bestimmten Zeit oder in regelmäßigen Abständen automatisch vom CTC-Modul ausgelöst.
* Sensoren werden über Schalter ausgelöst.
  Diese können sowohl von einer Person gedrückt, als auch von einer Lokomotive oder Waggon im Vorbeifahren ausgelöst werden.
* Trigger lösen als Folge einer anderen Aktion aus.
  Auslöser können sowohl Funktionen und Wert-Regler sein, als auch Sensoren und von Loks gelesene IDs.

Die oberste Ebene des Skripts legt auf welche Werte die Aktion wie reagieren soll.
In der Werte-Config werden die Aktionen definiert, die für eine Funktion möglich sind (sie sind Teil der cfg.xml).
So können, z. B. für das Licht einer Lok, die vier Aktionen:
* **off**
* **on**
* **front**
* **back**

definiert werden.

Für jede Aktion kann festgelegt werden, welche der in der IO-Config definierten Ausgänge, geschaltet werden soll.
Dies ist oft nur ein einzelner Ausgang, kann aber auch eine, mit Bedingungen versehene Abfolge sein, wie der Beispiel-Dialog für die Aktion **straight** (gerade) der Weiche **PI-Dev-LH5** zeigt.

![ctc-app-doku-bild]({% link assets/images/ctc-app-doku/0405-edit_cfg-straight.png %})

Bei Weichen ist zu beachten, dass das, in der Weichenliste angezeigte **Icon** über den **Namen der Aktion** bestimmt wird.
Die **Position** (0 bis 2) gibt die Spalte in der Weichenansicht an:

| **Funktion**          | **Name der Aktion** | **Zeichen** | **Icon**                                                               | **empfohlene Position** |
| Weiche links          | left_left           | l           | ![ctc-app-doku-icon]({% link assets/images/ctc-app-doku/left_left_active.png %})      | 0        |
|                       | left_straight       | s           | ![ctc-app-doku-icon]({% link assets/images/ctc-app-doku/left_straight_active.png %})  | 1        |
| Weiche rechts         | right_straight      | s           | ![ctc-app-doku-icon]({% link assets/images/ctc-app-doku/right_straight_active.png %}) | 1        |
|                       | right_right         | r           | ![ctc-app-doku-icon]({% link assets/images/ctc-app-doku/right_right_active.png %})    | 2        |
| Dreiwegweiche         | three_left          | l           | ![ctc-app-doku-icon]({% link assets/images/ctc-app-doku/three_left_active.png %})     | 0        |
|                       | three_straight      | s           | ![ctc-app-doku-icon]({% link assets/images/ctc-app-doku/three_straight_active.png %}) | 1        |
|                       | three_right         | r           | ![ctc-app-doku-icon]({% link assets/images/ctc-app-doku/three_right_active.png %})    | 2        |
| Kreuzungsweiche       | cross_notcross      | x           | ![ctc-app-doku-icon]({% link assets/images/ctc-app-doku/cross_notcross_active.png %}) | 0        |
|                       | cross_cross         | n           | ![ctc-app-doku-icon]({% link assets/images/ctc-app-doku/cross_cross_active.png %})    | 1        |
| Kreuzungsweiche 4-Weg | cross_hor           | h           | ![ctc-app-doku-icon]({% link assets/images/ctc-app-doku/Cross_hor_active.png %})      | 0        |
|                       | cross_vert          | v           | ![ctc-app-doku-icon]({% link assets/images/ctc-app-doku/Cross_vert_active.png %})     | 1        |
|                       | cross_left          | l           | ![ctc-app-doku-icon]({% link assets/images/ctc-app-doku/Cross_left_active.png %})     | 2        |
|                       | cross_right         | r           | ![ctc-app-doku-icon]({% link assets/images/ctc-app-doku/Cross_right_active.png %})    | 3        |

## Kommandos für Loks / Signale

Den folgenden Kommandos können von Lokomotiven interpretiert werden, wenn sie mit einer Balise verknüpft sind: 

| **Funktion**                   | **Zeichen** | **Bedeutung**                                    | **cmdDist** | **Beschreibung**                                                                                                                                                    | 
| Halt (Hp0 / rot)               | 'H', 'h'    | Hinweis auf ein kommendes rotes Signal.          | Distanz | Die Lok bremst ab und hält vor diesem Signal an. Wird das Signal wieder grün, so setzt die Lok ihre Fahrt fort und beschleunigt auf die gemäß Signalbild maximal zulässige Geschwindigkeit. |
| Fahrt (Hp1 / grün)             | 'F', 'f'    | Hinweis auf ein grünes Signal.                   | Distanz | Eine bisherige Geschwindigkeitsbegrenzung wird hiermit aufgehoben. Bei manueller Steuerung passiert nichts, sonst beschleunigt die Lok auf ihre Maximalgeschwindigkeit. |
| Langsamfahrt (Hp2 / grün+gelb) | 'W', 'w'    | Hinweis auf Langsamfahrt.                        | Distanz | Ist die Lok zu schnell, so bremst sie ab auf reduzierte Geschwindigkeit, z.B. um über einer abzweigenden Weiche nicht zu entgleisen.                                    |
| Geschwindigkeits-beschränkung  | 'L', 'l'    | Hinweis auf eine Geschwindigkeits-begrenzung.    | Geschwindigkeit | Ist die Lok zu schnell, so bremst sie ab auf diese Geschwindigkeit. Dabei wird die Distanz mit 10 multipliziert und als Geschwindigkeit interpretiert.          |
| Minimal-geschwindigkeit        | 'M', 'm'    | Hinweis auf ein kommendes rotes Signal mit einer zweiten Balise für den endgültigen Halt. | Distanz | Die Lok bremst auf Kriechgeschwindigkeit ab. Die zweite Balise vor dem Signal sendet dann Halt mit Distanz 0 cm oder Nothalt.  |
| Nothalt / Emergency-Stop       | 'E', 'e'    | Nothalt, z.B. kurz vor Gleisende oder Signal.    | Distanz | Die Lok hält schlagartig an. Wird das Signal wieder grün, so setzt die Lok ihre Fahrt fort und beschleunigt auf die gemäß Signalbild maximal zulässige Geschwindigkeit. |
| Stop                           | 'S', 's'    | Hinweis auf einen kommenden Stop gemäß Fahrplan. | Pause in Sekunden | Im Automatikbetrieb hält die Lok an, wartet die Pausenzeit ab und setzt dann die Fahrt fort.<BR> Ab CTC-App 4.08: Im manuellen Betrieb wie Schutzhalt mit Distanz 0.  |
| Rückfahrt                      | 'R', 'r'    | Hinweis auf einen kommenden Stop am Wendepunkt des Fahrplans (Pendelzug). | Pause in Sekunden | Im Automatikbetrieb hält die Lok an, wartet die Pausenzeit ab und setzt dann die Fahrt in umgekehrter Richtung fort.<BR>Ab CTC-App 4.08: Im manuellen Betrieb wie Schutzhalt mit Distanz 0. |
| Schutzhalt (Sh2)               | 'T', 't'    | Hinweis auf das Gleisende, z.B. Prellbock.       | Distanz | Die Lok bremst ab und hält vor diesem Schild an.                                                                                                                        |

Mit fast jedem Kommando ist eine Distanz in cm verbunden, ab der dieses Kommando gilt.
Die Lok berechnet ihren Bremsweg passend zu dieser Distanz, z.B. führt "Halt" mit Distanz "65" dazu, dass die Lok nach 65 cm steht.

**Hinweis:** Voraussetzung für eine funktionierende Zielbremsung ist ein korrekt eingemessener Motor-Sensor (siehe [Kapitel "Motor-Sensor kalibirieren"]({% link _app_doku/051-sensor-kalibrieren.md %}))

Die Kommandos können entweder an der Balise fest hinterlegt werden oder mithilfe von "Triggern" dynamisch geändert werden.
So lässt sich z.B. eine Balise mit dem Zustand eines Signals verknüpfen (siehe [Kapitel "Config - Aktionen verknüpfen"]({% link _app_doku/043-aktionen-verknuepfen.md %})).
Deshalb sollten Sie für Signalzustände dieselben Buchstaben verwenden wie für das zugehörige Lok-Kommando.   
