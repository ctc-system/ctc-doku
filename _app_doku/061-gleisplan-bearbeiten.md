---
layout: doku
title: "Gleisplan bearbeiten"
chapter: 6.1
---
## Gleisplan bearbeiten

Zum Erstellen eines Gleisplans, wählen Sie als erstes ein [CTC-Weichenmodul]({% link _data_sheet/CTC-Weichenmodul-H0.md %}) oder [CTC-Multi-I/O-Board]({% link _data_sheet/CTC-Multi-IO-Board.md %}) aus, auf dem der Gleisplan gespeichert werden soll.
Öffnen Sie dessen Config-Dialog:

![Schaltkasten bearbeiten]({% link assets/images/ctc-app-doku/0601-edit_switchbox.png %})

Dort wählen Sie **Gleisplan ändern**.
Es öffnet sich folgender Dialog:

![ctc-app-doku-bild]({% link assets/images/ctc-app-doku/ctc-app-doku-Bild19.png %})

Mit dem Button **Neuer Gleisplan** legen Sie einen neuen Gleisplan an.
Achten Sie dabei auf eindeutige Namen.

![ctc-app-doku-bild]({% link assets/images/ctc-app-doku/ctc-app-doku-Bild20.png %})

Danach erscheint ein Gitter, in das die Gleis-Symbole eingefügt werden können.
Dazu selektiert man in der linken Symbolspalte das Gleissymbol und in der rechten die Orientierung.
Anschließend setzen Sie das Gleissymbol in den Gleisplan, indem Sie die gewünschte Position im Gitter anklicken.

Über die Buttons rechts von **Zeilen** und **Spalten** kann die Größe des Gitters angepasst werden.

![ctc-app-doku-bild]({% link assets/images/ctc-app-doku/ctc-app-doku-Bild21.png %})

Nun müssen Sie nur noch eine Weiche zuordnen.
Dazu klicken Sie oben links die Action Gruppe "SingalTower" und dann unten links die gewünschte Weiche aus. 

![ctc-app-doku-bild]({% link assets/images/ctc-app-doku/ctc-app-doku-Bild22.png %})

Das **Stift-Symbol** ![ctc-app-doku-icon]({% link assets/images/ctc-app-doku/ctc-app-doku-Icon_Stift.png %}) wird dabei automatisch ausgewählt.
Ist die Weiche bereits zugeordnet, so wird ihre Position im Gleisplan orange umrandet.

Nun klicken Sie auf die gewünschte Position im Gleisbild und die Weiche wird zugeordnet.
Dass das funktioniert hat, erkennen Sie am orangen Rahmen im Gleisbild und dass die Weiche nun in Blau die aktuelle Stellung der Weiche angezeigt wird.

Um zu einer Weiche in der Liste herauszufinden, wo sie im Gleisplan liegt, klicken Sie die Weiche in der Liste an: Das zugehörige Weichensymbol im Gitter wird orange umrahmt:

![ctc-app-doku-bild]({% link assets/images/ctc-app-doku/ctc-app-doku-Bild18.png %})

