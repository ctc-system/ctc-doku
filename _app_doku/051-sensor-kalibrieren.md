---
layout: doku
title: "Motor-Sensor kalibrieren"
chapter: 5.1
---
Für punktgenaue Zielbremsung vor einem roten Signal oder auch die Lastregelung muss das CTC-Lokmodul wissen wie schnell die Lok tatsächlich fährt.
Dazu ist im Motor-Treiber der Lok ein Sensor verbaut, der anhand des Stromflusses einen groben Rückschluss auf die Drehzahl des Motors und erlaubt.
Mit dem Kalibriervorgang wird eine Kennlinie ermittelt, die die Werte des Sensors auf die Geschwindigkeit abbilden.  

Damit eine Kalibrierung durchgeführt werden kann, muss eine Messtrecke bestehend aus zwei Balisen vorhanden sein.
Während der Kalibrierung fährt die Lok selbständig im Rundkurs über die Balisen oder pendelt hin und her.

**Hinweis:** Im Rahmen des Starter-Sets ist dieser Vorgang detailliert beschrieben, siehe [Starter-Set Sensor kalibirieren]({% link _starterkit/Starterset_Sensor_kalibrieren.md %})

## Messtrecke definieren

Die Messtrecke wird im Dialog "Modellbahn bearbeiten" definiert:

![Messstrecke defnieren]({% link assets/images/ctc-app-doku/Messtrecke.png %})

Die Beiden Balisen müssen zum selben Block gehören (erste zwei Zeichen identisch) und direkt nacheinander kommen.
Der "Abstand zum Vorgänger" der Balise mit der höheren Nummer bestimmt dann die Länge der Messtrecke. 
                        
## Motor-Sensor kalibrieren
                 
Den Dialog "Motor-Sensor kalibrieren" öffnen Sie über den Button "Sensor kalibrieren" in der [Lok-Konfiguration]({% link _app_doku/040-module-konfigurieren.md %}) der betreffenden Lok.
                                        
![Sensor kalibrieren]({% link assets/images/ctc-app-doku/Sensor-kalibrieren.png %})

Ganz oben wird die Messstrecke angezeigt. 
Prüfen Sie noch mal ob Balisenbezeichnungen und Distanz stimmen.

Beschleunigen Sie die lok auf die minimale Geschwindigkeit mit der sie noch gut fährt.
Dann starten sie den Kalibriervorgang mit Klick auf "Pendeln starten" bzw. "Rundkurs starten".

**Hinweis:** Beim Pendeln braucht die Lok erheblich Platz an beiden Enden der Messtrecke.

Die Lok fährt dreimal die Messstrecke, und beschleunigt dann um 10% und fährt wieder dreimal die Messstrecke.
Das wiederholt sich bis die Lok 80% überschritten hat, dann hält sie an.

Der Dialog sieht dann z.B. so aus wie im Bild oben für die [Piko BR 147]({% link _lokumbau/BR147-Piko-51583-2.md %}).
Wenn Sie mit dem Ergebnis zufrieden sind, klicken Sie auf "Motor Config hochladen".

**Hinweis:** Wer es ganz genau wissen will, findet die während der Messung übertragenen Daten als "Motor-Setup.csv" in seinem Benutzerverzeichnis.
Diese lässt sich z.B. mit Excel oder LibreOffice öffnen und kann dort detailliert ausgewertet werden. 
