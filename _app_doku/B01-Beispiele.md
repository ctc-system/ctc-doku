---
layout: doku
title: "Anhang B: Beispiele"
chapter: B.01
---
In diesem Kapitel werden diverse Beispiele vorgestellt.

Ziel davon ist es aufzuzeigen, wie Konfigurationen für typische Szenarien aufgebaut werden.
Eine ganz genaue Schritt-für-Schritt-Anleitung erhalten Sie im extra Dokument ["CTC-Starter-Sets"]({% link _data_sheet/CTC-Starter-Sets.md %}), dessen Lektüre wir in jedem Fall vor dem Studieren dieser Beispiele empfehlen. 