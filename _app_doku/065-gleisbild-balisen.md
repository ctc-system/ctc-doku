---
layout: doku
title: "Balisen (ID-Sender) im Gleisplan"
chapter: 6.5
---
## Balisen anlegen und zuordnen
           
Für Balisen (ID-Sender) wird das hier dargestellte Symbol verwendet:

![Gleisbild: Balise einfügen]({% link assets/images/ctc-app-doku/ID-Sender-Gleisbild.png %})

## Balisen zuordnen

Im Falle von NFC-Balisen müssen wir vor wir dem nächsten Schritt sicherstellen, dass es mindestens einmal von einer Lokomotive gelesen wurde.
Erst dann taucht es in der Liste der Balisen (ID-Sender) im Gleisbild-Editor auf.

Nun kann der Position im Gleisbild eine Balise zugeordnet werden.
Dazu wählen Sie in der Tabelle links oben die Action-Gruppe "Balise" (früher "ID-Sender") aus und klicken die zu verknüpfende Balise an.
Dabei wird das Stift-Symbol der Werkzeugleiste aktiviert.
Nun klicken sie auf die Stelle im Gleisplan, an der die Balise liegt: 

![Gleisbild: Balise einfügen]({% link assets/images/ctc-app-doku/ID-Sender-NFC-Config.png %})

Im Falle einer IR-Balise sind Sie nun fertig, da dessen Konfiguration ja bereits bei der Konfiguration des damit verbundenen CTC-Moduls erfolgte.

## NFC-Balise (NFC-Tag) konfigurieren

Handelt es sich um einen NFC-Balise, so muss diese noch konfiguriert werden.
Dazu geben sie seine Positions-ID (3 Zeichen) und den Abstand zum Vorgänger-Tag ein.

Für die Positions-ID ist Folgendes zu beachten:
* Die ersten beiden Zeichen benennen den Gleisabschnitt zu dem das Tag gehört.
* Die dritte Ziffer bzw. Zahl gibt die Reihenfolge der Tags im Uhrzeigersinn an.