---
layout: doku
title: "Automatisierung: Blöcke"
chapter: 7.1
---
**Hinweis:** Die hier beschriebene Funktionalität setzt die **CTC-App ab Version 4.00 und zugehörige Lok-Firmware** zwingend voraus. 

## Blöcke (Gleisabschnitte)

Blöcke (Gleisabschnitte) werden in CTC durch zwei Buchstaben/Ziffern identifiziert, z.B. "B1".
Groß- und Kleinbuchstaben gelten dabei als unterschiedlich.
Die ersten beiden Zeichen der ID unserer Balisen werden als Name des Blocks verstanden, in dem sich die Balise befindet.

Auch wenn Sie keinen Block konfiguriert haben, existiert für jede Balise ein passender Block, nämlich der, der sich aus den ersten zwei Buchstaben ihrer ID ergibt.
Um einen Block im Gleisbild sichtbar zu machen, müssen Sie diesen aber anlegen.
Und auch wenn Sie Fahrstraßen anlegen wollen, müssen sie vorher Blöcke anlegen.

### Richtung (Uhrzeigersinn)

Balisen im selben Block werden an der dritten Stelle ihrer ID im Uhrzeigersinn aufsteigend nummeriert, also z.B. "AB1", "AB2", "AB3" bei den drei Balisen im Block "AB".
Mithilfe dieser Nummerierung kann die Lok ermitteln, in welche Richtung (Uhrzeigersinn) sie fährt.

Bei der Platzierung von Balisen und Blöcken im Gleisbild sollten Sie darauf achten, dass der kleine Pfeil im Symbol in Uhrzeigerrichtung zeigt.

### Regeln für die Benennung

Für die Positions-IDs dürfen Sie nur Ziffern (0 bis 9) und Buchstaben (A bis Z sowie a bis z) verwenden, keine Sonderzeichen und auch keine Umlaute.
Der Name eines Blocks muss genau zwei Zeichen lang sein.
Sonst gibt es keine besonderen Regeln für die Benennung von Blöcken.

Auf meinen Anlagen (hier die Testanlage) gehe ich wie folgt vor (Block- und Balisen-IDs wurden von Hand in das Gleisbild gemalt):
          
![Blöcke auf der Testanlage]({% link assets/images/ctc-app-doku/PIRail-Dev-Bloecke.png %})

* Ich beginne mit der Benennung meiner Bahnhöfe mit einem einzelnen Buchstaben, z.B. "A" und "B".
* Dann vergebe ich Ziffern für die Gleise (Bahnsteige) der Bahnhöfe, z.B. "A1", "A2", "A3".
* Lange Verbindungsstrecken, die aus mehreren Blöcken bestehen, erhalten einen separaten ersten Buchstaben.
* Durch Verwendung des passenden Kleinbuchstabens kann ich bei Bedarf noch mal 10 weitere Gleise im Bahnhof benennen (z.B. "a1", "a2").
* Für Rangierbereiche und Abstellgleise verwende ich ggf. einen neuen ersten Buchstaben.
* Ein Gleis, das Bahnhof "A" mit Bahnhof "B" verbindet benenne ich "AB", wenn es im Uhrzeigersinn von A nach B verläuft, sonst "BA".
  Auch hier habe ich ggf. noch die Kleinbuchstaben zur Verfügung.

Da ich mehrere Anlagen (H0-, Garten- und mehrere Testanlagen) im Haus habe, achte ich darauf, dass es keine Doppelverwendung gibt - es könnten ja auch mal mehrere Anlagen gleichzeitig an sein.

Die IDs und weitere **Informationen zu Blöcken und Balisen** erhalten Sie, wenn Sie den betreffenden Block:

![Popup Block-Info]({% link assets/images/ctc-app-doku/Block-Info.png %})

bzw. Balise im Gleisbild anklicken:

![Popup Balisen-Info]({% link assets/images/ctc-app-doku/Balise-Info.png %})

## Block anlegen /konfigurieren

Zum Erstellen eines Blocks wählen Sie sich als Erstes ein [CTC-Weichenmodul]({% link _data_sheet/CTC-Weichenmodul-H0.md %}) oder [CTC-Multi-I/O-Board]({% link _data_sheet/CTC-Multi-IO-Board.md %}) aus, auf dem der Gesamtplan gespeichert werden soll.
Öffnen Sie dessen Config-Dialog:

![Config-Dialog Button Automatisierung ändern]({% link assets/images/ctc-app-doku/Config-Automatisierung-aendern.png %})

Dort klicken Sie auf "Automatisierung ändern", dann auf den Plus-Button neben "Produkte und Aktionen":

![Config: Neuer Block]({% link assets/images/ctc-app-doku/Config-Neuer-Block.png %})

## Signale im Block

Für beide Enden eines Blocks kann je ein Ausfahrsignal festgelegt werden.
Dieses erhält als Positions-ID den Namen des Blocks gefolgt von einem Plus ('+'), falls es am Ende des Abschnitts im Uhrzeigersinn (rechts) steht.
Steht das Signal am anderen Ende des Blocks, so erhält es ein Minus ('-').

Sie können ein Signal einem Block zuordnen, indem Sie dem Signal im [Gleisbildeditor]({% link _app_doku/061-gleisplan-bearbeiten.md %}) eine Positions-ID geben:

![Block-Ausfahrsignal festlegen]({% link assets/images/ctc-app-doku/Block-Ausfahrsignal.png %})

Damit ist die CTC-App in der Lage einer Lok, die aufgrund eines roten Signals in einem Block zum Halten gebracht wurde, mitzuteilen, wenn das Signal die Fahrt wieder freigibt.

**Hinweis:** Damit eine Lok nicht durch das falsche Signal freigegeben wird, darf einem Block maximal ein Signal pro Fahrtrichtung zugeordnet werden.
