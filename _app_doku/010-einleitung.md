---
layout: doku
title: "Grundkonzept"
chapter: 1
---
![ctc-app-doku-bild]({% link assets/images/ctc-app-doku/CTC-Komponenten.png %})

CTC ist ein System aus Hard- und Software zur Steuerung von Modellbahnen. Es hat die folgenden Merkmale:
* **CTC-Module**: Elektronische Baugruppen zur Ansteuerung von Lokomotiven, Weichen, Signalen, Beleuchtungen, Aktions-Bausteine, etc. Auf jedem CTC-Modul übernimmt lokale Firmware und Konfiguration die lokalen Steuerungsaufgaben wie Motoransteuerung, Weichenschaltung, Lichter an/aus, etc.
  Die jeweilige Konfiguration ist dezentral im zugehörigen CTC-Modul gespeichert.
* **CTC-App**: die Software dient als Anwenderschnittstelle (Grafische Anwenderschnittstelle) und kann sowohl auf einem SmartPhone, einem Tablet mit Android oder iOS oder auch auf einem PC mit Windows, MAC oder Linux eingesetzt werden.
  Eine dedizierte und kostenaufwendige spezielle zentral Steuereinheit ist nicht erforderlich.
* CTC Module werden über die CTC-App konfiguriert.
* **Kommunikation über WLAN**: über einen dedizierten WLAN Access Punkt (z.B. eine FritzBox) findet die Kommunikation zwischen CTC-App und CTC-Modulen sowie der CTC-Module untereinander statt.
  Die Übertragungsgeschwindigkeit beträgt bis zu 54 Mbit/s.
  Ausser der Spannungsversorgung ist keinerlei Verkabelung notwendig.
* Über die CTC-App werden die CTC-Module angesprochen.
  Die grafische Anwenderschnittstelle ermöglicht es beispielsweise Fahrgeschwindigkeit, Weichenstellung, Signallichter etc zu steuern.
* **Exakte Positionsermittlung**: Bei kleinen Spuren (bis Spur 0) kann die CTC-App die exakte Position mit Hilfe dedizierter Infrarot-Sender im Gleis und Infrarot-Empfänger in der Lokomotive bestimmen.
  Ab Spur 1 wird dasselbe mit NFC-Tags im Gleis und einem NFC-Reader unter der Lok erreicht.
* **Waggon-Erkennung (Achszählung)**: Ab CTC-App 4.22 kann ein NFC-Reader unters Gleis montiert werden, um mit einem NFC-Tag versehene Waggons zu erkennen. 
  Damit hat CTC einen adäquaten Ersatz für Achszähler - mit dem Vorteil, dass die App genau weiß welcher Waggon gerade vorbeigefahren ist.
* **Sound** ist bei CTC per SUSI3 oder unter Verwendung eines DCC-Sound-Dekoders möglich. 
* Ein **Parallelbetrieb** mit anderen „marktgängigen“ Digital- und Analog-Systemen ist z.B. mithilfe des Z21-LAN-Protokolls möglich.

## Sichten der App

Die CTC-App besteht aus einer ganzen Reihe von Sichten.
In der Desktop-App können diese im Hauptfenster und in zusätzlichen Fenstern frei angeordnet und auch auf mehrere Bildschirme verteilt werden.
Handy- und Tablet-App haben ein weitgehend starres Layout.

Folgende Sichten gibt es:
* Die **Modul-Liste** zeigt alle CTC-Module, die sich bei der App seit ihrem Start gemeldet haben.
* Die **Lok-Liste** zeigt alle Loks, die sich bei der App seit ihrem Start gemeldet haben.
* Die **[Lok-Steuerung]({% link _app_doku/011-lok-steuern.md %})** bietet alle Funktionen zur manuellen Steuerung einer Lok.
* Das **[Schaltpult]({% link _app_doku/015-schalten.md %})** listet alle Weichen, Signale, ... und ermöglicht es diese zu schalten.
* Die **Sensor-Anzeige** zeigt alle Sensoren udn deren Zustand.
* Der **[Gleisplan]({% link _app_doku/016-gleisbild.md %})** zeigt die Modellbahn mit allen platzierten Gleisplänen.
* Die **[Block-Liste]({% link _app_doku/071-bloecke.md %})** zeigt alle Blöcke mit der jeweils dort befindlichen Lok und ihren Waggons.
* Die **[Fahrplan-Anzeige]({% link _app_doku/074-fahrplane.md %})** zeigt einen ausgewählten Fahrplan für den Automatik-Betrieb.
* Die **[BW-Anzeige]({% link _app_doku/102-Betriebswerk.md %})** zeigt Betriebswerke und die jeweils dort registrierten Waggons.
      
In der Desktop-App können neue Sichten über das Menü "Ansicht" als neue Fenster geöffnet werden.
Anschließend können Sie die Sicht an einen anderen Ort hinschieben, indem Sie sie auf den Titel der Sicht klicken und mit weiterhin gedrückter Maustaste die Sicht an den gewünschten Platz ziehen.
Graue Rechtecke visualisieren währen dem Ziehen wo die Sicht "eingedockt" wird. 

Die folgenden Bilder zeigen die CTC-App einmal auf einem PC und zweimal auf einem Android-Tablet:

![ctc-app-doku-bild]({% link assets/images/ctc-app-doku/01-main-light.png %})

![ctc-app-doku-bild]({% link assets/images/ctc-app-doku/Screenshot-Android-App.png %})

![ctc-app-doku-bild]({% link assets/images/ctc-app-doku/Screenshot-Android-App-3-Spalten.png %})