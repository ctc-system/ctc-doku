---
layout: doku
title: "Sound"
chapter: 9
---
Sollen CTC-Module auch mit Sound begeistern, so müssen Sie auf bestehende Digitaltechnik zurückgreifen.
Inzwischen gibt es mehrere Möglichkeiten das zu tun:
* CTC-Lokmodule können ein DCC-Signal erzeugen.
  Auf diese Weise kann entweder der mit der Lok mitgelieferte Digital-Dekoder weitergenutzt werden oder ein neuer Digitaldekoder angeschlossen werden.
  Mehr dazu erfahren Sie in [Kapitel 8.1 - "Altes Digital: DCC-Dekoder einbinden"]({% link _app_doku/081-DCC-dekoder.md %})
* Ab der mit CTC-App Version 4.19 mitgelieferte Firmware (20230902) kann die SUSI3-Schnittstelle für alle CTC-Lokmodule (außer CTC-Lokmodul-H0a) nachgerüstet werden.
  Bei Loks, die von Haus aus für den Sound ein SUSI3-Modul verwendet haben, kann so der Original-Sound erhalten bleiben, ohne den Original-Dekoder erhalten zu müssen.
  Dies geht z.B. mit [PIKOs Spur-G Modell der BR50]({% link _lokumbau/BR50-PikoG-37243-SUSI.md %}).
  Mehr zu SUSI-Sound erfahren Sie in [Kapitel 9.1 - "Sound mit SUSI-Schnittstelle"]({% link _app_doku/091-SUSI-sound.md %}) 