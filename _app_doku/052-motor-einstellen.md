---
layout: doku
title: "Motor einstellen"
chapter: 5.2
---

## Motor einstellen

In diesem Dialog können die Parameter der Motorregelung eines CTC-Lokmoduls mithilfe von Live-Daten optimiert werden.  
Dazu überträgt die Lok kontinuierlich den Wert des Motor-Sensors, den Soll-Wert und den vom PID-Regler berechneten Stellwert.
Solange alle drei Werte 0 sind, ist das Diagramm "eingefroren".

Die angezeigten Parameter gehören jeweils zur aktuell ausgewählten Betriebsart (Motor-Mode) der Lok.
Die Parameter kP, KI und kD sind nur im Motor-Mode "PID" von Bedeutung und werden im Motor-Mode "direct" ignoriert.
 
Bei Klick auf den entsprechenden Set-Button wird der links davon stehende Wert direkt an die Lok übertragen und verbleibt so bis zum Reset der Lok.
Erst, wenn Sie "Motor Config hochladen" klicken, werden Ihre Einstellungen dauerhaft gespeichert.

Parameter der Lastregelung (PID-Regler):
* **kP** bestimmt, wie direkt der Motor auf Änderungen des Sensors reagiert.
  Ein zu hoher Wert führt zu ruckeligem Fahrverhalten, ein zu niedriger dazu, dass es sehr lange dauert bis der Motor auf einen geänderten Soll- oder Sensorwert reagiert.
* **kI** bestimmt, wie stark der Motor auf die Summe aller Sollwertabweichungen reagiert.
* **kD** bestimmt, wie stark der Motor auf Differenz zwischen aktuellem und vorhergehendem Sensor reagiert.
* die Sample-Rate kann nur direkt in der cfg.xml eingestellt werden und sollte auf der Vorgabe von 20 ms bleiben.

Regelbereich des Motor-Ausgangs:
* **Min** und **Max** geben den Regelbereich des Motors vor (0..1023).
  Hiermit kann der untere Bereich, in dem sich der Motor nicht dreht, ausgeklammert und die Maximalgeschwindigkeit begrenzt werden.
* Mit **Kriechen** setzen Sie die Kriechgeschwindigkeit, die beim Entkuppeln ("Kupplungswalzer") gefahren wird sowie die Zielgeschwindigkeit für das Abbremsen auf Minimalgeschwindigkeit ist. 

![Motor-Setup]({% link assets/images/ctc-app-doku/ctc-app-doku-Bild13.png %})

Parameter für Bremsen und Beschleunigung:
* **Bremsen** ist die Beschleunigungskonstante für Bremsvorgänge (in mm/s²).
  Der angegebene Wert wird positiv angegeben und in der Lok mit -1 multipliziert.
  Ist der Wert 0, wird als Default-Wert 250 verwendet. 
* **Beschleunigen** ist die Konstante für Beschleunigungsvorgänge (in mm/s²).
  Ist der Wert 0, wird als Default-Wert 250 verwendet.
* **Bremsfaktor** wird durch 1000 geteilt und um 1 erhöht als Verstärkungsfaktor bei der Zielbremsung verwendet.
  Im Beispiel (800) wird also mit 1,8 multipliziert.
  Durch Erhöhen des Bremsfaktors kann der Knick nach unten am Ende eines Bremsvorgangs reduziert werden.
  Ein zu starker Bremsfaktor führt zu einer nach unten durchgebogenen Bremskurve.
* **Reaktion** gibt beim Bremsen die Zeit in Millisekunden an, bis die Verzögerung durch den Motor wirkung zeigt.
  Dieser Parameter funktioniert erst ab **Firmware 20230609** (CTC-App 4.17).

**Hinweis:** Solange der Dialog **Motor einstellen** geöffnet ist, sendet die Lok unsynchronisiert recht viele Datenpakete für das Diagramm.
Dieser erhöhte Datenverkehr kann zu häufigeren Verlusten von Datenpaketen (#Msg.Miss in der [Statistik-Anzeige]({% link _app_doku/035-Statistik.md %}) ) führen.

## Optimierung der Bremsparameter

Für die Optimierung der Zielbremsung benutze ich einen Rundkurs mit einem Signal, vor dem die Lok mithilfe von 2 Balisen angehalten wird.
Die Lok steht vor dem roten Signal und hat im Beispiel 70% als Geschwindigkeit eingestellt.
Dann wird das Signal geöffnet und sobald die Lok losgefahren ist wieder geschlossen.

Im folgenden Beispiel sieht man die mit 70% (700) ankommende Lok ([Piko BR 147]({% link _lokumbau/BR147-Piko-51583-2.md %})), dann den Bremsvorgang bis auf Minimalgeschwindigkeit (Parameter "Kriechen" 150), eine kurze Zeit Minimalgeschwindikeit und ganz rechts den Stop. 
Im ersten Versuch sind Bremsfaktor und Verzögerung auf 0 eingestellt:

![Bremsen vorher]({% link assets/images/ctc-app-doku/Motor-Setup-Bremsen-1.png %})

Nun wurde die Bremskurve optimiert und experimentell für Bremsfaktor 800 und Reaktion 60 als optimale Werte ermittelt: 

![Bremsen nachher]({% link assets/images/ctc-app-doku/Motor-Setup-Bremsen-2.png %})

**Hinweis:** Wer es ganz genau wissen will, findet die während der Messung übertragenen Daten als "Motor-Setup.csv" in seinem Benutzerverzeichnis.
Diese lässt sich z.B. mit Excel oder LibreOffice öffnen und kann dort detailliert ausgewertet werden.
