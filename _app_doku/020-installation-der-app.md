---
layout: doku
title: "Installation der App"
chapter: 2
---
## Tablet und Smartphone

Die Apps für Android und iOS werden über den jeweiligen App-Store installiert (je 9,99 EUR Gebühr):
* Android-App über [Google Play-Store](https://play.google.com/store/apps/details?id=de.pidata.rail.android)
* iOS-App über [App-Store](https://apps.apple.com/de/app/ctc-app/id1576119270)

**Hinweis:** Für die Gebühr im PlayStore haben wir uns entschieden, um unnötige Downloads und daraus resultierende Bewertungen gering zu halten. 

## Linux, Mac und Windows

Für die PC-Applikation finden Sie Installer für Linux, Mac OS/X und Windows in unserem [Download-Bereich]({% link de/download.md %}).
Die Installer legen jeweils passende Startmenü-Einträge an.

### Python

Um auch Firmware-Updates der CTC-Module machen zu können, brauchen Sie ein aktuelles Python, z.B. Version 3.8.
Unter Linux sollte das immer vorhanden sein, unter Windows muss es separat installiert werden.

Bei der Installation von **Python unter Windows** muss darauf geachtet werden, dass der Haken bei "**Add Python 3.8 to PATH**" gesetzt ist.

![ctc-app-doku-bild]({% link assets/images/ctc-app-doku/ctc-app-doku-Bild2.png %})

## MacOS/X

Wenn MacOS/X die Installation verwehrt, kann das an fehlender Internetverbindung liegen - die Signatur erfordert einen Online-Zugriff.

## Linux

Wenn Linux die Installation verweigert, dann mit dem "QApt-Paketinstallationsprogramm" öffen oder per Kommandozeile installieren, für die CTC-App Version 4.24 z.B.

`sudo apt install ./ctc-app_4.24_amd64.deb`

## Windows

Windows 10 beschwert sich manchmal über eine fehlende Signatur:

![Windows Setup-1]({% link assets/images/ctc-app-doku/Setup-Windows-1.png %})

Dann einfach auf den Link "Weitere Informationen" klicken.
Damit wird der Button "Trotzdem ausführen" eingeblendet:

![Windows Setup-2]({% link assets/images/ctc-app-doku/Setup-Windows-2.png %})

Jetzt können Sie auf "Trotzdem ausführen" klicken, um die CTC-App zu installieren.
