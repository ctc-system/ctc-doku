---
layout: doku
title: "Config - DCC-Dekoder anschließen"
chapter: 4.6
---
**WICHTIG:** Alles was hier beschreiben ist benötigt mindestens die **CTC-App Version 3.10** und die **Firmware 20200514**!

### Grundfunktion: Motor und Fahrlicht

Da die CTC-Lokmodule für den direkten Anschluss von Motor und Licht vorkonfiguriert sind, müssen diese zuerst aus der Konfiguration gelöscht werden.
Dazu öffnen Sie in der CTC-App die Konfiguation des Moduls ([siehe Bedienunfgsanleitung Kapitel 4]({% link _app_doku/040-module-konfigurieren.md %})).

![Config Dialog]({% link assets/images/ctc-app-doku/0406-edit_loco.png %})

Klicken Sie auf den Button "Config ändern", um die Funktionen Ihres Digital-Dekoders dem CTC-Modul bekannt zu machen.

Entfernen Sie dort alle angeschlossenen Produkte, indem Sie jeweils die Zeile in der Liste "Angeschlossene Produkte" markieren udn dann den Minus-Button rechts davon anklicken.
Danach sieht der Dialog wie folgt aus:

![Config Dialog]({% link assets/images/ctc-app-doku/0406-edit_cfg-leer.png %})

Dann wählen Sie den Motor-Port, an dem Sie den Digital-Dekoder angeschlossen haben:

![Motor in Lok-Config ausgewählt]({% link assets/images/ctc-app-doku/0406-edit_cfg-MotorPt.png %})

Nach Klick auf den Plus-Button rechts daneben öffnet sich folgender Dialog:

![Motor in Lok-Config ausgewählt]({% link assets/images/ctc-app-doku/0406-select_asset_file-DCC.png %})

Sie wählen "DCC.xml" aus und klicken auf "Übernehmen".
In folgenden Dialog vergeben Sie einen Namen für die Erweiterung (hier "DCC"), geben die DCC-Adresse an (bei neuen Loks ist das die 3) und wählen in der Tabelle "DCC-Lokomotive" aus:

![Erweiterung anschließen]({% link assets/images/ctc-app-doku/0406-select_extension-DCC.png %})

Nach Klick auf Übernehmen können Sie den Motor-Port aufklappen und sehen Ihre hinzugefügte Erweiterung:

![Config-Erweiterung angeschlossen]({% link assets/images/ctc-app-doku/0406-edit_cfg-DCC.png %})

Jetzt lohnt es sich ein erstes Mal, den Button "Hochladen" zu drücken, denn erst danach können Sie mit dem Test-Button die Funktionen des Digital-Dekoders ausprobieren.
Der Motor und das Fahrlicht sind schon korrekt verdrahtet, d.h. Sie können nun zur Lok-Steuerung wechseln und einen ersten Test wagen.

### Sound konfigurieren

Nachdem die Grundkonfiguration erledigt ist, können Sie sich um die Sound-Funktionen kümmern.

Dazu klicken Sie erneut auf "Config ändern" und klappen den Motor-Port auf, an dem Sie ihren Digital-Dekoder angeschlossen haben:

![Erweiterung Funktion ausgewählt]({% link assets/images/ctc-app-doku/0406-edit_cfg-DCC-F3.png %})

Um zu prüfen, hinter welcher Funktion sich was verbirgt, markieren Sie die Funktion (im Bild "DCC-F3") und drücken auf "Test".

Möchten Sie diese Funktion in der Lok-Steuerung verfügbar machen, dann drücken Sie auf den Plus-Button neben "angeschlossene Produkte".
Wählen Sie den Katalog "universell-loks.xml" und dort "Lok-Sound an LowSide" für Sound bzw. "Lok-Licht an LowSide" für Lichtfunktionen:

![Produkt auswählen: Sound]({% link assets/images/ctc-app-doku/0406-select_product-Sound.png %})

Dann markieren Sie rechts unten den "soundPin" und klicken auf den Button "Verbinden":

![Sound-Pin verbinden]({% link assets/images/ctc-app-doku/0406-edit_cfg-soundPin.png %})

Nachdem alle gewünschten DCC-Funktionen konfiguriert sind, sieht die Lok-Steuerung so aus:

![Sound-Pin verbinden]({% link assets/images/ctc-app-doku/0406-control_dialog-DCC.png %})
 