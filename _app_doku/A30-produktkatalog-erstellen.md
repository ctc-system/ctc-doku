---
layout: doku
title: "Config - Produktkatalog erstellen"
chapter: A3
---
Zum Erstellen eines Produktkatalogs benötigen Sie einen XML-Editor und die zugehörigen XML-Schema-Dateien.

Bitte kontaktieren Sie uns, wenn Sie einen eigenen Produktkatalog erstellen möchten.  