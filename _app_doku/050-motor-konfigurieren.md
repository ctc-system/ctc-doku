---
layout: doku
title: "Motor konfigurieren"
chapter: 5
---
Die Programmierung der CTC-Lokmodule reduziert den Aufwand zur Konfiguration auf wenige Schritte.

Wenn Sie nur manuell und ohne Lastregelung fahren wollen und auch weder Zugsicherung noch Automatisierung nutzen wollen, brauchen Sie nichts Weiteres zu tun.
Nach unserer Erfahrung ist die reine Motor-Ansteuerung des CTC-Moduls mit allen gängigen Gleich- und Wechselstrommotoren kompatibel.
Vom uralten 3-Pol-Motor über den 5-poligen Hochleistungsantrieb bis zu aktuellen Motoren mit Schwungmasse hat bisher alles mit CTC auf Anhieb funktioniert.

Die [Kalibrierung des Motor-Sensors]({% link _app_doku/051-sensor-kalibrieren.md %}) ist für folgende Funktionalitäten notwendig:
* Punktgenaue Zielbremsung vor z.B. einem roten Signal
* Lastregelung, d.h. dass die Lok bergauf wie bergab gleich schnell fährt

Der Schritt [Motor Einstellen]({% link _app_doku/052-motor-einstellen.md %}) erlaubt es die Regelparameter der Lastregelung und auch der Zielbremsung zu optimieren.  