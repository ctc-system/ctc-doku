---
layout: doku
title: "Sound mit SUSI-Schnittstelle"
chapter: 9.1
---
Die SUSI-Schnittstelle erlaubt die Trennung von Dekoder und Soundmodul.
Inzwischen gibt es winzig kleine SUSI-Soundmodule und sogar Lautsprecher mit eingebautem SUSI-Soundmodul.
Damit stehen auch bei beengtem Bauraum die Chancen gut, auch mit CTC in den Genuss von Lok-Sound zu kommen.

Die SUSI3-Schnittstelle kann derzeit für alle CTC-Lokmodule (außer CTC-Lokmodul-H0a) nachgerüstet werden.
Sie benötigen lediglich eine aktuelle Firmware (mindestens 20230902) und die CTC-App Verison 4.19 oder neuer.                                                                                                         
                                                             
Das SUSI3-Modul muss mit **3,3 Volt Signalpegeln** zurechtkommen (deshalb die "3" hinter SUSI) und als **Spannungsversorgung die Gleisspannung** vertragen.
Der Support von Uhlenbrock hat uns diese Daten für die aktuellen [IntelliSound 6 Module](https://www.uhlenbrock.de/de_DE/produkte/sound/index.htm) bestätigt. 

Da unsere bisherigen CTC-Lokmodule über keine SUSI-Buchse verfügen, muss für diese ein passender Adapter hergestellt werden.
Wie das geht, wird hier weiter unten und in unseren [Lokumbauten]({% link de/docu/Lokumbau.md %}) dokumentiert.
Bei Bedarf erstellen wir Ihnen gerne einen passenden Adapter.
           
## Original-Sound

Es gibt Loks, die von Haus aus für den Sound ein SUSI3-Modul verwenden.
In diesem Fall können Sie den Motor und die Lokfunktionen direkt mit CTC ansteuern und trotzdem den Original-Sound Ihrer Lok genießen.
Dies geht z.B. mit [PIKOs Spur-G Modell der BR50]({% link _lokumbau/BR50-PikoG-37243-SUSI.md %}).

## Sound nachrüsten

Wenn Sie keinen Wert auf den Original-Sound legen oder die auf CTC umzurüstende Lok gar keinen Sound hatte, können Sie ein SUSI3-Soundmodul erwerben und an das CTC-Lokmodul anschließen.

Die Märklin BR247, die uns immer auf unsere Messen begleitet haben wir inzwischen so mit SUSI3-Sound versehen.
Details dazu finden Sie im [Umbau-Bericht der BR247]({% link _lokumbau/BR247-Ma-36292.md %}).

## SUSI3-Adapter für CTC-Lokmodule
   
Die vier Pins der SUSI3-classic Buchse müssen wie folgt verbunden werden:
* Plus (rot) mit VBB
* Masse (schwarz) mit GND
* Daten (grau) über min. 470 Ohm Widerstand mit einem GPIO-Pin des Prozessors
* Takt (blau) über min. 470 Ohm Widerstand mit einem GPIO-Pin des Prozessors

### Adapter für CTC-Lokmodul-21mtc
                                       
![SUSI-Adapter 21mtc]({% link assets/images/ctc-app-doku/SUSI-Adapter-21mtc.jpg %})

Beim CTC-Lokmodul-21mtc werden
* Takt (blau) mit "Input-1 (Pin 33)" auf der zusätzlichen Buchse verbunden und
* Daten (grau) mit "Input-2 (Pin 32)" auf der zusätzlichen Buchse verbunden.

Außerdem müssen wir noch Plus (rot) mit VBB und Masse (schwarz) mit GND verbinden:
* Im Bild haben wir das direkt an den Lötpads der 21mtc-Buchse getan. 
* Alternativ können auch die deutlich größeren, für den Stützkondensator gedachten, Lötflächen auf der anderen Seite des CTC-Lokmoduls genutzt werden. 

### Adapter für CTC-Lokmodul-PluX22

Beim CTC-Lokmodul-PluX22 werden:
* Takt (blau) mit "NFC TX (Pin 25)" auf der zusätzlichen Buchse verbunden und
* Daten (grau) mit "NFC RX (Pin 23)" auf der zusätzlichen Buchse verbunden.

Außerdem müssen wir noch Plus (rot) mit VBB und Masse (schwarz) mit GND verbunden werden:
* Das kann wie bei 21mtc direkt an den Lötpads der 21mtc-Buchse getan werden.
* Alternativ können auch die deutlich größeren, für den Stützkondensator gedachten, Lötflächen des CTC-Lokmoduls genutzt werden.

### Adapter für CTC-Lokmodul-G

![SUSI-Adapter G]({% link assets/images/ctc-app-doku/SUSI-Adapter-G.jpg %})
                    
Beim CTC-Lokmodul-G nutzen wir die für Servos gedachten Anschlüsse:
* Takt (blau) wird mit "Servo-2 PWM" verbunden.
* Daten (grau) wird mit "Servo-1 PWM" verbunden.
Da die Servo-Anschlüsse bereits auf dem CTC-Modul mit einem 1 kOhm WIderstand versehen sind, brauchen wir hier keinen separaten Widerstand.

Über die für den 5 Volt Spannungsregler gedachten Buchse können wir Plus (rot) mit VBB und Masse (schwarz) mit GND verbinden.  