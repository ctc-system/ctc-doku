---
layout: doku
title: "Waggonerkennung konfigurieren"
chapter: 10.1
---
 
Zur Konfiguration der Waggon-Erkennung schließen Sie einen "NFC-Reader" aus dem Produktkatalog "universell-sensoren.xml" an ein CTC-Weichenmodul oder ein CTC-Multi-IO-Board an.

In der Config von CTC-Weichenmodul bzw. CTC-Multi-IO-Board verbinden Sie den NFC-Reader mit dem Anschluss "NFC-Port".
Ist der "NFC-Port" nicht vorhanden, so müssen Sie die Firmware Ihres CTC-Weichenmoduls bzw. CTC-Multi-IO-Boards auf den aktuellen Stand bringen.
Anschließend können Sie die IO-Config ersetzen, siehe dazu ["Kapitel 4 - Module konfigurieren"]({% link _app_doku/040-module-konfigurieren.md %}).
                                      
**Hinweis:** Sie können an CTC-Weichenmodul bzw. CTC-Multi-IO-Board nur **entweder** den zweiten IR-Sender **oder** einen NFC-Reader anschließen,
d.h. bevor Sie den NFC-Reader anschließen müssen Sie **sicherstellen, dass IRPort-2 nicht verwendet wird**.

Anschließend platzieren Sie den NFC-Reader im Gleisplan und geben ihm dabei eine Positions-ID wie bei einer Balise (3 Zeichen).
Wenn der NFC-Reader direkt neben einer Balise platziert wurde, können Sie die Positions-ID der Balise verwenden. 
