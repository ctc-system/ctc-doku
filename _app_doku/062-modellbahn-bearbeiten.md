---
layout: doku
title: "Modellbahn bearbeiten"
chapter: 6.2
---
## Modellbahn (Gesamtplan) bearbeiten

Zum Erstellen eines Gesamtplans wählen Sie sich als erstes ein [CTC-Weichenmodul]({% link _data_sheet/CTC-Weichenmodul-H0.md %}) oder [CTC-Multi-I/O-Board]({% link _data_sheet/CTC-Multi-IO-Board.md %}) aus, auf dem der Gesamtplan gespeichert werden soll.
Öffnen Sie dessen Config-Dialog:

![Schaltkasten bearbeiten]({% link assets/images/ctc-app-doku/0602-edit_switchbox.png %})

Dort wählen Sie **Modellbahn ändern**.
Es öffnet sich folgender Dialog:

![ctc-app-doku-bild]({% link assets/images/ctc-app-doku/ctc-app-doku-Bild23.png %})

Über den Button **Neuer Gleisabschnitt** können Sie einen Gleisabschnitt hinzufügen.
Die Verwendung mehrerer Gleisabschnitte bietet sich sowohl für sehr große Modellbahnen als auch für Modellbahnen mit mehreren Ebenen an.
Ein einzelner Gleisplan kann dabei durchaus auf mehreren Gleisabschnitten vorkommen.

![ctc-app-doku-bild]({% link assets/images/ctc-app-doku/ctc-app-doku-Bild24.png %})

Nun können die links aufgelisteten Gleispläne mithilfe der Pfeil-Buttons in den Gesamtplan aufgenommen werden.

![ctc-app-doku-bild]({% link assets/images/ctc-app-doku/ctc-app-doku-Bild25.png %})

Der jeweils orange umrahmte Gleisplan kann nun noch mit den Button neben **Position Zeile** und **Spalte** verschoben werden.
Die Selektion ändern Sie, indem Sie in den Listen den entsprechenden Gleisplan, anklicken.