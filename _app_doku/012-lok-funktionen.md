---
layout: doku
title: "Lok-Funktionen"
chapter: 1.2
---
Die Lokfunktionen befinden sich in einer Tabelle unterhalb der Loksteuerung:

![ctc-app-doku-bild]({% link assets/images/ctc-app-doku/0102-control_fragment-Fkt.png %})

Welche Funktionen eine Lok bietet, wird in der [Konfiguration der Lok]({% link _app_doku/040-module-konfigurieren.md %}) eingestellt.
Jede dort konfigurierte Funktion zeigt sich mit bis zu 5 Buttons für die verschiedenen Zustände der Funktion.
Der jeweils aktive Zustand wird farbig hervorgehoben.

Das bei den CTC-Lokmodulen ab Werk vorkonfigurierte Fahrlicht (2) kennt folgende Zustände (im Bild von links nach rechts):
* Licht aus
* Lichtautomatik: Je nach Fahrtrichtung brennt das Fahrlicht vorne oder hinten
* Licht vorne an
* Licht hinten an

Der Motor-Mode (1) ist ebenfalls bei allen CTC-Lokmodulen vorkonfiguriert:
* Der linke Button aktiviert die normale Betriebsart.
* Mit dem rechten Button kann die Lastregelung in Form eines PID-Reglers aktiviert werden.
  Voraussetzung hierfür ist, dass der [Motor-Sensor kalibriert]({% link _app_doku/051-sensor-kalibrieren.md %}) wurde.

Das Bild zeigt darüber hinaus Beispiele für über die [Konfiguration der Lok]({% link _app_doku/040-module-konfigurieren.md %}) hinzugefügte Funktionen:
* Mit (3) kann der Entkuppler deaktiviert, sowie vorne und hinten aktiviert werden.
* Mit (4) kann der Fahrsound aus- und eingeschaltet werden.
* Mit (5) können vier unterschiedliche Signalhorn-Sounds abgespielt werden.