---
layout: doku
title: "Gleisbild: Schalten und Statusanzeigen"
chapter: 1.6
---
In den CTC PC-Apps finden Sie das Gleisbild am unteren Fensterrand, auf Tablets und Smartphones als Reiter mit dem Gleis-Symbol.

![Gleisbild]({% link assets/images/ctc-app-doku/Gleisbild.png %})

Wie Sie ein Gleisbild erstellen erfahren Sie im [Kapitel 6 - Gleisbildstellwerk]({% link _app_doku/060-gleisbildstellwerk.md %}).
        
Links in der Ansicht sehen Sie eine Liste aller verfügbaren Gleisabschnitte.
Im Beispiel ist die Modellbahn in eine linke (Oben) und rechte (Unten) Hälfte aufgeteilt.
Die "Hauptebene" setzt beide Hälften zusammen und ist somit für Tablets und PCs geeignet.
"Oben" und "Unten" sind für die schmaleren Smartphone-Displays besser geeignet.  

Alle blau dargestellten Gleisstücke sind mit einer Aktion verbunden.
Dies kann sowohl eine Schaltaktion (Weichen, Signale, ...) als auch ein Sensor (Balise, Block, ...) sein.
  
## Weichen und Signale

Die Symbole von Weichen und Signalen sind identisch mit denen im Schaltpult.
Das Symbol gibt den aktuellen Zustand der Weiche bzw. des Signals wieder.
Ist das Symbol orange gefärbt, so ist es für eine Fahrstraße reserviert und kann nicht geschaltet werden. 

Beim Klick auf eine Weiche oder Signal wird diese zum nächsten Zustand weitergeschaltet, entsprechend der im Schaltpult dargestellten Reichenfolge von links nach rechts.

In der PC-App können Sie mit der rechten Maustaste den [Config-Dialog]({% link _app_doku/040-module-konfigurieren.md %}) des Moduls öffnen, in dem die Weiche bzw das Signal konfiguriert ist.                                                  
        
## Sensoren

Klassische Sensoren zeigen derzeit (noch) keinen Zustand an.

In der PC-App können Sie mit der rechten Maustaste den [Config-Dialog]({% link _app_doku/040-module-konfigurieren.md %}) des Moduls öffnen, in dem der Sensor konfiguriert ist.

## Balisen

Balisen zeigen den Zustand der Balise durch farbige Symbole an.
Der kleine Pfeil in der Mitte des Symbols gibt den Uhrzeigersinn (Pfeilspitze ist Rechts) an.
Für welche Fahrtrichtung das Symbol gilt, erkennt man daran, auf welcher Seite die Bögen eingefärbt sind (in Fahrtrichtung rechts).

| Symbol                                                                                            | Farbe  | Kommandos          | Bedeutung                                                                  |
| ![Balise-Icon inaktiv]({% link assets/images/ctc-app-doku/Balise_straight_inactive.png %})        | grau   |                    | Balise ist nicht zugeordnet oder das zugehörige CTC-Modul ist nicht aktiv. |
| ![Balise-Icon aktiv]({% link assets/images/ctc-app-doku/Balise_straight_active.png %})            | grau   | -                  | Balise ist aktiv, sendet aber kein Kommando an die Lok.                    |
| ![Balise-Icon grün]({% link assets/images/ctc-app-doku/Balise_straight_active_green_R0.png %})    | grün   | 'F', 'f'           | Balise ist aktiv und sendet das Kommando freie Fahrt.                      |
| ![Balise-Icon gelb]({% link assets/images/ctc-app-doku/Balise_straight_active_yellow_R0.png %})   | gelb   | 'W', 'w', 'L', 'l' | Balise ist aktiv und sendet das Kommando zur Geschwindigkeitsbegrenzung.   |
| ![Balise-Icon orange]({% link assets/images/ctc-app-doku/Balise_straight_active_orange_R0.png %}) | orange | 'M', 'm'           | Balise ist aktiv und sendet das Kommando Abbremsen.                        |
| ![Balise-Icon rot]({% link assets/images/ctc-app-doku/Balise_straight_active_red_R0.png %})       | rot    | 'H', 'h', 'S', 's', 'T', 't', 'E', 'e' | Balise ist aktiv und sendet ein Halt-Kommando.         |
| ![Balise-Icon lila]({% link assets/images/ctc-app-doku/Balise_straight_active_purple_R0.png %})   | lila   | 'R', 'r'           | Balise ist aktiv und sendet das Kommando Rückfahrt (ab CTC-App 4.08)       |

Details zu den Kommandos finden Sie im Abschnitt "Kommandos für Loks/Signale" in [Kapitel 4.5 - Skript bearbeiten]({% link _app_doku/045-skript-bearbeiten.md %}).

Darüber hinaus kann eine Balise angeklickt werden, um ihren genauen Zustand zu erfahren:

![Info zu Balise]({% link assets/images/ctc-app-doku/Gleisbild-Balise-Info.png %})

## Blöcke

Blöcke werden als farbig gefüllte Rechtecke dargestellt.
Der kleine Pfeil am Rand des Rechtecks gibt den Uhrzeigersinn (Pfeilspitze ist Rechts) an.

| Symbol                                                                                | Farbe  | Bedeutung                                                                 |
| ![Block-Icon grau]({% link assets/images/ctc-app-doku/block_straight_inactive.png %}) | grau   | Block ist nicht zugeordnet oder das zugehörige CTC-Modul ist nicht aktiv. |
| ![Block-Icon weiß]({% link assets/images/ctc-app-doku/block_free_active.png %})       | weiß   | Block ist aktiv und nicht belegt                                          |
| ![Block-Icon gelb]({% link assets/images/ctc-app-doku/block_reserved_active.png %})   | gelb   | Block ist aktiv und wurde von einer Fahrstraße reserviert                 |
| ![Block-Icon rot]({% link assets/images/ctc-app-doku/block_locked_active.png %})      | rot    | Block ist aktiv und mit einer Lok belegt                                  |

<BR>

Ein Block kann angeklickt werden, um seinen genauen Zustand zu erfahren:

![Info zu Block]({% link assets/images/ctc-app-doku/Gleisbild-Block-Info.png %})
    
Auf dem PC kann per Klick mit der rechten Maustaste ein reservierter Block wieder freigegeben werden:

![Block entsperren]({% link assets/images/ctc-app-doku/Gleisbild-Block-entsperren.png %})
