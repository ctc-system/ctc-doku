---
layout: doku
title: "Statistik"
chapter: 3.5
---
Über das Menü kann der Dialog "Statistik" geöffnet werden.
Dort werden Daten (ab Start der CTC-App) zu allen CTC-Modulen gesammelt:

![Statistik]({% link assets/images/ctc-app-doku/Statistik.png %})

| **Spalte**     | **Bedeutung**                         |
| Name           | Name des CTC-Moduls                   |
| IP-Addr.       | Derzeitige IP-Adresse des CTC-Moduls  |
| Last Msg. Rec. | Zeitstempel (App) zu dem die letzte Nachricht vom CTC-Modul eintraf |
| #Msg Rec.      | Anzahl Nachrichten, die die CTC-App vom CTC-Modul empfangen hat     |
| #Msg.Miss      | Anzahl Sync-Nachrichten, auf die das CTC-Modul nicht geantwortet hat  |
| Avg.Rev.Intv.  | Durchschnittlicher Abstand zwischen zwei Antworten auf die Sync-Nachricht |
| Max.Rec.Intv.  | Maximaler Abstand zwischen zwei Antworten auf die Sync-Nachricht |
| #Reboot        | Anzahl Neustarts des CTC-Moduls |
| Msg.Num        | Laufende Nummer der Nachricht vom CTC-Modul  |
| Dev.Time       | Systemzeit des CTC-Moduls |
| Dev.Heap       | Freier Heap-Speicher auf dem CTC-Modul |
| Sig            | Wifi-Empfangsqualität |

Eine kleine Zahl bei "#Msg.Miss" stellt kein Problem dar.
Wenn die Ausfälle jedoch bei einzelnen oder gar allen CTC-Module häufig sind, sollten Sie sich um die Optimierung Ihres WLANs kümmern.
Ein paar Tipps dazu finden Sie im [Kapitel 3 - "WiFi"]({% link _app_doku/030-wifi.md %}) 