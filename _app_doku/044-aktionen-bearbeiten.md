---
layout: doku
title: "Config - Aktionen bearbeiten"
chapter: 4.4
---
Auf dem Reiter "Aktionen" des Config-Dialogs können sie Aktionen bearbeiten, löschen und hinzufügen:

![Aktionen bearbeiten]({% link assets/images/ctc-app-doku/0404-edit_cfg-action_details.png %})
