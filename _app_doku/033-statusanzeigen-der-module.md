---
layout: doku
title: "Statusanzeigen der Module"
chapter: 3.3
---
Alle Module verfügen über eine Status-Anzeige:
* Gelbe LED bei CTC-Weichenmodul und CTC-Multi-I/O-Board 
* Frontlicht bei Lokomotiven

Der Startvorgang eines CTC-Moduls wird wie folgt angezeigt:
1. Die Anzeige **leuchtet** bis zur initialisierung des WLAN.
  Das geht so schnell, dass es bei einem normalen Start nicht wahrgenommen wird.
1. Solange das WLAN gesucht wird und bis das CTC-Modul erfolgreich eingebucht wurde, **blinkt** die Anzeige.
1. Sobald das CTC-Modul im WLAN eingebucht ist, **erlischt** die Anzeige.

Nach einer Minute erfolglosen Versuchs, das CTC-Modul ins WLAN einzubuchen, schaltet die Anzeige auf **Dauerleuchten** um.
Damit zeigt das CTC-Modul an, dass es sein eigenes WLAN aufgebaut hat und auf Konfiguration wartet.
Siehe dazu [Kapitel 3 - Module ins WLAN einbuchen]({% link _app_doku/031-module-einbuchen.md %}).