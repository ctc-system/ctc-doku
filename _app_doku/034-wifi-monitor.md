---
layout: doku
title: "WiFi-Monitor"
chapter: 3.4
---
Den WiFi-Monitor gibt es ab CTC-App Version 4.15.
Er zeigt die Empfangs-Signalstärke des WLANs einer Lok als Grafik über die Zeit.

Den Wifi-Monitor öffnen Sie über den Button "WiFi-Monitor" der [Lok-Konfiguration]({% link _app_doku/040-module-konfigurieren.md %}) der betreffenden Lok.
                                                                               
Das Beispiel zeigt den Verlauf des WLAN-Signals während einer Runde auf unserer Gartenbahn-Testanlage (siehe Artikel [Automatisierung im Garten]({% link _posts/de/szenarien/2022-08-12-Automatisierung-im-Garten-1.md %})):

![WiFi-Monitor Garten]({% link assets/images/ctc-app-doku/Wifi-Monitor-Garten-1.png %})

Die senkrechte Skala zeigt die Signalstärke in dB an, die waagerechte Skala die Zeit.
Die Beschriftung der Zeitachse ist die Zeit in zehntel Sekunden (x 0,1 s), gefolgt von der zuletzt passierten Balise.
Im Beispiel war die Empfangs-Signalstärke der Lok also nach **23,0 Sekunden (und nach Bailse TS5) -79 dB**.
Zwischen 44,8 Sekunden und 55,3 Sekunden zwischen Balisen TS1 und ST3 gab es zwei kurze **Ausfälle (-100 dB)**.

Das folgende Bild zeigt die Anlage, auf der die Messungen druchgeführt wurden:
* Vorne umkreist der Outdoor-Accesspoint (TP-Link AC1200) an dem die Lok die ganze Zeit eingebucht war
* Hinten mit "A" beschriftet die beiden Stellen, an denen das WLAN kurz ausfällt: Eine leichte Senke und davor Johannisbeerbüsche.
* Der mit "T" beschriftete Pfeil zeigt auf ein ca. 2 m langen Tunnel, der offensichtlich kein Problem darstellt.
  Allerdings ist er auch lediglich mit Betonplatten abgedeckt (kein Erdreich darüber) und in Richtung des Accesspoints offen.
* Die beiden unbeschrifteten waagerechten Pfeile markieren den Gleisverlauf.
  Die Strecke umrundet den Apfelbaum, führt dann entlang der Betonmauer bis an das ca. 15 Meter vom Accesspoint entfernte Ende des Gartens und kommt schließlich durch den Tunnel wieder zurück.
* Die Testfahrt wurde mit der in der Bildmitte links zu sehenden roten BR 251 durchgeführt.

![WiFi-Monitor Garten]({% link assets/images/ctc-app-doku/Wifi-Testanlage.jpg %})

Nach drei Runden sieht das Diagramm wie folgt aus.
Die drei Wiederholungen sind auch grafisch deutlich sichtbar:

![WiFi-Monitor Garten]({% link assets/images/ctc-app-doku/Wifi-Monitor-Garten-3.png %})

**Hinweis:** Wer es ganz genau wissen will, findet die während der Messung übertragenen Daten als "Wifi-XXX.csv" in seinem Benutzerverzeichnis (XXX ist der Name der Lok).
Diese lässt sich z.B. mit Excel oder LibreOffice öffnen und kann dort detailliert ausgewertet werden. 
