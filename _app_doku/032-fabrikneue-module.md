---
layout: doku
title: "Fabrikneue Module"
chapter: 3.2
---
Bei fabrikneuen Schaltmodulen ist lediglich die Firmware eingespielt, aber keine Konfiguration angelegt.
Bei fabrikneuen Lokmodulen sind Motor und Fahrlicht vorkonfiguriert.

Folgende Schritte sind zur Inbetriebnahme notwendig:

Einbuchen ins Modellbahn-WLAN: Dazu in der App den Menüpunkt **Einstellungen/Neue Module** suchen aufrufen und das CTC-Modul ins WLAN einbuchen.
Dabei werden die Daten Ihres WLANs (SSID und Passwort) in die net.cfg auf dem CTC-Modul eingetragen.

**Hinweis:** Nach dem Upload startet sich das CTC-Modul neu, d. h. es dauert einige Sekunden bis es wieder sichtbar wird.