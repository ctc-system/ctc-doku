---
layout: doku
title: "Altes Digital: DCC-Dekoder einbinden"
chapter: 8.1
---
Die meisten CTC-Lokmodule können über ihren Motorausgang ein DCC-Signal erzeugen und sich somit wie eine Mini-Digitalzentrale verhalten.
               
## Lokdekoder mit DCC

Der Umbau einer Lok gestaltet sich recht einfach, wenn denn genügend Platz für das zusätzliche CTC-Modul vorhanden ist:
Der Gleisanschluss des Digitaldekoders wird mit dem Motorausgang des CTC-Lokmoduls verbunden und das CTC-Lokmodul mit dem Gleisanschluss in der Lok.
Der [Artikel "Umbau PIKO BR 50 mit DCC Sound"]({% link _posts/de/szenarien/2021-07-25-Umbau-DCC-BR50.md %}) schildert Umbau und Konfiguration am Beispiel einer Gartenbahn-Lok von PIKO.

Die Konfiguration eines so angeschlossenen DCC-Dekoders ist in [Kapitel 4.6 "Config - DCC-Dekoder anschließen"]({% link _app_doku/046-DCC-dekoder-anschliessen.md %}) beschrieben.

## Weichendekoder mit DCC

Diese Funktionalität ist in Planung - bitte kontaktieren Sie uns bei Bedarf.

## CTC-Lokmodul als DCC-Zentrale

Diese Funktionalität ist in Planung - bitte kontaktieren Sie uns bei Bedarf.

Mit unserem CTC-Lokmodul-G kann eine kleine Modellbahnanlage mit Strom und Digitalsignal versorgt werden.

## Märklin: Motorola-II und mfx 
                               
Eine Unterstützung für Motorola-II (MM2) würde Zugang zu nahezu allen Märklin-Dekodern mit ausreichendem Funktionsumfang bieten.
Insbesondere da neuere Märklin-Dekoder sich auch per DCC mit vollem Funktionsumfang ansprechen lassen.

Die Implementierung von Motorola-II ist in Planung - bitte kontaktieren Sie uns bei Bedarf.

Wenn uns eine passende Bibliothek über den Weg läuft, ist auch Unterstützung für das Märklin mfx-Format denkbar.