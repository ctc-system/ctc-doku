---
layout: doku
title: "Waggonerkennung (Achszähler)"
chapter: 10
---
Ab CTC-App 4.22 kann ein NFC-Reader unters Gleis montiert werden, um mit einem NFC-Tag versehene Waggons zu erkennen.
Damit hat CTC einen adäquaten Ersatz für Achszähler - mit dem Vorteil, dass die App genau weiß welcher Waggon gerade vorbeigefahren ist.

Der [CTC-NFC-Reader]({% link _data_sheet/CTC-NFC-Reader.md %}) kann unter ein Gleis montiert und an ein CTC-Weichenmodul oder CTC-Multi-IO-Board angeschlossen werden. 

Mit einem NFC-Tag versehene Waggons können so eingelesen werden.
Anschließend können sie einem Betriebswerk zugeordnet werden.
Dort wird dann ein Bild und die Daten des Waggons (Name, Länge, ...) hinterlegt.
Ein Betriebswerk kann an jedem CTC-Weichenmodul oder CTC-Multi-IO-Board angelegt werden.