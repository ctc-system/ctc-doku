---
layout: doku
title: "Config - Aktionen verknüpfen"
chapter: 4.3
---
Ein Trigger kann mit einer anderen Aktion verknüpft werden und so auf eine Zustandsänderung dieser Aktion reagieren. 
                                        
Außer den im Folgenden genannten Beispielen finden sie eine stetig wachsende Menge an **Anleitungen im ["Anhang B: Beispiele"]({% link _app_doku/B01-Beispiele.md %})**.
Eine ganz genaue **Schritt-für-Schritt-Anleitung** erhalten Sie im extra **Dokument ["CTC-Starter-Sets"]({% link _data_sheet/CTC-Starter-Sets.md %})**.

## IR-Balise (IR-Sender) mit Signal verknüpfen

Die einfachste Art der Verknüpfung ist der Trigger einer IR-Balise. 
Sie wird bereits bei der Auswahl der IR-Balise aus dem Produktkatalog "universell-sensoren.xml" mit angelegt.
Sie müssen nur noch festlegen, auf welches Signal er reagieren soll und die Entfernung zum Signal eintragen.

Wählen Sie den Trigger der IR-Balise (hier AB3-Trigger):

![Aktionen bearbeiten]({% link assets/images/ctc-app-doku/0403-edit_cfg-IR-Trigger.png %})
                                                         
Klicken Sie dann auf "Auswählen" rechts vom Auslöser.
Es öffnet sich folgender Dialog:

![Aktionen bearbeiten]({% link assets/images/ctc-app-doku/0403-action_picker-Signal.png %})

Wählen Sie das zu verknüpfende Signal (in der Regel in Gruppe "SignalTower") oder Schild und klicken Sie dann auf übernehmen.

Der Name des ausgewählten Signals wird nun bei "Auslöser angezeigt".
Im Feld "auf (MAC)" sollte ein Stern angezeigt werden. 

Im simpelsten Fall lassen sie das vorgefertigte Skript unverändert.
Es übernimmt die Entfernung (cmdDist) aus den Parametern und als Kommando den Wert des Signals.

Im folgenden Beispiel wird für den Zustand 'H' (Halt) des Signals das Kommando 'M' ausgesendet.
In allen anderen Fällen ('*') wird der Zustand des Signals als Kommando übernommen ('?').

![Aktionen bearbeiten]({% link assets/images/ctc-app-doku/0403-edit_cfg-IR-minSpeed.png %})
         
## NFC-Balise mit Aktionen verknüpfen

Um eine NFC-Balise mit Aktionen verknüpfen zu können muss eine Balise angelegt sein.
Da eine NFC-Balise nicht an ein CTC-Modul angeschlossen ist, wird die Balise im Gleisbild gespeichert.
Wie das geht erfahren sie im Kapitel ["Balisen (ID-Sender) im Gleisplan"]({% link _app_doku/065-gleisbild-balisen.md %})

Ist die Balise erst einmal angelegt, kann zur NFC-Balise über den Button "Automatisierung ändern" ähnlich wie bei der IR-Balise der bereits angelegte Trigger bearbeitet werden:  

![Trigger NFC-Balise]({% link assets/images/ctc-app-doku/0403-edit_automation-NFC-Trigger.png %})

Klicken Sie dann auf "Auswählen" rechts vom Auslöser.
Es öffnet sich folgender Dialog:

![Aktionen bearbeiten]({% link assets/images/ctc-app-doku/0403-action_picker-Sign.png %})

Wählen Sie das zu verknüpfende Signal oder Schild (hier ein Schild) und klicken Sie dann auf übernehmen.

Der Name des ausgewählten Schilds wird nun bei "Auslöser angezeigt".
Im Feld "auf (MAC)" sollte ein Stern angezeigt werden.

Im simpelsten Fall lassen sie das vorgefertigte Skript unverändert.
Es übernimmt die Entfernung (cmdDist) aus den Parametern und als Kommando den Wert des Schilds.

![Trigger NFC-Balise Details]({% link assets/images/ctc-app-doku/0403-edit_cfg-trigger_details-NFC.png %})
                           
**Hinweis:** Befindet sich der Auslöser für einen Trigger auf demselben CTC-Modul wie der Trigger selbst, so kümmert sich das CTC-Modul direkt um die Abarbeitung des Auslösers.
Ansonsten kümmert sich die CTC-App darum, den entsprechenden Trigger auszulösen. 

Dem [Bearbeiten von Skripten]({% link _app_doku/045-skript-bearbeiten.md %}) ist ein eigenes Kapitel gewidmet. 
