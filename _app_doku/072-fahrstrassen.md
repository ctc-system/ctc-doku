---
layout: doku
title: "Automatisierung: Fahrstraßen"
chapter: 7.2
---
Eine Fahrstraße stellt eine Verbindung zwischen zwei Blöcken her.
Dazu werden im zugehörigen Skript alle dazwischen liegenden Weichen passend gestellt.

## Fahrstraßen konfigurieren

Eine Fahrstrasse wird jeweils bei dem Block angelegt, an dem sie beginnt.
       
Dazu wählen Sie im Dialog "Automatisierung bearbeiten" einen Block aus und klicken auf den Plus-Button.

![Fahrstrasse am Block anlegen]({% link assets/images/ctc-app-doku/Block-Fahrstrasse-Neu.png %})
                               
Die neu angelegte Fahrstraßen-Gruppe enthält ein Skript für "Wechsel auf off" (keine Fahrstraße aktiv) und ein zweites Skript für die neue Fahrstraße.
Wählen Sie die zweite Fahrstraße (Skript) und klicken Sie dann auf den Button "..." unter "End-Block", um das Ende der Fahrstraße auszuwählen:

![End-Block der Fahrstrasse auswählen]({% link assets/images/ctc-app-doku/Block-Fahrstrasse-End-Block.png %})

Dann klicken Sie auf den Button "Hinzufügen", um den ersten Schaltbefehl ("call") der Fahrstraße festzulegen:

![End-Block der Fahrstrasse auswählen]({% link assets/images/ctc-app-doku/Block-Fahrstrasse-Neuer-Befehl.png %})

Nach klick auf "Übernehmen" wechselt das Skript-Fenster auf den neu angelegten Schaltbefehl.
Dann klicken Sie auf den Button "..." unter "Führe Aktion aus" um festzulegen, was geschaltet werden soll.

![End-Block der Fahrstrasse auswählen]({% link assets/images/ctc-app-doku/Block-Fahrstrasse-Aktion-waehlen.png %})
                                 
Die Auswahlliste unter "Parameter" wird automatisch mit den von der gewählten Aktion erlaubten Werten belegt.
Für die Weiche im Beispiel wird nun "straight" (gerade) gewählt:

![End-Block der Fahrstrasse auswählen]({% link assets/images/ctc-app-doku/Block-Fahrstrasse-Aktion-param.png %})
                                             
Nach Hinzufügen eines zweiten Schaltbefehls sieht die Fahrstraße wie folgt aus:

![Fahrstrasse am Block fertig]({% link assets/images/ctc-app-doku/Block-Fahrstrasse.png %})

**Hinweis:** Details zum Bearbeiten von Skripten finden Sie im [Kapitel "Config - Skript bearbeiten"]({% link _app_doku/045-skript-bearbeiten.md %}).
           
## Weitere Fahrstraßen 

Weitere Fahrstraßen können Sie entweder als neue Fahrstraßen-Gruppe am jeweiligen Block oder als neues Skript unter einer bereits angelegten Fahrstraßen-Gruppe erstellen.

Dabei bietet es sich an, alle von einem Block in eine Richtung abgehenden Fahrstraßen als Skripte in einer gemeinsamen Fahrstraßen-Gruppe anzulegen, z.B. so für die Einfahrt in die drei Gleise des Bahnhofs "B":

![Gruppe mehrerer Fahrstrassen]({% link assets/images/ctc-app-doku/Block-Fahrstrasse-Mehrere.png %})

Die zugehörige Zeile im Schaltpult sieht dann so aus:

![Mehrere Fahrstrassen im Schaltpult]({% link assets/images/ctc-app-doku/Schaltpult-Fahrstrasse-mehrere.png %})
