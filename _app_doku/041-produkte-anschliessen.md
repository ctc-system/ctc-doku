---
layout: doku
title: "Config - Produkte anschließen"
chapter: 4.1
---
Der einfachste Weg CTC-Module in Betrieb zu nehmen geht über die Funktion "Produkt anschließen".
Diese erreichen Sie über den Button "Config ändern" der Lok- bzw. Schaltkasten-Konfiguration (siehe vorherige Seite). 
 
![Konfiguration ändern (leer)]({% link assets/images/ctc-app-doku/0401-edit_cfg-empty.png %})

Die hier dargestellte Konfiguration wird auf dem CTC-Modul in zwei Dateien abgelegt:
* In der Config (cfg.xml) werden die, über die App steuerbaren Funktionen wie Licht, Entkuppler, Weichen, Signale, ... definiert.
* In der IO-Config (ioCfg.xml) werden die, für das CTC-Modul konfigurierten Ein- & Ausgabepins und Schnittstellen definiert.
  Da diese auf dem CTC-Modul fest verbaute Teile beschreiben, können sie über den Dialog in der App bewusst nicht geändert werden.

Der Inhalt der IO-Config wird unter der Überschrift "CTC-Modul: Pins, Ports, Chips" angezeigt.
Alles anderen angezeigten Daten entstammen der Config. 

## Produkt hinzufügen

Um ein Produkt hinzuzufügen, klicken Sie auf das Plus-Symbol rechts von der Tabelle "Angeschlossene Produkte".
Es öffnet sich eine Liste der mit der CTC-App mitgelieferten Produktkataloge. 

![Auswahl des Produktkatalogs]({% link assets/images/ctc-app-doku/0401-select_asset_file.png %})

Sie können einen der mitgelieferten Kataloge auswählen oder über den Button "Aus Dateisystem auswählen" einen Produktkatalog vom Dateisystem laden.
Hier wurde der mitgelieferte Katalog "universell-weichen.xml" ausgewählt und dann auf "Übernehmen" geklickt:

![Auswahl eines Produkts]({% link assets/images/ctc-app-doku/0401-select_product.png %})
          
Wählen Sie nun das passende Produkt aus.
Bei einigen Produkten können Sie auch noch aus unterschiedlichen Konfigurationen auswählen.

Dann geben Sie noch einen Namen für das angeschlossene Produkt an und klicken auf "Übernehmen".

Nun müssen Sie nur noch angeben, welche Anschlüsse des Produkts mit welchen Pins bzw. Ports des CTC-Moduls verbunden sind:

![Verbinden der Pins eines Produkts]({% link assets/images/ctc-app-doku/0401-edit_cfg-connect.png %})
             
1. Das gerade hinzugefügte Produkt muss in der Tabelle "Angeschlossene Produkte" ausgewählt sein.
   Dann wählen Sie in der Tabelle "Pins, Port und Erweiterungen" den entsprechenden Anschluss des CTC-Moduls.
2. Mit dem Button "Testen" können Sie prüfen, welcher Pin des angeschlossenen Produkts am gewählten Anschluss des CTC-Moduls hängt. 
3. Dann wählen Sie den durch den Test ermittelten Pin in der Tabelle "Anschlüsse und Parameter" aus.
4. Durch Klick auf den Knopf "Verbinden" wird der Pin des Produkts logisch mit dem Anschluss des CTC-Moduls verbunden.

Nun sehen Sie die erstellte Verbindung in den beiden Tabellen "Pins, Port und Erweiterungen" und "Anschlüsse und Parameter": 

![Pin verbunden]({% link assets/images/ctc-app-doku/0401-edit_cfg-connected.png %})
 
Obige vier Schritte wiederholen Sie, bis alle Anschlüsse des Produkts verbunden sind.

**ACHTUNG:** Die CTC-App passt darauf auf, dass die Anschlüsse des Produkts nur mit passenden Pins oder Ports verbunden werden können.
Wenn Sie allerdings ein falsches Produkt ausgewählt haben, kann dies durchaus zu Beschädigungen an CTC-Modul und/oder Produkt führen.
Hier sind vor allem die elektromagnetische Antriebe zu erwähnen, wie sie z.B. in Märklin-Weichen (deren Endabschalter nicht funktioniert) verwendet werden. 
Werden diese nicht als Impuls (Pulse), sondern als Schalter (Switch) angeschlossen, sind sie nach wenigen Sekunden kaputt.  

## Beispiel-Config eines CTC-Multi-I/O-Boards
                        
So sieht die Konfiguration des CTC-Multi-I/O-Boards unserer Demo-Anlage aus: 

![ctc-app-doku-bild]({% link assets/images/ctc-app-doku/0401-edit_cfg.png %})
