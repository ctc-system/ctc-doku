---
layout: doku
title: "Gleisbildstellwerk"
chapter: 6
---

![ctc-app-doku-bild]({% link assets/images/ctc-app-doku/ctc-app-doku-Bild17.png %})

Das Gleisbildstellwerk ermöglicht das Schalten von Weichen über eine schematische Darstellung der Modellbahn.
Dabei wird die Modellbahn aus einzelnen (Teil-) Gleisplänen aufgebaut.
Die Gleispläne sowie der Gesamtplan werden jeweils in einem [CTC-Weichenmodul]({% link _data_sheet/CTC-Weichenmodul-H0.md %}) oder einem [CTC-IO-Modul]({% link _data_sheet/CTC-Multi-IO-Board.md %}) gespeichert.

Somit kann für jedes zusammenhängende Teil einer Modellbahn (Modul, Segment) ein Gleisplan erstellt und in auf diesem Teil abgelegt werden. 

Der Gesamtplan und dessen Bestandteile werden automatisch eingelesen, sobald sich das entsprechende CTC-Modul meldet.
              
Wie Sie das Gleisbildstellwerk benutzen und was die dort dargestellten Symbole bedeuten, erfahren Sie im [Kapitel 1.6 - Gleisbild: Schalten und Statusanzeigen]({% link _app_doku/016-gleisbild.md %}).

Bevor ein Gesamtplan erstellt werden kann, müssen die einzelnen (Teil-) Gleispläne erstellt werden.

Um herauszufinden, auf welchen Modulen bereits ein Gleisplan oder ein Gesamtplan existiert, öffnen Sie den **Konfigurator** über das Menü **Einstellungen/Konfigurator**.
Dort sind in der Device-Liste entsprechende Kreuze gesetzt.
