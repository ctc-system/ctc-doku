---
layout: doku
title: "Kabelfarben"
chapter: A4
---
Leider gibt es eine ganze Reihe von Normen und Hersteller-Konventionen bzgl. farblicher Kennzeichnung von Kabeln und Steckern.
In diesem Kapitel treffen wir eine **Festlegung aus CTC-Sicht** für Kabel, die Sie an CTC-Modulen und in von uns umgebauten Modulen vorfinden.

**Hinweis:** An diese Farbregeln halten wir uns ab **Juli 2021**.
Leider haben wir uns zu spät Gedanken um Kabelfarben gemacht, d.h. bei allem, das von uns vor Juli 2021 geliefert wurde, können Sie sich nicht darauf verlassen, dass die Kabelfarben einer Systematik folgen!
                    
Aus **unserer** Sicht wird folgende Zuordnung allgemein erwartet:
- Minuspol: schwarz oder blau
- Pluspol: rot

Deshalb haben wir uns gegen die NEM-Verwendung von blau für den Pluspol in der Lok entschieden.
Andere Farbnormen finden Sie hier:

Im Folgenden verstehen wir unter "Markierung" die Farbe eines Steckers oder eines Schrumpfschlauchs am Ende des Kabels.

## Einzelne Kabel

### Lokeinbau

| Kabelfarbe                                                    | Markierung                                                | Beschreibung                                  | Polarisierung           | Norm             |
|---------------------------------------------------------------|-----------------------------------------------------------|-----------------------------------------------|-------------------------|------------------|
| ![braun]({% link assets/images/color/brown.png %}) braun      | -                                                         | Gleis links / beide Gleise                    | undefiniert oder Masse  | Märklin          |
| ![rot]({% link assets/images/color/red.png %}) rot            | -                                                         | Gleis rechts / Mittelleiter                   | undefiniert oder Phase  | NEM 658, Märklin |
| ![schwarz]({% link assets/images/color/black.png %}) schwarz  | -                                                         | CTC-Modul bzw. Dekoder Minuspol               | minus (GND)             | -                |
| ![orange]({% link assets/images/color/orange.png %}) orange   | -                                                         | CTC-Modul bzw. Dekoder Pluspol                | plus (VBB)              | Märklin          |
| ![grün]({% link assets/images/color/green.png %}) grün        | -                                                         | Motor links                                   | wechselnd               | Märklin          | 
| ![grün]({% link assets/images/color/green.png %}) grün        | ![weiß]({% link assets/images/color/white.png %}) weiß    | AUX high-side                                 | plus (VBB) geschaltet   | -                | 
| ![grau]({% link assets/images/color/grey.png %}) grau         | -                                                         | Motor rechts                                  | wechselnd               | NEM 658          |
| ![grau]({% link assets/images/color/grey.png %}) grau         | ![weiß]({% link assets/images/color/white.png %}) weiß    | Licht vorne high-side                         | plus (VBB) geschaltet   | -                |
| ![grau]({% link assets/images/color/grey.png %}) grau         | ![gelb]({% link assets/images/color/yellow.png %}) gelb   | Licht hinten high-side                        | plus (VBB) geschaltet   | -                |
| ![violett]({% link assets/images/color/purple.png %}) violett | -                                                         | AUX low-side                                  | minus (GND) geschaltet  | AUX 2: NEM 658   |
| ![weiß]({% link assets/images/color/white.png %}) weiß        | -                                                         | Licht vorne low-side                          | minus (GND) geschaltet  | NEM 658          |
| ![gelb]({% link assets/images/color/yellow.png %}) gelb       | -                                                         | Licht hinten low-side                         | minus (GND) geschaltet  | NEM 658, Märklin |

### Modellbahn-Anlage

Weichen-Module, IO-Boards, Signale, ...

| Kabelfarbe                                                   | Markierung                                             | Beschreibung                                       | Polarisierung                    | Norm          |
|--------------------------------------------------------------|--------------------------------------------------------|----------------------------------------------------|----------------------------------|---------------|
| ![braun]({% link assets/images/color/brown.png %}) braun     | -                                                      | Gleis links / beide Gleise                         | undefiniert oder Masse           | Märklin       |
| ![rot]({% link assets/images/color/red.png %}) rot           | -                                                      | Gleis rechts / Mittelleiter                        | undefiniert oder Phase           | NEM 658, Märklin |
| ![grün]({% link assets/images/color/green.png %}) grün       | -                                                      | Weiche mit Umpolung bzw. motorisch: Antrieb links  | wechselnd                        | Märklin       | 
| ![grau]({% link assets/images/color/grey.png %}) grau        | -                                                      | Weiche mit Umpolung bzw. motorisch: Antrieb rechts | wechselnd                        | NEM 658       |
| ![schwarz]({% link assets/images/color/black.png %}) schwarz | -                                                      | Weiche/Licht: Minuspol                             | minus (GND)                      |               |
| ![gelb]({% link assets/images/color/yellow.png %}) gelb      | -                                                      | Weiche/Licht: Spannungsversorgung / Pluspol        | plus (VBB)                       | Märklin       |
| ![blau]({% link assets/images/color/blue.png %}) blau        | ![grün]({% link assets/images/color/green.png %}) grün | Weiche: Masse Antrieb Stellung gerade              | minus (GND) geschaltet           | Märklin       |
| ![blau]({% link assets/images/color/blue.png %}) blau        | ![grün]({% link assets/images/color/green.png %}) grün | Flügelsignal: Masse Antrieb Stellung grün          | minus (GND) geschaltet           | Märklin       |
| ![blau]({% link assets/images/color/blue.png %}) blau        | ![rot]({% link assets/images/color/red.png %}) rot     | Weiche: Masse Antrieb Stellung abbiegen            | minus (GND) geschaltet           | Märklin       |
| ![blau]({% link assets/images/color/blue.png %}) blau        | ![rot]({% link assets/images/color/red.png %}) rot     | Flügelsignal: Masse Antrieb Stellung rot           | minus (GND) geschaltet           | Märklin       |
| ![weiß]({% link assets/images/color/white.png %}) weiß       | -                                                      | Signalleitung oder LED                             | Prozessor plus (3,3V) geschaltet |               |

## Mehradrige Kabel 

### IR-Balise

Am Weichenmodul 4-adriges Kabel (weiss,braun,grün,gelb) mit Buchsenleiste im 2,54mm-Raster.
An der IR-Balise (Gleis) 2-adriges Kabel mit Stiftleiste im 2,54mm-Raster.

| Kabelfarbe                  | Markierung                                                | Beschreibung                 | Polarisierung           | Norm             |
|-----------------------------|-----------------------------------------------------------|------------------------------|-------------------------|------------------|
| braun (IR-1) / grün (IR-2)  | ![grün]({% link assets/images/color/green.png %}) grün    | IR-Balise: Minuspol (TX-GND) | TX-Signal               | -                |
| weiss (IR-1) / gelb (IR-2)  | ![rot]({% link assets/images/color/red.png %}) rot        | IR-Balise: Pluspol (PWM-VCC) | PWM-Signal              | -                |

### IR-Empfänger in Lok

3-adriges Kabel mit gewinkelter 8-fach-Stiftleiste im 1,27 mm Raster.

| Kabelfarbe                                                    | Markierung                                          | Beschreibung                      | Polarisierung     | Norm             |
|---------------------------------------------------------------|- ---------------------------------------------------|-----------------------------------|-------------------|------------------|
| ![schwarz]({% link assets/images/color/black.png %}) schwarz  | ![rot]({% link assets/images/color/red.png %}) rot  | IR-Empfänger: Minuspol (GND)      | minus (GND)       | -                |
| ![rot]({% link assets/images/color/red.png %}) rot            | -                                                   | IR-Empfänger: Pluspol (VCC, 3,3V) | plus 3,3V (VCC)   | -                |
| ![lila]({% link assets/images/color/purple.png %}) lila       | -                                                   | IR-Empfänger: TXD                 | Serielle Daten    | -                |

### NFC-Reader
       
Am NFC-Reader angelötete Kabel.

| Kabelfarbe                                                    | Markierung           | Beschreibung                    | Polarisierung           | Norm             |
|---------------------------------------------------------------|----------------------|---------------------------------|-------------------------|------------------|
| ![schwarz]({% link assets/images/color/black.png %}) schwarz  | -                    | NFC-Reader: Minuspol (GND)      | minus (GND)             | -                |
| ![rot]({% link assets/images/color/red.png %}) rot            | -                    | NFC-Reader: Pluspol (VCC, 3,3V) | plus 3,3V (VCC)         | -                |
| ![grün]({% link assets/images/color/green.png %}) grün        | -                    | NFC-Reader: TXD                 | Serielle Daten          | -                |
| ![blau]({% link assets/images/color/blue.png %}) blau         | -                    | NFC-Reader: RXC                 | Serielle Daten          | -                |

<BR>

**Anmerkungen**:

* Bitte melden Sie sich bei Unklarheiten - das ist in der Regel billiger als reparieren.
* Ergänzungen die aus Ihrer Sicht fehlen nehmen wir gerne entgegen.

## Sonstige Links zu Kabelfarben

Hier einige Links, die wir uns angeschaut und auf deren Basis wir unsere Farben definiert haben.            

Normen:
* [NEM 658 - Elektrische SchnittstellePluX12 / 16 / 22](https://www.morop.eu/downloads/nem/de/nem658_d.pdf) 
* [NEM 650 - Elektrische Schnittstellenfür Modellfahrzeuge](https://www.morop.eu/downloads/nem/de/nem650_d.pdf)
* [NEM 605 - Anlagenverdrahtung](https://www.morop.eu/downloads/nem/de/nem605_d.pdf)

Märklin-Sammler-Infos.de (Peter Roland):
* [Decoder-Kabelfarben DCC-Norm, ESU und Märklin](http://www.maerklin-sammler-infos.de/tipps/tipps/verkabelung/kabeldecoderfarben.htm)
* [Kabel- und Steckerfarben für die Verkabelung](http://www.maerklin-sammler-infos.de/tipps/tipps/verkabelung/kabel_steckerfarben.htm)
  
Der Moba (Wiki):
* [Farbkodierung Kabel](https://www.der-moba.de/index.php/Farbkodierung_Kabel)
