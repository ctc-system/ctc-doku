---
layout: doku
title: "Config - Aktionen"
chapter: 4.2
---
Aktionen sind der Kern der Steuerungsfunktionen von CTC.
Mit ihnen legen Sie sowohl fest, welche Knöpfe und Regler wo in der CTC-App sichtbar werden, als auch was automatisch passieren soll.

**Hinweis:** Wenn Sie, wie im Kapitel zuvor beschrieben, ein Produkt angeschlossen haben, wurden auch schon passende Aktionen angelegt.
Teilweise müssen diese aber noch fertig konfiguriert werden, z.B. ist bei einer IR-Balise bereits ein Trigger zur Verknüpfung mit einem Signal vorhanden, das Signal muss aber noch als "Auslöser" ausgewählt werden. 

![Config: Aktionen]({% link assets/images/ctc-app-doku/0402-edit_cfg-actions.png %})

Es gibt folgende Arten von Aktionen:
* Funktion: Zeigt sich als Buttons in der CTC-App, z.B. um Weichen und Signale zu stelen oder Lok-Funktionen zu aktivieren.
* Wert-Regler: Zeigt sich als Plus/Minus-Buttons und Schieberegler in der CTC-App um einen numerischen Wert zu verstellen.
* Trigger: Damit kann die CTC-App oder das CTC-Modul auf die Änderung einer beliebigen anderen Aktion reagieren, z.B. kann ein Signal auf rot gehen, nachdem eine Weiche gestellt wurde.
* Sensor: Diese Aktion ist im CTC-Modul mit einem Sensor, z.B. einem Kontaktgleis verbunden.
  Der Wert des Sensors wird in der CTC-App angezeigt und kann (wie alle Aktionen) einen Trigger anstoßen. 
* Timer: Diese Aktion dient dazu Dinge regelmäßig oder nach einer bestimmten Zeit zu tun, z.B. ist unsere IR-Balise ein Timer, der zyklisch eine Infrarot-Nachricht sendet. 
           
Jede Aktion hat einen Zustand in Form eines Buchstaben oder einer Zahl.
Dieser Zustand kann im Trigger genutzt werden, um nur auf bestimmte Zustandsänderungen zu reagieren.

Wenn Sie ein Produkt hinzufügen, werden übrigens auch gleich passende Aktionen mit angelegt.

**Hinweis:** Aktionen, die keinem CTC-Modul zugeordnet werden können, bearbeiten Sie über den Button "Automatisierung ändern".
Dies sind z.B. die Trigger einer NFC-Balise. 

## Funktion

![Config: Funktion]({% link assets/images/ctc-app-doku/0402-edit_cfg-enum_details.png %})

Eine Funktion ist eine Sammlung zusammengehörender Taster bzw. Schalter.
Jedem dieser Taster ist ein Skript (siehe [Kapitel 4.5 - Skript bearbeiten]({% link _app_doku/045-skript-bearbeiten.md %})) zugeordnet, das ausgeführt wird, wenn dieser Taster gedrückt wird.

So hat z.B. eine Weiche zwei Taster, einer für "geradeaus" und einen für "abbiegen".
Bei Drücken auf den Taster führt das entsprechende CTC-Modul das zugehörige Skript aus.
Im Falle unserer Weiche wird dann beispielsweise ein 250ms lange Impuls auf den Weichenantrieb gegeben und so die Weiche gestellt.

Die Funktion meldet immer ihren zuletzt aufgerufenen Taster als Zustand.
Das bietet dann die Basis um mithilfe eines Triggers Folgeaktionen auslösen zu können.

Im Unterschied zu den meisten Modellbahnsteuerungen darf unsere Funktion aber auch aus mehr als zwei Tastern bestehen.

## Wert-Regler

![Config: Wert-Regler]({% link assets/images/ctc-app-doku/0402-edit_cfg-range_details.png %})

Mit einem Wert-Regler kann ein numerischer Wert verstellen, z.B. ein Servo auf einen bestimmten Winkel gestellt werden.
Der offensichtlichste Wert-Regler ist der Geschwindigkeitsregler einer Lokomotive.

Der Zustand eines Wert-Reglers ist der zuletzt eingestellte Wert.

## Trigger

![Config: Trigger]({% link assets/images/ctc-app-doku/0402-edit_cfg-trigger_details.png %})

Ein Trigger ist einer Funktion sehr ähnlich, wird aber nicht durch einen Taster ausgelöst, sondern durch die Zustandsänderung einer anderen Aktion.

Wie die Funktion besteht der Trigger aus mehreren Skripten (siehe [Kapitel 4.5 - Skript bearbeiten]({% link _app_doku/045-skript-bearbeiten.md %})), die abhängig vom neuen Zustand der auslösenden Aktion ausgeführt werden.

Ist der Trigger auf demselben CTC-Modul abgelegt wie die auslösende Aktion, so wird dieser Trigger direkt vom CTC-Modul angestoßen.
Ansonsten kümmert sich die CTC-App darum, den Trigger anzustoßen.

## Timer

![Config: Timer]({% link assets/images/ctc-app-doku/0402-edit_cfg-timer_details.png %})

Ein Timer kann entweder regelmäßig (zyklisch) ausgeführt werden oder nur einmalig.

Der Timer führt, nachdem seine Zeit abgelaufen, sein Skript (siehe [Kapitel 4.5 - Skript bearbeiten]({% link _app_doku/045-skript-bearbeiten.md %})) aus.
So können Sie z.B. motorische Weichen ansteuern, indem Sie im Skript einer Funktion den Motor einschalten und einen Timer aufrufen, der dann nach vorgegebener Zeit (z.B. 5 Sekunden) den Motor wieder ausschaltet.  

Der IR-Sender ist ein Sonderfall: Er hat kein Skript und es wird (fest einprogrammiert) regelmäßig eine Nachricht per Infrarot gesendet.
Die Nachricht wird aus dem Namen der IR-Sennders und den vorgegebenen Parametern dist, cmd und cmdDist zusammengesetzt.
Hier empfehlen wir das Produkt "IR-Balise" aus dem Produktkatalog "universell-sensoren.xml" hinzuzufügen.

## Sensor

![Config: Sensor]({% link assets/images/ctc-app-doku/0402-edit_cfg-sensor_details.png %})

Ein Sensor wird mit einem Eingangspin des CTC-Moduls (aktuell nur Multi-I/O-Board) verknüpft wird.
Der Sensor gibt dann den Zustand des Eingangspins als seinen Zustand weiter.

Mithilfe eines Triggers kann nun eine Folgeaktion gestartet werden.
 