---
layout: doku
title: "Lokomotive: Autopilot"
chapter: 1.3
---
Im Unterschied zu klassischen Digital-Systemen steuert sich bei CTC die Lok immer selbst.
Über die CTC-App werden lediglich Sollwerte für die Geschwindigkeit vorgegeben.

Für jede Lok mit einem ID-Reader ist auch bei manueller Steuerung schon ein bisschen Automatik vorhanden:
* Überschreitet der Lokführer die zulässige Höchstgeschwindigkeit, so reduziert die Lok die Geschwindigkeit selbständig.
* Missachtet der Lokführer ein rotes Signal, so führt die Lok eine Zwangsbremsung durch.

Da die Lok selbst nicht sehen kann, müssen Geschwindigkeitsbegrenzungen und Signalzustände der Lok mithilfe von Balisen mitgeteilt werden.
Wer sich schon mal mit dem europäischen [Zugsicherungssystem ETCS](https://de.wikipedia.org/wiki/European_Train_Control_System) beschäftigt hat, wird erstaunliche Parallelen zu CTC feststellen. 

Über diese einfachen Zugsicherungsmaßnahmen hinaus besteht auch die Möglichkeit, einen Autopiloten zu aktivieren.

![ctc-app-doku-bild]({% link assets/images/ctc-app-doku/0103-control_fragment-autopilot.png %})

Der Autopilot kennt folgende Betriebsarten:
1. Fahrt mit vorgegebener Maximalgeschwindigkeit unter Beachtung von Geschwindigkeitsbegrenzungen und Signalen.
2. Fahrt nach Fahrplan

Bevor mit Autopilot gefahren wird, sollte die Lok wissen in welcher Orientierung sie auf dem Gleis steht.
Zeigt das Symbol auf dem Button (12) ein "r" (im Uhrzeigersinn) oder "l" (gegen Uhrzeigersinn) so kenn die Lok ihre Orientierung.

Durch Klick auf dem Button (12) können Sie die Orientierung einstellen bzw. ändern.

**Hinweis:** Wenn die Lok zwei Balisen desselben Blocks passiert hat, kennt sie ihre Orientierung.
Der Button zeigt diese dann an. 

## Fahrt mit vorgegebener Maximalgeschwindigkeit

Diese Betriebsart wird aktiviert, indem man die Lok manuell auf die gewünschte Maximalgeschwindigkeit einstellt und dann den Autopiloten (13) aktiviert.
Danach fährt die Lok selbständig weiter bis der Autopilot wieder beendet wird.

Die Lok fährt nie schneller als die bei der Aktivierung des Autopiloten eingestellte Geschwindigkeit:
* Wird der Lok über eine Balise eine Geschwindigkeitsbegrenzung mitgeteilt, so reduziert sie die Geschwindigkeit.
  Wird die Geschwindigkeitsbegrenzung wieder aufgehoben, so beschleunigt die Lok auf die eingestellte Maximalgeschwindigkeit.
* Wird der Lok über eine Balise ein rotes Signal mitgeteilt, so hält die Lok selbständig vor dem Signal.
  Schaltet das Signal wieder auf Grün, so nimmt die Lok selbständig die Fahrt wieder auf.
* Wird der Lok über eine Balise ein Aufenthalt mitgeteilt, so hält die Lok für die von der Balise mitgeteilte Zeit an.
  Danach setzt die Lok die Fahrt wieder fort.
* Wird der Lok über eine Balise ein Wendepunkt mitgeteilt, so hält die Lok für die von der Balise mitgeteilte Zeit an.
  Danach setzt die Lok die Fahrt in entgegengesetzter Richtung wieder fort.

## Fahrt nach Fahrplan
     
Die Fahrt nach Fahrplan wird aktiviert, indem man bei einer stehenden Lok den Autopiloten (13) aktiviert.
Anschließend startet man einen Fahrplan, der in dem Block beginnt, indem die Lok gerade steht.
Dann sendet die CTC-App den Fahrplan an die Lok und diese beginnt den Fahrplan abzuarbeiten.

Ein Fahrplan besteht aus einer chronologischen Reihenfolge von Balisen, die die Lok passieren muss.
Dabei kann zu jeder Balise ein Kommando mitgegeben werden, das die Lok ausführt, sofern es nicht im Widerspruch zu dem von der Balise ausgesendeten Kommando steht.
D.h. auch in dieser Betriebsart werden von Balisen ausgesendete Geschwindigkeitsbegrenzungen und Signalzustände berücksichtigt.

## Lok-Zustände (11)

Der Zustand (11) der Loksteuerung wird in der Zeile unter dem Loksymbol angezeigt.
Folgende Zustände kann die Lok einnehmen:

| Symbol                                                                     | Name       | Bedeutung                                                                                                                                                      |
|----------------------------------------------------------------------------|------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------|
| ![Idle]({% link assets/images/ctc-app-doku/loco_lock_open.png %})          | Frei       | Die Lok steht und ist frei für manuelle Steuerung.                                                                                                             |
| ![Manuell]({% link assets/images/ctc-app-doku/loco_lock_me.png %})         | Manuell    | Die Lok wird von Ihnen manuell gesteuert.                                                                                                                      |
| ![Other]({% link assets/images/ctc-app-doku/loco_lock_other.png %})        | Anderer    | Die Lok wird von einer anderen Person bzw. einer anderen CTC-App aus manuell gesteuert.                                                                        |
| ![Halt]({% link assets/images/ctc-app-doku/loco_lock_halt.png %})          | Halt       | Die Lok hat ein rotes Signal erkannt und bremst noch oder steht bereits vor diesem Signal. Die Lok fährt weiter, sobald das Signal die Fahrt wieder frei gibt. |
| ![Limit]({% link assets/images/ctc-app-doku/loco_lock_limit.png %})        | Limit      | Die Lok hat eine Geschwindigkeitsbegrenzung erkannt und drosselt ggf. die Geschwindigkeit.                                                                     |
| ![Stop]({% link assets/images/ctc-app-doku/loco_lock_stop.png %})          | Stop       | Die Lok bremst, um an einem Bahnhof anzuhalten.                                                                                                                |
| ![Reverse]({% link assets/images/ctc-app-doku/loco_lock_reverse.png %})    | Rückfahrt  | Die Lok bremst, um an einem Wendebahnhof anzuhalten. Anschließend fährt Sie in die andere Richtung weiter.                                                     |
| ![Pause]({% link assets/images/ctc-app-doku/loco_lock_pause.png %})        | Pause      | Die Lok hat mit "Stop" an einem Bahnhof angehalten und wird nach Ablauf des Aufenthalts weiter fahren.                                                         |
| ![PauseRev]({% link assets/images/ctc-app-doku/loco_lock_pauserev.png %})  | Pause-Rück | Die Lok hat mit "Rückfahrt" an einem Bahnhof angehalten und wird nach Ablauf des Aufenthalts weiter fahren.                                                    |
| ![Continue]({% link assets/images/ctc-app-doku/loco_lock_continue.png %})  | Fortsetzen | Die Lok setzt Ihre Fahrt nach "Stop" oder "Rückfahrt" fort. Hat sie fertig beschleunigt wechselt sie zu "Autopilot".                                           |
| ![Drive]({% link assets/images/ctc-app-doku/loco_lock_drive.png %})        | Autopilot  | Die Lok fährt automatisch ohne Geschwindigkeitsbegrenzung.                                                                                                     |
                 

