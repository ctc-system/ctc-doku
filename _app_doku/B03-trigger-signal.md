---
layout: doku
title: "Config-Beispiel: Signal abhängig von Weichenstellung"
chapter: B.03
---
In diesem Beispiel wird ein Trigger dazu verwendet, ein Signal auf Rot zu stellen, wenn die zugehörige Weiche den Fahrweg schließt.

Ausgangspunkt für dieses Beispiel sind ein mithilfe des Produktkatalogs konfiguriertes Signal und eine Weiche.
Wie das funktioniert ist in [Kapitel 4.1 "Config - Produkte anschließen"]({% link _app_doku/041-produkte-anschliessen.md %}) beschrieben.
                              
Im Beispiel soll das Flügelsignal auf Rot gehen, wenn die Weiche auf geradeaus gestellt wurde. 

![Gleisplan]({% link assets/images/ctc-app-doku/Trigger-Signal-0.png %})
                                                                                 
Dazu öffnen Sie die Config des Signals, indem Sie im Gleisplan mit rechter Maustaste auf das Signal klicken.
Dann wechseln Sie auf der Reiter "Aktionen" und markieren das Signal (hier PI-Dev-G-Signal).
Anschließend klicken Sie auf den Plus-Button rechts von "Produkte udn Aktionen".
Es öffnet sich folgender Dialog:

![Config Trigger am Signal 1]({% link assets/images/ctc-app-doku/Trigger-Signal-1.png %})

Dort wählen Sie "Trigger" und klicken auf "OK".
Dann klicken Sie auf den "..."-Button neben dem Namen des Triggers, um diesem einen geeigneten Namen zu geben:

![Config Trigger am Signal 2]({% link assets/images/ctc-app-doku/Trigger-Signal-2.png %})
               
Als Auslöser wählen Sie die Weiche (hier PI-Dev-Weiche-G), indem sie auf den Button "auswählen" klicken.
Normalerweise finden Sie Weichen in der Gruppe "SignalTower".
Im Beispiel unserer Messe-Anlage wurden jedoch die Weichen und Signale der Spur-G in eine separate Gruppe "PI-Dev-G" verschoben:   

![Config Trigger am Signal 3]({% link assets/images/ctc-app-doku/Trigger-Signal-3.png %})
                     
Wählen Sie die Weiche aus und klicken Sie dann auch "Übernehmen".
Beim Übernehmen wurden automatisch passende Skripte für alle Funkionen der Weiche angelegt:

![Config Trigger am Signal 4]({% link assets/images/ctc-app-doku/Trigger-Signal-4.png %})

Für das Beispiel müssen Sie nun nur noch für den Weichenzustand "s" (straigt, gerade) das Kommando "Rot" des Signals aufrufen.
Dazu fügen Sie den Befehl "call" hinzu, indem Sie die Wenn-Bedingung für "s" markieren und dann auf "Hinzufügen klicken": 
                          
![Config Trigger am Signal 5]({% link assets/images/ctc-app-doku/Trigger-Signal-5.png %})

Wählen Sie "call" aus und klicken Sie auf "Übernehmen".
Dann wählen Sie rechts vom Skript-Bereich noch die Aktion aus (hier PI-Dev-G-Signal) und dann den Parameter für die Aktion (hier "Hp0_Bar" für Flügelsignal Rot): 

![Config Trigger am Signal 6]({% link assets/images/ctc-app-doku/Trigger-Signal-6.png %})

Anschließend klicken Sie hoch auf "Hochladen" um die geänderte Config im CTC-Modul zu speichern.

**Hinweis:** Da die Gartenbahn-Weiche im Beispiel Ihre Position mit Sensoren bestimmt funktionert das Schließen des Signals auch dann, wenn die Weiche von Hand umgelegt wird.
Siehe dazu den [Artikel "LGB Weiche mit Positionssensor"]({% link _posts/de/szenarien/2021-10-31-LGB-Weiche-Positionssensor.md %}).