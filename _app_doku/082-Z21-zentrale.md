---
layout: doku
title: "Altes Digital: Z21 Zentrale einbinden"
chapter: 8.2
---
Mithilfe des [Z21-LAN-Protokolls](https://www.z21.eu/media/Kwc_Basic_DownloadTag_Component/47-1652-959-downloadTag/default/69bad87e/1646977660/z21-lan-protokoll.pdf) kann die CTC-App Loks steuern und Weichen schalten, die an einer Digitalzentrale angeschlossen sind.
Die Digital-Loks und -Weichen müssen einmalig konfiguriert werden und können danach wie normale CTC-Module über die CTC-App gesteuert werden. 
Die Konfiguration wird in einem beliebigen CTC-Weichenmodul oder CTC-Multi-IO-Board gespeichert.  

Die **Digitalzentrale muss natürlich das Z21-LAN-Protokoll unterstützen** und über das LAN bzw. WLAN **von der CTC-App aus erreichbar** sein.
                   
**Voraussetzung für diese Funktion ist die CTC-App Version 4.05.**

## Z21 Lok konfigurieren

Für die Speicherung der Konfiguration der über das Z21-LAN-Protokoll zu steuernde Digital-Lok suchen wir uns ein beliebiges CTC-Weichenmodul oder CTC-Multi-IO-Board aus.
Dazu gehen wir in den Config-Dialog des CTC-Moduls: 

![Gleisbild nach Ankunft der Lok]({% link assets/images/ctc-app-doku/Config-Digital-Adapter.png %})

Dort klicken wir auf "Digital-Adapter ändern".
Es öffnet sich folgender Dialog:

![Digital-Config leer]({% link assets/images/ctc-app-doku/Digital-Config-leer.png %})

Wir klicken auf "Neue DCC-Lok" und geben den Namen an, unter dem die Lok später erscheinen soll.
Ich habe mich für den Präfix "Z21_" entschieden, um über die Z21 gesteuerte Loks von echten CTC-Loks unterscheiden zu können.

![Digital-Config neue Lok]({% link assets/images/ctc-app-doku/Digital-Config-Lok-Neu.png %})

Dann klicken wir auf "Übernehmen".
Unter "Pins, Ports und Erweiterungen" erscheint die Zeile "Anschlüsse".
Diese markieren wir und klicken auf den Plus-Button daneben.
Im sich öffnenden Dialog geben wir die IP-Adresse der Z21-Zentrale an:

![Digital-Config IP-Adresse angeben]({% link assets/images/ctc-app-doku/Digital-Config-IP.png %})

Dann wählen wir den neu angelegten Port mit der IP-Adresse und klicken erneut auf den Plus-Button.
Wir wählen die Erweiterungskonfiguration "dcc.xml".
In sich öffnenden Dialog tragen wir einen beliebigen Namen ein - wir sehen ihn nur in dieser Konfiguration.
Hinter Bus-Adresse tragen wir die DCC-Adresse unserer Lok ein - im Beispiel ist das der DCC-Default "3".
In der Liste wählen wir "DCC Lokomotive":

![Digital-Config DCC Erweiterung Lok]({% link assets/images/ctc-app-doku/Digital-Config-DCC-Erweiterung.png %})

Dann klicken wir auf "übernehmen".
Die Lok wurde angelegt und Motor und Fahrlicht sind fertig konfiguriert:

![Digital-Config DCC Lok angelegt]({% link assets/images/ctc-app-doku/Digital-Config-Lok-initial.png %})
       
Nun können weitere Funktionen, z.B. Sound konfiguriert werden.
Dies geschieht genau gleich wie bei einem an ein CTC-Lokmodul angeschlossenen DCC-Dekoder, siehe [Kapitel 4.6 "Config - DCC-Dekoder anschließen"]({% link _app_doku/046-DCC-dekoder-anschliessen.md %}).

Nach dem Speichern taucht die Digital-Lok in der Lsite der CTC-Module auf:

![Digital-Lok in Modulliste]({% link assets/images/ctc-app-doku/Digital-Lok-Modul.png %})
