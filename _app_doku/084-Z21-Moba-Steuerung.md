---
layout: doku
title: "Modellbahn-Steuerungssoftware via Z21"
chapter: 8.4
---
Über den Menüpunkt "Z21 Zentrale Starten" kann in der PC-Version der CTC-App die Emulation einer Z21-Zentrale aktiviert werden.
Dabei reagiert die CTC-App nicht nur auf Kommandos, die per Z21-WLAN-Protokoll eintreffen, sondern meldet auch, wenn immer eine Lok eine Balise passiert (Belegtmeldung).

Damit ist alle Funktionalität vorhanden um CTC mit einer Modellbahn-Steuerungssoftware wie z.B. [iTrain](https://www.berros.eu/de/itrain/) zu verbinden.
Wie iTrain sich mit CTC verbinden lässt können wir Ihnen gerne an unserem Messestand demonstrieren.
                        
Wie Adressen von Loks und Weichen sowie die Lok-Funktionen zugeordnet werden erfahren Sie im vorhergehenden Kapitel "Z21 Handregler nutzen".  

## Balisen-Adresse (Belegtmelder) einstellen

Balisen erscheinen im Z21-LAN-Protokoll als CAN-Belegtmelder.

Auch diese brauchen eine Adresse, die in der CTC-App an der Balise engetragen werden.
Wir nutzen auch hier den Parameter "dccAddr":

![Config Balisen-Adresse]({% link assets/images/ctc-app-doku/Config-DCC-Balise.png %})



