---
layout: doku
title: "Automatisierung: Fahraufträge"
chapter: 7.3
---
Ein typischer Fahrauftrag (Job) ist die Fahrt von einem Bahnhof zu einem anderen.
Im folgenden Beispiel wurde der Fahrauftrag von Block B4 über B3 und BA nach A3 geschaltet.
Die reservierten Weichen sind orange und die reservierten Blöcke gelb eingefärbt (Block- und Balisen-IDs wurden von Hand in das Gleisbild gemalt):

![Fahrstrasse B4 nach B3 im Gleisbild]({% link assets/images/ctc-app-doku/Fahrauftrag-B4-A3.png %})

Nun kann die Lok V247-030, die im Block B4 steht, in die richtige Fahrtrichtung eingestellt werden und durch Klick auf den Automatik-Button auf die Reise geschickt werden.

In unserem Beispiel wurde nur eine Sequenz von Fahrstraßen, aber keine Balisen im Job festgelegt.
Das geht gut, wenn alle Balisen auf der Strecke geeignete Informationen liefern. In userem Fall:
* wurde die Balise BA3 durch die Fahrstraße von BA nach A4 auf das Kommando M;70 (Minimalgeschwindigkeit in 70 cm) eingestellt.
* zeigt die Balise A30 konstant das Kommando H;0 (Halt sofort) an.
* ist der Prellbock am Ende von A3 als (im Uhrzeigersinn) rechtes Ausfahrsignal dem Block A3 zugeordnet.
  Das verhindert, dass die Lok nach Erreichen des Blocks A3 wieder losfährt.

Bei Ankunft von V247-030 im Block A3 werden alle Reservierungen wieder zurückgenommen.
Das Gleisbild sieht dann wie folgt aus:

![Gleisbild nach Ankunft der Lok]({% link assets/images/ctc-app-doku/Fahrauftrag-B4-A3-Ankunft.png %})

## Fahrauftrag anlegen

Zum Anlegen eines Fahrauftrags öffnen wir die "Automatisierung Config", wählen den obersten Eintrag von "Produkte und Aktionen".
Dann klicken wir rechts daneben auf den Plus-Button:

![Fahrauftrag anlegen]({% link assets/images/ctc-app-doku/Fahrauftrag-anlegen.png %})

## Fahrauftrag mit Balisen-Kommandos

Das folgende Bild zeigt das Skript einen Fahrauftrag, in dem auch die zu passierenden Balisen aufgelistet sind:

![Fahrauftrag-Skript mit Balisen]({% link assets/images/ctc-app-doku/Fahrauftrag-Skript-Balise.png %})

Beim Passieren von Balise B11 wird die Lok abbremsen, um nach 55 cm die Minimalgeschwindigkeit erreicht zu haben.
Bei B10 wird sie dann schließlich für 15 Sekunden anhalten, auch wenn das Ausfahrsignal die Fahrt freigibt. 