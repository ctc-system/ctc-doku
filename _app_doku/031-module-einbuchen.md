---
layout: doku
title: "Module ins WLAN einbuchen"
chapter: 3.1
---
**Hinweis:** Wenn Sie unseren **WLAN-Einbuch-Service** erworben haben, sind uns Ihr WLAN-Name (SSID) und Netzwerkkennung (WLAN-Passwort) bekannt und Sie erhalten Ihre neuen CTC-Module grundsätzlich vorkonfiguriert.
Sie müssen diese also **nicht mehr einbuchen** und können direkt mit ["Kapitel 4 - Module konfigurieren"]({% link _app_doku/040-module-konfigurieren.md %}) fortfahren.
Der WLAN-Einbuch-Service ist **Bestandteil aller CTC-Starter-Sets und CTC-Router**.

Zum Einbuchen von Modulen gibt es zwei Möglichkeiten:
* Der erste Weg düfte Ihnen von vielen anderen Produkten mit WLAN bekannt vorkommen: Sie verbinden sich mit dem Konfigurations-WLAN des CTC-Moduls und geben ihre WLAN-Daten in ein Browser-Formular ein.
* Der zweite Weg geht deutlich komfortabler über die CTC-App, scheitert aber zumindest unter Windows immer wieder am bockigen Betriebssystem. 
      
**Hinweise:**
* Die Länge von Passwort und SSID war bis Firmware 20230329 auf 20 Zeichen beschränkt.
  Neuere Firmware hält sich an den WiFi-Standard: SSID (32 Zeichen), WLAN-Passwort (63 Zeichen).
* Die CTC-Module zeigen über Blinkcodes der Status-LED bzw. des Frontlichts ihren Zustand an, siehe [Kapitel 3.3 "Statusanzeigen der Module"]({% link _app_doku/033-statusanzeigen-der-module.md %}). 

## Einbuchen mit der CTC-App

**Hinweise:**
* Auf iOS (iPhone/iPad) ist diese Funktion ist leider nicht verfügbar, da Apple hierzu keine geeignete API bereitstellt.
* Unter Android muss die globale Funktion "Standort" aktiviert sein, so wie es auch für Navigations-Apps nötig ist.
  Je nach Hersteller und Android-Version kann die Aktualisierung der gefundenen WLANs sehr langsam sein - das soll Strom sparen.

Die CTC-App kann auf Knopfdruck im Konfigurationsmodus befindliche Module erkennen und in die WLAN-Konfiguration des CTC-Moduls schreiben.
Nach dem Schreiben der WLAN-Konfiguration setzt sich das CTC-Modul automatisch zurück und bucht sich dann ins Modellbahn-WLAN ein.

Ein CTC-Modul schaltet in den Konfigurationsmodus, wenn:

* es ein neues CTC-Modul (ohne WLAN-Konfiguration) ist. 
* es bereits konfiguriert ist, aber sein WLAN nicht findet. Dann schaltet es nach einer Minute in den Konfigurationsmodus, wartet zwei Minuten auf eine Verbindung und setzt sich dann automatisch zurück.

Dass ein CTC-Modul im Konfigurationsmodus ist erkennen Sie daran, dass seine Statusanzeige (LED bzw. Frontlicht) dauerhaft leuchtet.

Über das Menü **Einstellungen/Neue Module suchen** sucht die App nach verfügbaren WiFi-Netzwerken:

![ctc-app-doku-bild]({% link assets/images/ctc-app-doku/ctc-app-doku-Bild3.png %})

Zum Einbuchen geben Sie die SSID und das Passwort Ihres Modellbahn-WLANs an und am besten auch einen Namen für das Modul.
Dann **wählen Sie das Modul aus** und klicken auf **Modul einbuchen**. 

![ctc-app-doku-bild]({% link assets/images/ctc-app-doku/ctc-app-doku-Bild4.png %})

Dabei bucht sich Ihr PC/Tablet/SmartPhone in das Konfigurations-WLAN des CTC-Moduls ein, und schreibt die Konfigurationsdatei netCfg.xml auf das CTC-Modul.
Wie nach allen Konfigurationsänderungen setzt sich das CTC-Modul selbstständig zurück und ist anschließend in Ihrem Modellbahn-WLAN sichtbar.

**Hinweise:**
* Vor allem unter Windows 10 kann es zu Problemen beim Aktivieren des richtigen WLANs kommen.
  Wenn die CTC-App den Hinweis "I/O-Fehler beim Hochladen" anzeigt, hilft es meist ein zweites Mal auf den Button "Modul einbuchen" zu klicken.
  Hilft auch das nicht, können Sie das Einbuchen über die WLAN-Verwaltung von Windows selbst vornehmen, bevor Sie "Modul einbuchen" anklicken.
* Wenn der PC beim Einbuchen keine Kabelverbindung zum Modellbahn-Netzwerk hat, verliert er beim Einbuchen die Verbindung zum Modellbahn-Netzwerk.
  Manchmal muss man ihn danach von Hand ins Modellbahn-WLAN zurückholen.

## Einbuchen über die WebSite des CTC-Moduls
           
**Hinweis:**
Diese Möglichkeit gibt es erst ab der Firmware 20210220, die mit der CTC-App v3.05 mitgeliefert wird und auf allen nach dem 21.02.2021 von CTC gelieferten Modulen vorinstalliert ist.

Ein CTC-Modul, das sich im Konfigurationsmodus befindet, sehen Sie in der Liste verfügbarer WLANs Ihres PC, Tablet bzw. SmartPhone.
Die Konfigurations-WLANs Ihrer CTC-Module erkenne Sie an der Buchstaben-Ziffern-Kombination (MAC-Adresse in Hex-Code) am Anfang.
Hier ein Beispiel von Kubuntu Linux:
![ctc-app-doku-bild]({% link assets/images/ctc-app-doku/Wifi-Liste-Kubuntu.png %})

<br/>

1. Verbinden Sie Ihren PC, Tablet oder SmartPhone mit diesem Konfigurations-WLAN.
1. Öffnen Sie den Internet-Browser und geben Sie als Adresse **http://192.168.4.1** ein.
   ![ctc-app-doku-bild]({% link assets/images/ctc-app-doku/Einbuchen-index-html.png %})
1. Klicken Sie auf den Link "Edit WiFi SSID and password".
   Geben Sie SSID (WLAN-Name, Netzwerkkennung) und das Passwort (Netzwerkschlüssel) Ihres Modellbahn-WLANs ein.
   ![ctc-app-doku-bild]({% link assets/images/ctc-app-doku/Einbuchen-editWifi-html.png %})
1. Klicken Sie auf "Save".
   ![ctc-app-doku-bild]({% link assets/images/ctc-app-doku/Einbuchen-setwifi-ok.png %})
1. Das CTC-Modul startet automatisch neu und sollte nun in Ihrem Modellbahn-WLAN sichtbar sein.

**Hinweis:**
Das Modul bleibt nur ca. 2 Minuten im Konfigurationsmodus und startet dann neu.
Wenn das Modul zwar eine WLAN-Konfiguration hat, sich aber nicht in dieses WLAN einbuchen kann, dann wechselt es nach ca. 1 Minute in den Konfigurationsmodus. 
Wenn Sie sich also bei SSID oder Passwort **vertippt** haben, dann müssen Sie **eine Minute warten**, bis Sie das Modul wieder über sein Konfigurations-WLAN erreichen können.

**Hinweis:**
Ob sich das CTC-Modul im Konfigurationsmodus befindet, erkennen Sie an der [Statusanzeige des CTC-Moduls]({% link _app_doku/033-statusanzeigen-der-module.md %}).