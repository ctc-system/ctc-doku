---
layout: doku
title: "Anhang - HTML-Seite"
chapter: A2
---
## HTML-Seite

Die HTML-Seite bietet den direkten Zugriff auf die XML-Konfigurationsdateien des CTC-Moduls.

**ACHTUNG: Falsche Einstellungen in den XML-Dateien, insbesondere der "ioCfg.xml", können das CTC-Modul und auch die angeschlossene Lok/Weiche/... dauerhaft zerstören!**

![ctc-app-doku-bild]({% link assets/images/ctc-app-doku/ctc-app-doku-Bild14.png %})

### Download

Zum Herunterladen einer Config-Datei den Link **download** neben dem Namen der Config-Datei klicken.

Je nach Browser-Einstellung landet die Datei im Download-Ordner des Browsers oder Sie werden nach dem Speicherort gefragt.

Auf diesem Weg können Sie zum einen Ihre Konfiguration sichern, und zum anderen uns im Support-Fall Ihre Konfiguration zukommen lassen.

**Hinweis:** Wenn Sie mit der CTC-App die Config ändern werden diese Änderungen in der Datei "cfg.xml" des CTC-Moduls gespeichert.
In der "netCfg.xml" finden Sie SSID und Passwort Ihres Modellbahn-WLANs.

### Upload

Zum Hochladen einer Config-Datei den Link **Upload file** klicken.

In der Auslieferung der Desktop-App finden sich im Ordner **Config** für jeden Modul-Typ (Firmware) eine passende **ioCfg.xml**.
Die **netCfg.xml**, **fileCfg.xml** und (leere) **cfg.xml** sind für alle CTC-Module identisch.

![ctc-app-doku-bild]({% link assets/images/ctc-app-doku/ctc-app-doku-Bild27.png %})

**Hinweis:** Nach jedem Upload startet sich das CTC-Modul neu, d.h. es dauert einige Sekunden bis es wieder sichtbar wird.

### Edit

Durch Klick auf **edit** erreichen Sie einen einfachen Text-Editor.
In der Zip-Datei, der Desktop-App finden Sie, nach Modul-Type sortiert, passende Konfigurationsdateien, deren Inhalt Sie als Vorlage für eigene Konfigurationen nehmen können.
Die Bearbeitung des Textes in einem Editor, der mit XML umgehen kann (z. B. Notepad++) reduziert das Risiko, defekte Konfigurationen zu erzeugen.

![ctc-app-doku-bild]({% link assets/images/ctc-app-doku/ctc-app-doku-Bild15.png %})

Nach Klick auf **Save** bestätigt das CTC-Modul dem Empfang der Konfigurationsdatei und setzt sich anschließend zurück.

![ctc-app-doku-bild]({% link assets/images/ctc-app-doku/ctc-app-doku-Bild16.png %})