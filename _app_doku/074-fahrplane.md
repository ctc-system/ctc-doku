---
layout: doku
title: "Automatisierung: Fahrpläne"
chapter: 7.4
---
Ein Fahrplan ist eine Sammlung von Fahraufträgen, die zu im Fahrplan festgelegten Uhrzeiten (Modellzeit) gestartet werden.

Die Fahrpläne finden Sie als neuen Reiter in der Hauptansicht der CTC-App:  

![Fahrplan]({% link assets/images/ctc-app-doku/Fahrplan.png %})

## Fahrplan ausführen

Zum Ausführen eines Fahrplans wählen Sie diesen aus.
Dann stellen Sie über den Button "Zeit" die Modell-Urzeit ein und drücken auf den Play-Button, um die Uhr loslaufen zu lassen.
Sobald die Modell-Uhrzeit einer der Abfahrtzeiten entspricht, wird der zugehörige Fahrauftrag ausgeführt.  

## Fahrpläne erstellen

Zum Erstellen eines Fahrplans wählen Sie sich als Erstes ein [CTC-Weichenmodul]({% link _data_sheet/CTC-Weichenmodul-H0.md %}) oder [CTC-Multi-I/O-Board]({% link _data_sheet/CTC-Multi-IO-Board.md %}) aus, auf dem der Gesamtplan gespeichert werden soll.
Öffnen Sie dessen Config-Dialog:

![Fahrplan]({% link assets/images/ctc-app-doku/Config-Fahrplaene-aendern.png %})
                               
Es erscheint folgender Dilaog:

![Fahrplan]({% link assets/images/ctc-app-doku/Fahrplan_aendern.png %})
                  
Dort klicken Sie auf "Neuer Fahrplan, um einen neuen Fahrplan zu erstellen.

Mit dem Plus-Button können Sie einen Fahrauftrag zum Fahrplan hinzufügen:

![Fahrplan]({% link assets/images/ctc-app-doku/Fahrplan_Fahrauftrag_neu.png %})
                                                                         
Anschließend können Sie mit dem Stift-Button die Abfahrtzeit ändern.