---
layout: doku
title: "Automatisierung"
chapter: 7
---
Mithilfe von Balisen (ID-Sendern) lässt sich ein grundlegender Automatik-Betrieb einrichten.
Dazu werden den Balisen Kommandos zugeordnet, die von einer mit ID-Reader ausgestatteten Lok interpretiert werden, wenn sie die Balise passiert.
Balisen und ID-Reader gibt es für CTC auf Basis von IR (Infrarot) und von NFC (RFID).
Für die folgenden Betrachtungen gibt es keinen Unterschied zwischen IR und NFC.

**Hinweis:** Die hier beschriebene Funktionalität setzt die **CTC-App ab Version 4.00 und zugehörige Lok-Firmware** zwingend voraus.
Mit älteren CTC-Versionen konfigurierte Signale und Trigger von Balisen (ID-Sendern) **müssen angepasst werden**.
Meist ist es einfacher im [Config-Dialog]({% link _app_doku/041-produkte-anschliessen.md %}) die betreffenden Zeilen unter "Angeschlossene Produkte" zu entfernen und dann neu hinzuzufügen. 
         
Die Grundbausteine der Automatisierung mit CTC sind:
* Fahraufträge
* Blöcke
* Fahrstraßen
* Fahrpläne
             
## Blöcke/Gleisabschnitte (Block)

Blöcke sind bei der echten Bahn die Grundlage, um Zusammenstöße von Zügen zu vermeiden.
Dabei wird der gesamte Gleisplan in einzelne Blöcke (Gleisabschnitte) aufgeteilt.
Weichen (Abzweige) dürfen nur zwischen Blöcken existieren.  

**Das Wissen, wie eine Modellbahnsteuerung auf Basis von Blöcken funktioniert, ist essenziell für das Verständnis der Automatisierung in CTC.**
Dieses Konzept hat Dominik Mahrer auf [modellbahn.mahrer.net](https://modellbahn.mahrer.net/technisches/automatikbetrieb) schön beschrieben.

## Signale
          
Signale funktionierten in Verbindung mit Balisen (ID-Sendern) schon in CTC Version 3 als echte Zugbeeinflussung.

Wenn eine Balise mit einem Signal verknüpft ist, sendet sie den Zustand des Signals (z.B. Halt) und dessen Entfernung an die Lok und diese reagiert entsprechend (z.B. Halt vor dem roten Signal).

Mit CTC Version 4 kommt hinzu, dass die Fahrtrichtung der Lok berücksichtigt wird, sie also vor dem roten Signal nur anhält, wenn dieses für ihre Fahrtrichtung gilt.
                                                      
Außerdem kann in der Lok der Abstand des ID-Readers zum vorderen und hinteren Lok-Ende definiert werden.
Bei der Zielbremsung wird dieser Abstand mit berücksichtigt.

Signale dürfen übrigens auch rein virtuell existieren, d.h. man sieht sie nur im Gleisbild.
Auf der Anlage ist dann kein Signal installiert.

## Steuernde Schilder

Statt Signalen können Sie auch Schilder verwenden.
Ein Schild wird wie ein Signal als Produkt zu einer Weiche hinzugefügt, taucht aber nicht im Schaltpult auf und kann somit im Spielbetrieb auch nicht geändert werden.
Ein Schild hat keine Anschlüsse, aber Parameter zur Festlegung der Bedeutung des Schildes.

Auch bei Schildern haben Sie die Option diese rein virtuell aufzustellen, d.h. Sie sehen diese nur im Gleisbild.
Auf der Anlage ist dann kein Schild aufgestellt.

## Fahrstraßen (Path)

Eine Fahrstraße stellt eine Verbindung zwischen zwei Blöcken her, indem sie alle dazwischen liegenden Weichen passend stellt und schließlich das Ausfahrsignal des Startblocks öffnet.

Beim Schalten der Fahrstraße werden alle betroffenen Weichen, Signale und Blöcke reserviert und erst wieder freigegeben, wenn eine Lok den Block am Ende der Fahrstraße erreicht. 

## Fahraufträge (Job)

Ein typischer Fahrauftrag ist die Fahrt von einem Bahnhof zu einem anderen, ggf. mit Zwischenstopps.
                                                                                                   
Die simpelste Form eines Fahrauftrags ist nur eine Sequenz von zu schaltenden Fahrstraßen.
                                                                
Für den Automatik-Betrieb kann der Fahrauftrag um die Balisen (genaugenommen deren dreibuchstabige IDs) ergänzt werden, die eine Lok in chronologischer Reihenfolge abfahren soll.
Dabei kann jeder Balise noch ein Kommando zugeordnet werden.

Wird von der Balise ebenfalls ein Kommando (z.B. Halt in 60cm) ausgesendet, so entscheidet die Lok, ob sie das von der Balise ausgesendete oder im Fahrauftrag verzeichnete Kommando ausführt.
Ein Halt hat dabei z.B. immer Vorrang vor einer Geschwindigkeitsreduktion.

Ein Fahrauftrag mit Balisen gilt immer für die Lok, die im Startblock des Fahrauftrags steht.

## Fahrpläne (Schedule)

Ein Fahrplan ist eine Sammlung von Fahraufträgen, die zu im Fahrplan festgelegten Uhrzeiten (Modellzeit) gestartet werden.
Auf diese Weise kann eine Voll-Automatisierung konfiguriert werden.

## Lok-Sensor konfigurieren

Damit die Lok eine Zielbremsung durchführen kann, muss sie natürlich auch wissen, wie weit ihr ID-Reader vom Lokanfang bzw. -ende entfernt ist.
Dazu dienen die beiden neuen Parameter "dFront" (vorne) und "dBack" (hinten) der ID-Reader, die die Entfernung des ID-Readers in cm festlegen.

**Hinweis:** Bei Pendelzügen mit der Lok an einem Ende des Zugs müssen Sie bei der Platzierung Ihrer Balise im Gleis natürlich bedenken, dass bei der Rückwärtsfahrt der ID-Reader ganz schön weit weg vom Zuganfang ist.

## Ereignisse verarbeiten (Trigger)

Mit sogenannten Triggern (Auslöser) können Sie auf Ereignisse (bzw. Zustandsänderungen) auf Ihrer Modellbahn reagieren, z.B.
* Eine Weiche wurde geschaltet.
* Ein Signal wurde rot.
* Eine Lok hat eine Balise passiert.

Mit einem Trigger legen Sie fest auf welches Ereignis wie reagiert werden soll.
Der Trigger wird immer bei der Aktion angelegt, die durch ihn verändert werden soll.
Mehr dazu erfahren Sie im [Kapitel 4.3 - Aktionen verknüpfen]({% link _app_doku/043-aktionen-verknuepfen.md %})).

## Beispiele

Im Folgenden werden einige typische Anwendungen der Automatisierung kurz beschrieben.
In den "Artikeln rund um CTC" werden wir nach und nach ausführlichere Beschreibungen veröffentlichen. 

### Automatischer Halt am Signal

Damit eine CTC-Lok automatisch vor einem roten Signal halten kann, wird eine Balise mit ausreichend Abstand vor dem Signal ins Gleis eingebaut.
Für den Abstand sind sowohl die eigentliche Bremsstrecke als auch die Entfernung des ID-Readers zum Anfang des Zuges zu berücksichtigen.

Die Balise wird dann mit dem Signal verknüpft und sendet so jeweils den Zustand des Signals als Kommando aus.
Die Entfernung zum Signal wird in der Config des Triggers eingetragen.
Wie ein Trigger konfiguriert wird erfahren Sie im [Kapitel 4.3 - Aktionen verknüpfen]({% link _app_doku/043-aktionen-verknuepfen.md %})). 

Für die korrekte Funktion ist es wichtig, dass bei der Konfiguration des Signals die richtige Richtung ausgewählt wurde.
Denn die Lok reagiert nur auf die zu ihrer Richtung passenden Kommandos.

**Hinweis:** Mit Richtung der Lok ist nicht ihre am Steuerpult einstellbare Fahrtrichtung (vorwärts/rückwärts) gemeint, sondern ob sie im oder gegen den Uhrzeigersinn fährt.
Diese Richtung ermittelt die Lok aus den gelesenen IDs.

### Automatischer Halt mit zwei Balisen

Mit einer zweiten Balise direkt vor dem Signal lässt sich ein auf wenige Millimeter genaues Anhalten der Lok erreichen.
Dabei wird von der ersten Balise das Kommando "Minimalgeschwindigkeit" mit einer Distanz kurz vor der zweiten Balise ausgesendet.
Die zweite Balise sendet dann das Kommando "Halt" sofort (in 0 cm) aus.

Vor allem beim Stop in Abstellgleisen bietet das den Vorteil, dass die Lok über der Balise zum Stehen kommt und so auch bein nächsten Einschalten der Modellbahn sofort weiß, wo sie ist.  

### Haltestelle

Ein Halt an einem Bahnhof erreichen Sie durch Einsatz von zwei Balisen. 
Die erste teilt der Lok mit, bis kurz vor der zeiten Balise auf Minimalgeschwindigkeit abzubremsen.
Die zweite übermittelt dann den Befehl "Stop" und wie lange die Lok warten soll bis sie weiter fährt.
Alternativ kann der Befehl "Rückfahrt" verwendet werden, bei dem die Lok nach der Wartezeit in die entgegengesetzte Richtung weiter fährt. 

Die Kommandos können den Balisen fest zugeordnet oder über den Fahrauftrag der Lok übermittelt werden.

Alternativ kann die Lok auch durch ein Signal zum Halten gebracht werden und ihre Fahrt erst mit dem nächsten Eintrag im Fahrplan fortsetzen.

### Pendelzug

Ein Pendelzug wird durch mindestens zwei Fahraufträge, nämlich Hinfahrt und Rückfahrt ermöglicht.
Diese werden dann so oft wie gewünscht in einen Fahrplan eingefügt.

### Fahrt nach Fahrplan

Für die Fahrt nach Fahrplan werden zuerst einzelne Fahraufträge angelegt.
Diese können dann einzeln getestet werden.

Anschließend wird aus diesen Fahraufträgen ein Fahrplan mit Abfahrtzeiten erstellt.
Dabei dürfen einzelne Fahraufträge auch beliebig oft verwendet werden.
Sie müssen dabei allerdings beachten, dass ein Fahrauftrag nur ausgeführt wird, wenn im Startblock des Fahrauftrags auch eine Lok steht.
Je nachdem wohin welche Lok sie durch andere Fahraufträge in den Startblock bewegt haben kann es lso durchaus sein, dass ein und er selbe Fahrauftrag immer wieder von einer anderen Lok ausgeführt wird.  
