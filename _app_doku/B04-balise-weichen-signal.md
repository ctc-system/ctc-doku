---
layout: doku
title: "Config-Beispiel: Balise abhängig von Weichen und Signal"
chapter: B.04
---
In diesem Beispiel aus unserer Messe-Anlage befindet sich eine Balise am Eingang eines Bahnhofs.
Wenn die Weichen so gestellt sind, dass die Einfahrt in Gleis 2 möglich ist, soll sie mit dessen Signal gekoppelt werden.
    
Im Gleisplan sind die beteiligten zwei Weichen, das Signal und die Balise beschriftet.

![Gleisplan]({% link assets/images/ctc-app-doku/B04-Gleisplan.png %})
                        
Die Balise soll wie folgt funktionieren:
* Freie Fahrt (Balise grün), wenn Weiche "PI-Dev-LH5" auf gerade steht.
* Bremsen auf Minimalgeschwindigkeit (Balise orange) wenn Weichen "PI-Dev-LH5" rechts und "PI-Dev-LH4" links stehen, sowie Signal "PI-Dev-SB2r" rot zeigt.
* Langsamfahrt (Balise gelb) in allen anderen Fällen. 

Offensichtlich dürfte noch sein, dass die Balise drei Trigger braucht, nämlich je einen für die Weichen "PI-Dev-LH4" und "PI-Dev-LH5", sowie einen für das Signal.
Die Trigger werden sinnvollerweise so benannt, dass sie den Namen des Auslösers enthalten:

![Alle Trigger]({% link assets/images/ctc-app-doku/B04-Alle-Trigger.png %})

Die Trigger setzten das Kommando ("cmd") der Balise nur in den Fällen selbst, wo es keine Abhängigkeit vom Zustand der anderen Beteiligten gibt.
Für allen anderen Fällen wurde eine neue Funktion "AB3-fkt" hinzugefügt, die entscheidet wie das Kommando zu setzten ist.

Der Trigger, der auf die Weiche "PI-Dev-LH5" reagiert, setzt also bei gerade ('s') das Kommando Freie Fahrt (Balise grün).
Bei rechts ('r') ruft er die Funktion "AB3-fkt":

![Trigger LH5]({% link assets/images/ctc-app-doku/B04-Trigger-on-LH5.png %})

Der Trigger, der auf die Weiche "PI-Dev-LH4" reagiert, setzt also bei gerade ('s') das Kommando Langsamfahrt (Balise gelb).
Bei links ('l') ruft er die Funktion "AB3-fkt":

![Trigger LH4]({% link assets/images/ctc-app-doku/B04-Trigger-on-LH4.png %})

Der Trigger, der auf das Signale "PI-Dev-SB2r" reagiert, ruft immer die Funktion "AB3-fkt":

![Trigger S-B2r]({% link assets/images/ctc-app-doku/B04-Trigger-on-SB2r.png %})
    
Die eigentliche Magie passiert in der Funktion "AB3-ftk".
Sie nutzt den Trick aus, dass der Zustand eines Triggers immer der Wert seines Auslösers ist.

Wird also die Weiche LH5 auf rechts ('r') geschaltet, so wird von der CTC-App der Trigger "AB3-on-LH5" aufgerufen und diesem der neue Wert der Weiche ('r') übergeben.
Der Zustand (Wert) des Triggers wird dabei ebenfalls auf 'r' gesetzt.
Damit wird es möglich, dass die Funktion "AB3-fkt" den Zustand der Weichen "PI-Dev-LH4" und "PI-Dev-LH5" sowie des Signals "PI-Dev-SB2r" prüfen und das Kommando der Balise passend setzen kann:

![Funktion AB3-fkt]({% link assets/images/ctc-app-doku/B04-AB3-Funktion.png %})
