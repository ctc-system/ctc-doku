---
layout: doku
title: "Module konfigurieren"
chapter: 4
---
**Hinweis:** Die Konfiguration von CTC-Modulen ist derzeit unter Android und iOS nur teilweise verfügbar.

Zu den Konfigurationsdialogen kommt man über das **Stift-Symbol**: ![ctc-app-doku-icon]({% link assets/images/ctc-app-doku/ctc-app-doku-Icon_Stift.png %})    

* im Konfigurator (Liste aller Module) am rechten Rand der Zeile.
* in der Lokomotiven-Liste rechts neben der Lok 
* in der Steuerungsseite unten rechts
* in der Weichen-Liste rechts neben der Weiche
  
## Liste aller Module (Konfigurator)

In dieser Liste (Menü **Einstellungen/Konfigurator**) finden Sie alle CTC-Module, auch solche, die aufgrund eines Fehlers in der Konfiguration nicht richtig starten konnten:

![Konfigurator]({% link assets/images/ctc-app-doku/0400-configurator.png %})

Außerdem sehen Sie hier, auf welchen Modulen ein Gleisplan und/oder eine Modellbahn (Gesamtplan) für das Gleisbildstellwerk abgelegt sind (siehe Kreuze in den entsprechenden Spalten).
        
## Config-Backup

über den Menüpunkt "Einstellungen / Config Backup" der CTC-App (Desktop) können sie die Config-Dateien aller gerade in der CTC-App sichtbaren CTC-Module sichern.
Dabei wählen Sie einen Ordner auf Ihrer Festplatte als Backup-Ordner aus.
Danach wird für jedes erreichbare CTC-Modul in diesem Backup-Ordner ein Ordner mit dem Namen des CTC-Moduls angelegt und in diesem alle Config-Dateien gesichert.

## Lok-Konfiguration

![Lok bearbeiten]({% link assets/images/ctc-app-doku/0400-edit_loco.png %})

## Schaltkasten-Konfiguration

![Schaltkasten bearbeiten]({% link assets/images/ctc-app-doku/0400-edit_switchbox.png %})

## Unbekannte Module (Unknown Device)

![ctc-app-doku-bild]({% link assets/images/ctc-app-doku/ctc-app-doku-Bild8.png %})
      
## Firmware

Das Feld "Firmware" gibt an, um welche Firmware (bzw. Hardware) es sich bei diesem CTC-Modul handelt.
Das Feld "Firmware Version" zeigt an, von wann diese Firmware ist (Jahr Monat Tag), z.B. steht 20230829 für den 29. August 2023.

Über der Button "Neue Firmware hochladen" können Sie die Firmware dieses CTC-Moduls aktualisieren.
Dabei wird Ihnen zuerst die mit der CTC-App mitgelieferte Version der Firmware angeboten, sie können aber auch eine Datei von einer anderen Stelle auswählen.
Welche Firmware-Version die CTC-App Versionen jeweils mitbringen erfahren Sie im [Download-Bereich]({% link de/download.md %}).

## Log-Ausgabe

Unter den Firmware-Feldern finden Sie die Log-Ausgabe.
Dort wurden schon immer beim Hochladen der Firmware die Meldungen des Upload-Tools angezeigt.
Ab CTC-App 4.19 und zugehöriger Firmware-Version werden dort auch alle Log-Meldungen des CTC-Moduls ausgegeben.
Somit lassen sich Konfigurationsfehler deutlich leichter finden.

Ab CTC-App 4.20 ist die Log-Ausgabe editierbar.
Damit wird es möglich diese mit der Tastenkombinationen "Strg-A" zu markieren und dann mit "Strg-C" in die Zwischenablage zu kopieren.
Von dort kann sie dann in Support-Anfrage-Mails kopiert werden.

## Config ändern

In der Config (cfg.xml) wird festgelegt welche Produkte (Weichen, Signale, Lampen, ...) an Ihr CTC-Modul angeschlossen sind und wie sie gesteuert werden können, also wo und wie sie in der CTC-App angezeigt und geschaltet werden.
Die verfügbaren Anschlüsse finden sich in der I/O-Config (ioCfg.xml), die bereits bei der Auslieferung auf Ihrem CTC-Modul vorhanden ist. 

Dabei gibt es folgende unterschiedliche Möglichkeiten, die im Detail in den folgenden Kapiteln beschrieben sind:
* [Produktkatalog]({% link _app_doku/041-produkte-anschliessen.md %}): Dieses mit CTC Version 3 neue eingeführte Konzept reduziert die Konfiguration auf wenige leicht verständliche Schritte.
* [Aktionen verknüpfen]({% link _app_doku/043-aktionen-verknuepfen.md %}): Ausgehend von den über den Produktkatalog automatisch angelegten Aktionen können Sie diese verknüpfen, um z.B. eine Lok automatisch vor einem roten Signal halten zu lassen.  
* Eigene [Aktionen erstellen]({% link _app_doku/044-aktionen-bearbeiten.md %}): Wenn Sie Aktionen wünschen, die so nicht im Produktkatalog stehen, können Sie weitere eigene Aktionen hinzufügen.  
* [DCC-Dekoder anschließen]({% link _app_doku/046-DCC-dekoder-anschliessen.md %}) und konfigurieren: Hierbei kommt leider die ganze Komplexität von DCC durch, weshalb dem ein eigenes Kapitel gewidmet wurde.  
* Eigenen [Produktkatalog erstellen (Anhang)]({% link _app_doku/A30-produktkatalog-erstellen.md %}): Für Standard-Produkte ist das unsere Aufgabe, aber es wird bestimmt ganz spezielle Produkte geben, die in unserem Katalog fehlen.
  
## Config ersetzen

Mit diesem Button können Sie die Konfiguration Ihres CTC-Moduls in den Auslieferungszustand zurückversetzen oder eine alternative Grundkonfiguration laden.

**Hinweise:** 
* Bitte beachten Sie, dass es sich dabei um den mit der CTC-App mitgelieferten Stand der Konfiguration handelt.
  Bevor Sie die Konfiguration ersetzen, sollten Sie deshalb die Firmware Ihres CTC-Moduls aktualisieren.
* Bis CTC-App Version 4.18 gab es an dieser Stelle den Button "Config löschen".

## IO-Config ersetzen

Ab CTC-App Version 4.19 ist es möglich, die ioCfg.xml des CTC-Moduls durch einen neuere oder alternative Variante zu ersetzen.
Bisher ging das nur über die [HTML-Seite des Moduls]({% link _app_doku/A20-anhang-html-seite.md %}).

**Hinweise:** 
* Bitte beachten Sie, dass es sich dabei um den mit der CTC-App mitgelieferten Stand der Konfiguration handelt.
  Bevor Sie die Konfiguration ersetzen, sollten Sie deshalb die Firmware Ihres CTC-Moduls aktualisieren.
* Der Weg über die HTML-Seite des Moduls geht auch heute noch, sollte aber vermieden werden, da beim Weg über die HTML-Seite keinerlei Plausibilitätschecks gemacht werden.

## Digital-Adapter ändern

Mit "Digitaladapter ändern" können Sie CTC dazu überreden Loks und Weichen anzusteuern, die an eine Digitalzentrale angeschlossen sind.
Mehr dazu erfahren Sie im [Kapitel 8.2 - "Altes Digital: Z21 Zentrale einbinden"]({% link _app_doku/082-Z21-zentrale.md %}).

## WiFi-Config ändern

Über diesen Dialog kann die WiFi-Config (netCfg.xml) eines gerade eingebuchten CTC-Moduls geändert werden.
So können Sie z.B. eine Lok auf Ihrer Anlage zu Hause für das Modelbahn-WLAN Ihres Modellbahnclubs umkonfigurieren, bevor Sie das Haus verlassen. 

**ACHTUNG:** Nach dem Hochladen setzt sich das CTC-Modul automatisch zurück und ist dann nur noch, in dem neu konfigurierten Modellbahn-WLAN, zu sehen.

![WiFi-Config ändern]({% link assets/images/ctc-app-doku/0400-edit_wificfg.png %})

Wenn hierbei etwas schiefging, können Sie das CTC-Modul, wie im [Kapitel 3 - Module ins WLAN einbuchen]({% link _app_doku/031-module-einbuchen.md %}) beschrieben, wieder in Ihr Modellbahn-WLAN zurückholen.
 
## Gleisplan und Modellbahn ändern

Mit diesen beiden Knöpfen gelangen Sie zur Konfiguration des Gleisbildstellwerks der CTC-App.
Sie sind im [Kapitel 6 - Gleisbildstellwerk]({% link _app_doku/060-gleisbildstellwerk.md %}) beschrieben.

## Automatisierung ändern

Mit diesem Knopf gelangen Sie zur Konfiguration der Aktionen, die mit dem Automatikbetrieb zu tun haben und nicht direkt einem CTC-Modul zugeordnet werden können.
Dies sind:
* Trigger für NFC-Balisen
* Blöcke (Gleisabschnitte) und Aktionen zum Schalten von Fahrstraßen zwischen Blöcken
* Fahraufträge

Sie sind im [Kapitel 7 - Automatisierung]({% link _app_doku/070-automatisierung.md %}) beschrieben.

## Fahrpläne ändern

Mit diesem Knopf können Sie Fahrpläne ändern.
Wie das geht, ist im [Kapitel 7.4 - Fahrpläne]({% link _app_doku/074-fahrplane.md %}) beschrieben.

 