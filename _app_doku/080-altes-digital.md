---
layout: doku
title: "Altes Digital: Brücke zu DCC & Co."
chapter: 8
---
Wir sind der Überzeugung, dass CT&C die nächste Generation der Modellbahnsteuerung ist.
Deswegen erlauben wir uns hier auch vom "alten" oder "Klassischen" digital zu reden, wenn es um DCC, Motorola-Format (MM) oder Märklin mfx, ... geht.

Da wir zum einen nicht so schnell CTC-Module für alle erdenklichen Anwendungsfälle entwickeln können und zum anderen unseren Kunden einen fließenden Übergang zu CTC ermöglichen wollen, beschäftigt sich diese Kapitel mit den diversen Brücken zwischen CTC und alten Digitalsystemen. 

Derzeit gibt es folgende Möglichkeiten CTC mit der alten Digitalwelt zu verbinden:
- Parallelbetrieb: CTC-Loks fahren auch mit Digitalstrom, können also gleichzeitig mit altem Digital betriebenen Loks auf denselben Gleisen fahren.
  Die Steuerungen beider Welten sind in diesem Fall komplett getrennt.
- Dekoderanschluss: DCC-Lokdekoder können an den Motorausgang eines CTC-Lokmoduls angeschlossen und so über CTC gesteuert werden.
  Damit lassen sich geliebte Sound-Dekoder auch mit CTC weiter verwenden, sofern genug Platz für das zusätzliche CTC-Modul in der Lok ist.
  Die Unterstützung von DCC-Zubehördekodern (Weichen, ...) und das Motorola-II-Protokoll sind in Planung. 
- Z21-Zentrale einbinden: Ab Version 4.05 kann CTC über das [Z21-LAN-Protokoll](https://www.z21.eu/media/Kwc_Basic_DownloadTag_Component/47-1652-959-downloadTag/default/69bad87e/1646977660/z21-lan-protokoll.pdf) mit Digitalzentralen wie der [Z21 von Roco](https://www.z21.eu/de/z21-system/allgemein) kommunizieren.
  Damit lassen sich mit der CTC-App klassisch digitale Lokomotiven, Weichen, etc. über eine Digitalzentrale mit Z21-LAN-Protokoll steuern.
- Z21-Handregler einbinden: Ebenfalls ab Version 4.05 kann CTC sich wie eine Digitalzentrale mit Z21-LAN-Protokoll verhalten.
  Damit können Handregler wie die [Z21 WLAN-Maus von Roco](https://www.z21.eu/de/produkte/z21-wlanmaus) dazu verwendet werden, CTC-Loks zu steuern und CTC-Weichen zu schalten.

Für alle Brücken zur alten Digitalwelt gilt natürlich, dass CTC seine Stärken nur bedingt ausspielen kann.
Die technischen Beschränkungen der alten Digitalwelt werden Sie erst los, wenn Sie komplett auf CTC umsteigen.

## Parallelbetrieb

Zum Parallelbetrieb gibt es nicht viel zu sagen.
CTC-Module können mit bis zu 24V Gleichstrom, Digitalstrom und auch Wechselstrom betrieben werden.
Mehr dazu im separaten [Dokument "Stromversorgung und Router"]({% link de/docu/Stromversorgung-und-Router.md %})
    
Dabei werden die alten Digital-Loks und -Weichen wie bisher gesteuert und dei CTC-Module über die CTC-Apps.
Eine Verbindung beider Welten ist in diesem Szenario nicht vorgesehen.



