---
layout: doku
title: "Lokomotiven steuern"
chapter: 1.1
---
## Fahren

![ctc-app-doku-bild]({% link assets/images/ctc-app-doku/0101-control_fragment.png %})
              
Mit folgenden Buttons (Knöpfen) und Anzeigen können Sie die angezeigte Lok steuern:
* Mit (1) können Sie zwischen den verfügbaren Lokomotiven wechseln, also eine andere Lok auswählen.
* Mit (2) wird die Lok aktiviert (blau) und wieder pausiert (rot). 
  Beim Aktivieren werden die eingestellte Fahrtrichtung (3)/(4) und Geschwindigkeit (5) eingestellt, d.h. wenn der Schieberegler (5) auf einem Wert größer Null steht, fährt die Lok los.
* Mit (4) aktivieren Sie die Fahrtrichtung vorwärts, mit (5) rückwärts.
  In beiden Fällen wird beim Klick auf den Button auch (2) auf aktiviert (blau) gestellt.
  Die eingestellte Fahrtrichtung erkennen Sie am farbigen Pfeil im Button: Gelb für vorwärts, rot für rückwärts.
* Mit (5) stellen Sie die Geschwindigkeit ein.
  Der eingestellte Wert wird darunter als Zahl (8) angezeigt.
* Mit (6) wird die Geschwindigkeit schrittweise verringert unf mit (7) erhöht.

Bild und Name der Lok können über deren [Konfiguration]({% link _app_doku/040-module-konfigurieren.md %}) verändert werden.
Beides wird in der Lok gespeichert.
      
## Weitere Funktionen und Anzeigen

![ctc-app-doku-bild]({% link assets/images/ctc-app-doku/0101-control_fragment-auto.png %})

* Das WLAN-Symbol (10) zeigt an, ob die Lok aktuell erreichbar ist (grün) sich seit mehr als 3 Sekunden (gelb) bzw. mehr als 10 Sekunden (rot) nicht mehr gemeldet hat.
  Beim grünen WLAN-Symbol wird außerdem die Signalstärke angezeigt, sie dazu auch [Kapitel 3 "WiFi"]({% link _app_doku/030-wifi.md %}).
* Links vom Lok-Name wird der Status der Lok (11) angezeigt, die einzelnen Symbole sind in [Kapitel 1.3 "Autopilot"]({% link _app_doku/013-lok-autopilot.md %}) beschrieben.
* Wenn Ihre Lok über einen IR-Empfänger verfügt kann mit (12) die Orientierung der Lok eingestellt werden: r = im Uhrzeigersinn, l = gegen Uhrzeigersinn. 
  Das ist wichtig damit die Lok nur dann vor einem roten Signal hält, wenn sie von vorne darauf zu fährt. 
  Nach Überfahren von zwei Balisen desselben Blocks stellt die Lok ihre Orientierung selbst ein und das Symbol auf dem Button wird ggf. angepasst. 
* Mit (13) werden der Automatikbetrieb ein- und ausgeschaltet.  
  Ein "A" auf dem Button zeigt den aktivierten Automatikbetrieb an.
  Wird eine Lok durch einen Balise zum Bremsen/Halten vor einem roten Signal gezwungen aktiviert sich der Automatikbetrieb von selbst, was dann am Symbol auf dem Button zu erkennen ist.
* In der Mitte (14) finden sich noch einige Statusinformationen zur Lok, auf die an geeigneter Stelle dieser Doku eingegangen wird.