---
layout: doku
title: "WiFi"
chapter: 3
---
Dieses Kapitel fasst alles zusammen, was mit Konfiguration, Einrichtung und Optimierung des WLANs (WiFi) zu tun hat.

## Grundlagen

Die ersten Schritte mit den CTC-Modulen und deren WLAN stellen sich immer wieder als größere Hürde heraus.
Deshalb beginnen wir hier mit ein paar Grundlagen.

Grundsätzlich nutzt CTC die Standard-Funktionalität des WLANs.
Der Begriff WLAN ist nur im deutschen gebräuchlich und zu allgemein - der genutzte Standard heißt "**WiFi**".
Auch unser CTC-Router ist eine ganz normale Fritz!Box auf der wir ein paar wenige Schritte der Einrichtung für Sie vornehmen (siehe unten Abschnitt "Router-Konfiguration").
            
Ein gewöhnliches WLAN hat immer einen Namen (**SSID**) und sollte eine **Verschlüsselung** (Passwort) haben, damit nicht jeder ungehindert hineinkommt.
Die Verschlüsselung kennt die Standards **WPA, WPA2 und WPA3**.
* WPA ist weitgehend wirkungslos, da es sich mit wenig Aufwand und frei verfügbaren Tools knacken lässt.
* WPA2 ist ebenfalls knackbar, aber mit deutlich höherem Aufwand.
  Bei den meisten Geräten (auch bei den CTC-Modulen) ist das aber das Maximum an Sicherheit, das unterstützt wird.
* Ist nur WPA3 aktiviert, kann sich das CTC-Modul nicht mit dem WLAN verbinden.
* Sie sollten Ihr Modellbahn-WLAN also auf "WPA2" oder "WPA2 + WPA3" einstellen. 
 
Zusätzlich zur nicht eindeutigen SSID gibt es auch eine **MAC-Adresse** des WLANs.
Diese ist weltweit eindeutig, wird aber normalerweise nur auf technischer Ebene verwendet und dem Anwender oft gar nicht gezeigt.

Ein **WLAN-Access-Point** ist ein Gerät, das über Funk (WLAN) den Zugang zu einem Netzwerk anbietet und keine weiteren Netzwerkfunktionalitäten anbietet.
Ein **WLAN-Router hingegen** enthält den vollen Umfang einer Netzwerkzentrale und ermöglicht in der Regel auch den Zugang zum Internet.
Für CTC wichtig ist, dass der WLAN-Router mit der **DHCP**-Funktion die für die Kommunikation relevanten **IP-Adressen** vergibt.
Die kleinste Variante eins WLAN-Routers ist der **Hotspot** eines Mobiltelefons.

Es kann beliebig viele WLANs mit derselben SSID geben.
Das nutzt man z.B. beim sogenannten **Mesh** aus, um mithilfe mehrerer WLAN-Access-Points (und einem WLAN-Router) das WLAN für eine größere Fläche verfügbar zu machen.
Dabei müssen aber alle diese WLAN-Access-Points mit dem zugehörigen WLAN-Router verbunden sein.
Diese WLAN-Access-Points nennt man auch "**WLAN-Repeater**".
Wenn das CTC-Modul mehrere WLANs mit derselben SSID sieht, ist es schwer vorhersehbar mit welchem es sich verbinden wird. 

Ein CTC-Modul kennt zwei Betriebsarten für das WLAN:
* Im **Konfigurationsmodus** baut das CTC-Modul sein eigenes WLAN auf, verhält sich also wie ein Mini-Router.
  In diesem Modus kann das Modul nichts anderes als die Konfigurationen empfangen, insbesondere SSID und Passwort des Modellbahn-WLANs.
  Das WLAN des CTC-Moduls hat nur eine SSID, aber kein Passwort, ist also unverschlüsselt.
* Im normaler Betrieb kennt das CTC-Modul eine SSID und ein zugehöriges Passwort und versucht sich beim Start in das WLAN mit dieser SSID einzubuchen. 

## WLAN-Icon

Da die Empfangsqualität des WiFi-Signals einen wesentlichen Einfluss auf CTC hat, wird diese an diversen Stellen mit folgenden Symbolen angezeigt:

| **Symbol**                                                                                   | **Bedeutung**                       |
| ![ctc-app-doku-icon]({% link assets/images/ctc-app-doku/wlan_connected_green_top.png %})     | Optimaler WiFi-Empfang (> -67 dB)   |
| ![ctc-app-doku-icon]({% link assets/images/ctc-app-doku/wlan_connected_green_good.png %})    | Guter WiFi-Empfang (> -70 dB)       |
| ![ctc-app-doku-icon]({% link assets/images/ctc-app-doku/wlan_connected_green_ok.png %})      | Akzeptabler WiFi-Empfang (> -80 dB) |
| ![ctc-app-doku-icon]({% link assets/images/ctc-app-doku/wlan_connected_green_bad.png %})     | Schlechter WiFi-Empfang (<= -80 dB) |
| ![ctc-app-doku-icon]({% link assets/images/ctc-app-doku/wlan_connected_green_black.png %})   | CTC-Modul mit alter Firmware, die noch keine Signalstärke überträgt. |
| ![ctc-app-doku-icon]({% link assets/images/ctc-app-doku/wlan_connecting_yellow_black.png %}) | CTC-Modul hat sich mindestens 3 Sekunden nicht mehr gemeldet. |
| ![ctc-app-doku-icon]({% link assets/images/ctc-app-doku/wlan_disconnected_red_black.png %})  | CTC-Modul hat sich über 10 Sekunden lang nicht mehr gemeldet |

Die letzten beiden Symbole (gelb und rot) sagen nichts über die Empfangsqualität des CTC-Moduls aus.
Es kann durchaus auch sein, dass das CTC-Modul noch gut mit dem WLAN verbunden ist, und die CTC-App die Verbindung zum WLAN verloren hat.
Dies ist vor allem dann naheliegend, wenn alle CTC-Module auf gelb oder rot wechseln.
       
## Router-Konfiguration

In diesem Abschnitt sammeln wir Dokumente zur Konfiguration von Routern:
* [Fritz!Box 4040]({% link assets/pdf/CTC-Router-Setup-FB-4040.pdf %}) 

## Tipps zur WLAN-Verbindung

Sowohl Laptops als auch SmartPhones und Tablets habe einen immer stärker werdenden Drang zum Internet.
Das führt dazu, dass sie sich ungern mit WLANs ohne Internetzugriff verbinden und diese gerne auch spontan wieder verlassen.
CTC braucht keinen Internetzugang, weshalb das Modellbahn-WLAN gerne auch ohne Verbindung zum Internet eingerichtet wird.

Die Kunst ist nur den Geräten, auf denen die CTC-App läuft beizubringen, sich mit dem Modellbahn-WLAN zu verbinden und dieses auch nicht spontan zu verlassen.
Dazu ein paar Tipps:
* PCs und Laptops, die beim Modellbahnbetrieb nicht bewegt werden müssen, per Netzwerkkabel anschließen.
* Die Option "automatisch Verbinden" für alle WLANs ausschalten oder höchstens fürs Modellbahn-WLAN auswählen
* Das SmartPhone auf Flugmodus schalten und dann nur das WLAN aktivieren.
* Bei Problemen prüfen, ob PC bzw. Mobilgerät noch mit dem richtigen WLAN verbunden sind. 

## Tipps zur Optimierung der WiFi-Empfangsqualität

* Laptops oder PC, die nicht bewegt werden, sollten Sie per LAN-Kabel mit dem WLAN-Router verbinden.
  Oft hat sich bei Empfangsproblemen gezeigt, dass die Ursache der Laptop und nicht das CTC-Modul war. 
* Nutzen Sie das 5 GHz-WLAN für das Gerät, auf dem die CTC-App installiert ist.
  Damit erzwingen Sie den Umweg über den WLAN-Router, der oftmals eine deutlich günstigere Position zur Kommunikation mit den CTC-Modulen hat.
* Für SmartPhone/Tablet gibt es sogenannte WiFi-Analyzer-Apps, z.B. für Android den werbefreien ["WiFi Analyzer (open-source)"](https://play.google.com/store/apps/details?id=com.vrem.wifianalyzer).
  Mit diesen können Sie sich an verschiedene Stellen Ihrer Anlage bewegen und prüfen wie gut dort der Empfang Ihres Modellbahn-WLANs ist und wie viel konkurrierende WLANs Sie haben.
  Gegebenenfalls lohnt es sich den Kanal des Modellbahn-WLANS zu ändern.
* Mithilfe des [WiFi-Monitors]({% link _app_doku/034-wifi-monitor.md %}) können sie sich die Signalstärke von Loks genauer und auch über die Zeit anzeigen lassen.
  Die angezeigte Grafik können Sie nutzen, um die optimale Position für Ihren WLAN-Router zu finden.