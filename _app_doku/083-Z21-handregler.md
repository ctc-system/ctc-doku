---
layout: doku
title: "Z21 Handregler nutzen"
chapter: 8.3
---
Über den Menüpunkt "Z21 Zentrale Starten" kann in der PC-Version der CTC-App die Emulation einer Z21-Zentrale aktiviert werden.
Dabei reagiert die CTC-App auf Kommandos, die per Z21-WLAN-Protokoll eintreffen und leitete diese an CTC-Module weiter.

**Hinweise:**
* Ab CTC-App Version 4.05 ist es möglich die Geschwindigkeit und Funktionen einer Lok zu regeln.
* Ab CTC-App Version 4.06 können auch Weichen geschaltet werden.
* Getestet haben wir mit Rocos WLAN-Maus und Rocos Z21 Android App.

## Lok-Adresse einstellen
     
Die Lok-Adresse stellt man ein, indem man die Config der Lok öffnet und dort am Haupt-Motor den Parameter "dccAddr" hinzufügt:

![Config Lokadresse]({% link assets/images/ctc-app-doku/Config-DCC-Lokadresse.png %})
                                                                                   
## Lok-Funktion zuordnen

Um Lok-Funktionen zu den DCC-Funktionsnummern geht man ebenfalls in die Config der Lok.
Dort wählt man die Funktion, die mit einer DCC-Funktion verknüpft werden soll und fügt dort die Parameter "dccFx+" sowie "dccFx-" hinzu, wobei "x" für die Funktionsnummer steht.
Dann setzt man bei "dccFx+" den Wert des StateScripts das bei "Funktion ein" gerufen werden soll und bei "dccFx-" den Wert des StateScripts das bei "Funktion aus" gerufen werden soll.

Im Beispiel wurde DCC F0 der Funktion "Light" zugeordnet und festgelegt, dass das diese bei "ein" das Skript "1" (on) und bei "aus" das Skript "0" (off) aufrufen soll:  

![Config Lokfunktion]({% link assets/images/ctc-app-doku/Config-DCC-Lokfunktion.png %})

## Lok fahren

Sobald die Z21 Zentrale gestartet ist und der Handregler auf die IP-Adresse der CTC-App konfiguriert ist, können alle CTC-Loks gesteuert werden, die über eine "dccAddr" verfügen.

Die DCC-Adresse wird ab CTC-App Version 4.06 bei "Loks" in der Spalte "Nr." angezeigt.

**Hinweis:** Die Lok kann nur gesteuert werden, wenn sie in der CTC-App für manuelle Steuerung eingestellt wurde.

## Weichen-Adresse einstellen

Die Weichen-Adresse stellt man ein, indem man die Config der Weiche öffnet und dort an der Funktion den Parameter "dccAddr" hinzufügt:

![Config Lokadresse]({% link assets/images/ctc-app-doku/Config-DCC-Weichenadresse.png %})
                         
Die Skripte der Funktion werden in der bei "Button Position" angegebenen Reihenfolge zugeordnet:
* 0 für dccAddr "aus"
* 1 für dccAddr "ein"
* Alle weiteren für den Zustand "ein" der auf dccAddr folgenden Adressen. Im Screenshot oben also Adresse 31 "ein" für das Skript an Position 2 ("right").
