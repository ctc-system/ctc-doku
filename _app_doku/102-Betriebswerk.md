---
layout: doku
title: "Betriebswerk (BW)"
chapter: 10.2
---
Beim großen Vorbild dient das Betriebswerk (BW) als Heimat von Loks und Waggons.
Bei CTC ist es erst mal nur die Waggon-Verwaltung, d.h. hier werden die IDs der auf die Waggons geklebten NFC-Tags mit einem Bild, Name, ... verknüpft.

## Betriebswerk anlegen

Zum Anlegen eines Betriebswerks (BW) öffnen Sie den Config-Dialog des CTC-Weichenmoduls oder CTC-Multi-IO-Boards, in dem Sie das BW speichern wollen.
Dort klicken Sie auf "Betriebswerk (BW) anlegen".
Es erscheint folgender Dialog:

![BW anlegen]({% link assets/images/ctc-app-doku/BW-Anlegen.png %})
                                                               
Geben Sie einen Namen ein - als Vorschlag sehen Sie den Namen des CTC-Moduls.
Dann klicken Sie auf "Anlegen".

Ab jetzt können sie über die Ansicht "BW-Anzeige" die Waggons des neuen BWs verwalten.
           
## Betriebswerk anzeigen
                          
Über das Menü "Ansicht" können Sie die BW-Anzeige öffnen:

![BW-Anzeige]({% link assets/images/ctc-app-doku/BW-Anzeige.png %})
                                                         
Hinter "Betriebswerk (BW):" finden Sie eine Klapp-Liste mit allen Betriebswerken Ihrer Modellbahn. 

## Waggons zu einem BW hinzufügen

Fahren Sie mit einem neuen Waggon zuerst über den NFC-Reader.
Dann ist die ID des NFC-Tags der CTC-App bekannt und Sie können die restlichen Daten dews Waggons erfassen.

**Hinweis:** Damit ein gelesenes Tag als neuer Waggon erkannt werden kann, muss der NFC-Reader einem Block zugeordnet sein.
Das machen Sie beim Platzieren des NFC-Readers im Gleisbild, ähnlich wie beim Anlegen von NFC-Balisen.  

Dazu klicken Sie nun auf "Neuer Waggon".
Es öffnet sich eine Liste mit allen noch nicht zugeordneten NFC-Tags:

![Neuer Waggon - Tag auswählen]({% link assets/images/ctc-app-doku/BW-Neuer-Waggon.png %})

Wählen Sie das Tag aus und klicken Sie auf Übernehmen:

![Neuen Waggon bearbeiten]({% link assets/images/ctc-app-doku/BW-Neuer-Waggon-2.png %})

Jetzt können Sie das Waggon-Bild auswählen, dem Waggon einen Namen geben, sowie die Länge des Waggons und die Position des Tags eingeben.

Sobald Sie auf "Waggon hochladen" klicken wird der Waggon im Betriebswerk (auf dem CTC-Modul) gespeichert.

## Waggon bearbeiten

Zum Bearbeiten eines Waggons klicken Sie in der Waggon-Liste des Betriebswerks auf den Stift rechts vom Waggon.
Es öffnet sich folgender Dialog:

![Waggon bearbeiten]({% link assets/images/ctc-app-doku/BW-Waggon-bearbeiten.png %})
