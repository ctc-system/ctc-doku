---
layout: doku
title: "Config-Beispiel: Zeitgesteuerter Halt"
chapter: B.02
---
In diesem Beispiel wird ein Timer dazu verwendet mithilfe eines Lichtsignals einen Zug für eine bestimmte Zeit halten zu lassen, z.B. in einem Bahnhof.

Ausgangspunkt für dieses Beispiel ist ein Signal, das mit zwei Balisen verknüpft ist: Eine zum Abbremsen und eine zweite zum punktgenauen Halten.
Wie das funktioniert ist in [Kapitel 4.3 "Config - Aktionen verknüpfen"]({% link _app_doku/043-aktionen-verknuepfen.md %}) sowohl für IR-Balisen als auf NFC-Balisen beschrieben.

Für den zeitgesteuerten Halt fügen Sie zwei Timer und einen Trigger zum Signal hinzu.

Der Trigger soll auslösen, wenn eine beliebige Lok die Balise direkt vor dem Signal erreicht und dort stehen bleibt.
Dazu öffnen Sie die Config des Signals, klicken unter "Aktionen" das Signal an und klicken dann auf den Plus-Button rechts neben "Produkte und Aktionen":

![Config zeitgesteuerter Halt 0]({% link assets/images/ctc-app-doku/Timer-Ampel-0.png %})
                        
Dann wählen Sie "Trigger" und klicken auf "OK".
Im Beispiel heißt der neue Trigger "B-Einfahrt-re-trg".

Auf dieselbe weise legen Sie auch die beiden Timer "B-Einfahrt-freigeben" und "B-Einfahrt-sperren" an.
Nach dem Anlegen sollten Sie der neuen Aktion noch einen passenden Namen geben, indem Sie auf den Button "..." neben Name in "Details der Aktion" klicken.
Erst dann bearbeiten Sie die einzelnen Skripte.

Beim Trigger klicken Sie auf den Button "Quelle" um die Balise als Quelle des Ereignisses auszuwählen.
Der eigentliche Auslöser des Ereignisses ist die Lok.
Da die Zeitsteuerung aber für jede Lok funktionieren soll, wird bei "Auslöser" nichts ausgewählt.
Der Stern bei "auf (MAC)" erscheint nach der Auswahl der Quelle automatisch.

Der dem Skript übergebene Buchstabe ist das Kommando, das die Lok von der Balise empfangen hat.
Das nutzen Sie aus um nur dann, wenn die Lok angehalten wurde ('h'), auch den Timer für das Andern des Signals auf Grün zu starten.
Das zweite "Wenn" reagiert auf alle anderen Kommandos und mach nichts außer den Trigger zurückzusetzen, sodass er beim nächsten Halt wieder reagiert.

Um den Timer im Skript für Halt ('h') zu starten klciken Sie auf die Zeile "Wenn 'BA2' von * == 'h'" und dann auf "Hinzufügen".
Im Popup wählen sie den Befehl "call".
Anschließend können Sie in der Liste unter "Führe Aktion aus" den vorher angelegten Timer "B-Einfahrt-freigeben" auswählen 

![Config zeitgesteuerter Halt 1]({% link assets/images/ctc-app-doku/Timer-Ampel-1.png %})

Hinweis: Ein Trigger löst nur aus, wenn der übergebene Wert von seinem eigenen abweicht.
Dazu speichert der Trigger den zuletzt erhaltenen Wert.
Welchen Wert ein Trigger hat können Sie im Schaltpult sehen, wenn Sie dort die passende Action-Gruppe (normalerweise "Trigger") wählen:

![Wert des Triggers]({% link assets/images/ctc-app-doku/Timer-Ampel-Triggerwert.png %})

Der Timer zum Öffnen des Signales ("B-Einfahrt-freigeben") erhält nur einen einzigen Befehl.
Nämlich einen Aufruf ("call") der Funktion Grün (Hp1_Light) des Lichtsignals: 

![Config zeitgesteuerter Halt 2]({% link assets/images/ctc-app-doku/Timer-Ampel-2.png %})

Damit das Lichtsignal nach Durchfahrt der Lok wieder auf Rot geht, ergänzen Sie die Funktion "Hp1_Light ('f')" um den Start des Timers zum Schließen des Signals ("B-Einfahrt-sperren"):

![Config zeitgesteuerter Halt 3]({% link assets/images/ctc-app-doku/Timer-Ampel-3.png %})

Der Timer zum Schließen des Signales ("B-Einfahrt-sperren") erhält nur einen einzigen Befehl.
Nämlich einen Aufruf ("call") der Funktion Rot (Hp0_Light) des Lichtsignals:

![Config zeitgesteuerter Halt 4]({% link assets/images/ctc-app-doku/Timer-Ampel-4.png %})
