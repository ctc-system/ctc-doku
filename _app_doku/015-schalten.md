---
layout: doku
title: "Schaltpult: Schalten von Weichen, Signalen, ..."
chapter: 1.5
---
Über den Reiter "Schaltpult" erreichen Sie die Buttons zum Schalten von Weichen, Signalen, ...:

![ctc-app-doku-bild]({% link assets/images/ctc-app-doku/Schaltpult.png %})

Die Schaltfunktionen sind in Gruppen unterteilt.
Diese werden über den [Produktkatalog]({% link _app_doku/041-produkte-anschliessen.md %}) vorbelegt, können aber in der Konfiguration der CTC-App beliebig angepasst werden.
Auch das Hinzufügen neuer Gruppen ist möglich.
