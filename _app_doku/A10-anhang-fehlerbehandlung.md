---
layout: doku
title: "Anhang - Fehlerbehandlung"
chapter: A1
---
Zu diesem Thema finden Sie im Bereich **[Fehlersuche FAQ]({% link de/faq/Fehlersuche-FAQ.md %})** weitere Informationen.

Die Statusanzeige (LED bzw. Frontlicht) ist im [Kapitel 7 - Statusanzeigen der Module]({% link _app_doku/033-statusanzeigen-der-module.md %}) beschrieben.

## Log-Ausgaben der CTC-Module

Ab CTC-App 4.19 und zugehöriger Firmware werden die Log-Meldungen der CTC-Module an die CTC-App übertragen und im Config-Dialog angezeigt, siehe "Log-Ausgabe" in [Kapitel 4 "Module konfigurieren"]({% link _app_doku/040-module-konfigurieren.md %}).

Dort finden sich z.B. auch Hinweise auf Konfigurationsfehler.
Außerdem können diese Ausgaben uns bei Support-Anfragen helfen eine Fehlerursache einzugrenzen.

Möchten sie die Meldungen von Start eines CTC-Moduls sehen gehen Sie wie folgt vor:
1. Öffnen Sie die CTC-App und schalten Sie den Strom für das betreffende CTC-Modul ein.
2. Öffnen Sie den Config-Dialog des CTC-Moduls.
3. Schalten Sie den Strom für das betreffende CTC-Modul für ein paar Sekunden aus und dann wieder an.
4. Sobald das CTC-Modul wieder in der CTC-App sichtbar ist (WLAN-Symbol grün) erscheinen die Meldungen vom Start des CTC-Moduls in der Log-Ausgabe. 

## Config-Dateien der CTC-Module

Der einfachste Weg die Config-Dateien aller CTC-Module herunterzuladen und zu sichern geht über den Menüpunkt "Einstellungen / Config Backup" der CTC-App (Desktop).
Dabei wählen Sie einen Ordner auf Ihrer Festplatte als Backup-Ordner aus.
Danach wird für jedes erreichbare CTC-Modul in diesem Backup-Ordner ein Ordner mit dem Namen des CTC-Moduls angelegt und in diesem alle Config-Dateien gesichert.

Bei Support-Anfragen ist es oft hilfreich, wenn Sie die Config-Dateien der betroffenen CTC-Module mitschicken.

## Log-Dateien der CTC-App

Bei der Support-Anfragen oder auch Fehlerberichten hilft uns die Log-Datei der CTC-App oft weiter.
Die Desktop-App legt in Ihrem Benutzerverzeichnis (Persönlicher Order) einen Ordner "CTC-App" an und darunter einen Ordner "logs".
Dort finden Sie die Log-Datei unter dem Namen "pi-rail.log".
                                           
Wie man das Benutzerverzeichnis findet haben wir im Folgenden zusammengetragen.
Manchmal frage ich mich, ob manche Betriebssystemhersteller nicht besser Scherzartikel statt Software produzieren sollten.

### Benutzerverzeichnis auf Linux

Unter Linux ist das Benutzerverzeichnis (User Home) "/home/USER", wobei USER für Ihren Benutzernamen steht.
Alle gängigen Datei-Explorer für Linux befinden sich nach dem Öffnen im Benutzerverzeichnis. 

### Benutzerverzeichnis auf MacOSX

Der Apfel treibt ein nettes Versteckspiel mit Ihnen.
Als Benutzerverzeichnis wird uns hier "/home/USER/Library/Application Support" zugeteilt.
Diesen Ordner erreichen Sie im Finder, indem Sie das Menü "Gehe zu" anklicken und dann die ALT-Taste drücken.
Schwups ist ein neuer Menüeintrag "Library" da, den Sie dann anklicken.
Anschließend funktioniert der Finder wieder wie erwartet: Ordner "Application Support" anklicken, dann "CTC-App" und schließlich "logs". 

### Benutzerverzeichnis auf Windows

Unter Windows ist das Benutzerverzeichnis (derzeit) prominent in der Leiste links im Explorers platziert.
Direkt unter Desktop findet sich als einer der ersten Einträge ein Ordner mit dem eignen Namen.
Danach wird es undurchsichtig.
Hier hat man uns den Ordner "AppData" und darunter "Roaming" zugeteilt.
Dort angekommen öffnen Sie den Ornder "CTC-App" und dort den Ordner "logs".

