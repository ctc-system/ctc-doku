---
layout: umbau
title: "Anschluss M-Gleis Weiche"
modul: Weichenmodul Universal
date: 2025-01-04 
hersteller: Märklin
produkte: Weichen 5117, 5118, 5119, 5137, 5138, 5139, 5140, 5141, 5142, 5202, 5203, 5204
umbauart: Doppelspule
author: Peter Rudolph
---
In diesem Artikel zeige ich Ihnen wie Sie ein [CTC-Weichenmodul]({% link _data_sheet/CTC-Weichenmodul-H0.md %}) mit der [Artikelnummer WI-M-4L-U]({{site.baseurl}}/shop.html#!/CTC-Weichenmodul-universal/p/725604076/category=176888458) in ein Märklin M-Gleis einbauen.

![M-Weiche fertig]({% link assets/images/anschliessen/weiche-m-gleis/M-Weiche-fertig.jpg %})

Zuerst löten wir passende Stecker an das CTC-Weichenmodul an:
![M-Weiche fertig]({% link assets/images/anschliessen/weiche-m-gleis/Weichenmodul-M-Gleis.jpg %})

**Hinweis:** Die Variante auf diesem Foto ist übrigens noch eine alte Version des CTC-Weichenmoduls, die noch nicht für Wechselstrom (AC) geeignet war, da der Stützkondensator fehlt (siehe dazu auch den Hinweis am Ende des Artikels).

Wie auch beim Umbau von alten Loks muss darauf geachtet werden, dass alle Verbindungen zum Gleis durch eine Diode abgesichert werden.
Ansonsten riskieren Sie irreparablen Schaden am CTC-Modul.

Im Falle unserer M-Gleis-Weiche ist die Lampe auf der einen Seite mit dem Elektromagnet-Antrieb und auf der anderen Seite direkt mit dem Gleis verbunden.
Um die Elektronik zu schützen, bauen wir in diese Verbindung eine Diode ein:
![M-Weiche Diode an Lampe]({% link assets/images/anschliessen/weiche-m-gleis/M-Weiche-Diode.jpg %})

Den einen Gleiskontakt (Masse) holen wir uns über eine Lötöse unter der Befestigung des Weichenantriebs (braunes Kabel).
Den anderen Gleiskontakt entweder über die bekannten Laschen oder wie wir durch Anlöten am Mittelleiter (gelbes Kabel).
Der Weichenantrieb selbst wird wie gewohnt über das gelbe und die beiden blauen Kabel angeschlossen:
![M-Weiche Diode an Lampe]({% link assets/images/anschliessen/weiche-m-gleis/M-Weiche-angeschlossen.jpg %})

**ACHTUNG**: Für den Betrieb mit Wechselstrom (AC) wie ihn z.B. die klassischen Märklin-Trafos liefern, muss das CTC-Modul **unbedingt um einen Stützkondensator** ergänzt werden, wie er im Bild oben (blaues Bauteil) zu sehen ist.

Nun wird das CTC-Modul noch in einen Schrumpfschlauch geschoben und kann dann vom Deckel des Weichenantriebs eingeklemmt werden, wie es das einleitende Bild zeigt. 

## CTC-Modul ins WLAN einbuchen

Wenn das CTC-Weichenmodul noch nicht ins Modellbahn-WLAN eingebucht ist, müssen Sie dies als Erstes erledigen.
Die Anleitung dazu finden Sie in der Bedienungsanleitung im [Kapitel 3.1 "Module ins WLAN einbuchen"]({% link _app_doku/031-module-einbuchen.md %}).

## Konfiguration öffnen

Ein eingebuchtes CTC-Weichenmodul finden Sie unter seinem Namen in der Modul-Liste in der CTC-App.
Dort klicken Sie auf das Stift-Symbol, um die "Schaltkasten-Konfiguration" zu öffnen.

![Config öffnen Modulliste]({% link assets/images/anschliessen/config-oeffnen/Config-oeffnen-1.png %})

Dort klicken Sie auf den Button "Config ändern":

![Config ändern]({% link assets/images/anschliessen/config-oeffnen/Config-oeffnen-2.png %})

## Produkt hinzufügen

Im Config-Dialog klicken Sie auf den Plus-Button rechts von "Angeschlossene Produkte" um auszuwählen, was für ein Produkt Sie angeschlossen haben:

![Config bearbeiten 1]({% link assets/images/anschliessen/weiche-magnetisch/Weiche-Config-1.png %})

Wählen Sie den Produktkatalog "universell-Weichen" und klicken Sie dann auf "Übernehmen":

![Config bearbeiten 2]({% link assets/images/anschliessen/weiche-magnetisch/Weiche-Config-2.png %})

Geben Sie Ihrer Weiche einen sinnvollen Namen und wählen Sie das passende Produkt sowie Produktkonfiguration aus.
Dann klicken Sie auf "Übernehmen":

![Config bearbeiten 3]({% link assets/images/anschliessen/weiche-magnetisch/Weiche-Config-3.png %})

## Produkt verbinden

Nun müssen Sie noch angeben, mit welchen Anschlüssen des CTC-Moduls die Weiche verbunden ist.
Der oben verwendete Stecker ist an "W1-gruen" und "W1-rot" angelötet.
Sie wählen "W1-grün" aus:

![Config bearbeiten 4]({% link assets/images/anschliessen/weiche-magnetisch/Weiche-Config-4.png %})

Dann können Sie durch Klick auf den Button "Testen" prüfen, ob die Weiche schaltet und in welche Richtung.
In meinem Beispiel schaltet W1-grün auf gerade.
Deshalb wähle ich nun bei "Anschlüsse und Parameter" die Zeile "gerade" aus.
Mit Klick auf "Verbinden" ordnen Sie dem links markierten "W1-gruen: Output-Pin #16" den Anschluss "gerade" zu:

![Config bearbeiten 5]({% link assets/images/anschliessen/weiche-magnetisch/Weiche-Config-5.png %})

Dann verbinden Sie noch auf dieselbe Weise "W1-rot" (Output-Pin #17) mit dem Anschluss "abbiegen":

![Config bearbeiten 6]({% link assets/images/anschliessen/weiche-magnetisch/Weiche-Config-6.png %})

## Konfiguration hochladen

Wenn alle Pins verbunden sind, können Sie den Button "Hochladen" klicken, um die Änderungen an der Config auf das CTC-Weichenmodul zu speichern:

![Config bearbeiten 7]({% link assets/images/anschliessen/weiche-magnetisch/Weiche-Config-7.png %})
                    
Das CTC-Weichenmodul startet neu.
 
## Weiche im Schaltpult

Nach wenigen Sekunden taucht die neu konfigurierte Weiche im Schaltpult auf:

![Weiche im Schaltpult]({% link assets/images/anschliessen/weiche-magnetisch/Weiche-Schaltpult.png %})
