---
layout: umbau
title: "Anschluss Rocoline Dreiwegweiche"
modul: Weichenmodul Universal
date: 2025-01-04 
hersteller: Roco
produkte: Dreiwegweiche 42543 mit Antrieb 42620
umbauart: Doppelspule
author: Peter Rudolph
---
Diese Einbauanleitung passt für die Dreiwegweiche 42543 aus dem Programm "Rocoline mit Bettung" von Roco:

**TODO Bild Dreiwegweiche Rocoline**

Sie verwendet zwei der Antriebe 42620.
Im Beispiel wird als Steckverbindung eine 3er-Stiftleiste an die Kabel angelötet:
Am [CTC-Weichenmodul]({% link _data_sheet/CTC-Weichenmodul-H0.md %}) mit der [Artikelnummer WI-M-4L-U]({{site.baseurl}}/shop.html#!/CTC-Weichenmodul-universal/p/725604076/category=176888458) finden sich drei Kabel, an die im Beispiel eine passende Buchse angelötet wurde.

## CTC-Modul einbauen und anschließen

Das CTC-Modul wird auf oder unter der Modellbahnanlage montiert.

So wird der Antrieb an das CTC-Weichenmodul angeschlossen:
* Die Stiftleisten von den Weichenantrieben in die Buchsen am CTC-Weichenmodul stecken.
* Das braune Kabel mit dem Minuspol der Stromversorgung oder dem Gleis verbinden.
* Das rote Kabel mit dem Pluspol oder dem anderen Gleis verbinden.

**TODO Anschlussplan**

Das folgende Bild zeigt eine Rechts-Weiche mit eingebauten Weichenantrieben und CTC-Weichenmodul.

**TODO Bild Dreiwegweiche mit Einbau**

An die vierpolige Buchse des Weichenmoduls können zwei IR-Balisen angeschlossen werden.
Im Beispiel auf dem Foto wurde die vierpolige Buchse entfernt und die Kabel (rot, blau, rosa, grau) an Kupferkontakte an der Gleisverbindung angelötet.

## CTC-Modul ins WLAN einbuchen

Wenn das CTC-Weichenmodul noch nicht ins Modellbahn-WLAN eingebucht ist, müssen Sie dies als Erstes erledigen.
Die Anleitung dazu finden Sie in der Bedienungsanleitung im [Kapitel 3.1 "Module ins WLAN einbuchen"]({% link _app_doku/031-module-einbuchen.md %}).

## Konfiguration öffnen

Ein eingebuchtes CTC-Weichenmodul finden Sie unter seinem Namen in der Modul-Liste in der CTC-App.
Dort klicken Sie auf das Stift-Symbol, um die "Schaltkasten-Konfiguration" zu öffnen.

![Config öffnen Modulliste]({% link assets/images/anschliessen/config-oeffnen/Config-oeffnen-1.png %})

Dort klicken Sie auf den Button "Config ändern":

![Config ändern]({% link assets/images/anschliessen/config-oeffnen/Config-oeffnen-2.png %})

## Produkt hinzufügen

Im Config-Dialog klicken Sie auf den Plus-Button rechts von "Angeschlossene Produkte" um auszuwählen, was für ein Produkt Sie angeschlossen haben:

![Config bearbeiten 1]({% link assets/images/anschliessen/dreiweg-magnetisch/Dreiweg-Config-1.png %})

Wählen Sie den Produktkatalog "universell-Weichen" und klicken Sie dann auf "Übernehmen":

![Config bearbeiten 2]({% link assets/images/anschliessen/dreiweg-magnetisch/Dreiweg-Config-2.png %})

Geben Sie Ihrer Weiche einen sinnvollen Namen und wählen Sie das passende Produkt sowie Produktkonfiguration aus.
Dann klicken Sie auf "Übernehmen":

![Config bearbeiten 3]({% link assets/images/anschliessen/dreiweg-magnetisch/Dreiweg-Config-3.png %})

## Produkt verbinden

Nun müssen Sie noch angeben, mit welchen Anschlüssen des CTC-Moduls die Weiche verbunden ist.
Der oben verwendete Stecker ist an "W1-gruen" und "W1-rot" angelötet.
Sie wählen "W1-grün" aus:

![Config bearbeiten 4]({% link assets/images/anschliessen/dreiweg-magnetisch/Dreiweg-Config-4.png %})

Dann können Sie durch Klick auf den Button "Testen" prüfen, ob die Weiche schaltet und in welche Richtung.
In meinem Beispiel schaltet W1-grün auf gerade.
Deshalb wähle ich nun bei "Anschlüsse und Parameter" die Zeile "gerade" aus.
Mit Klick auf "Verbinden" ordnen Sie dem links markierten "W1-gruen: Output-Pin #16" den Anschluss "links" zu:

![Config bearbeiten 5]({% link assets/images/anschliessen/dreiweg-magnetisch/Dreiweg-Config-5.png %})

Dann verbinden Sie noch auf dieselbe Weise "W1-rot" (Output-Pin #17) mit dem Anschluss "li-gerade":

![Config bearbeiten 6]({% link assets/images/anschliessen/dreiweg-magnetisch/Dreiweg-Config-6.png %})
                 
Für den zweiten Antrieb gehen sie genauso vor wie für den ersten.

## Konfiguration hochladen

Wenn alle Pins verbunden sind, können Sie den Button "Hochladen" klicken, um die Änderungen an der Config auf das CTC-Weichenmodul zu speichern:

![Config bearbeiten 7]({% link assets/images/anschliessen/dreiweg-magnetisch/Dreiweg-Config-7.png %})

Das CTC-Weichenmodul startet neu.

## Weiche im Schaltpult

Nach wenigen Sekunden taucht die neu konfigurierte Dreiwegweiche im Schaltpult auf:

**TODO**

