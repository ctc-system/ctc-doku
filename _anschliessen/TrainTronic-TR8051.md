---
layout: umbau
title: "Anschluss TrainTronic TR8051"
modul: Weichenmodul Universal
date: 2025-01-10 
hersteller: TrainTronic
produkte: Weichenantrieb TR8051
umbauart: Motor-3pol
author: Peter Rudolph
---
Diese Einbauanleitung passt für den motorischen Weichenantrieb TR8051 von TrainTronic
![TR8051 am CTC-Modul]({% link assets/images/anschliessen/weiche-TR8051/traintronic-TR8051.jpg %})

Der TR8051 wird über zwei Taster geschaltet.
Das Verhalten eines Taster kann man mithilfe eines Optokopplers nachbilden.

**Hinweis:** Vom direkten Anschluss an die Schaltausgänge eines CTC-Weichenmoduls sollte man absehen, da nicht vorhersehbar ist, ob dabei der TR8051 oder auch das CTC-Weichenmodul dauerhaften Schaden nimmt.  

Am [CTC-Weichenmodul]({% link _data_sheet/CTC-Weichenmodul-H0.md %}) mit der [Artikelnummer WI-M-4L-U]({{site.baseurl}}/shop.html#!/CTC-Weichenmodul-universal/p/725604076/category=176888458) finden sich drei Kabel, an die im Beispiel ein passender Adapter angelötet wurde.
Der Adapter (siehe Schaltplan unten) besteht aus:
* einem 2-fach Optokoppler LTV-826
* drei 2,2 kOhm Widerständen
* einer LED zur Anzeige des Schaltimpulses
* und einer 3er Schraubklemme.

![TR8051 am CTC-Modul]({% link assets/images/anschliessen/weiche-TR8051/Optokoppler-Adapter.jpg %})

## CTC-Modul einbauen und anschließen

Das CTC-Modul wird auf oder unter der Modellbahnanlage montiert.

So wird der Antrieb an das CTC-Weichenmodul angeschlossen:
* Die mittlere Klemme des Adapters (beide Emitter der Optokoppler) mit Klemme 9 (Taster/LED) des TR8051 verbinden
* Die beiden äußeren Klemmen (jeweils Collector der Optokoppler) des Adapters mit Klemmen 8 (Taster/LED 2) und 10 (Taster/LED 1) des TR8051 verbinden.
* Die Klemmen 6 und 7 (Speisung/DCC) des TR8051 mit dem Gleis oder der Stromversorgung verbinden. 
* Das braune Kabel mit dem Minuspol der Stromversorgung oder dem Gleis verbinden.
* Das rote Kabel mit dem Pluspol oder dem anderen Gleis verbinden.
* Das gelbe Kabel vom Weichenantrieb ist mit beiden Anoden der Optokoppler verbunden.
* Die beiden blauen Kabel sind mit jeweils einer der Kathoden der beiden Optokoppler verbunden.

**Hinweis:** Weiche und Antrieb dürfen auch an unterschiedliche Stromversorgungen angeschlossen werden.  

![Anschluss Weiche]({% link assets/images/anschliessen/weiche-TR8051/Weiche_TR8051.png %})

Das folgende Bild zeigt eine Rechts-Weiche mit eingebautem Weichenantrieb und CTC-Weichenmodul.
![TR8051 am CTC-Modul]({% link assets/images/anschliessen/weiche-TR8051/traintronic-TR8051.jpg %})

An die verbleibenden drei einzelnen Kabel (2x blau und 1x gelb) kann ein Signal oder eine weitere Weiche angeschlossen werden.
An die vierpolige Buchse können zwei IR-Balisen angeschlossen werden.

## CTC-Modul ins WLAN einbuchen

Wenn das CTC-Weichenmodul noch nicht ins Modellbahn-WLAN eingebucht ist, müssen Sie dies als Erstes erledigen.
Die Anleitung dazu finden Sie in der Bedienungsanleitung im [Kapitel 3.1 "Module ins WLAN einbuchen"]({% link _app_doku/031-module-einbuchen.md %}).

## Konfiguration öffnen

Ein eingebuchtes CTC-Weichenmodul finden Sie unter seinem Namen in der Modul-Liste in der CTC-App.
Dort klicken Sie auf das Stift-Symbol, um die "Schaltkasten-Konfiguration" zu öffnen.

![Config öffnen Modulliste]({% link assets/images/anschliessen/config-oeffnen/Config-oeffnen-1.png %})

Dort klicken Sie auf den Button "Config ändern":

![Config ändern]({% link assets/images/anschliessen/config-oeffnen/Config-oeffnen-2.png %})

## Produkt hinzufügen
               
Der TrainTronic TR8051 sieht mithilfe des Adapters für CTC aus wie ein klassischer Doppelspulenantrieb. 

Im Config-Dialog klicken Sie auf den Plus-Button rechts von "Angeschlossene Produkte" um auszuwählen, was für ein Produkt Sie angeschlossen haben:

![Config bearbeiten 1]({% link assets/images/anschliessen/weiche-magnetisch/Weiche-Config-1.png %})

Wählen Sie den Produktkatalog "universell-Weichen" und klicken Sie dann auf "Übernehmen":

![Config bearbeiten 2]({% link assets/images/anschliessen/weiche-magnetisch/Weiche-Config-2.png %})

Geben Sie Ihrer Weiche einen sinnvollen Namen und wählen Sie das passende Produkt sowie Produktkonfiguration aus.
Dann klicken Sie auf "Übernehmen":

![Config bearbeiten 3]({% link assets/images/anschliessen/weiche-magnetisch/Weiche-Config-3.png %})

## Produkt verbinden

Nun müssen Sie noch angeben, mit welchen Anschlüssen des CTC-Moduls die Weiche verbunden ist.
Der oben verwendete Stecker ist an "W1-gruen" und "W1-rot" angelötet.
Sie wählen "W1-grün" aus:

![Config bearbeiten 4]({% link assets/images/anschliessen/weiche-magnetisch/Weiche-Config-4.png %})

Dann können Sie durch Klick auf den Button "Testen" prüfen, ob die Weiche schaltet und in welche Richtung.
In meinem Beispiel schaltet W1-grün auf gerade.
Deshalb wähle ich nun bei "Anschlüsse und Parameter" die Zeile "gerade" aus.
Mit Klick auf "Verbinden" ordnen Sie dem links markierten "W1-gruen: Output-Pin #16" den Anschluss "gerade" zu:

![Config bearbeiten 5]({% link assets/images/anschliessen/weiche-magnetisch/Weiche-Config-5.png %})

Dann verbinden Sie noch auf dieselbe Weise "W1-rot" (Output-Pin #17) mit dem Anschluss "abbiegen":

![Config bearbeiten 6]({% link assets/images/anschliessen/weiche-magnetisch/Weiche-Config-6.png %})

## Konfiguration hochladen

Wenn alle Pins verbunden sind, können Sie den Button "Hochladen" klicken, um die Änderungen an der Config auf das CTC-Weichenmodul zu speichern:

![Config bearbeiten 7]({% link assets/images/anschliessen/weiche-magnetisch/Weiche-Config-7.png %})

Das CTC-Weichenmodul startet neu.

## Weiche im Schaltpult

Nach wenigen Sekunden taucht die neu konfigurierte Weiche im Schaltpult auf:

![Weiche im Schaltpult]({% link assets/images/anschliessen/weiche-magnetisch/Weiche-Schaltpult.png %})