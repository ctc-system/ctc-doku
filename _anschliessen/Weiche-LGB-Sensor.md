---
layout: umbau
title: "Anschluss LGB-Weiche mit Sensor"
modul: Weichenmodul-G2 Outdoor
date: 2025-01-04
hersteller: LGB
produkte: Weichenantrieb 12010 + Sensor 12070
umbauart: Motor-2pol
author: Peter Rudolph
---
Gegenüber den klassischen Digitaldekodern kommt man mit CTC schon ohne besondere Maßnahmen recht dicht an den Wunsch heran, in der App zu shen, wie eine Weiche tatsächlich steht.
Erreicht wird dies dadurch, dass die CTC-App den Zustand basierend auf der Rückmeldung der Weiche anzeigt und nicht nur dass diese das Kommando abgeschickt hat.

Was der CTC-App trotzdem verborgen bleibt ist, wenn die Weiche mechanisch blockiert ist oder sie von Hand umgelegt wird.
Zumindest für LGB-Weichen gibt es eine perfekte Lösung, die ich Ihnen in diesem Artikel vorstellen möchte.

Unter der Artikelnummer 12070 bietet LGB einen Zusatzschalter, der auf den Weichenantrieb aufgesteckt wird.
Er enthält zwei Mikroschalter, die vom Weichenstellhebel betätigt werden.
Da der Zusatzschalter ohne weitere Feder mit der Weichenzunge verbunden ist, kann er zuverlässig deren Position wiedergeben.

Gegenüber dem üblichen Anschluss einer LGB-Weiche an das [CTC-Weichenmodul-G]({% link _data_sheet/CTC-Weichenmodul-G.md %}) (rot, braun, grün, grau) verbinden wir noch:
* GND (schwarz) mit den beiden mittleren Anschlüssen des LGB Zusatzschalters.
* VBB (orange) mit den Anschlüssen SENS-1 und SENS-2 des CTC-Weichenmodul-G.
* Die beiden äußeren Anschlüsse des LGB Zusatzschalters (blau) mit den Anschlüssen SENS-1-GND und SENS-2-GND des CTC-Weichenmodul-G.
                       
**Hinweis:** Das Foto zeigt noch das ältere CTC-Weichenmodul-G:

![Weiche LGB mit Sensor verkabelt]({% link assets/images/anschliessen/weiche-lgb/LGB-Weiche-Sensor-verkabelt.jpg %})

## CTC-Modul ins WLAN einbuchen

Wenn das CTC-Weichenmodul noch nicht ins Modellbahn-WLAN eingebucht ist, müssen Sie dies als Erstes erledigen.
Die Anleitung dazu finden Sie in der Bedienungsanleitung im [Kapitel 3.1 "Module ins WLAN einbuchen"]({% link _app_doku/031-module-einbuchen.md %}).

## Konfiguration öffnen

Ein eingebuchtes CTC-Weichenmodul finden Sie unter seinem Namen in der Modul-Liste in der CTC-App.
Dort klicken Sie auf das Stift-Symbol, um die "Schaltkasten-Konfiguration" zu öffnen.

![Config öffnen Modulliste]({% link assets/images/anschliessen/config-oeffnen/Config-oeffnen-1.png %})

Dort klicken Sie auf den Button "Config ändern":

![Config ändern]({% link assets/images/anschliessen/config-oeffnen/Config-oeffnen-2.png %})

## Produkt hinzufügen

Im Config-Dialog klicken Sie auf den Plus-Button rechts von "Angeschlossene Produkte" um auszuwählen, was für ein Produkt Sie angeschlossen haben:

![Config bearbeiten 1]({% link assets/images/anschliessen/weiche-lgb/Weiche-Config-1.png %})

Wählen Sie den Produktkatalog "universell-Weichen" und klicken Sie dann auf "Übernehmen":

![Config bearbeiten 2]({% link assets/images/anschliessen/weiche-lgb/Weiche-Config-2.png %})

Geben Sie Ihrer Weiche einen sinnvollen Namen und wählen Sie das passende Produkt sowie Produktkonfiguration aus.
Dann klicken Sie auf "Übernehmen":

![Produktkatalog: Weiche mit Sensor]({% link assets/images/anschliessen/weiche-lgb/Produktkatalog-Weiche-Sensor.png %})

Dann ordnen Sie noch die Anschlüsse des Produkts zu und erhalten folgende Konfiguration:

![Produktkatalog: Weiche mit Sensor]({% link assets/images/anschliessen/weiche-lgb/Config-Weiche-Sensor.png %})

## Konfiguration hochladen

Wenn alle Pins verbunden sind, können Sie den Button "Hochladen" klicken, um die Änderungen an der Config auf das CTC-Weichenmodul zu speichern:

Das CTC-Weichenmodul startet neu.

## Weiche im Schaltpult

Nach wenigen Sekunden taucht die neu konfigurierte Weiche im Schaltpult auf:

![Weiche mit Sensor im Schaltpult]({% link assets/images/anschliessen/weiche-lgb/Weiche-Sensor-im-Schaltpult.png %})

Die blaue Markierung für die Stellung der Weichen zeigt nun aber die tatsächliche Stellung, wie über die beiden im CTC-Weichenmodul-G enthaltenen Sensoren ermittelt.
Dies lässt sich einfach ausprobieren, indem man die Weiche mit dem Finger statt über die CTC-App verstellt.

Kann die Weiche z.B. aufgrund eines Steinchens nur teilweise umschalten, bleibt also auf halbem Weg stecken, so erkennt man das daran, dass keine der beiden Stellungen blau markiert ist:

![Weiche mit Sensor blockiert]({% link assets/images/anschliessen/weiche-lgb/Weiche-Sensor-blockiert.png %})

Wer möchte, kann nun auch noch Trigger mit den beiden Weichenstellungen verknüpfen und erhält so eine Logik, die auf die tatsächliche Weichenstellung statt auf das Schaltkommando reagiert.
