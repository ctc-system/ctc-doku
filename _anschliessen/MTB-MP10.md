---
layout: umbau
title: "Anschluss mtb MP10"
modul: Weichenmodul Universal
date: 2025-01-10 
hersteller: mtb
produkte: Weichenantrieb MP10
umbauart: Motor-3pol
author: Peter Rudolph
---
Diese Einbauanleitung passt für den Weichenantrieb MP10 von mtb
![MP10 am CTC-Modul]({% link assets/images/anschliessen/weiche-mtb-mp10/MTB-MP10.jpg %})

Am [CTC-Weichenmodul] mit der [Artikelnummer WI-M-4L-U]({{site.baseurl}}/shop.html#!/CTC-Weichenmodul-universal/p/725604076/category=176888458) finden sich drei Kabel, an die im Beispiel eine passende Buchse angelötet wurde.

## CTC-Modul einbauen und anschließen

Das CTC-Modul wird auf oder unter der Modellbahnanlage montiert.
* Das gelbe Kabel (W1-VBB) mit dem Anschluss "+COM" des MP10 verbinden.
* Die beiden blauen Kabel (W1-Rot udn W1-Grün) mit den Anschlüssen "M1" und "M2" de MP10 verbinden.
* Das braune Kabel mit dem Minuspol der Stromversorgung oder dem Gleis verbinden.
* Das rote Kabel mit dem Pluspol oder dem anderen Gleis verbinden.

**TODO Schaltbild**

An die verbleibenden drei einzelnen Kabel (2x blau und 1x gelb) kann ein Signal oder eine weitere Weiche angeschlossen werden.
An die vierpolige Buchse können zwei IR-Balisen angeschlossen werden.

## CTC-Modul ins WLAN einbuchen

Wenn das CTC-Weichenmodul noch nicht ins Modellbahn-WLAN eingebucht ist, müssen Sie dies als Erstes erledigen.
Die Anleitung dazu finden Sie in der Bedienungsanleitung im [Kapitel 3.1 "Module ins WLAN einbuchen"]({% link _app_doku/031-module-einbuchen.md %}).

## Konfiguration öffnen

Ein eingebuchtes CTC-Weichenmodul finden Sie unter seinem Namen in der Modul-Liste in der CTC-App.
Dort klicken Sie auf das Stift-Symbol, um die "Schaltkasten-Konfiguration" zu öffnen.

![Config öffnen Modulliste]({% link assets/images/anschliessen/config-oeffnen/Config-oeffnen-1.png %})

Dort klicken Sie auf den Button "Config ändern":

![Config ändern]({% link assets/images/anschliessen/config-oeffnen/Config-oeffnen-2.png %})

## Produkt hinzufügen

Der MP10 wurde zwar wie ein klassischer Doppelspulenantrieb angeschlossen.
Da er sich aber nur so lange bewegt wie der Schaltimpuls anliegt, braucht er eine Konfiguration die den Schaltausgang für wenigstens 3 Sekunden aktiv hält. 

Im Config-Dialog klicken Sie auf den Plus-Button rechts von "Angeschlossene Produkte" um auszuwählen, was für ein Produkt Sie angeschlossen haben:

![Config bearbeiten 1]({% link assets/images/anschliessen/motorisch-3pol/Weiche-Config-1.png %})

Wählen Sie den Produktkatalog "universell-Weichen" und klicken Sie dann auf "Übernehmen":

![Config bearbeiten 2]({% link assets/images/anschliessen/motorisch-3pol/Weiche-Config-2.png %})

Geben Sie Ihrer Weiche einen sinnvollen Namen und wählen Sie das passende Produkt sowie Produktkonfiguration aus.
Dann klicken Sie auf "Übernehmen":

![Config bearbeiten 3]({% link assets/images/anschliessen/motorisch-3pol/Weiche-Config-3.png %})

## Produkt verbinden

Nun müssen Sie noch angeben, mit welchen Anschlüssen des CTC-Moduls die Weiche verbunden ist.
Der oben verwendete Stecker ist an "W1-gruen" und "W1-rot" angelötet.
Sie wählen "W1-grün" aus:

![Config bearbeiten 4]({% link assets/images/anschliessen/motorisch-3pol/Weiche-Config-4.png %})

Dann können Sie durch Klick auf den Button "Testen" prüfen, ob die Weiche reagiert und in welche Richtung.
Der Kurze Scahltimpuls von 500 ms reicht nicht, um die Weiche zu stellen, aber um zu erkennen, in welche Richtung sich der Antrieb bewegt.
In meinem Beispiel schaltet W1-grün auf gerade.
Deshalb wähle ich nun bei "Anschlüsse und Parameter" die Zeile "gerade" aus.
Mit Klick auf "Verbinden" ordnen Sie dem links markierten "W1-gruen: Output-Pin #16" den Anschluss "gerade" zu:

![Config bearbeiten 5]({% link assets/images/anschliessen/motorisch-3pol/Weiche-Config-5.png %})

Dann verbinden Sie noch auf dieselbe Weise "W1-rot" (Output-Pin #17) mit dem Anschluss "abbiegen":

![Config bearbeiten 6]({% link assets/images/anschliessen/motorisch-3pol/Weiche-Config-6.png %})

## Konfiguration hochladen

Wenn alle Pins verbunden sind, können Sie den Button "Hochladen" klicken, um die Änderungen an der Config auf das CTC-Weichenmodul zu speichern:

![Config bearbeiten 7]({% link assets/images/anschliessen/motorisch-3pol/Weiche-Config-7.png %})

Das CTC-Weichenmodul startet neu.

## Weiche im Schaltpult

Nach wenigen Sekunden taucht die neu konfigurierte Weiche im Schaltpult auf:

![Weiche im Schaltpult]({% link assets/images/anschliessen/weiche-magnetisch/Weiche-Schaltpult.png %})   
