---
layout: umbau
title: "Anschluss C-Gleis Doppelkreuzungsweiche"
modul: Weichenmodul C-Gleis
date: 2025-01-04 
hersteller: Märklin
produkte: Kreuzungsweichen 24624, 24720
umbauart: Doppelspule
author: Peter Rudolph
---
Diese Einbauanleitung passt für die Kreuzungsweiche 24624:
![Kreuzungsweiche]({% link assets/images/anschliessen/weiche-c-gleis/Kreuzweiche-C-Gleis.jpg %})

sowie die schlanke Kreuzungsweiche 24720 aus dem C-Gleis-Programm von Märklin.

Sie verwenden beide einen der Antriebe 74490, 74491 oder 74492, die über eine Buchse vom Stecksystem JST ZH verfügen.
Am [CTC-Weichenmodul]({% link _data_sheet/CTC-Weichenmodul-H0.md %}) mit der [Artikelnummer WU-M-4L-C1]({{site.baseurl}}/shop.html#!/CTC-Weichenmodul-C-Gleis/p/725604063/category=176888458) findet sich ein Kabel mit passendem Stecker.

## CTC-Modul einbauen und anschließen

Das CTC-Modul wird auf die für den Märklin-Dekoder vorgesehenen Stifte gesetzt und angedrückt.                       

So wird der Antrieb an das CTC-Weichenmodul angeschlossen:
* Den Stecker des am CTC-Weichenmodul angelöteten Kabels in den Weichenantrieb stecken.
* Den Kabelschuh am braunen Kabel auf eine Lasche mit Beschriftung "0" (Schiene) stecken.
* Den Kabelschuh am roten Kabel auf eine Lasche mit Beschriftung "B" (Mittelleiter) stecken.

![Anschluss Kreuzungsweiche]({% link assets/images/anschliessen/weiche-c-gleis/Weiche_C-Gleis.png %})

Das folgende Bild zeigt eine Rechts-Weiche mit eingebautem Weichenantrieb und CTC-Weichenmodul.
**TODO Weiche von unten**

An die drei einzelnen Kabel (2x blau und 1x gelb) kann ein Signal oder eine weitere Weiche angeschlossen werden.
An die vierpolige Buchse können zwei IR-Balisen angeschlossen werden.

## CTC-Modul ins WLAN einbuchen

Wenn das CTC-Weichenmodul noch nicht ins Modellbahn-WLAN eingebucht ist, müssen Sie dies als Erstes erledigen.
Die Anleitung dazu finden Sie in der Bedienungsanleitung im [Kapitel 3.1 "Module ins WLAN einbuchen"]({% link _app_doku/031-module-einbuchen.md %}).

## Konfiguration öffnen

Ein eingebuchtes CTC-Weichenmodul finden Sie unter seinem Namen in der Modul-Liste in der CTC-App.
Dort klicken Sie auf das Stift-Symbol, um die "Schaltkasten-Konfiguration" zu öffnen.

![Config öffnen Modulliste]({% link assets/images/anschliessen/config-oeffnen/Config-oeffnen-1.png %})

Dort klicken Sie auf den Button "Config ändern":

![Config ändern]({% link assets/images/anschliessen/config-oeffnen/Config-oeffnen-2.png %})

## Produkt hinzufügen

Im Config-Dialog klicken Sie auf den Plus-Button rechts von "Angeschlossene Produkte" um auszuwählen, was für ein Produkt Sie angeschlossen haben:

![Config bearbeiten 1]({% link assets/images/anschliessen/kreuz-1x-magnetisch/Kreuz-Config-1.png %})

Wählen Sie den Produktkatalog "universell-Weichen" und klicken Sie dann auf "Übernehmen":

![Config bearbeiten 2]({% link assets/images/anschliessen/kreuz-1x-magnetisch/Kreuz-Config-2.png %})

Geben Sie Ihrer Weiche einen sinnvollen Namen und wählen Sie das passende Produkt sowie Produktkonfiguration aus.
Dann klicken Sie auf "Übernehmen":

![Config bearbeiten 3]({% link assets/images/anschliessen/kreuz-1x-magnetisch/Kreuz-Config-3.png %})

## Produkt verbinden

Nun müssen Sie noch angeben, mit welchen Anschlüssen des CTC-Moduls die Weiche verbunden ist.
Der oben verwendete Stecker ist an "W1-gruen" und "W1-rot" angelötet.
Sie wählen "W1-grün" aus:

![Config bearbeiten 4]({% link assets/images/anschliessen/kreuz-1x-magnetisch/Kreuz-Config-4.png %})

Dann können Sie durch Klick auf den Button "Testen" prüfen, ob die Weiche schaltet und in welche Richtung.
In meinem Beispiel schaltet W1-grün auf gerade.
Deshalb wähle ich nun bei "Anschlüsse und Parameter" die Zeile "gerade" aus.
Mit Klick auf "Verbinden" ordnen Sie dem links markierten "W1-gruen: Output-Pin #16" den Anschluss "gerade" zu:

![Config bearbeiten 5]({% link assets/images/anschliessen/kreuz-1x-magnetisch/Kreuz-Config-5.png %})

Dann verbinden Sie noch auf dieselbe Weise "W1-rot" (Output-Pin #17) mit dem Anschluss "abbiegen":

![Config bearbeiten 6]({% link assets/images/anschliessen/kreuz-1x-magnetisch/Kreuz-Config-6.png %})

## Konfiguration hochladen

Wenn alle Pins verbunden sind, können Sie den Button "Hochladen" klicken, um die Änderungen an der Config auf das CTC-Weichenmodul zu speichern:

![Config bearbeiten 7]({% link assets/images/anschliessen/kreuz-1x-magnetisch/Kreuz-Config-7.png %})

Das CTC-Weichenmodul startet neu.

## Weiche im Schaltpult

Nach wenigen Sekunden taucht die neu konfigurierte Doppelkreuzungsweiche im Schaltpult auf:

**TODO**
