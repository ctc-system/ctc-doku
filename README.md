# CTC-Doku

Quelltext zur Dokumentation von CTC-System, generierte Doku auf https://ctc-system.gitlab.io/ctc-doku

## Setup of Jekyll preview on Ubuntu
1. Follow instructions on https://jekyllrb.com/docs/installation/ubuntu/
2. Remove Gemfile.lock
3. Run "bundle install" in terminal (a restart might be needed after)
4. Now you can execute "./run.sh" in WebStorm or IntelliJ terminal
                
## Setup on MAC
1. Follow instructions on https://jekyllrb.com/docs/installation/macos/
2. For Mac _config.yml had to be changed:
```
exclude: ['README.md', 'LICENSE', "*.bak", vendor/]
```
Important is the last entry ```vendor/``` (Mac uses that folder for downloading ruby executables). 

## Link Checking
* html-proofer does not work (does not ignore site.baseurl for file system checking)
* link-checker even does not compile
* http://validator.w3.org/checklink works for onsite checking
* Firefox plugin "Link Analyzer" works fine for single page check (invok via context menu on page)