**Hinweis: Texte / Bilder aus Z21 Handregler kopiert / referenziert**
* Suche nach **Handregler_ref** um Referenzen zu finden
* Suche nach **TODO** um Lücken zu finden
* Suche nach **Image** um Bilder zu finden


# Z21: Konfiguration iTrain

Ab CTC-App Version **TODO** können CTC-Module über Win-Digipet gesteuert werden.

In der CTC-App müssen alle Module und **TODO**Balisen mit einer DCC-Adresse versehen werden, die dann mit den Einstellungne in Win-Digipet korrepsondieren.
(**Handregler_ref**)Die Einstellungen in der CTC-App sind im Kapitel "8. Altes Digital" beschrieben.

**TODO** Sensor in CTC

**TODO** **Image**Z21_CTC_Balise.png


### Generell
Edit -> Interfaces

Dort alle Controltypes aktivieren: **TODO**

**Image** Z21_iTrain_Generell_1.png

Die IP-Adresse der Z21-Zentrale einstellen:

**Image** Z21_iTrain_Generell_2.png

Bei Bedarf **TODO** Feedback report address einstellen:

**Image** Z21_iTrain_Generell_3.png


## Lokomotive

Edit -> Locomotives

**Image** Z21_iTrain_Locomotives.png


## Funktion

Edit -> locomotives -> Tab "Functions"

**Image** Z21_iTrain_Locomotives_Functions.png


## Weiche

Edit -> Accessories

**Image** Z21_iTrain_Accessories_1.png

View -> Keyboard

Anzeige der schaltbaren Accessories (z.B. Weichen) und deren aktueller Zustand:

**Image** Z21_iTrain_Accessories_2.png


## Sensor

Edit -> Locomotves -> Tab "Options"

"Occupancy" aktivieren:

**Image** Z21_CTC_Balise.png


## Balise

Es wird von 8 Ports an einer Rückmeldeadresse ausgegangen; CTC verwendet nur den Port 1.

Edit -> Feedbacks

Für jede Balise die Adesse im Format <dccAddr>.<Port> einstellen:

**Image** Z21_iTrain_Feedbacks_1.png

Edit -> Preferences -> Tab "Interface"

"Feedback addresses" auf "8" stellen:

**Image** Z21_iTrain_Feedbacks_2.png

View -> Feedback Monitor

Anzeige der konfigurierten Balisen und deren aktueller Zustand:

**Image** Z21_iTrain_Feedbacks_3.png

