**Hinweis: Texte / Bilder aus Z21 Handregler kopiert / referenziert**
* Suche nach **Handregler_ref** um Referenzen zu finden
* Suche nach **TODO** um Lücken zu finden
* Suche nach **Image** um Bilder zu finden


# Z21: Konfiguration Win-Digipet

Ab CTC-App Version **TODO** können CTC-Module über Win-Digipet gesteuert werden.

In der CTC-App müssen alle Module und **TODO**Balisen mit einer DCC-Adresse versehen werden, die dann mit den Einstellungne in Win-Digipet korrepsondieren.
(**Handregler_ref**)Die Einstellungen in der CTC-App sind im Kapitel "8. Altes Digital" beschrieben.

**TODO** Sensor in CTC

**TODO** **Image**Z21_CTC_Balise.png


## Generell
Datei → Systemeinstellungen

**Image** 21_Win_Digipet_Generell_1.png


## Lokomotive und Funktionen
Datei -> **TODO**

**Image** Z21_Win_Digipet_Fahrzeug_Decoder_Funktionen.png


## Weiche
**TODO**

**Image** **TODO**


## Sensor
**TODO**

**Image** **TODO**


## Balise
neue Balise anlegen:

Datei → Rückmeldungs-Konfiguration → an Roco-CAN ‚Adresse hinzufügen‘
**Image** Z21_Win_Digipet_Rueckmeldung_1.png

Balise konfigurieren:

Datei → Rückmeldungs-Konfiguration → an Adresse ‚Adresse bearbeiten‘
**Image** Z21_Win_Digipet_Rueckmeldung_2.png


