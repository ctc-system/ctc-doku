---
layout: umbau
title: "Umbau BR44 Märklin 34880"
lokmodul: H0a
date: 2021-04-13
baureihe: "BR 44"
spur: H0
hersteller: Märklin
artikelnr: 34880
umbauart: "H0-Digital"
schaltplan: true
author: Peter Rudolph
---
Die Lok wird von unten geöffnet über die beiden Schrauben neben den Zylindern und die Schraube vor der letzten Antriebsachse.
Der Tendern ist nur geklipst:

![BR44 offen]({% link assets/images/umbau/ma34880/ma34880-offen.jpg %})
                        
Die Lok präsentiert sich als eine Märklin-Lok mit Delta-Elektronik (Märklin Nr. 66415) im Tender.

Die Fotos dokumentieren den Zustand bevor ich damit begann Drähte abzulöten:

![BR44 Verkabelung]({% link assets/images/umbau/ma34880/ma34880-verkabelung.jpg %})

![BR44 Verkabelung-2]({% link assets/images/umbau/ma34880/ma34880-verkabelung-2.jpg %})

Dann habe ich die Platine mit der Delta-Elektronik ausgelötet.
Die alte Rundfunkdrossel wird nicht mehr gebraucht.
                                 
Die Verdrahtung des CTC-Moduls erfolgt gemäß folgendem Schaltplan:

![BR44 Schaltplan]({% link assets/images/umbau/ma34880/Ma-34880-Schaltbild_H0a.png %})

Aus dem Zubehör für Lokumbau H0a (CTC-Nr. ZL-UA) wird die Ferrit-Drossel (3,3 uH) an das schwarze Kabel angelötet, das direkt zum Motor führt.
Das andere Ende der Drossel wird später mit dem Motorausgang des CTC-Moduls verbunden.

An die beiden mit der Feldspule verbundenen Kabel (grün und blau) werden zwei Dioden in unterschiedlicher Durchlass-Richtung angeschlossen.
Die anderen Enden der beiden Dioden werden miteinander verbunden und später an den Motorausgang des CTC-Moduls angeschlossen.

![BR44 Umbau-1]({% link assets/images/umbau/ma34880/ma34880-umbau-1.jpg %})

Da jeweils ein Pol von Frontlicht und Rauchgenerator mit dem Lokgehäuse verbunden sind, muss in deren Zuleitung ebenfalls je eine Diode eingelötet werden (Markierungsring auf der Seite der Lampe bzw. des Rauchgenerators).

Nun werden das Gleis (rot und braun), der Motor (schwarz und grün), das Frontlicht (grau) und der Rauchgenerator (lila) an das [CTC-Lokmodul-H0a]({% link _data_sheet/CTC-Lokmodul-H0a.md %}) angeschlossen:

![BR44 Umbau-2]({% link assets/images/umbau/ma34880/ma34880-umbau-2.jpg %})

Auf der Rückseite des CTC-Lokmoduls wird der Stützkondensator angeschlossen (Pluspol gelb):

![BR44 Umbau-3]({% link assets/images/umbau/ma34880/ma34880-umbau-3.jpg %})

Da das Rücklicht direkt auf die Delta-Platine gesteckt war muss dafür eine neue Halterung gebaut werden:

![BR44 Umbau-4]({% link assets/images/umbau/ma34880/ma34880-umbau-4.jpg %})

Der Schleifer befindet sich unter dem Tender und somit hat es zwischen den Antriebsachsen Platz für den IR-Empfänger.
Sogar ein passendes Loch für die Kabel ist bereits im Gehäuse:

![BR44 Umbau-5]({% link assets/images/umbau/ma34880/ma34880-umbau-5.jpg %})
