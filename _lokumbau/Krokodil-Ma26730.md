---
layout: umbau
title: "Umbau Krokodil Märklin 26730 (Sinus-Motor)"
lokmodul: 21mtc
date: 2023-09-04
baureihe: "SBB Ce 6/8 II (Krokodil)"
spur: H0
hersteller: Märklin
artikelnr: 26730
umbauart: "H0-Sinus"
schaltplan: true
author: Peter Rudolph
---
Unter dem Namen "Sinus-Antrieb" hat Märklin einige Zeit bürstenlose Motoren in seine Loks eingebaut.
Diese Motoren erfordern eine spezielle Ansteuerung, die CTC-Lokmodule derzeit nicht anbieten.
Die Lücke hat [rail4you.ch](https://rail4you.ch) mit der [Treiber-Elektronik "DsM-2.0"](https://rail4you.ch/remotorisierung) gestopft.
Dieser Treiber kann an einen Brücken-Ausgang eines CTC-Lokmoduls angeschlossen werden. 
                      
Hier sehen wir die Original-Elektronik. 
Diese bestand auch aus einem Spezial-Dekoder (ausgebaut, im Bild rechts) und einer Hilfsplatine (zwischen Motor und Dekoder).
Gut zu erkennen ist auch das 8-adrige Flachbandkabel zwischen Motor und Dekoder:

![Krokodil offen]({% link assets/images/umbau/ma26730/Ma26730-offen.jpg %})
        
Das folgende Bild zeigt die für den Umbau auf CTC benötigten Bauteile:

![Krokodil Bauteile Umbau]({% link assets/images/umbau/ma26730/Ma26730-Bauteile.jpg %})
                                        
Vor der Lok:
* Sinus-Treiber von rail4you (links oben)
* 21-mtc Adapter Platine von eBay (rechts neben Sinus-Treiber)
* CTC-Lokmodul-21mtc (mitte)
* Pufferkondensator 1000 uF (rechts oben)
* IR-Empfangsdiode und Schrumpfschlauch (links unten)
* Kabel für IR-Empfänger mit 10-pol Ministecker

Die Verdrahtung von Stromversorgung und Beleuchtung erfolgt wie gewöhnlich bei Loks mit schweizer Lichtwechsel.
Ungewöhnlich ist nur der Anschluss des Motors.
Hier der komplette Schaltplan:

![Krokodil Schaltplan]({% link assets/images/umbau/ma26730/Ma-26730-Schaltbild.png %})

Am 21mtc-Adapter werden angelötet:
* die 2 Kabel von Lokgehäuse/Räder (braun) und vom Schleifer/Pantograf (Mittelleiter, rot) an Gleis1 und Gleis2 - wie rum ist egal.
* die 6 Kabel der Beleuchtung vorn und hinten (je gelb, braun/gelb und grau) an die Anschlüsse F0f, F0r, F1, F2, F3 und F4 am 21mtc-Adapter an.
  Auch hier muss keine spezielle Zuordnung eingehalten werden, da wir später bei der Konfiguration die Anschlüsse testen und dann zuordnen.
* der jeweils gemeinsame Pluspol der Beleuchtung (orange) wird mit VBB (Decoder-Plus) verbunden.  

![Krokodil verdrahtet-1]({% link assets/images/umbau/ma26730/Ma26730-verdrahtet-1.jpg %})
              
Für den Sinus-Treiber werden angelötet: 
* die 2 Kabel der Stromversorgung: rot an V+ (am CTC-Modul mit VBB bezeichnet) und Schwarz an GND (siehe Bild oben)
* die 2 Kabel für den Abgriff des Motorsignals vom CTC-Lokmodul-21mtc an den 10-poligen Ministecker, an dem bereits die Kabel für den IR-Empfänger angelötet sind.
  Das weiße Kabel wurde an "ExtOut-1 (Pin 25)" und das gelbe Kabel an "ExtOut-2 (Pin 21)" angelötet.

![Krokodil verdrahtet-2]({% link assets/images/umbau/ma26730/Ma26730-verdrahtet-2.jpg %})

Das Flachbandkabel vom Sinus-Motor wird in die entsprechende Aufnahme des Sinus-Treibers geschoben (Kontakte zur Platine gekehrt) und dann verriegelt:

![Krokodil verdrahtung Sinus-Treiber]({% link assets/images/umbau/ma26730/Ma26730-Sinus-Treiber.jpg %})
                    
Der Sinus-treibe wird mit der mitgelieferten Schraube im Lokgehäuse (im Bild links) montiert.
Das CTC-Lokmodul-21mtc wird durch einen Schrumpfschlauch isoliert und dann gemeinsam mit dem Kondensator in der Mitte der Lok untergebracht:

![Krokodil umgebaut]({% link assets/images/umbau/ma26730/Ma26730-umgebaut.jpg %})
           
Der IR-Sender fand am Motorblock mittig unter der Lok einen geeigneten Platz:

![Krokodil IR-Empfänger]({% link assets/images/umbau/ma26730/Ma26730-IR.jpg %})

Für die Konfiguration des Lichts wird das vorinstrallierte Licht entfernt und dann "Fahrlicht SBB LowSide, je 2x gelb und 1x rot" aus dem Produktkatalog "universell-loks" hinzugefügt:

![Krokodil Config Licht]({% link assets/images/umbau/ma26730/Ma26730-Config-Licht.png %})

Für den Motor wird eine aktuelle IO-Config (min. CTC-App 4.19) benötigt.
Für ältere CTC-Lokmodule kann diese nach Aktualisieren der Firmware mit dem neuen Button "IO-Config ersetzen" ersetzt werden (siehe [Kapitel 4 - "Module konfigurieren"]({% link _app_doku/040-module-konfigurieren.md %})).

Dann kann der Motor vom "MotorPt" getrennt und mit "BridgePt" verbunden werden:

![Krokodil Config Motor]({% link assets/images/umbau/ma26730/Ma26730-Config-Motor.png %})
