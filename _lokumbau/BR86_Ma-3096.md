---
layout: umbau
title: "Umbau BR86 Märklin 3096"
lokmodul: H0a
date: 2020-08-23
baureihe: "BR 86"
spur: H0
hersteller: Märklin
artikelnr: 3096
umbauart: "Märklin H0 Analog"
speziell: Entkuppler
author: Peter Rudolph
---
Die Lok wird geöffnet über die von oben gut erkennbare Schraube im Dom vor dem Führerhaus:

![BR86 offen]({% link assets/images/umbau/ma3096/ma3096-offen.jpg %})
                        
Die Lok präsentiert sich als eine klassische analoge Märklin-Lok mit Allstrom-Motor und Entkuppler.
Eine ausführliche Anleitung für Loks dieser Art findet sich im Artikel [Umbau E103 Märklin 3054]({% link _posts/de/szenarien/2020-09-09-Umbau-E103-Ma.md %}).
                                                   
Die Kabel werden aus den Führungen gelöst.
Das Foto dokumentiert den Zustand bevor man beginnt Drähte abzulöten: 

![BR86 Verkabelung]({% link assets/images/umbau/ma3096/ma3096-verkabelung.jpg %})
    
Nun wird der Umschalter ausgebaut.
Die alte Rundfunkdrossel und der Kondensator zwischen Motor und Gehäuse werden nicht mehr gebraucht:

![BR86 zerlegt]({% link assets/images/umbau/ma3096/ma3096-zerlegt.jpg %})
                                         
Dann wird aus dem Zubehör für Lokumbau H0a (CTC-Nr. ZL-UA) die Ferrit-Drossel (3,3 uH) an den offenen Motorpol angelötet.
Das andere Ende der Drossel wird später mit dem Motorausgang des CTC-Moduls verbunden.

Am anderen Motorpol ist immer noch die Feldspule angeschlossen.
An deren Anschlüsse, die zuvor mit dem Umschalter verbunden waren, werden zwei Dioden in unterschiedlicher Durchlass-Richtung angeschlossen.
Die anderen Enden der beiden Dioden werden miteinander verbunden und später an den Motorausgang des CTC-Moduls angeschlossen.
                    
Da Front- und Hecklampe nicht mit dem Lokgehäuse verbunden sind kann bei diesen auf eine Diode verzichtet werden.

Im folgenden Bild wurden auch schon das [CTC-Lokmodul-H0a]({% link _data_sheet/CTC-Lokmodul-H0a.md %}) und der Stützkondensator probeweise platziert:

![BR86 Umbau-1]({% link assets/images/umbau/ma3096/ma3096-umbau-1.jpg %})
                                                              
Nun werden der Motor, das Licht, die beiden Entkuppler und der Stützkondensator an das CTC-Modul angelötet.
Der Draht am Minuspol des Stützkondensators wird als gemeinsamer GND-Punkt genutzt. 

Im folgenden Bild sind das CTC-Modul und der Stützkondensator eingebaut.
Motor (lila Kabel), Licht (gelbe und braune Kabel) und Entkuppler (graue Kabel) sind angeschlossen:

![BR86 Umbau-2]({% link assets/images/umbau/ma3096/ma3096-umbau-2.jpg %})
       
Nun fehlt noch der IR-Empfänger.
Bei der BR86 bietet es sich an, diesen an der Aufhängung des vorderen Laufrads zu fixieren:

![BR86 Umbau-3]({% link assets/images/umbau/ma3096/ma3096-umbau-3.jpg %})
                                
Das Kabel des IR-Empfängers passt mit durch denselben Schlitz wie die Kabel des vorderen Entkupplers.
                                             
Hier sieht man den IR-Empfänger fertig eingebaut:

![BR86 Umbau-4]({% link assets/images/umbau/ma3096/ma3096-umbau-4.jpg %})

