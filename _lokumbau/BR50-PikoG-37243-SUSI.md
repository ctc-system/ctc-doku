---
layout: umbau
title: "Umbau BR50 Piko 37243 mit SUSI-Sound"
lokmodul: "G-3,5A"
date: 2023-08-25
baureihe: "BR 50"
spur: G
hersteller: Piko
artikelnr: 37243
umbauart: "G-SUSI-Sound"
schaltplan: true
author: Peter Rudolph
---
![BR50 fertig]({% link assets/images/umbau/pikoG37243/PikoG37243-fertig.jpg %})

Im [ersten Umbau der BR50 von PIKO]({% link _posts/de/szenarien/2021-07-25-Umbau-DCC-BR50.md %}) blieb zum Erhalt des Orginal-Sounds der Digital-Dekoder in der Lok.
Das [CTC-Lokmodul-G]({% link _data_sheet/CTC-Lokmodul-G.md %}) erzeugte ein DCC-Signal und konnte so den Orginal-Dekoder mit vollem Funktionsumfang ansteuern.

Erfreulicherweise ist aber auch beim Original-Dekoder der Sound auf ein separates SUSI3-Soundmodul ausgelagert.
Da CTC nun SUSI3-Soundmodule unterstützt, können wir den Motor und die Lokfunktionen direkt mit CTC ansteuern und trotzdem den Original-Sound genießen.

Dekoder und Soundmodul befinden sich im Tender und funktionieren übrigens auch ohne angekoppelte Lok.
Zum Öffnen des Tenders wird die Wand vor dem Kohlevorrat noch oben abgezogen.
Dann kann der Kohlevorrat nach vorne herausgezogen werden.
Dabei muss man aufpassen, dass man das Kabel des Servos, der den Kohlevorrat verstellt, nicht abreißt. 

Hier sehen wir noch einmal den Original-Dekoder und links unterhalb davon das SUSI3-Soundmodul:

![BR50 Original Dekoder]({% link assets/images/umbau/pikoG37243/PikoG37243-Dekoder.jpg %})

Der Umbau gestaltet sich bei dieser Lok besonders einfach, da PIKO die gesamte elektrische Verkabelung im Handbuch mustergültig dokumentiert hat.
Dabei wird wie folgt verkabelt:                

![BR50 Schaltplan]({% link assets/images/umbau/pikoG37243/PikoG37243-Schaltplan.png %})
                                                                     
Das folgende Foto zeigt das bereits eingebaute und mit Lok und SUSI3-Soundmodul verbundene CTC-Lokmodul-G.
Der NFC-Reader ist ebenfalls angeschlossen.
Wie das geht, findet sich in der [Doku des ersten Umbaus]({% link _posts/de/szenarien/2021-07-25-Umbau-DCC-BR50.md %}).
Das Bild zeigt oben die Anschlussplatine von PIKO.
Die kleine rote Platine rechts davon ist das originale SUSI3-Soundmodul von PIKO.
Am rechten Rand des Bildes sieht man das CTC-Lokmodul-G.
Für das SUSI3-Soundmodul wurde eine kleine Adapterplatine geschaffen, die auf die eigentlich für Servos gedachten Anschlüsse aufgesteckt wurde und diese mit einer SUSI classic Buchse verbindet. 

![BR50 Verkabelung]({% link assets/images/umbau/pikoG37243/PikoG37243-verkabelung.jpg %})

Der Servo für den Kohlevorrat kann bei derzeitigen CTC-Lokmopdul-G leider nicht angeschlossen werden, da die beiden Servo-Ausgänge für die SUSI3-Schnittstelle zweckentfremdet wurden.

Das Verstauen der Kabel ist mir zugegebenermaßen nicht so gut gelungen wie PIKO es mit dem Original-Dekoder gemacht hatte:

![BR50 Einbau]({% link assets/images/umbau/pikoG37243/PikoG37243-einbau.jpg %})

Damit das ganze funktioniert bedarf es einer neuen **Firmware (min. 20230902)** für das CTC-Lokmodul-G.
Diese findet sich in der **CTC-App ab Version 4.19**, die auch die notwendigen Dateien für die SUSI-Konfiguration mitbringt.

Außerdem muss die **passende IO-Config** auf das Lokmodul-G geladen werden. 
Sie heißt "alt_SUSI_statt_Servo" und ist **ab CTC-App Version 4.24** vorhanden.
Dazu öffnen wir den Config-Dialog des CTC-Lokmoduls und wählen dort "IO-Config ersetzen":

![BR50 IO-Config ersetzen 1]({% link assets/images/umbau/pikoG37243/PikoG37243-ioCfg-ersetzen-1.png %})

Wir wählen "alt_SUSI_statt_Servo" und klicken dann auf "Übernehmen".
Es wird die Beschreibung der gewählten IO-Config angezeigt:

![BR50 IO-Config ersetzen 2]({% link assets/images/umbau/pikoG37243/PikoG37243-ioCfg-ersetzen-2.png %})

Da es die passende ist klicken wir auf "Hochladen".
Das CTC-Lokmodul startet neu.

Zur weiteren Konfiguration des CTC-Moduls öffnen wir den Config-Dialog der Lok und klicken dort auf "Config ändern".
Ist dort der "SUSIPort" vorhanden, so ist die "ioCfg.xml" neu genug:

![BR50 SUSI Config 1]({% link assets/images/umbau/pikoG37243/PikoG37243-Cfg-SUSI-1.png %})

Wir selektieren den SUSIPort und klicken dann auf den Plus-Button rechts neben "Pins, Ports und Erweiterungen".
Dann wählen wir "susi.xml" und es erschient folgender Dialog:

![BR50 SUSI Config 2]({% link assets/images/umbau/pikoG37243/PikoG37243-Cfg-SUSI-2.png %})

Wir geben der Erweiterung den Namen "SUSI" und (bei SUSI immer) die Busadresse 0.
Dann klicken wir auf "Übernehmen".
Die SUSI-Erweiterung erscheint nun unter dem SUSIPort.

Anschließend klicken wir auf den PLus-Button rechts neben "Angeschlossene Produkte" und wählen den Katalog "universell-loks.xml".
Dort wählen nun für jeden einzelnen Sound der Lok "Lok-Sound an LowSide" und geben ihm den passenden Namen.
Hier beispielhaft für die "Pfeife":

![BR50 SUSI Config 3]({% link assets/images/umbau/pikoG37243/PikoG37243-Cfg-SUSI-3.png %})

Nach Klick auf "Übernehmen" müssen wir den neuen Sound noch mit der passenden SUSI-Funktion verbinden, hier "SUSI-F2":

![BR50 SUSI Config 4]({% link assets/images/umbau/pikoG37243/PikoG37243-Cfg-SUSI-4.png %})

Die Funktion zum Ein- und Ausschalten des Fahrsounds wird genauso konfiguriert wie jeder andere Sound.
Hier liegt er auf SUSI-F4.

Welcher Sound welcher Funktion zugeordnet ist, ist bei PIKOs BR50 auf einem separat mitgelieferten Zettel dokumentiert. 
