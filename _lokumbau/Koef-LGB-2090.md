---
layout: umbau
title: "Umbau Köf D10 LGB 2090"
lokmodul: "G-3,5A"
date: 2021-09-19
baureihe: "Köf (D10)"
spur: G
hersteller: LGB
artikelnr: 2090
umbauart: "G-Analog"
author: Peter Rudolph
---
Siehe Artikel [Umbau LGB D10]({% link _posts/de/szenarien/2021-09-19-Umbau-LGB-D10.md %}).
