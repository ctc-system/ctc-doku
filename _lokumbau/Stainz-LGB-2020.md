---
layout: umbau
title: "Umbau Stainz LGB 2020"
lokmodul: H0a
date: 2021-03-30
baureihe: "Stainz"
spur: G
hersteller: LGB
artikelnr: 2020
umbauart: "G-Analog"
author: Peter Rudolph
---
Siehe Artikel [Umbau LGB Stainz 2020 und 2021]({% link _posts/de/szenarien/2021-03-30-Umbau-LGB-Stainz.md %}).
