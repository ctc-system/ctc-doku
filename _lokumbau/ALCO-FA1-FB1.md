---
layout: umbau
title: "Umbau ALCO FA1 und FB1"
lokmodul: "G-6A"
date: 2021-03-19
baureihe: "ALCO FA1/FB1"
spur: G
hersteller: "Railway Express Agency"
artikelnr: "22000-01"
umbauart: "G-Analog"
author: Peter Rudolph
---
Diese US-Lok besteht aus zwei Triebeinheiten (FA1 und FB1) mit je zwei Motoren.
Sie werden wird von unten geöffnet über eien Vielzahl von Schrauben entlang des Gehäuserands.
Hier die FA1 nach dem Öffnen:

![ALCO FA1 offen]({% link assets/images/umbau/alco-fa1-fb1/alco-fa1-offen.jpg %})
                             
... und die FB1: 

![ALCO FB1 offen]({% link assets/images/umbau/alco-fa1-fb1/alco-fb1-offen.jpg %})
 
In beiden Modell hat der Vorbesitzer offensichtlich Umbauten vorgenommen, bei der FA1 eine Digital-Umrüstung durchgeführt.
Nachdem ich erst einige Zeit versucht habe, die Verkabelung zu verstehen, entschloss ich mich zu einer Neuverkabelung.

Da US-Loks der Spur-G für Ihren hohen Strombedarf bekannt sind, entschloss ich mich das [CTC-Lokmodul-G]({% link _data_sheet/CTC-Lokmodul-G.md %}) in der Variante mit 6 A Peak (Nr. L-G2-60) zu verwenden.
Jeder der beiden Loks erhält ein eigenes Lokmodul, sodass jeder der vier Motoren einen eigenen Motorausgang bekommt.
Die beiden Loks werden später über die CTC-App miteinander gekoppelt.

Die Rauchgeneratoren habe ich erst mal nur abgeklemmt und deren Inbetriebnahme auf später verschoben.

Hier die Verdrahtung des FA1:

![ALCO FA1 Umbau-1]({% link assets/images/umbau/alco-fa1-fb1/alco-fa1-umbau-1.jpg %})

![ALCO FA1 Umbau-2]({% link assets/images/umbau/alco-fa1-fb1/alco-fa1-umbau-2.jpg %})

Und die Verdrahtung des FB1:

![ALCO FB1 Umbau-1]({% link assets/images/umbau/alco-fa1-fb1/alco-fb1-umbau-1.jpg %})

![ALCO FB1 Umbau-2]({% link assets/images/umbau/alco-fa1-fb1/alco-fb1-umbau-2.jpg %})
                                                   
**TODO: Bilder und Text zu NFC-Reader**