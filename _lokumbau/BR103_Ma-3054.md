---
layout: umbau
title: "Umbau BR103 Märklin 3054"
lokmodul: H0a
date: 2020-09-09 
baureihe: "BR 103"
spur: H0
hersteller: Märklin
artikelnr: 3054
umbauart: "H0-Ma-Analog"
author: Klaus Gungl
---
Siehe Artikel [Umbau E103 Märklin 3054]({% link _posts/de/szenarien/2020-09-09-Umbau-E103-Ma.md %}).
