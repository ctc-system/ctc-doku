---
layout: umbau
title: "Umbau RhB GE 4/4 II Bemo 1258"
lokmodul: "PluX22"
date: 2022-12-18
baureihe: "RhB GE 4/4 II"
spur: H0m
hersteller: Bemo
artikelnr: 1258
umbauart: "H0m-Analog"
author: Peter Rudolph
---
    
Auf unserem Messestand sollte auch die Spur H0m nicht zu kurz kommen.
Dafür habe ich ein StartSet von Bemo bei eBay erstanden und dann die darin enthaltene Lok audf CTC umgerüstet.

Aus der beiliegenden Dokumentation geht klar hervor, wie die Lok zu zerlegen ist.
Erst werden die beiden Kupplungen herausgezogen.
Dann können die vier in der Seitenwand verborgenen Klipse geöffnet und das Gehäuse nach oben abgezogen werden.

Im Bild habe ich auch schon den Dachaufbau entfernt, denn dort wird später das CTC-Modul Platz finden:

![GE 4/4 II offen]({% link assets/images/umbau/bemo1258/Bemo1258-offen.jpg %})

Nach etwas Grübelei habe ich mich dazu entschieden die Lokplatine zu entfernen.
Leider hält diese auch den Motor fest, sodass dafür passender Ersatz nötig ist.
Ein PluX22-Adapter, den ich bei eBay erstanden hatte, verfügte zum Glück über passende Schraublöcher.
Ich konnte damit zwar nur zwei der vier Schrauben der alten Platine nutzen, aber auch so ist der Motor gut genug fixiert.
Das Bild zeigt die Lok mit fertig eingebautem [CTC-Lokmodul-PluX22]({% link _data_sheet/CTC-Lokmodul-PluX22.md %}).
Der Stützkondensator füllt die rückwärtige Fahrerkabine:

![GE 4/4 II Umbau-1]({% link assets/images/umbau/bemo1258/Bemo1258-Umbau-1.jpg %})

Wie so oft bei analogen Lokomotiven war bei der Beleuchtung vorne und hinten ein Pol über das Gehäuse angeschlossen.
Diese Verbindung habe ich auf den jeweiligen Platinen mit einem Cutter-Messer durchtrennt.
Die Trennstellen erkennt man im Foto jeweils direkt neben dem Lötpunkt des orangen Drahts:

![GE 4/4 II Umbau-2]({% link assets/images/umbau/bemo1258/Bemo1258-Umbau-2.jpg %})

Für den IR-Empfänger habe die mittige Bodenverkleidung entfernt und ein 3mm-Loch durch das Gehäuse gebohrt.
Dann konnte ich das Kabel durchführen, den IR-Empfänger anlöten und schließlich auf das Lokgehäuse kleben:

![GE 4/4 II Umbau-3]({% link assets/images/umbau/bemo1258/Bemo1258-Umbau-3.jpg %})
                                                                          
Nun musste nur noch das Kabel des IR-Empfängers auf das CTC-Lokmodul-PluX22 gesteckt werden:

![GE 4/4 II Umbau-4]({% link assets/images/umbau/bemo1258/Bemo1258-Umbau-4.jpg %})
         
Dann kam der hässliche Teil: Das Gehäuse unterhalb des Dachaufbaus musste so weit aufgetrennt werden, dass das Lokmodul durchpasst.
Mit einem anderen PluX22-Adapter, der eine niedrigere Buchse aufweist, würde der Dachaufbau auch wieder sauber draufpassen.
Für meine Messe-Lok bleibt der Dachaufbau ganz weg, was dann den ungehinderten Blick auf das CTC-Lokmodul-PluX22 erlaubt:

![GE 4/4 II Umbau-5]({% link assets/images/umbau/bemo1258/Bemo1258-Umbau-5.jpg %})

