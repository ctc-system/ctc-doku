---
layout: umbau
title: "Umbau BR53 Märklin 3102"
lokmodul: PluX22
date: 2024-07-27
baureihe: "BR 53"
spur: H0
hersteller: Märklin
artikelnr: 3102
umbauart: "H0-Ma-Analog-PluX22"
speziell: "CTC-Einbauadapter"
schaltplan: true
author: Peter Rudolph
---
Die BR 53 ist unser erster Umbau einer analogen Märklin-Lok mithilfe unseres eigenen Adapters für PluX22.           

**Hinweis:** Dieser Umbau basiert auf korrigierter IO-Config und Produktkatalog, die in der CTC-App ab Version 4.30 enthalten sind.
         
Das Lok-Gehäuse wird geöffnet, indem man die Abdeckung direkt vor dem Führerstand nach ofen abzieht.
Darunter befindet sich die erste von drei Schrauben.
Die anderen beiden befinden sich unter der Vorlaufachse.
Sind alle drei Schrauben entfernt können Kessel und Führerstand abgenommen werden.

Im Tender befinden sich Umschalter und Rücklicht.
Zum Öffnen des Tenders entfernen wir die Schraube, die sich unten zwischen den Drehgestellen des Tenders befindet.
Danach lässt sich das Oberteil der Tenders nach oben abheben.

![BR53 Offen vor Umbau]({% link assets/images/umbau/ma3102/ma3102-offen.jpg %})

Für diesen Umbau verwenden die ["Adapterplatine_PluX22"]({% link _data_sheet/Adapter-PluX22.md %}) und den ["Umbausatz-Analog"]({{site.baseurl}}/shop.html#!/Lok-Umbausatz-Analog/p/725782778/category=176902513).
Der Verdrahtungsplan sieht dann so aus: 

![Schaltbild PluX22-Adapter]({% link assets/images/umbau/ma3102/Ma-3102-Schaltbild-Plux22.png %})

Wir benötigen hier alle im Umbausatz-Analog mitgelieferten Dioden.
Sie werden auf die Adapterplatine gesteckt und dann verlötet.
Dabei ist unbedingt auf die Position des Markierungsrings zu achten (siehe Bild). 
                                                                                                   
An der Seite des Motors, die nicht mit der Feldspule verbunden ist bringen wir die Drossel an.

Die dritte Hand hält uns die Adapterplatine während wir die Kabel vom Gleis und zum Motor anlöten.
Bei unserem Exemplar waren die Originalkabel in einem sehr schlechten Zustand, weshalb wir sie durch neue Kabel ersetzt haben:

![BR53 Gleis und Motor]({% link assets/images/umbau/ma3102/ma3102-gleis-motor.jpg %})

Die Kabel sind:
* Mittelleiter rot
* Gehäuse/Räder braun
* Feldspule links/rechts grau
* Drossel am Motor grün

Für den IR-Empfänger platzieren wir vor dem Schleifer.
So kännen wir das Kabel von der Elektronik im Tender durch das vorhandene Loch am Ende des Lok-Chassis durchführen:

![BR53 IR Empfänger]({% link assets/images/umbau/ma3102/ma3102-IR.png %})

Dann löten wir noch die Kabel von den beiden Lichtern (vorne weiß, hinten gelb) an die Adapterplatine an:

![BR53 Licht]({% link assets/images/umbau/ma3102/ma3102-licht.jpg %})

Das vordere Licht und der Rauchsatz hängen leider am selben Kabel und Blechkontakt.
Wir verzichten deshalb auf die Rauchfunktion und kleben den Kontakt mit Isolierband ab:

![BR53 Rauchsatz]({% link assets/images/umbau/ma3102/ma3102-rauchsatz.jpg %})

Alternativ könnten wir uns überlegen, wie wir Lampe udn Rauchsatzanschluss voneinander trennen und dann den Rauchsatz mit einem neu zu verlegenden Kabel und Diode an AUX1 anschließen.
               
Als Letztes wird noch der Pufferkondensator angelötet (Plus orange, Minus schwarz):

![BR53 Kondensator]({% link assets/images/umbau/ma3102/ma3102-Kondensator.jpg %})
 
Nun kann das CTC-Lokmodul-PluX22 auf den Adapter gesteckt und dann der IR-Empfänger auf das Lokmodul gesteckt werden:

![BR53 Kondensator]({% link assets/images/umbau/ma3102/ma3102-lokmodul.jpg %})
      
Bevor wir die Lok wieder zusammenbauen setzten wir sie auf einen Rollenprüfstand.
Dort konfigurieren wir das Lokmodul und testen, ob alles funktioneirt.

## IO-Config ersetzen

Falls Sie einer CTC-Lokmodul-PluX22 haben, das vor dem 27.06.2025 geliefert wurde, müssen Sie für diesen Umbau die IO-Config des Lokmoduls ersetzten.
Diese können Sie auch tun, wenn das Lokmodul bereits eingebaut ist.

Der Grund dafür ist, dass wir bisher die Schaltausgänge als LowSide eingestellt (und auch nur so genutzt) haben.
Für den Umbau der Analog-Lok benötigen wir nun aber die Eingenschaft der Halbbrücke (HalfBridge) gegen Plus zu schalten (HighSide).
                                                                   
Die alte IO-Config erkennen Sie daran, das im Dialog "Config ändern" hintern den Schaltausgänge (z.B. F0F und FoR) in Klammern "LowSide" statt "HalfBridge" steht. 
             
Zum Ersetzen der IO-Config öffnen wir den Config-Dialog der Lok und klicken dort auf den Button "IO-Config-ersetzten".
Es öffnet sich folgender Dialog: 

![BR53 Config-1]({% link assets/images/umbau/ma3102/ma3102-IO-Config-ersetzen.png %})
                                 
Dort wählen wir die Konfiguration "standard" und klicken auf "Übernehmen"

## Licht-Configuration

![BR53 Config-1]({% link assets/images/umbau/ma3102/ma3102-Config-1.png %})

![BR53 Config-2]({% link assets/images/umbau/ma3102/ma3102-Config-2.png %})

![BR53 Config-3]({% link assets/images/umbau/ma3102/ma3102-Config-3.png %})

![BR53 Config-4]({% link assets/images/umbau/ma3102/ma3102-Config-4.png %})
