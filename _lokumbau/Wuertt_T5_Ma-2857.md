---
layout: umbau
title: "Umbau Württembergische T5 Märklin 2857"
lokmodul: PluX22
date: 2024-01-27
baureihe: "T5"
spur: H0
hersteller: Märklin
artikelnr: 2857
umbauart: "H0-Ma-Analog-PluX22"
schaltplan: true
author: Peter Rudolph
---
![Lok fertig, offen]({% link assets/images/umbau/ma2857/Ma2857-fertig.jpg %})

Dieses Mal ist einer meiner Jugendträume dran: Die Württembergische T5 aus dem Set zum 125-jährigen Jubiläum von Märklin.
Damals war es für mich unerschwinglich, inzwischen konnte ich es bei eBay günstig erstehen.
                                                                             
Nachdem wir uns dazu entschieden haben, das CTC-Lokmodul-H0a nicht weiter zu produzieren, zeige ich hier, wie ein Analog-Umbau mit [CTC-Lokmodul-PluX22]({% link _data_sheet/CTC-Lokmodul-PluX22.md %}) und Adapterplatine erfolgt.

Nach dem Öffnen gibt es die erst kleine Überraschung: Zusätzlich zu Motor und Umschalter findet sich eine mir bisher unbekannte Platine in der Lok:

![Lok offen]({% link assets/images/umbau/ma2857/Ma2857-offen.jpg %})

Vermutlich soll sie für ein bei konstant helles Licht sorgen, denn die Lämpchen sind nur für 1,5 Volt ausgelegt, wie ich später festgestellt habe.
Bei meinen diversen Umbauten hat es sich allerdings bewährt, die verbaute Elektronik nicht weiter verstehen zu wollen, sondern einfach auszubauen:

![Lok zerlegt]({% link assets/images/umbau/ma2857/Ma2857-zerlegt.jpg %})

Bevor ich mit dem Einbau beginne, probiere ich erst mal wie CTC-Lokmodul und Einbauadapter am besten reinpassen.
Dies ist insbesondere wichtig, als der Einbauadapter von TAMS von vorne oder von hinten gesteckt werden kann und das einen Einfluss auf die Pinbelegung der Lötkontakte hat.
Hier habe ich mich für Stecken von vorne entschieden:

![Probeplatzierung Lokmodul]({% link assets/images/umbau/ma2857/Ma2857-probe.jpg %})

Daraus ergibt sich folgender Schaltplan:

![Schaltplan]({% link assets/images/umbau/ma2857/T5-Umbau-PluX22-EinbauVar-1.png %})

Dann beginne ich mit dem Motor.
Zuerst werden an die beiden Anschlüsse der Feldspule jeweils eine Diode angeschlossen.
Dabei unbedingt darauf achten, dass diese in unterschiedlicher Richtung (siehe die grauen Ringe) angeschlossen weden.
Die anderen Enden der Dioden werden miteinander verbunden:

![Dioden an Feldspule]({% link assets/images/umbau/ma2857/Ma2857-Motor-Dioden.jpg %})

An die beiden Dioden wird nun ein graues Kabel angelötet, das mit "Motor-" des Adapters verbunden wird.
Dann ziehe ich noch einen Schrumpfschlauch über die Dioden, der lang genug ist, dass alle blanken Kabel und Lötstellen abgedeckt sind: 

![Motor-Kabel 1]({% link assets/images/umbau/ma2857/Ma2857-Motor-Kabel-1.jpg %})
                
Anschließend löte ich die Drossel an den zweiten Motorpol und ein grünes Kabel an das andere Ende der Drossel.
Auch die Drossel wird in einen Schumpfschlauch eingepackt:

![Motor Drossel]({% link assets/images/umbau/ma2857/Ma2857-Motor-Drossel.jpg %})

Den Gleisanschluss bekomme ich über das schwarze (Original-) Kabel vom Schleifer (Bildmitte) und eine Lötöse, die ich an geeigneter Stelle ans Chassis schraube.
Hier habe ich die Stelle genutzt, an der vorher die alte Lok-Elektronik befestigt war:

![Gleis Lötöse]({% link assets/images/umbau/ma2857/Ma2857-Gleis-1.jpg %})

Die Lämpchen sind in diesem Fall mit zwei Kabeln angeschlossen, d.h. wir brauchen keine Schutzdioden.
Da sie aber nur für 1,5 Volt ausgelegt sind, müssen wir Sie durch solche ersetzen, die die volle Gleisspannung aushalten.
Bei der Recherche im Internet habe ich passende Birnchen mit LED für bis zu 22 Volt gefunden.

Zum verlösten des gemeinsamen Pluspols der Lämpchen nutze ich das Pluspol-Bein des Stützkondensators:

![Licht vorne]({% link assets/images/umbau/ma2857/Ma2857-Licht-vorne.jpg %})

Hier ein Foto vom hinteren Licht:

![Licht hinten]({% link assets/images/umbau/ma2857/Ma2857-Licht-hinten.jpg %})

Da die Lötpads doch recht klein sind, lohnt es sich vorher zu überlegen, in welcher Reihenfolge ich die Kabel auf den Adapter löte.
Hier ein Foto der komplett verkabelten Lok:

![Adapter und Verkabelung]({% link assets/images/umbau/ma2857/Ma2857-Adapter.jpg %})
                                           
Erst jetzt stecke ich das CTC-Lokmodul (vor der richtigen Seite!) in den Adapter. 
Bevor ich mühsam alles verstaue, mache ich noch einen ersten Funktionstest auf dem Rollenprüfstand:

![Funktionstest]({% link assets/images/umbau/ma2857/Ma2857-Funktionstest.jpg %})

Dann schiebe ich noch einen Schrumpfschlauch um Lokmodul und Adapter udn schrumpfe diesen vorsichtig, sodass ich ihn später wieder abziehen kann: 

![Lok fertig, offen]({% link assets/images/umbau/ma2857/Ma2857-fertig-offen.jpg %})

Wie so oft war wieder was anderes wichtiger und der Einbau des IR-Empfängers musste vertagt werden.
Ich ergänze diesen Artikel, sobald der IR-Empfänger eingebaut ist.  