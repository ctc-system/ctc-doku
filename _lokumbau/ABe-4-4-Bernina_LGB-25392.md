---
layout: umbau
title: "Umbau Bernina Triebwagen LGB 25392 mit DCC"
lokmodul: "G2-3,5A"
date: 2023-08-25
baureihe: "ABe 4/4"
spur: G
hersteller: LGB
artikelnr: 25392
umbauart: "G-DCC-Sound"
schaltplan: true
author: Peter Rudolph
---
Der Bernina-Triebwagen ist ein aktuelles LGB-Modell mit Sound-Dekoder.

Bei elektrischen Lokomotiven interessieren mich die Fahrsounds nicht wirklich, aber gelegentlich mal die Pfeife tuten lassen ist schon nett.
Also habe ich mich dazu entschieden, den Motor direkt über das [CTC-Lokmodul-G2]({% link _data_sheet/CTC-Lokmodul-G.md %}) anzusteuern und für die Sounds den original LGB-Dekoder zu nutzen.
Das führt zu einer bisher nicht gezeigten Variante der Verdrahtung:
Ich nutze beide Motorausgänge für die Motoren und schließe den DCC-Dekoder an zwei Halbbrücken an.

Mit dem alten CTC-Lokmodul-G gab es da erst mal eine böse Überraschung:
Der LGB-DCC-Dekoder zieht zumindest beim Starten mehr als das Limit von 1 Ampere des Halbbrücken-Treibers.
Das führte zu der Entscheidung für das CTC-Lokmodul-G2 einen leistungsstärkeren Treiber mit 2 Ampere zu verbauen.

So sah die originale Elektronik des Bernina-Triebwagens nach dem Öffnen des Gehäuses aus: 

![Bernina Offen mit Original-Dekoder]({% link assets/images/umbau/lgb25392/LGB25392_Offen_Dekoder.jpg %})
                            
Beim Design des CTC-Lokmodul-G2 haben wir auch die Kabel mit 4er-Buchse berücksichtigt, die in aktuellen LGB-Modellen verbaut werden:
wir bestücken die Anschlüsse für Gleis und Motor optional mit passenden Stiftleisten.

Wie bisher immer bei LGB-Modellen reicht die 3,5 A Variante des CTC-Lokmodul-G2.
Im Bild sieht man deutlich die beiden von den Motorblöcken kommenden schwarzen Kabel mit grauer 4er-Buchse sowie das braun-rote Kabel zwischen den zwei Halbbrücken und dem DCC-Dekoder:

![Bernina CTC-Modul verdrahtet]({% link assets/images/umbau/lgb25392/LGB25392_Modul_verdrahtet.jpg %})

Die vielen Kabel der Beleuchtung an das CTC-Modul anzuschließen habe ich mir gespart:
Licht anknipsen kann der DCC-Dekoder genauso gut wie das CTC-Modul.

Im Bernina-Trewibwagen hat es masse genug Platz für das CTC-Modul zusätzlich zum originalen DCC-Dekoder:

![Bernina CTC-Modul eingebaut]({% link assets/images/umbau/lgb25392/LGB25392_Modul_eingebaut.jpg %})
            
Beim anschließenden Zusammenbau war ich froh über die zwei helfenden Hände meiner Frau.
    
## Config anpassen

Im Auslieferungszustand des CTC-Lokmodul-G2 ist das Licht an CTC-Modul angeschlossen.
Da das in meinem Fall nicht stimmt, entferne ich mit dem Minus-Button das Licht aus der Liste der angeschlossenen Produkte.

Dann wähle ich links die "Bridge-1" (HB-1 / HB-2) und klicke auf den Plus-Button bei "Pins, Ports und Erweiterungen", um den DCC-Anschluss hinzuzufügen: 

![Bernina Config DCC]({% link assets/images/umbau/lgb25392/LGB25392_Cfg_DCC-1.png %})
                      
Wie bei DCC üblich wird die Standard-Adresse 3 verwendet.
           
Den mit der DCC-Erweiterung hinzugefügten "Motor" benenne ich um zu "DCC-Motor".

Damit das am DCC-Dekoder vorhandene Fahrlicht auf Fahrtrichtungsänderungen des Motors am CTC-Modul reagiert, füge ich einen Trigger am DCC-Motor hinzu:

![Bernina Config DCC]({% link assets/images/umbau/lgb25392/LGB25392_Cfg_DCC-Motor-Trigger-1.png %})

Als Auslöser für den Trigger wähle ich den Hauptmotor (Motor-1):             

![Bernina Config DCC]({% link assets/images/umbau/lgb25392/LGB25392_Cfg_DCC-Motor-Trigger-2.png %})

Dann ergänze ich das Trigger-Skript so, dass der DCC-Motor alle Zustandsänderungen von Motor1 übernimmt.
Den Parameter-Wert "$" (übernehme Wert vom Auslöser) kann man leider in der CTC-App (noch) nicht auswählen, d.h ich musste etwas anderes auswählen und dann in der HTML-Seite durch das Zeichen "$" ersetzen.
Der fertige Trigger sieht dann so aus: 

![Bernina Config DCC]({% link assets/images/umbau/lgb25392/LGB25392_Cfg_DCC-Motor-Trigger-3.png %})

Schließlich habe ich noch die Pfeife (Lok-Sound aus "universell-loks.xml") und das Innenlicht (Lok-Licht aus "universell-loks.xml") hinzugefügt und verbunden:  

![Bernina Config DCC]({% link assets/images/umbau/lgb25392/LGB25392_Cfg_DCC-fertig.png %})

Es gäbe noch diverse weitere Sounds, aber mir reicht die eine Pfeife.

Den NFC-Reader hatte ich tatsächlich vergessen einzubauen - das muss ich bei Gelegenheit noch nachholen.  
