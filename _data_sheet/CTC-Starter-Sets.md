---
layout: datasheet
title:  "CTC-Starter-Sets"
tags: Starter-Set
---

CTC-Starter-Sets gibt es **schon ab 274,- EUR**.

Zum ersten Starter-Set gibt es eine ausführliche [Doku anhand einer Beispielanlage]({% link de/docu/Starterkit.html %}).

Die Starter_Set enthalten nur noch je ein Lokmodul und ein Weichenmodul.
Mit den jeweiligen Ergänzungs-Sets lassen sie sich zum ursprünglichen Umfang (2 Loks und 2 Weichen) erweitern,

## CTC-Starter-Set C-Gleis

Inhalt:
* Die CTC-Steuerungs-App
* Ein betriebsbereiter [CTC-Router-FB 4040]({% link _data_sheet/CTC-Router.md %}) (ohne Beschränkungen zum späteren Vollbetrieb)
* 1 Stück [CTC-Lokmodul 21mtc]({% link _data_sheet/CTC-Lokmodul-21mtc.md %}) für Loks mit 21mtc Stecker
* 1 Stück [CTC-Weichenmodul]({% link _data_sheet/CTC-Weichenmodul-H0.md %}) für je 1 Weiche und 1 Signal mit Doppelspulenantrieb
* 1 Stück [CTC-IR-Empfangsdiode]({% link _data_sheet/CTC-IR-Empfaenger.md %}), Einmal pro Lok die mit Zugbeeinflussung ausgerüstet werden soll
* 2 Stück [CTC-IR-Balise Bausatz]({% link _data_sheet/CTC-IR-Sender.md %}) je 5 IR-LEDs, 1 kOhm Widerstand, Platine
* 2 Stück TVS-Diode zum Schutz der CTC-Module gegen Überspannung

[Jetzt kaufen]({{site.baseurl}}/shop.html#!/CTC-Starter-Set-C-Gleis/p/725587420/category=176901546){: .button}

Als Zubehör erhalten Sie einen Umbausatz für Analog-Loks, sowie eine Adapterplatine, sodass in diese das CTC-Lokmodul-21mtc eingesteckt werden kann.
                 
## CTC-Starter-Set Universal

Inhalt:
* Die CTC-Steuerungs-App
* Ein betriebsbereiter [CTC-Router-FB 4040]({% link _data_sheet/CTC-Router.md %}) (ohne Beschränkungen zum späteren Vollbetrieb)
* 1 Stück [CTC-Lokmodul PluX22]({% link _data_sheet/CTC-Lokmodul-PluX22.md %}) für Loks mit PluX22 Stecker
* 1 Stück [CTC-Weichenmodul]({% link _data_sheet/CTC-Weichenmodul-H0.md %}) für je 1 Weiche und 1 Signal mit Doppelspulenantrieb
* 1 Stück [CTC-IR-Empfangsdiode]({% link _data_sheet/CTC-IR-Empfaenger.md %}), Einmal pro Lok die mit Zugbeeinflussung ausgerüstet werden soll
* 2 Stück [CTC-IR-Balise Bausatz]({% link _data_sheet/CTC-IR-Sender.md %}) je 5 IR-LEDs, 1 kOhm Widerstand, Platine
* 2 Stück TVS-Diode zum Schutz der CTC-Module gegen Überspannung

[Jetzt kaufen]({{site.baseurl}}/shop.html#!/CTC-Starter-Set-Universal/p/725577988/category=176901546){: .button}

Als Zubehör erhalten Sie einen Umbausatz für Analog-Loks, sodass in diese das CTC-Lokmodul-PluX22 eingesteckt werden kann.

## Starter-Set G2 3,5A (indoor)

Inhalt:
* Die CTC-Steuerungs-App
* Der betriebsbereite [CTC-Router-FB 4040]({% link _data_sheet/CTC-Router.md %}) (ohne Beschränkungen zum späteren Vollbetrieb)
* 1 Stück [CTC-Lok-Modul G2]({% link _data_sheet/CTC-Lokmodul-G.md %}) mit 3,5 A Peak , für Spur 0 bis LGB
* 1 Stück [CTC-Weichen-Modul G2 (indoor)]({% link _data_sheet/CTC-Weichenmodul-G.md %}) inkl. Klemmen und Gehäuse
* 1 Stück [NFC-Reader]({% link _data_sheet/CTC-NFC-Reader.md %}) 
* 2 Stück NFC-Balisen zum Aufstecken auf die Schwelle
* 2 Stück TVS-Diode zum Schutz der CTC-Module gegen Überspannung

[Jetzt kaufen]({{site.baseurl}}/shop.html#!/CTC-Starter-Set-G-3-5A-indoor/p/725579244/category=176901546){: .button}

## Starter-Set G2 3,5A (outdoor)

Inhalt:
* Die CTC-Steuerungs-App
* Der betriebsbereite [CTC-Router tp-link EAP225O]({% link _data_sheet/CTC-Router.md %}) (ohne Beschränkungen zum späteren Vollbetrieb)
* 1 Stück [CTC-Lok-Modul G2]({% link _data_sheet/CTC-Lokmodul-G.md %}) mit 3,5 A Peak , für Spur 0 bis LGB
* 1 Stück [CTC-Weichen-Modul G2 (outdoor)]({% link _data_sheet/CTC-Weichenmodul-G.md %}) inkl. Klemmen und Gehäuse
* 1 Stück [NFC-Reader]({% link _data_sheet/CTC-NFC-Reader.md %})
* 2 Stück NFC-Balisen zum Aufstecken auf die Schwelle
* 2 Stück TVS-Diode zum Schutz der CTC-Module gegen Überspannung

[Jetzt kaufen]({{site.baseurl}}/shop.html#!/CTC-Starter-Set-G-3-5A-outdoor/p/726396019/category=176901546){: .button}

## Service

Über die angebotenen Starter-Sets hinaus stellen wir Ihnen gerne ein auf Sie zugeschnittenes Set zusammen.

