---
layout: datasheet
title:  "CTC-NFC-Balise"
tags: NFC-Balise NFC-Tag Balise Gartenbahn
printpage: CTC-NFC-Reader-Print
---
![NFC-Reader und -Balise]({% link assets/images/NFC-Reader/NFC-Reader-und-Tag.jpg %})

Speziell für den Einsatz im Garten, haben wir eine Alternative zu unserer [CTC-IR-Balise]({% link _data_sheet/CTC-IR-Sender.md %}) geschaffen:
Dabei setzen wir auf [Nahfeldkommunikation (NFC)](https://de.wikipedia.org/wiki/Near_Field_Communication), statt auf optische Infrarot-Signale.

Die Möglichkeiten bzgl. Positionserkennung und Automatisierung sind bei CTC-NFC-Reader identisch, zu denen des IR-Empfängers.
Allerdings besteht bei NFC keine Möglichkeit die übertragenen Daten zu beeinflussen.
Es wird lediglich, eine weltweit eindeutige Nummer übertragen, die in der NFC-Balisen schon vom Hersteller abgelegt wurde.

Die NFC-Balise bieten wir Ihnen, in an die [Eurobalise](https://de.wikipedia.org/wiki/Eurobalise) angelehnter Optik, an:
![NFC-Balise]({% link assets/images/NFC-Reader/NFC-Tags-Balise.jpg %})

Die Halter sind aus wetterfestem Kunststoff.
Das unter dem gelben Deckel platzierte Tag ist nach IP68 spezifiziert. Es übersteht also auch einen Regenguss, bei dem es zeitweise untergeht.

[NFC-Balise kaufen]({{site.baseurl}}/shop.html#!/NFC-Balise/p/725782970/category=176900083){: .button}

Alternativ können Sie auch gerne andere NFC-Tags verwenden, z. B. die NFC-Nägel, die Sie bei uns beziehen können:
![NFC-Nagel]({% link assets/images/NFC-Reader/NFC-Nagel-2.jpg %})
Diese werden in den Boden gesteckt, sodass ihr Kopf ca. 5 mm über die Schwellen hinaus ragt.

[NFC-Nagel kaufen]({{site.baseurl}}/shop.html#!/NFC-Nagel/p/726427951/category=176900083){: .button}
