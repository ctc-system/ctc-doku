---
layout: datasheet
title:  "CTC-NFC-Reader"
tags: NFC-Reader NFC-Balise NFC-Tag Balise Gartenbahn
printpage: CTC-NFC-Reader-Print
---
![NFC-Reader und -Balise]({% link assets/images/NFC-Reader/NFC-Reader-und-Tag.jpg %})

Speziell für den Einsatz im Garten, haben wir eine Alternative zu unserer [CTC-IR-Balise]({% link _data_sheet/CTC-IR-Sender.md %}) geschaffen:
Dabei setzen wir auf [Nahfeldkommunikation (NFC)](https://de.wikipedia.org/wiki/Near_Field_Communication), statt auf optische Infrarot-Signale.

Die Möglichkeiten bzgl. Positionserkennung und Automatisierung sind bei CTC-NFC-Reader identisch, zu denen des IR-Empfängers.
Allerdings besteht bei NFC keine Möglichkeit die übertragenen Daten zu beeinflussen.
Es wird lediglich, eine weltweit eindeutige Nummer übertragen, die in der [NFC-Balisen]({% link _data_sheet/CTC-NFC-Balise.md %}) schon vom Hersteller abgelegt wurde.

Der NFC-Reader hat eine Größe von 40 mm x 42 mm:
![NFC-Reader]({% link assets/images/NFC-Reader/NFC-Reader.jpg %})

[NFC-Reader kaufen]({{site.baseurl}}/shop.html#!/NFC-Reader/p/725782807/category=176902513){: .button}

Er wird unter die Lok montiert, sodass sich ein Leseabstand von ca 2 cm ergibt:
![NFC-Reader unter Lok]({% link assets/images/NFC-Reader/NFC-Reader-Lok-2.jpg %})

Angeschlossen wird der NFC-Reader mit 4 Kabeln an das [CTC-Lokmodul-G]({% link _data_sheet/CTC-Lokmodul-G.md %}).

**Hinweis:** An das CTC-Lokmodul-H0a, das gerne für kleine Gartenbahn-Loks genutzt wird, kann der CTC-NFC-Reader leider nicht angeschlossen werden.
Betroffenen Kunden machen wir bei Bedarf ein attraktives Upgrade-Angebot.  
