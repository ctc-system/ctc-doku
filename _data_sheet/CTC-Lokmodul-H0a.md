---
layout: datasheet
title:  "CTC-Lokmodul-H0a"
tags: Lokmodul Lok
printpage: CTC-Lokmodul-H0a-Print
---
**Das CTC-Lokmodul-H0a ist ausverkauft und wird nicht mehr weiter angeboten.**
Als Ersatz empfehlen wir das [CTC-Lokmodul-PluX22]({% link _data_sheet/CTC-Lokmodul-PluX22.md %}) mit unserem [CTC-Adapter für PluX22]({% link _data_sheet/Adapter-PluX22.md %}).

Das CTC-Lokmodul-H0a ist optimiert für den Einbau in alte Analog-Lokomotiven, in denen ein Pol der Beleuchtung fest mit dem Gehäuse verbunden ist.
Die vier Schaltausgänge reichen für Licht vorne und hinten und z.B. zwei elektrische Kupplungen oder eine elektrische Kupplung und einen Rauchsatz.    

| Eigenschaft         | Beschreibung                                                          |
|---------------------|-----------------------------------------------------------------------|
| Optimiert für       | Analog-Umbau                                                          |
| Maße                | 35 x 20 mm                                                            |
| Prozessor           | Espressif ESP8285                                                     |
| Eingangsspannung    | ca. 9 V bis 24 V DC, Digitalstrom oder AC (mit Stützkondensator)      |
| Motorausgang        | Gleichstrom-Motor: Dauerstrom 1,0 A, Peak 2,8 A                       |
| Leistungsausgänge   | 4x 0,5 A Highside                                                                     |
| Strom Summe max.    | 2 A                                                                   |
| Schnittstellen      | CTC-IR-Empfänger, I2C                                                 |
| Strom online passiv | ca. 20 mA bei 16 V                                                    |
| Strom booten        | ca. 50 mA bei 16 V zzgl. Schaltstrom für Initialisierung der Ausgänge |

<br/>

In der Übersicht "[CTC-Lokmodule]({% link de/docu/CTC-Lokmodule.md %})" finden Sie weitere Module und Links zu Einbauanleitungen.

![Lokmodul]({% link assets/images/module/Loko-H0-Ma-ESP8285-2.jpg %})
              
## Anschluss

**Hinweise:**
* Bei der Wahl der Eingangsspannung bitte unbedingt daran denken, dass am CTC-Modul angeschlossene Motoren, Lampen, ... auch diese Spannung vertragen müssen!

Standardkonfiguration
* Die Kontakte Gleis-A und Gleis-B mit dem Gleis bzw. der Spannungsversorgung verbinden.
  Alternativ kann Gleis-A auch über eine Schraube (Bohrung in der Platine) angeschlossen werden.
  Dabei ist es gleichgültig, ob sie das Gleis mit einem Analog-Trafo, einer Digitalzentrale oder einem Netzteil versorgen, sofern die maximale Spannung von 24 V nicht überschritten wird.
  Da am Eingang ein Gleichrichter hängt, spielt es auch keine Rolle, welchen Pol Sie mit Gleis-A und Gleis-B verbinden.
* Den Stützkondensator (Elektrolyt, min. 400 uF, empfohlen 1000 uF) an VBB (Pluspol) und GND anschließen.
* Den Motor mit Motor + und Motor - verbinden (für Anschluss des Elektromagneten von Allstrommotoren siehe separate Anleitung).
* Pluspol von Licht vorne und hinten mit den entsprechenden Anschlüssen verbinden.
  Den Minuspol jeweils mit GND verbinden oder über eine Diode mit dem Minus des Gleisanschlusses.
* Optional Aux-Anschlüsse mit Pluspol z.B. der elektrischen Kupplung verbinden.
  Den Minuspol jeweils mit GND verbinden oder über eine Diode mit dem Minus des Gleisanschlusses.
* Anschluss des CTC IR-Empfängers an den Programmierstecker (RX, VCC, GND)

Die Zuordnung der Pins zu Schaltern in der App kann frei konfiguriert werden (Cfg.xml).

## Weitere Anschlüsse

Der I2C-Bus und die anderen "Reserved" Pins werden von der aktuellen Firmware nicht unterstützt.
     
## Lieferung
                                                                                           
Das CTC-Lokmodul-H0a liefern wir immer wie folgt mit Kabeln nach unserem [Farbschema]({% link _app_doku/A40-kabelfarben.md %}):

![Lokmodul]({% link assets/images/module/Loko-H0-Ma-ESP8285-2-Kabel.jpg %})
