---
layout: datasheet
title:  "CTC-IR-Empfänger"
tags: IR-Empfänger
printpage: CTC-IR-Sender-Print
---
Bei den IR-Empfängern handelt es sich um Infrarot-Empfänger für einer Wellenlänge von 950 nm und einer Modulationsfrequenz von 38 kHz.
Das Bild zeigt den links den aktuell verfügbaren [Vishay TSOP98438](https://www.vishay.com/en/product/82831/) und rechts den sich noch in der Evaluierung befindlichen, etwa halb so dicken [Vishay TSOP39338](https://www.vishay.com/en/product/82774).

![IR-Empfänger und Kabel]({% link assets/images/module/IR-Empfaenger.jpg %})

[Jetzt kaufen]({{site.baseurl}}/shop.html#!/CTC-IR-Empfanger-fur-Lok-10pol/p/725577870/category=176902513){: .button}

Die Pin sind in beiden Fotor von links nach rechts nummeriert und wie folgt belegt:
* Beim größeren TSOP98438: 1-Out (Daten, violett), 2-GND (schwarz), 3-Vs (3,3V, rot)
* Beim kleineren TSOP39338: 1-GND (schwarz), 2-Vs (3,3V, rot), 3-Out (Daten, violett), 4-GND (schwarz).
  Es genügt, wenn einer der beiden GND angeschlossen ist.

Wir liefern immer Kabel mit zum Lokmodul passenden Stecker mit.
Das Foto zeigt ein CTC-Lomodul-PluX22 (mit 21mtc sieht das ganau glecih aus):
![IR-Empfänger und Kabel]({% link assets/images/module/IR-Kabel-Lokmodul.jpg %})
