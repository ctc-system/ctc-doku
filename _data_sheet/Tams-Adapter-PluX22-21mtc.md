---
layout: datasheet
title:  "TAMS Adapter für PluX22 und 21mtc"
tags: PluX22 21mtc Adapter Lokmodul Lok
printpage: Tams-Adapter-PluX22-21mtc-Print
---
Zum Einbau in Loks, die nicht bereits über einen passenden Stecker bzw Buchse verfügen, haben wir mit [TAMS Nr. 70-01045-01](https://tams-online.de/epages/642f1858-c39b-4b7d-af86-f6a1feaca0e4.sf/de_DE/?ObjectPath=/Shops/642f1858-c39b-4b7d-af86-f6a1feaca0e4/Products/70-01045-01) eine Universal-Lösung gefunden, die allerdings aufgrund der kleinen Lötpads gute Lötkenntnisse erfordert.
Für PluX22 haben wir deshalb einen [eigenen Adapter]({% link _data_sheet/Adapter-PluX22.md %}) entwickelt. 

Ergänzend zur guten offiziellen Doku auf der verlinkten WebSite bei TAMS finden Sie hier Verdrahtungspläne für Standard-Fälle:

**Hinweis:** Für IR-Empfänger und NFC-Reader gibt es in den Standards PluX22 und 21mtc keine passenden Anschlüsse.
Deshalb werden diese über die separate 10-poligen Buchse an das CTC-Lokmodul angeschlossen. 

## PluX22 Einbauvariante 1

![PluX22 Einbauvariante 1]({% link assets/images/module/PluX22-Adapter-EinbauVar-1.png %})
     
<BR/>

## PluX22 Einbauvariante 2

![PluX22 Einbauvariante 2]({% link assets/images/module/PluX22-Adapter-EinbauVar-2.png %})

<BR/>

## 21mtc Einbauvariante 1

![21mtc Einbauvariante 1]({% link assets/images/module/21mtc-Adapter-EinbauVar-1.png %})

<BR/>

## 21mtc Einbauvariante 2

![21mtc Einbauvariante 2]({% link assets/images/module/21mtc-Adapter-EinbauVar-2.png %})
