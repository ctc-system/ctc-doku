---
layout: datasheet
title:  "CTC-Router"
tags: WLAN CTC-Router Router
---
Der CTC-Router ist ein [WLAN-Access-Point]({% link de/docu/WLAN-Access-Point.md %}), der von uns, für Ihre Modellbahn vorkonfiguriert wird.

Hierzu suchen wir uns ein geeignetes Modell aus und prüfen dieses ausgiebig.

Unser derzeitiger Favorit ist die FRITZ!Box 4040:

![CTC-Router FritzBox 4040]({% link assets/images/module/Router-FB-4040.jpg %})

[FB-4040 kaufen]({{site.baseurl}}/shop.html#!/CTC-Router-FritzBox-4040-indoor/p/725596015/category=176901546){: .button}
        
### Outdoor-Router
  
Für den Betrieb im Garten empfehlen wir einen wetterfesten Router.
In Kürze bieten wir, ebenfalls vorkonfiguriert, den tp-link EAP225O an.

[EAP225O kaufen]({{site.baseurl}}/shop.html#!/CTC-Router-tp-link-EAP225O-outdoor/p/726387587/category=176901546){: .button}
                                                          
Bei unserer eigenen Gartenbahn nutzen wir als CTC-Router eine FritzBox beim Schattenbahnhof in der Garage und zwei tp-link EAP225O als Repeater im Garten.
            
### Service WLAN-Einbuchung

Da wir den CTC-Router, egal ob er Teil des Starter-Sets war oder separat bestellt wurde, komplett fertig konfiguriert haben, sind uns Ihr Netzwerkname (SSID) und Ihre Netzwerkkennung (Passwort) bekannt.
Deshalb erhalten Sie dann Ihre CTC-Module von uns immer bereits in Ihr Modellbahn-WLAN eingebucht.
Dieser Service ist der Grund für den Preisaufschlag des CTC-Routers gegenüber einer serienmäßigen FRITZ!Box.
