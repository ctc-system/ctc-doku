---
layout: datasheet
title:  "CTC-Weichenmodul"
tags: Weichenmodul Weiche Signal
printpage: CTC-Weichenmodul-Print
---

| Feature             | Beschreibung                                                                       |
|:--------------------|:-----------------------------------------------------------------------------------|
| Prozessor           | Espressif ESP-32                                                                   |
| Maße                | 68 x 26 mm - selbe Form wie Märklin C-Gleis Dekoder                                |
| Eingangsspannung    | ca. 9V bis 24V DC (geglätteter Gleichstrom), Digitalstrom, AC mit Stützkondensator |
| Leistungsausgänge   | 4                                                                                  |
| Strom Summe max.    | 2 A                                                                                |
| Schnittstellen      | 2 CTC IR-Balisen (je max. 12mA), NFC-Reader (statt zweiter IR-Balise)              |
| Strom online passiv | ca. 20mA bei 16V                                                                   |
| Strom booten        | ca. 50mA bei 16V zzgl. Schaltstrom für Initialisierung der Ausgänge                |

<br/>

Das CTC-Weichenmodul erhalten Sie in drei Ausführungen:
* Mit einem Märklin-Stecker und einer Buchsenleiste zur Montage in eine C-Gleis-Weiche oder C-Gleis-Kreuzung.
* Mit zwei Märklin-Steckern zur Montage in eine C-Gleis-Dreiwegweiche.
* Mit zwei 3er-Buchsenleisten und Montagehalter für alle anderen elektromagnetischen Antriebe mit 2 Spulen.

In der Übersicht "[Schalten mit CTC]({% link de/docu/CTC-Schaltmodule.md %})" finden Sie weitere Module und Links zu Einbauanleitungen.

## Variante mit einem Märklin-Stecker

Diese Variante ist zum Einbau in Märklin C-Gleis-Weichen gedacht.
Zusätzlich kann ein weiterer elektromagnetischen Antriebe mit je 2 Spulen angeschlossen werden, z.B. ein Flügelsignal.

![Weichenplatine]({% link assets/images/module/Weiche-H0-1x-Maerklin.jpg %})

[Jetzt kaufen]({{site.baseurl}}/shop.html#!/CTC-Weichenmodul-C-Gleis/p/725604063/category=176888458){: .button}

## Variante mit zwei Märklin-Steckern

Diese Variante ist zum Einbau in Märklin C-Gleis-Dreiwegweichen gedacht.                                   

[Jetzt kaufen]({{site.baseurl}}/shop.html#!/CTC-Weichenmodul-C-Gleis-2-Antriebe/p/725596051/category=176888458){: .button}

## Variante "Universal" mit Kabeln

Diese Variante ist für zwei elektromagnetische Antriebe mit je 2 Spulen gedacht.
Mit dem beigelegten Montagehalter kann das CTC-Weichenmodul unter der Modellbahnanlage festgeschraubt werden.

![Weichenplatine]({% link assets/images/module/Weiche-H0-2x-Buchse.jpg %})

[Jetzt kaufen]({{site.baseurl}}/shop.html#!/CTC-Weichenmodul-universal/p/725604076/category=176888458){: .button}

## Pinbelegung

Die aktuelle (zweite) Serie wird seit Juli 2021 ausgeliefert.
Man erkennt sie am seitlich überstehenden Elektrolytkondensator (in den Bildern unten links), der den Betrieb mit Wechselstrom und ungeglättetem Gleichstrom ermöglicht.
Die Verkabelung entspricht immer einer der in den drei oben erwähnten Varianten. 

![Weichenplatine]({% link assets/images/module/Weiche-ESP32-4-Pins.jpg %})
 
### Anschluss

**Hinweise:**
* Bei der Wahl der Eingangsspannung bitte unbedingt daran denken, dass am CTC-Modul angeschlossene Motoren, Lampen, ... auch diese Spannung vertragen müssen!

Standardkonfiguration
* Die Kontakte Gleis-A und Gleis-B mit dem Gleis bzw. der Spannungsversorgung verbinden.
  Dabei ist es gleichgültig, ob es sich um eine Digitalzentrale oder ein Netzteil handelt, sofern die maximale Spannung von 24 V nicht überschritten wird.
  Da am Eingang ein Gleichrichter hängt, spielt es auch keine Rolle, welchen Pol Sie mit Gleis-A und Gleis-B verbinden.
* Die Spannungsversorgung des Weichenantriebs (Pluspol, bei Märklin gelbes Kabel) mit W1-VBB bzw. W2-VBB verbinden.
* Die Schaltanschlüsse des Weichenantriebs (Minuspol, bei Märklin blaue Kabel mit grünem bzw. rotem Stecker) mit W1-Grün und W1-Rot bzw. W2-Grün und W2-Rot verbinden.
* Die CTC-IR-Balise mit IR1-GND (Minuspol, grün) und IR1-VCC (Pluspol, rot) bzw. IR2-GND und IR2-VCC verbinden.
  Siehe dazu auch das Datenblatt des [CTC-IR-Balise]({% link _data_sheet/CTC-IR-Sender.md %}).
  Bitte auf keinen Fall den Widerstand (typisch 1 kOhm, min. 330 Ohm) in der IR-Balise vergessen, sonst ist das Weichenmodul kaputt.
* Den [NFC-Reader]({% link _data_sheet/CTC-NFC-Reader.md %}) an NFC RX, NFX TX sowie GND und VCC anschließen.
  Bitte beachten Sie, dass die zweite IR-Balise und der NFC-Reader nicht gleichzeitig genutzt werden können.
* LEDs jeweils an einen der Anschlüsse LED-1 bis LED-3 und GND anschließen.
  Beim Anschluss eines Lichtsignals ggf. dort Widerstände entfernen, da LED-1 bis LED-3 bereits über je einen 1 kOhm Widerstand verfügen und gegen 3,3 Volt schalten. 

Die Zuordnung der Pins zum Schalten kann in der App konfiguriert (Cfg.xml) werden, sowie die Dauer des Schaltimpulses.

---

## Archiv

### Pinbelegung erste Serie

Die erste Serie wurde bis Juni 2021 ausgeliefert.
Die Verkabelung wurde noch mit jedem Kunden einzeln abgesprochen.

![Weichenplatine]({% link assets/images/Weiche_ESP32.jpg %})

**Hinweise**:
* Der LED-Anschluss liegt parallel zur LED auf der Platine und darf in Summe mit max. 12mA belastet werden (die LED verfügt über einen 150 Ohm Widerstand)
* Durch Anpassung der ioCfg.xml können die I2C-Anschlüsse bei aktueller Firmware für einen NFC-Reader verwendet werden - kontaktieren Sie uns bei Bedarf.   
* **Ohne Stützkondensator ist dieses (alte) Modul nur für Betrieb an Schaltnetzteilen geeignet!**
