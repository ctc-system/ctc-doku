---
layout: datasheet
title:  "CTC-Adapter für PluX22"
tags: PluX22 Adapter Lokmodul Lok
printpage: Adapter-PluX22-Print
---
Zum Einbau in Loks, die nicht bereits über einen passenden Stecker bzw Buchse verfügen, haben wir einen Adapter mit PluX22-Schnittstelle geschaffen.

| Eigenschaft      | Beschreibung                                 |
|------------------|----------------------------------------------|
| Optimiert für    | Loks ohne Digitalschnittstelle               |
| Maße             | 30 x 14,5 mm                                 |
| Schnittstellen   | PluX22-Buchse, optional SUSI3-Classic-Buchse |

Den Adapter liefern wir in drei Varianten:

| **Nur mit PluX22-Buchse** | **Mit PluX22-Buchse und SUSI3-Classic** | **Mit PluX22-Buchse, SUSI3-Classic und Schraubklemmen** |
| ![PluX22]({% link assets/images/module/Adapter-PluX22.jpg %}) | ![PluX22]({% link assets/images/module/Adapter-PluX22-SUSI.jpg %}) | ![PluX22]({% link assets/images/module/Adapter-G-PluX22-SUSI.jpg %}) |
| [Jetzt kaufen]({{site.baseurl}}/shop.html#!/Lokadapter-PluX22/p/725577930/category=176902513){: .button} | [Jetzt kaufen]({{site.baseurl}}/shop.html#!/Lokadapter-PluX22-mit-SUSI/p/725577921/category=176902513){: .button} | [Jetzt kaufen]({{site.baseurl}}/shop.html#!/Lokadapter-PluX22-mit-Klemmen-und-SUSI/p/725788637/category=176902513){: .button} |

Die Platine bietet außerdem Platz für:
* 2 Dioden zum Anschluss von Motoren mit Feldspulen (z.B. alte Märklin-Loks).
* je eine Diode für Front- und Rücklicht zum Anschluss von Beleuchtung mit nur einem Draht (zweiter Pol am Gehäuse). 

**Hinweise:** 
* Für IR-Empfänger und NFC-Reader gibt es im Standard PluX22 keine passenden Anschlüsse.
  Deshalb werden diese über die separate 10-poligen Buchse an das CTC-Lokmodul angeschlossen.
* Beim Lokmodul-PluX22 ist das Fahrlicht als Schalter gegen Minus (LowSide) konfiguriert.
  Soll es gegen Pluspol schalten (HighSide), so muss im Config-Dialog das Licht entfernt und durch eines mit HighSide ersetzt werden.
  Siehe dazu den Umbau der [Märklin BR53 (Borsig I)]({% link _lokumbau/BR53_Ma-3102.md %}). 

## Verdrahtung

Das Beispiel zeigt die Verdrahtung einer alten Analog-Lok mit Feldspule und elektrischem Entkuppler vorne und hinten:

![Verdrahtung]({% link assets/images/module/Loko-Adapter-PluX22.png %})
                                       
Hier der Blick von hinten, wie es sich z.B. bei der Variante mit Schraubklemmen ergibt:

![Verdrahtung]({% link assets/images/module/Loko-Adapter-PluX22-hinten.png %})
                                 
Die Dioden müssen separat bestellt und selbst eingelötet werden.
Die PluX22-Buchse ist immer aufgelötet. 

Das Loch in der Platine ist elektrisch mit Gleis-1 verbunden, sodass diese Verbindung alternativ über das Anschrauben am Gehäuse hergestellt werden kann.
Bei alten Märklin-Loks kann die Platine dort angeschraubt werden, wo zuvor der Umschalter festgemacht war.
Ein Kabel zu den Rädern/Gehäuse ist dann nicht nötig.

Hat die Lok einen Permanentmagnet so entfallen die beiden rechten Dioden und die obere wird durch ein Drahtstück ersetzt.
Der Motor wird dann an Motor+ und Motor-A angeschlossen.
Motor-B bleibt unbelegt.

Wenn Front- und Rücklicht zwei Drähte haben (also keine Verbindung zum Lokgehäuse) kännen die beiden linken Dioden durch Drahtstücke ersetzt werden.