---
layout: datasheet
title:  "CTC-IR-Balise"
tags: IR-Balise IR-Sender Balise C-Gleis
printpage: CTC-IR-Sender-Print
---
Bei den IR-Balisen handelt es sich um Infrarot-Leuchtdioden mit einer Wellenlänge von 950 nm.
Davon werden für eine IR-Balise mehrere parallel geschaltet und mit einem Vorwiderstand versehen, sodass sie nur wenige Zentimeter weit senden.
Der Vorwiderstand muss so bemessen sein, dass maximal 10mA Strom bei 3,3V fließen.

Da wir noch mit der Bauform experimentieren, können wir im Moment keine fertigen Sender anbieten.

Unser Bausatz für das Märklin C-Gleis enthält derzeit fünf IR-LEDs mit 950nm Wellenlänge (z.B. Vishay TSUS 4300) und einen 1 kOhm Vorwiderstand.

[Jetzt kaufen]({{site.baseurl}}/shop.html#!/CTC-IR-Balise-Bausatz/p/725728705/category=176900083){: .button}
           
**Hinweis:** Für den Einsatz bei Gartenbahnen empfehlen wir NFC statt IR, siehe [CTC-NFC-Reader]({% link _data_sheet/CTC-NFC-Reader.md %})

![IR-Balise und Lok mit IR-Empfänger]({% link assets/images/IR-Sender/IR-mit-Lok.jpg %})
       
Für unsere Lok-Module gibt es den IR-Empfänger jeweils zusammen mit einem Kabel und passenden Mini-Stecker.
Um den Einbau zu erleichtern wird der IR-Empfänger jeweils separat mitgeliefert und muss von Ihnen noch angelötet werden.
Im Bild oben wurde der IR-Empfänger (schwarzes Kästchen) unter die vordere Kupplung (gelb) geklebt.

## Bausatz IR-Balise für das C-Gleis

Das folgende Bild zeigt das noch unbearbeitete C-Gleis und die Teile des IR-Balisen-Bausatzes:

![IR-Balise Gleis und Bausatz]({% link assets/images/IR-Sender/IR-Sender-Bausatz.jpg %})

Zum Selbstbau der IR-Balise wird wie folgt vorgegangen:

1. Mit der Platine Maß nehmen und 5 Löcher in das Gleis bohren (3 mm Durchmesser), dort wo das unter dem Gleis die Montagestifte vorhanden sind:
   ![Gleis mit Löchern für IR-Balise]({% link assets/images/IR-Sender/Gleis-gebohrt.jpg %})
1. Die IR-LEDs in die vorgesehenen Löcher der Platine stecken (langes Bein zur Gleismitte) und dann beides in das C-Gleis einsetzen.
   ![IR-LEDs ins Gleis einführen]({% link assets/images/IR-Sender/IR-LEDs-einfuehren.jpg %})
1. Erst jetzt die IR-LEDs verlöten.
1. Die Platine herausnehmen, den Widerstand einsetzen und verlöten.
   ![IR-Platine mit LEDs und Widerstand]({% link assets/images/IR-Sender/IR-Platine-fertig.jpg %})
1. Die Platine wieder einsetzen und Anschlussdrähte und Stecker anlöten:
   ![IR-Balise Kabel anlöten]({% link assets/images/IR-Sender/IR-Sender-Kabel.jpg %})
   Dabei kommt der Pluspol (im Bild gelbes Kabel, roter Schrumpfschlauch) an PWM-VCC und der Minuspol (im Bild graues Kabel, grüner Schrumpfschlauch) an TX-GND.
1. Ob die IR-LEDs alle funktionieren können Sie mit der Handy-Kamera feststellen.
   Dazu schließen Sie die IR-Balise an ca. 16V Gleichspannung an (roter Schrumpfschlauch an den Pluspol).
   In der Kamera-App Ihres Handys sollten Sie nun die LEDs lila schimmern sehen (ggf. Fremdlicht etwas abschirmen):
   ![IR-Balise mit Handy testen]({% link assets/images/IR-Sender/IR-Sender-testen.jpg %})
   Da die LEDs nur in einem recht spitzen Winkel nach oben strahlen, kann es sein, dass Sie das Handy etwas hin und her bewegen müssen, um alle LEDs prüfen zu können.
   Sind die IR-Balisen am CTC-Modul angeschlossen (3,3 V), so sieht die Handy-Kamera leider nichts mehr.
1. Nun fehlt nur noch der Anschluss an ein CTC-Weichenmodul oder das CTC-IO-Board. 
   Der Stecker am CTC-Modul ist mit Schrumpfschlüchen in denselben Farben (rot, grün) markiert:
   ![CTC-Weichenmodul mit angeschlossener IR-Balise]({% link assets/images/Weichenmodul-mit-IR-Sender.jpg %})

## IR-Balise für das C-Gleis ohne Bausatz

Eine IR-Balise können Sie natürlich auch ohne unseren Bausatz herstellen, so wie es die bisherige Anleitung gezeigt hat.
Als LEDs verwenden wir Vishay TSUS 4300 (Wellenlänge 950 nm) und der Widerstand beträgt 1 kOhm.

![IR-Balise Eigenbau]({% link assets/images/IR-Sender/IR-Sender-Eigenbau.jpg %})

![IR-Balise Eigenbau von unten]({% link assets/images/IR-Sender/IR-Sender-Eigenbau-von-unten.jpg %})
