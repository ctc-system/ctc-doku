---
layout: datasheet
title:  "CTC-Lokmodul-G2"
tags: Lokmodul Lok
printpage: CTC-Lokmodul-G-Print
---

Das CTC-Lokmodul-G2 ist optimiert für den Einbau in Spur-1 und Gartenhbahn-Lokomotiven.
Loks mit vielen Funktionen profitieren von den zwei Motoranschlüssen, 10 Schaltausgängen und 2 Servo-Anschlüssen dieses Lokmoduls.
Außerdem kann ein SUSI3-Soundmodul angeschlossen werden. 

| Eigenschaft            | Beschreibung                                                      |
|------------------------|-------------------------------------------------------------------|
| Optimiert für          | Gartenbahn-Loks, Loks mit viel Strombedarf, Loks mit 2 Motoren    |
| Maße                   | 81 x 30 mm (älteres Lokmodul-G 78 x 30 mm)                        |
| Prozessor              | Espressif ESP32                                                   |
| Eingangsspannung       | ca. 9 V bis 24 V DC, Digitalstrom oder AC (mit Stützkondensator)  |
| Motorausgang           | 2x Gleichstrom-Motor: Peak 6 A bzw. 3,5 A                         |
| Leistungsausgänge (SW) | 6x 1,2 A LowSide                                                  |
| Halbbrücken (HB)       | 4x 2,5 A (älteres Lokmodul-G: 4x 1,0 A)                           |
| Servos                 | 2x (benötigen externe Stromversorgung oder Zusatzplatine)         |
| Strom Summe max.       | 15 A                                                              |
| Schnittstellen         | CTC-NFC-Reader, 2x Sensor-Input (Optokoppler), CTC-IR-Empfänger   |
| Sound                  | SUSI3-Classic (älteres Lokmodul-G mit Adapter auf den Servo-Pins) |
| Strom online passiv    | 35 mA bei 22 V                                                    |
| Strom booten           | 50 mA bei 22 V zzgl. Schaltstrom für Initialisierung der Ausgänge |

<br/>

Das CTC-Lokmodul-G2 bieten wir in zwei Varianten mit unterschiedlichen Treiber-Bausteinen bestückt an
* für zwei Motoren mit je 3,5 A Peak <br/>
  [G2-3.5A jetzt kaufen]({{site.baseurl}}/shop.html#!/Lokmodul-G2-mit-3-5-A-Peak/p/725538297/category=176866649){: .button}
* für zwei Motoren mit je 6 A Peak <br/>
  [G2-6A jetzt kaufen]({{site.baseurl}}/shop.html#!/Lokmodul-G2-mit-6-A-Peak/p/725541407/category=176866649){: .button}

In aktuelle LGB-Loks sind die Motorblöcke (inkl. Gleiskontakt) über ein Flachbandkabel mit Buchse im 2,54 mm Raster angeschlossen.
Hierzu bieten wir beim CTC-Lokmodul-G2 jeweils eine Bestückungsvariante an, in der zwei Schraubklemmen durch passende Stiftleisten ersetzt sind. 

* Das älter CTC-Lokmodul-G1 (ohne SUSI) ist nur noch in der Variante mit 6 A Peak verfügbar <br/>
  [G1-6A jetzt kaufen]({{site.baseurl}}/shop.html#!/Lokmodul-G1-mit-6-A-Peak-ohne-SUSI/p/725587473/category=176866649){: .button}

In der Übersicht "[CTC-Lokmodule]({% link de/docu/CTC-Lokmodule.md %})" finden Sie weitere Module und Links zu Einbauanleitungen.

## Pinbelegung Lokmodul-G2

Die Pinbelegung aller vier Varianten des CTC-Lokmodul-G2 ist identisch.
Die unterschiedlichen Treiberbausteine sind nur anhand des Aufdrucks auf den Bausteinen erkennbar.
Wir markieren die 3,5 A Variante mit einem roten und die 6 A Variante mit einem grünen Punkt.
Hier die universelle Variante mit Schraubklemmen:                       

![Lokmodul-G2]({% link assets/images/module/Loko-G2-Esp32-Pins.jpg %})
  
Hier die für neuere LGB-Loks gedachte Variante mit Stiftleisten für Motor- und Gleisanschluss: 

![Lokmodul-G2-LGB]({% link assets/images/module/Loko-G2-LGB.jpg %})

## Pinbelegung (älteres) Lokmodul-G
            
![Lokmodul-G]({% link assets/images/module/Loko-G-Esp32-3-Pins.jpg %})

**Hinweise zum alten Lokmodul-G:**
* Die Pinbeschriftung bei "Servo-5V" war bis zum 19.06.2023 falsch (VBB und 5V vertauscht)!
* Bei vor dem 09.07.2021 ausgelieferten CTC-Lokmodul-G waren die SW-Anschlüsse paarweise vertauscht.
  Dies lässt sich per Software-Konfiguration korrigieren.

## Anschluss

**Hinweise:**
* Bei der Wahl der Eingangsspannung bitte unbedingt daran denken, dass am CTC-Modul angeschlossene Motoren, Lampen, ... auch diese Spannung vertragen müssen!

Standardkonfiguration
* Die Kontakte Gleis-A und Gleis-B mit dem Gleis bzw. der Spannungsversorgung verbinden.
  Dabei ist es gleichgültig, ob sie das Gleis mit einem Analog-Trafo, einer Digitalzentrale oder einem Netzteil versorgen, sofern die maximale Spannung von 24 V nicht überschritten wird.
  Da am Eingang ein Gleichrichter hängt, spielt es auch keine Rolle, welchen Pol Sie mit Gleis-A und Gleis-B verbinden.
  Aber beide Gleis-A bzw. Gleis-B Anschlüsse **müssen jeweils mit demselben Gleiskontakt** verbunden sein.
* Den Stützkondensator (Elektrolyt, min. 400 uF, empfohlen 6800 uF) an VBB (Pluspol) und GND anschließen.
* Die Motoren jeweils mit Mo1+ und Mo1- (bzw. Mo2+ und Mo2-) verbinden.
* Minuspole von Lichtern jeweils mit einem der SW-x Anschlüsse verbinden.
  Den Pluspol jeweils mit einem beliebigen VBB-Anschluss verbinden.
* Optional weitere SW-Anschlüsse z.B. mit Minuspol der elektrischen Kupplung, des Rauchsatzes oder weiterer Beleuchtung verbinden.
  Den Pluspol jeweils mit einem beliebigen VBB-Anschluss verbinden.
* Den CTC-NFC-Reader an NFC-RX, NFC-TX sowie VCC und GND anschließen.
* Das SUSI3-Soundmodul in die SUSI3-Classic-Buchse einstecken.

Die Zuordnung der Pins zu Schaltern in der App kann frei konfiguriert werden (Cfg.xml).

## Weitere Anschlüsse

Für Servos, z.B. um Pantographen heben und senken zu können, brauchen Sie eine separate 5V-Spannugsversorgung. 
Passende Platinen können Sei bei uns erwerben.

Anschluss der Servos:
* Stromversorgungsplatine auf die Buchsenleiste stecken.
  Dabei auf die richtige Polung achten.
* Servos auf die Pfostenleisten stecken.
  Auch hier muss auf die Polung geachtet werden.

Die Zuordnung der Servos zu Schaltern in der App kann frei konfiguriert werden (Cfg.xml).

## Montagehalter

Die CTC-Lokmodul-G und -G2 wird immer mit einem Montagehalter geliefert.
Das Foto zeigt das CTC-Lokmodul-G:

![Lokmodul-G]({% link assets/images/module/Loko-G-2-mit-Halter.jpg %})

Der Abstand der Schraublöcher beträgt 49 mm, wie beim Bleigewicht der LGB-Köf (D10).