---
layout: datasheet
title:  "CTC-Weichenmodul-G"
tags: Weichenmodul Weiche Signal
printpage: CTC-Weichenmodul-G-Print
---

| Feature             | Beschreibung                                                                       |
|:--------------------|:-----------------------------------------------------------------------------------|
| Prozessor           | Espressif ESP-32                                                                   |
| Maße                | 53 x 31 mm (inkl. Schraubklemmen)                                                  |
| Eingangsspannung    | ca. 9V bis 24V DC (geglätteter Gleichstrom), Digitalstrom, AC mit Stützkondensator |
| Leistungsausgänge   | 4x 2 A Halbbrücke (Version G1: 1 A)                                                |
| Servos              | 2x (nur Version G2, benötigen externe Stromversorgung)                             |
| Strom Summe max.    | 3 A (Version G1: 2 A)                                                              |
| Sensoreingänge      | 4x Digital per Optokoppler mit 2 kOhm (vor 2023 2x)                                |
| Schnittstellen      | 2 CTC IR-Balise (je max. 12mA), I2C Bus                                            |
| Strom online passiv | TODO                                                                               |
| Strom booten        | TODO zzgl. Schaltstrom für Initialisierung der Ausgänge                            |

[Jetzt kaufen]({{site.baseurl}}/shop.html#!/CTC-Weichenmodul-G2-indoor/p/725797325/category=176888458){: .button}

<br/>

Das CTC-Weichenmodul-G erhalten Sie in drei Ausführungen:
* Mit Schraubklemmen und Halter zur Montage unter bzw. auf der Modellbahnplatte (auf Anfrage).
* Mit Schraubklemmen und Gehäuse optimiert für LGB-Weichen und PIKO-G-Weichen.
* Als Outdoor-Variante mit im Gehäuse vergossener Elektronik.

In der Übersicht "[Schalten mit CTC]({% link de/docu/CTC-Schaltmodule.md %})" finden Sie weitere Module und Links zu Einbauanleitungen.

## Version G2 mit Schraubklemmen (ab 2023)

![Weichenplatine-G2]({% link assets/images/module/Weiche-G2-pins.jpg %})

## Alte Version (G1) mit Schraubklemmen (bis 2022)

![Weichenplatine-G]({% link assets/images/module/Weiche-G-3-Pins.jpg %})

### Anschluss

**Hinweise:**
* Bei der Wahl der Eingangsspannung bitte unbedingt daran denken, dass am CTC-Modul angeschlossene Motoren, Lampen, ... auch diese Spannung vertragen müssen!
                               
Standardkonfiguration
* Die Kontakte Gleis-A und Gleis-B mit dem Gleis bzw. der Spannungsversorgung verbinden.
  Dabei ist es gleichgültig, ob es sich um eine Digitalzentrale oder ein Netzteil handelt, sofern die maximale Spannung von 24 V nicht überschritten wird.
  Da am Eingang ein Gleichrichter hängt, spielt es auch keine Rolle, welchen Pol Sie mit Gleis-A und Gleis-B verbinden.
* Die Schaltanschlüsse des Weichenantriebs mit HB-1 und HB-2 (bzw HB-3 und HB-4) verbinden.
* Die CTC-IR-Balisen mit IR1-GND (Minuspol, grün) und IR1-VCC (Pluspol, rot) bzw. IR2-GND und IR2-VCC verbinden.
  Siehe dazu auch das Datenblatt des [CTC-IR-Senders]({% link _data_sheet/CTC-IR-Sender.md %}).
  Bitte auf keinen Fall den Widerstand (typisch 1 kOhm, min. 330 Ohm) in der IR-Balise vergessen, sonst ist das CTC-Weichenmodul-G kaputt.
* Sensoren (z.B. Schaltgleise) mit den 4 Sensoreingängen verbinden.
  Ab der Version 2023 (G2) muss nicht mehr auf die Polarität geachtet werden.
  Der Strom wird über einen 2 kOhm Widerstand begrenzt, sodass bis 24 V keine weitere Strombegrenzung nötig ist. 

Die Zuordnung der Pins zum Schalten kann in der App konfiguriert (Cfg.xml) werden, sowie die Dauer des Schaltimpulses.

### Weitere Anschlüsse

* Der I2C-Bus (I2C-SCL und I2C-SDA) und die Pins IO4 und IO33 werden von der aktuellen Firmware nicht unterstützt.
  Bei der Outdoor-Variante sind diese Pins aufgrund des Vergusses auch nicht zugänglich.  
    
**HINWEIS: Für Versorgung mit AC ist der (in den Bildern blaue) Stützkondensator zwingend erforderlich.**

### Mit Halter

Hier sehen Sie die Weiche-G2 mit Halter:

![Weichenmodul-G2 mit Halter]({% link assets/images/module/Weiche-G2-Halter.jpg %})
              
### Universal-Gehäuse

Hier sehen Sie die Weiche-G2 in unserem Universal-Gehäuse:

![Weichenplatine-G im Gehäuse]({% link assets/images/module/Weiche-G-im-Gehaeuse.jpg %}) 
            
Es kann entweder völlig separat montiert werden (ohne Bild) oder seitlich an einen LGB- oder Piko-Antrieb.
Das Bild zeigt noch die etwas kleinere Version G1: 

![Weichenplatine-G mit Antrieb]({% link assets/images/module/Weiche-G-Gehaeuse-an-Antrieb.jpg %})

Oder auf die gegenüber dem Weichenantrieb an die Weiche.
Das Bild zeigt noch die etwas kleinere Version G1:

![Weichenplatine-G an Weiche montriert]({% link assets/images/module/Weiche-G-Gehaeuse-an-Weiche.jpg %})

## Outdoor-Variante

Für den Betrieb im Garten (Outdoor) haben wir uns nach erfolglosen Experimenten mit Sprühlack dazu entschlossen, die Elektronik komplett vergießen zu lassen.
Das ist dann zwar etwas teurer, funktioniert aber sogar dann noch wenn die Bahn unter Wasser steht.

![Weichenplatine-G Outdoor]({% link assets/images/module/Weiche-G-Outdoor.jpg %})
                 
Verschlossen sieht das dann so aus:

![Weichenplatine-G Outdoor]({% link assets/images/module/Weiche-G-Outdoor-zu.jpg %})

[Jetzt kaufen]({{site.baseurl}}/shop.html#!/CTC-Weichenmodul-G2-outdoor/p/725810507/category=176888458){: .button}

So haben wir getestet:

**Nässe-Test**

Das CTC-Weichenmodul-G wurde mitsamt des LGB-Antriebs im angeschlossenen Zustand (Strom eingeschaltet) untergetaucht.
Dann wurde beides wieder aus dem Wasserbad genommen und zum Frost-Test übergegangen.

**Frost-Test**

Das (noch tropfnasse) CTC-Weichenmodul-G wurde mitsamt des LGB-Antriebs mehrere Tage im Tiefkühlschrank gelagert und danach sofort wieder in Betrieb genommen.
Das Weichenmodul funktionierte auf Anhieb, der LGB-Antrieb musste erst mechanisch befreit werden - er war eingefroren.

## Version G1 ohne Schraubklemmen

**Lieferbar nur auf gesonderte Anfrage.**

![Weichenplatine-G für LGB]({% link assets/images/module/Weiche-G-3-ohne.jpg %})

### Einbau Version G1 in LGB-Weichen
                          
**Achtung, diese Einbau-Variante erhöht den LGB-Antrieb um ca. 6 mm.   
Lieferbar nur auf gesonderte Anfrage.**

Leider gibt es zumindest teilweise Loks (u.a. die LGB Zahnradlok HGe 2/2), die dann am Weichenantrieb hängen bleiben. 

![Weichenplatine-G für LGB]({% link assets/images/module/Weiche-G-2-LGB.jpg %})

#### Anschluss

* Die kürzeren Kabel (HB-1 und HB-2, im Bild links) mit den Schraubklemmen des LGB-Weichenantriebs verbinden.
* Die längeren Kabel (Gleis-A und Gleis-B, im Bild rechts) mit dem Gleis verbinden.
* Falls eine Weichenlaterne vorhanden ist, wird deren Minuspol mit GND und der Pluspol mit HB-3 verbunden. 

#### Weitere Anschlüsse

Alle weiteren Anschlüsse bleiben bei dieser Variante ungenutzt.

### Einbau Version G1 in Piko G Weichen

**Achtung, diese Einbau-Variante erhöht den PIKO-Antrieb um ca. 6 mm.  
Lieferbar nur auf gesonderte Anfrage.**

Leider gibt es zumindest teilweise Loks, die dann am Weichenantrieb hängen bleiben.

![Weichenplatine-G für Piko]({% link assets/images/module/Weiche-G-2-Piko.jpg %})

#### Anschluss

* Die kürzeren Kabel (HB-1 und HB-2, im Bild unten, grün und blau) mit den Schraubklemmen des Piko-Weichenantriebs verbinden.
* Die längeren Kabel (Gleis-A und Gleis-B, im Bild oben rechts, gelb und braun) mit dem Gleis verbinden.
* Falls eine Weichenlaterne vorhanden ist, wird deren Minuspol mit GND (im Bild obne links, gelb) und der Pluspol mit HB-3 (im Bild unten, schwarz) verbunden.

#### Weitere Anschlüsse

Alle weiteren Anschlüsse bleiben bei dieser Variante ungenutzt.