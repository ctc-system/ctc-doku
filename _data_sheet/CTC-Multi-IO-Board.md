---
layout: datasheet
title:  "CTC-Multi-I/O-Board"
tags:   IO-Board Weiche Signal
printpage: CTC-Multi-IO-Board-Print 
---

| Feature             | Beschreibung                                                                       |
|:--------------------|:-----------------------------------------------------------------------------------|
| Optimiert für       | Montage unter der Anlage                                                           |
| Maße                | 74 x 46 mm                                                                         |
| Prozessor           | Espressif ESP-32                                                                   |
| Eingangsspannung    | ca. 9V bis 24V DC (geglätteter Gleichstrom), Digitalstrom, AC mit Stützkondensator |
| Leistungsausgänge   | 8x 1,2 A, kurschlussfest (Summe max. 1,5 A)                                        |
| Servos              | 2x (benötigen externe Stromversorgung)                                             |
| Strom Summe max.    | 2 A                                                                                |
| Sensoreingänge      | 4x Digital (Optokoppler mit 2 kOhm), z.B. für Schaltgleis                          |
| Schnittstellen      | 2 CTC-IR-Balisen (je max. 12 mA), NFC-Reader                                       |
| Strom online passiv | ca. 20 mA bei 16V                                                                  |
| Strom booten        | ca. 50 mA bei 16V zzgl. Schaltstrom für Initialisierung der Ausgänge               |

[Jetzt kaufen]({{site.baseurl}}/shop.html#!/CTC-Weichenmodul-Multi-IO-indoor/p/725521983/category=176888458){: .button}

<br/>

In der Übersicht "[Schalten mit CTC]({% link de/docu/CTC-Schaltmodule.md %})" finden Sie weitere Module und Links zu Einbauanleitungen.

## Pinbelegung

![IO-Board mit Pinbelegung]({% link assets/images/module/IO-Board-ESP32-6-Pins.jpg %})

## Steckverbinder

Wir liefern das CTC-Multi-I/O-Board standardmäßig mit Schraubklemmen:

![IO-Board mit Klemmen]({% link assets/images/module/IO-Board-ESP32-6-Klemmen.jpg %})

Alternativ können Sie es ohne Anschlussklemmen bestellen und dann nach ihren Wünschen mit Steckern im 2,54 mm Raster bestücken.
Hier ein paar Beispiele (im Bild von links nach rechts):
* [Federkraftklemme RIA CONNECT](https://www.reichelt.de/federkraftklemme-4-pol-0-08-0-5-mm-rm-2-5-ast-021-04-p72168.html)
* [Schraubklemme PHOENIX-CONTACT](https://www.reichelt.de/leiterplattenklemme-2-polig-rm-2-54-phc-1725656-p60874.html) - unsere Standardbestückung
* [Buchsenleiste senkrecht](https://www.reichelt.de/rnd-buchsenleiste-4-pol-rm-2-54-mm-rnd-205-00644-p208870.html):
  Denken Sie daran, dass eine Steckverbindung per Buchsenleiste, die senkrecht nach unten hängt, sich leicht lösen kann.
* Buchsenleiste gewinkelt
 
![Anschlussklemmen]({% link assets/images/module/Anschlussklemmen.jpg %})

Aufgrund der Kurzschlussgefahr durch die offenen stehenden Pins raten wir von Stiftleisten eher ab.
  
## Anschluss

**Hinweise:**
* Bei der Wahl der Eingangsspannung bitte unbedingt daran denken, dass am CTC-Modul angeschlossene Motoren, Lampen, ... auch diese Spannung vertragen müssen!

Standardkonfiguration
* Die Kontakte Gleis-A und Gleis-B mit dem Gleis bzw. der Spannungsversorgung verbinden.
  Dabei ist es gleichgültig, ob es sich um eine Digitalzentrale oder ein Netzteil handelt, sofern die maximale Spannung von 24 V nicht überschritten wird.
  Da am Eingang ein Gleichrichter hängt, spielt es auch keine Rolle, welchen Pol Sie mit Gleis-A und Gleis-B verbinden.  
* Aktoren (Weichen, Signale, ...) mit den Schaltausgängen (SW-x) verbinden.
  Dabei wird der Minuspol mit dem jeweiligen Schaltausgang (SW-x) und der Pluspol mit VBB verbunden.
  Die mit VBB beschrifteten Ausgänge sind alle intern verbunden, d.h. es ist egal welchen Sie verwenden und Sie können auch mehrere Aktoren gemeinsam an einen der VBB-Kontakte anschließen.
* Die CTC-IR-Balisen mit IR1-GND (Minuspol, grün) und IR1-VCC (Pluspol, rot) bzw. IR2-GND und IR2-VCC verbinden.
  Siehe dazu auch das Datenblatt der [CTC-IR-Balise]({% link _data_sheet/CTC-IR-Sender.md %}).
  Bitte auf keinen Fall den Widerstand (typisch 1 kOhm, min. 330 Ohm) in der IR-Balise vergessen, sonst ist das Multi-I/O-Board kaputt. 
* Sensoren (z.B. Schaltgleise) mit den 4 Sensoreingängen verbinden.
  Hier ist lediglich auf die Polarität (GND = Minuspol) und eine Spannung von max. 24 V zu achten.
  Ein 2 kOhm Widerstand und ein Optokoppler auf dem Multi-I/O-Board sorgen dafür, dass die Elektronik nicht beschädigt werden kann. 
* Waggon-Reader (NFC-Reader) anschließen: Der NFC-Reader wird über VCC (3,3V) und GND mit Strom versorgt. 
  Für die Datenübertragung werden NFC-TX und NFC-RX verwendet.   
  **Hinweis:** Bei Verwendung des NFC-Readers kann der zweite IR-Empfänger nicht genutzt werden.  

Die Zuordnung der Pins zu Schaltern in der App kann konfiguriert werden (Cfg.xml).

Für die zwei (optionalen) Servos:
* Die Versorgungsspannung für die Servos entweder über die Klemmen 5V/GND anschließen oder eine Huckepack-Platine auflöten.
* Die Servos-Anschlüsse mit den Servo-Ausgängen verbinden (Achtung: Pinbelegung der Stecker ist nicht standardisiert!).

**HINWEIS: Für Versorgung mit AC ist der (im ersten Bild blaue) Stützkondensator zwingend erforderlich.**

## Outdoor-Variante

Für den Betrieb im Garten (Outdoor) haben wir uns nach erfolglosen Experimenten mit Sprühlack dazu entschlossen, die Elektronik komplett vergießen zu lassen.
Das ist dann zwar etwas teurer, funktioniert aber sogar dann noch wenn die Bahn unter Wasser steht.

![IO-Board Outdoor]({% link assets/images/module/IO-Board-Outdoor.jpg %})

![IO-Board Outdoor verschlossen]({% link assets/images/module/IO-Board-Outdoor-zu.jpg %})

[Outdoor-Variante kaufen]({{site.baseurl}}/shop.html#!/CTC-Weichenmodul-Multi-IO-outdoor/p/725531354/category=176888458){: .button}
