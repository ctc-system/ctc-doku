---
layout: datasheet
title:  "CTC-Lokmodul-PluX22"
tags: Lokmodul Lok 21mtc
printpage: CTC-Lokmodul-PluX22-Print
---
Das CTC-Lokmodul-PluX22 ist für Loks mit der [Standard-Buchse PluX22](http://normen.railcommunity.de/RCN-122.pdf) gedacht.
Aufgrund des genormten Steckers kann dieses Lokmodul ohne Lötarbeiten eingebaut werden.
Für ältere Loks gibt es am Markt diverse Adapterplatinen mit größeren Lötpads, Kabeln oder auch für NEM-652 (8-Pol), NEM-651 (6-pol).

| Eigenschaft         | Beschreibung                                                                  |
|---------------------|-------------------------------------------------------------------------------|
| Optimiert für       | Loks mit PluX22, per Adapter auch NEM-651, NEM-652 oder zum Anlöten           |
| Maße                | 30 x 16 mm                                                                    |
| Prozessor           | Espressif ESP32                                                               |
| Eingangsspannung    | ca. 9 V bis 24 V DC oder Digitalstrom oder AC (mit Stützkondensator)          |
| Motorausgang        | 1x Gleichstrom-Motor: Dauerstrom 1,0 A, Peak 3,5 A                            |
| Schaltausgänge      | 8x 500 mA LowSide (F0L, F0R, AUX1 bis AUX6) <BR> AUX7 ist nicht angeschlossen |
| Strom Summe max.    | 2 A                                                                           |
| Schnittstellen      | CTC-IR-Empfänger, Zugbus (z.B. als I2C), 2x Servo, 1x Input                   |
| Strom online passiv | TBD                                                                           |
| Strom booten        | 50 mA bei 15 V                                                                |

[Jetzt kaufen]({{site.baseurl}}/shop.html#!/CTC-Lokmodul-PluX22/p/725292894/category=176866649){: .button}

<br/>

In der Übersicht "[CTC-Lokmodule]({% link de/docu/CTC-Lokmodule.md %})" finden Sie weitere Module und Links zu Einbauanleitungen.

## Pinbelegung
              
Details zu Pinbelegung und Maßen finden sich in der [RailCommunity Norm RCN-122](https://normen.railcommunity.de/RCN-122.pdf).

![Lokmodul PluX22]({% link assets/images/module/Loko-PluX22-Pins.jpg %})
               
## Anschluss

**Hinweise:**
* Bei der Wahl der Eingangsspannung bitte unbedingt daran denken, dass am CTC-Modul angeschlossene Motoren, Lampen, ... auch diese Spannung vertragen müssen!
* Der Anschluss AUX-7 ist nicht belegt.
* Der Zugbus wird von der aktuellen Firmware nicht unterstützt.

Zum Anschließen
* das CTC-Modul auf die 22-poligen PluX22 Buchse der Lokomotive stecken.
* den optionalen CTC IR-Empfängers auf die 10-poligen Buchse am CTC-Modul stecken (rote Markierung zeigt zur Platinenmitte)

Die Zuordnung der Licht- und AUX-Anschlüsse zu Schaltern in der App, kann frei konfiguriert werden (Cfg.xml).

## Weitere Anschlüsse

* An NFC-TX, NFC-RX, VCC und GND kann ein NFC-Reader angeschlossen werden, z.B. für kleine Spur-G Loks.
* Unter ExtIn-1 und ExtIn-2 stehen zwei digitale Eingänge zur Verfügung
  
## IR-Empfänger

Zum IR-Empfänger liefern wir Ihnen ein Kabel mit passendem 10-poligen Stecker.
Eingebaut in einer Piko-Lok BR 147 (#51583-2) sieht das dann so aus:

![Lokmodul PluX22 in Lok]({% link assets/images/module/Loko-PluX22-Einbau.jpg %})

**Hinweis:** Da der Stützkondensator in der Piko-Lok nicht verdrahtet war, haben wir in direkt an unser CTC-Lokmodul-PluX22 angelötet. 
