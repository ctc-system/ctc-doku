---
layout: datasheet
title:  "CTC-Lokmodul-21mtc"
tags: Lokmodul Lok 21mtc
printpage: CTC-Lokmodul-21mtc-Print
---
Das CTC-Lokmodul-21mtc ist nur für Loks mit Märklins [21mtc Schnittstelle](https://normen.railcommunity.de/RCN-121.pdf) sinnvoll.
Aufgrund des genormten Steckers kann dieses Lokmodul ohne Lötarbeiten eingebaut werden.
Für ältere Loks gibt es am Markt diverse Adapterplatinen mit größeren Lötpads oder Kabeln. 

| Eigenschaft         | Beschreibung                                                              |
|---------------------|---------------------------------------------------------------------------|
| Optimiert für       | Loks mit Märklin 21mtc Schnittstelle                                      |
| Maße                | 30 x 15,5 mm                                                              |
| Prozessor           | Espressif ESP32                                                           |
| Eingangsspannung    | ca. 9 V bis 24 V DC oder Digitalstrom oder AC (mit Stützkondensator)      |
| Motorausgang        | 1x Gleichstrom-Motor: Dauerstrom 1,0 A, Peak 2,8 A                        |
| Schaltausgänge      | 6x 500 mA LowSide (F0L, F0R, AUX1 bis AUX 4) <BR> 2x Digital (AUX5, AUX6) |
| Strom Summe max.    | 2 A                                                                       |
| Schnittstellen      | CTC-IR-Empfänger, Zugbus (z.B. als I2C), 2x Servo, 2x Input               |
| Strom online passiv | TBD                                                                       |
| Strom booten        | 50 mA bei 15 V                                                            |

[Jetzt kaufen]({{site.baseurl}}/shop.html#!/CTC-Lokmodul-21mtc/p/725533783/category=176866649){: .button}

<br/>

In der Übersicht "[CTC-Lokmodule]({% link de/docu/CTC-Lokmodule.md %})" finden Sie weitere Module und Links zu Einbauanleitungen.

## Pinbelegung

Details zu Pinbelegung und Maßen finden sich in der [RailCommunity Norm RCN-121](https://normen.railcommunity.de/RCN-121.pdf).

Das Bild zeigt noch die erste Version:
![Lokmodul 21mtc]({% link assets/images/module/Loko-21mtc-Pins.jpg %})

## Anschluss

**Hinweise:**
* Bei der Wahl der Eingangsspannung bitte unbedingt daran denken, dass am CTC-Modul angeschlossene Motoren, Lampen, ... auch diese Spannung vertragen müssen!
* Der Zugbus wird von der aktuellen Firmware nicht unterstützt.
* Beim Anlöten von Kabeln für den Pufferkondensator darauf achten, dass das Stecken der 21mtc-Buchse nicht behindert wird.

Zum Anschließen
* das CTC-Modul auf den 22-poligen 21mtc Stecker stecken.
* den optionalen CTC IR-Empfängers auf die 10-poligen Buchse am CTC-Modul stecken (rote Markierung zeigt zur Platinenmitte).

Die Zuordnung der Licht- und AUX-Anschlüsse zu Schaltern in der App kann frei konfiguriert werden (Cfg.xml).

## Weitere Anschlüsse

* Unter ExtOut-1 und ExtOut-2 stehen zwei weitere Schaltausgänge mit max. 500 mA zur Verfügung.
* Unter Input-1 und Input-2 stehen zwei digitale Eingänge zur Verfügung
  
## IR-Empfänger

Zum IR-Empfänger liefern wir Ihnen ein Kabel mit passendem 10-poligen Stecker.
Eingebaut in einer Märklin-Lok BR 247 (#36292) sieht das dann so aus:

![Lokmodul 21mtc in Lok]({% link assets/images/module/Loko-21mtc-Einbau.jpg %})

