---
layout: datasheet
title:  "CTC-Lokmodul-H0d"
tags: Lokmodul Lok
printpage: CTC-Lokmodul-H0d-Print
---

**Das CTC-Lokmodul-H0d wird nicht mehr weiter angeboten.**
Als Ersatz empfehlen wir das [CTC-Lokmodul-PluX22]({% link _data_sheet/CTC-Lokmodul-PluX22.md %}) mit unserem [CTC-Adapter für PluX22]({% link _data_sheet/Adapter-PluX22.md %}).

Das CTC-Lokmodul-H0d ist optimiert für den Einbau in ältere Lokomotiven die entweder schon Digital sind oder auch als Digital-Variante angeboten wurden.
Loks mit vielen Funktionen profitieren von den 10 Schaltausgängen und 2 Servo-Anschlüssen dieses Lokmoduls.
Mit einem zusätzlichen Motor-Treiber-Modul kann auch ein zweiter Motor angesteuert werden.   

| Eigenschaft         | Beschreibung                                                          |
|---------------------|-----------------------------------------------------------------------|
| Optimiert für       | Ältere Digital-Loks, Loks mit vielen Funktionen, Loks mit 2 Motoren   |
| Maße                | 35 x 20 mm                                                            |
| Prozessor           | Espressif ESP32                                                       |
| Eingangsspannung    | ca. 9 V bis 24 V DC, Digitalstrom oder AC (mit Stützkondensator)      |
| Motorausgang        | 1x Gleichstrom-Motor: Dauerstrom 1,0 A, Peak 2,8 A <br>1x Anschluss für zusätzliches Motor-Treiber-Modul |
| Leistungsausgänge   | 8x 0,5 A LowSide<br> 2x 1,0 A High-/Lowside                                  |
| Servos              | 2x (benötigen externe Stromversorgung)                                |
| Strom Summe max.    | 2 A                                                                   |
| Schnittstellen      | CTC-IR-Empfänger, I2C                                                 |
| Strom online passiv | TBD                                                                   |
| Strom booten        | TBD                                                                   |

<br/>

**Hinweis:** In der Übersicht "[CTC-Lokmodule]({% link de/docu/CTC-Lokmodule.md %})" finden Sie weitere Module und Links zu Einbauanleitungen.

## Pinbelegung

![Lokmodul]({% link assets/images/module/Loko-Ho-Ma-ESP32-2.jpg %})

## Anschluss

Standardkonfiguration
* Die Kontakte Gleis-A und Gleis-B mit dem Gleis bzw. der Spannungsversorgung verbinden.
  Alternativ kann Gleis-A auch über eine Schraube (Bohrung in der Platine) angeschlossen werden.
  Dabei ist es gleichgültig, ob sie das Gleis mit einem Analog-Trafo, einer Digitalzentrale oder einem Netzteil versorgen, sofern die maximale Spannung von 24 V nicht überschritten wird.
  Da am Eingang ein Gleichrichter hängt, spielt es auch keine Rolle, welchen Pol Sie mit Gleis-A und Gleis-B verbinden.
* Den Stützkondensator (Elektrolyt, min. 400 uF, empfohlen 1000 uF) an VBB (Pluspol) und GND anschließen.
* Den Motor mit Motor + und Motor - verbinden (für Anschluss des Elektromagneten von Allstrommotoren siehe separate Anleitung).
* Minuspole von Lichtern vorne mit SW-1 und hinten mit SW-2 verbinden.
  Den Pluspol jeweils mit einem beliebigen VBB-Anschluss verbinden.
* Optional weitere SW-Anschlüsse z.B. mit Minuspol der elektrischen Kupplung, des Rauchsatzes oder weiterer Beleuchtung verbinden.
  Den Pluspol jeweils mit einem beliebigen VBB-Anschluss verbinden.
* Anschluss des CTC IR-Empfängers an den Programmierstecker (RX, VCC, GND)

Die Zuordnung der Pins zu Schaltern in der App kann frei konfiguriert werden (Cfg.xml).

## Weitere Anschlüsse

Der I2C-Bus, die Servos, der zweite Motor und die Reserved Pins werden von der aktuellen Firmware nicht unterstützt.
Bitte kontaktieren Sie und bei Bedarf.
