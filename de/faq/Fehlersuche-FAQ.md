---
layout: page
title:  "Fehlersuche FAQ"
---
## Die CTC-App startet, zeigt aber keine Module

1. Prüfen Sie, ob Sie mit dem richtigen LAN/WLAN verbunden sind.
2. Prüfen Sie, ob Ihr LAN mit einem anderen Netz verbunden ist als Ihr WLAN.
3. Prüfen Sie, ob Ihre Firewall UDP Kommunikation auf den Ports 9750 und 9751 erlaubt.

## Wie prüfe ich ob Datenpakete fließen?

Das geht am besten mit dem Programm Wireshark.

Dort verwenden Sie als Filterregel  
 `udp.port==9751`

## Wie prüfe ich, ob die App die Ports registrieren konnte?

**Linux**

Kommando:  
`lsof -Pnl +M -i`  
oder  
`netstat -tlnup`

Listet alle registrierten Ports.
Hier sollte bei "UDP *:9750" und "UDP *:9751" jeweils "java" stehen.
Beide Einträge sollten verschwinden, wenn Sie die CTC-App beenden.

[Anleitung zu lsof siehe hier](https://www.cyberciti.biz/faq/find-out-which-service-listening-specific-port/)

## Wie prüfe ich, ob die Firewall Ports blockiert?

**Ubuntu Linux**

Kommando:  
`sudo ufw status verbose`

[Anleitung siehe hier](https://www.cyberciti.biz/faq/how-to-open-firewall-port-on-ubuntu-linux-12-04-14-04-lts/)

## Device-Module fehlen in der App

**In der App werden weder Loks noch Weichen angezeigt**

* Prüfen Sie ob Ihr PC/Tablet/Smartphone mit dem richtigen Netz (WLAN) verbunden ist.
* Stellen Sie sicher, dass Sie nur mit einem Netz verbunden sind:
    * Wenn Sie per Kabel mit einem anderen Netz verbunden sind als per WLAN funktioniert die Kommunikation mit den Device-Modulen nicht.
    * Wenn Sie auf SmartPhone/Tablet "Mobile Daten" aktiv haben, funktioniert die Kommunikation mit den Device-Modulen nur teilweise.
* Öffnen Sie **Einstellungen/Konfigurator**. Dort werden alle Device-Module angezeigt, v. a. auch solche, bei denen ein Fehler auftrat.

**Eine einzelne Lok oder Weiche wird nicht angezeigt**

* Öffnen Sie **Einstellungen/Konfigurator**. Dort werden alle Device-Module angezeigt, v. a. auch solche, bei denen ein Fehler auftrat.
Taucht das Modul dort als "UnknownDevice" auf, so wird in dessen Edit-Dialog unter **Letztes Datenpaket** eine Fehlermeldung angezeigt.

## Allgemeine Kommunikationsstörungen

**Hohe Anzahl #msgMiss in Statistik-Anzeige**

* Prüfen Sie ob ein Dialog **Motor einstellen** geöffnet ist.
  In diesem Fall sendet die Lok ihre Motordaten unsynchronisiert.
* Verbinden Sie Ihren PC per Kabel mit dem Modellbahn-WLAN-Router.
  Schlechte Treiber können zu einem "schwerhörigen" PC führen.
* Prüfen Sie, wer sonst noch in Ihrem Modellbahn-WLAN aktiv ist.
  Vor allem, wenn Ihr Modellbahn-WLAN auch eine Verbindung zum Internet hat, können "geschwätzige" Apps auf PC/Tablet/SmartPhone die Kommunikation mit den Device-Modulen stören.
* Prüfen Sie, welche WLANs es sonst noch in Ihrer Umgebung gibt und auf welchen Kanälen diese senden.
  Das geht z. B. über die Oberfläche der FritzBox oder spezielle Apps für Tablet/SmartPhone.

## Nach Änderungen an der Konfiguration startet das CTC-Modul nicht mehr

Solange Sie die Änderungen mit der CTC-App gemacht haben sollte das eigentlich nicht passieren. 
Bitte teilen Sie es uns in jedem Fall mit, was Sie gemacht haben, damit wir den Fehler nachvollziehen und beheben können.

Um Ihr CTC-Modul wiederzubeleben, ersetzen Sie die Config (cfg.xml) auf dem CTC-Modul durch eine leere Config.
Dazu gehen Sie wie folgt vor:
1. Schalten Sie Ihr Modellbahn-WLAN aus.
2. Schließen Sie das CTC-Modul an Strom an - es müsste nun in den Konfigurationsmodus schalten und sein eigenes WLAN öffnen.
3. Verbinden Sie sich vom PC aus mit dem WLAN des CTC-Moduls.
4. Öffnen Sie den Internet-Browser und gehen Sie auf http://192.168.4.1
5. Klicken Sie auf "Upload file" und wählen Sie die Datei "cfg.xml" aus dem Ordner "config" Ihrer CTC-App. 

## iPhone/iPad zeigt keine CTC-Module

Obwohl das iPhone/iPad mit dem richtigen WLAN verbunden ist, zeigt es keine CTC-Module an.
* Prüfen Sie unter den iPhone/iPad Einstellungen im Bereich "Datenschutz / Lokales Netzwerk", ob dort die CTC-App gelistet und aktiviert ist. 
