---
layout: page
title:  "DCC FAQ"
---
## Im ESU Profi-Prüfstand funktioniert der Motor nicht
                                                                          
Ich habe den Gleiseingang des ESU Profi-Prüfstands 53900 an den Motorausgang eines CTC-Lokmoduls angeschlossen, das für DCC konfiguriert ist.
Wenn ich nun meinen Piko SmartDecoder 4.1 in den ESU Profi Prüfstand einstecke kann ich über die CTC-App das Licht ein- und ausschalten, aber der Motor funktioneirt nicht.

Das scheint eine Inkompatibilität zwischen ESU Profi-Prüfstands 53900 und einigen Dekodern zu sein, [siehe Artikel im Stummi-Forum](https://www.stummiforum.de/t192265f5-Piko-Smart-Sound-Decoder-und-mSD-funktioniert-nicht-im-ESU-Profipr-fstand.html).
