---
layout: page
title:  "Grundlagen FAQ"
---
**Was ist der Unterschied zwischen CTC und PI-Rail?**

PI-Rail ist die unter OpenSource veröffentlichte Basis der CTC-Produkte.
Auf den [PI-Rail Projektseiten auf GitLab](https://gitlab.com/pi-rail) finden Sie die vollständigen Quelltexte der CTC-App und der Firmware der CTC-Module. 

[CTC (Clever Train Control)](https://www.ctc-system.de) liefert Ihnen vor allem fertig nutzbare Module für Loks, Weichen, Signale, etc., sowie Beratung und die Dienstleistung der Umrüstung auf CTC.  

---

**Warum braucht CTC Gleichstrom? Märklin läuft doch mit Wechselstrom.**

CTC funktioniert mit Gleich-, Wechsel- und Digitalstrom.

Die Ansteuerung des Motors erfolgt mit gepulstem Gleichstrom (PWM).
Dies stellt kein Problem dar, da Märklin-Motoren schon immer für Gleichstrom geeignet waren.

Bei Wechselstrom ist darauf zu achten, dass die Stützkondensatoren auf den Modulen groß genug sind, um die für den Prozessor nötige Gleichspannung zu erzeugen.
Die aktuelleen [CTC-Weichenmodule]({% link _data_sheet/CTC-Weichenmodul-H0.md %}) und [CTC-Multi-I/O-Boards]({% link _data_sheet/CTC-Multi-IO-Board.md %}) verfügen über einen ausreichend dimensionierten Stützkondensator.

In die Lokomotiven sollte sowieso ein größerer Stützkondensator eingebaut werden.

---

**Kann die Anlage auch mit einer Steuerungssoftware über den PC betrieben werden?**

Derzeit gibt es keine Unterstützung seitens der Hersteller von Steuerungssoftware.
Erste Gespräche wurden aber bereits geführt.

---

**Welche Steuerungssoftware würden Sie empfehlen?**

Das können wir beantworten, sobald es welche mit einer Schnittstelle zu CTC gibt.

---

**Müssen bereits digitalisierte Lokomotiven, Kräne etc. umgebaut werden?**

Ja, wenn Sie einen gemeinsamen Betrieb über die CTC-App wünschen.

Sie können aber auf einer Anlage Loks, Weichen, ... über die bestehende Digital-Zentrale steuern und gleichzeitig auf CTC umgerüstete Loks und Weichen mit der CTC-App steuern.
Allerdings wissen dann beide Systeme nichts voneinander, d.h. insbesondere dass automatisierte Abläufe nur sehr eingeschränkt möglich sind.

---

**Benötigt man dann keine Zentrale mehr?**

Richtig - CTC kommt ganz ohne Zentrale aus. Sie benötigen nur eine Stromversorgung auf dem Gleis und einen [WLAN-Access-Point]({% link de/docu/WLAN-Access-Point.md %}).

---

**Welchen Zweck erfüllt das CTC-Multi-I/O-Board?**

Das [CTC-Multi-I/O-Board]({% link _data_sheet/CTC-Multi-IO-Board.md %}) ist vergleichbar mit einem [CTC-Weichenmodul]({% link _data_sheet/CTC-Weichenmodul-H0.md %}), hat aber 8 statt 4 Schaltausgänge und zusätzlich 4 Sensor-Eingänge sowie 2 Anschlüsse für Modellbau-Servos.

---

**Was ist im Zusammenhang mit den alten Metallgleisen und dem Kunstoffgleis und den Infrarotsendern zu beachten?**

Die Infrarotsender müssen zwischen den Schwellen angebracht werden. Bei Metall- und C-Gleisen müssen somit Löcher in die Schienen gebohrt werden.

Wir experimentieren gerade mit dünnen Elektronik-Folien.
Wenn das klappt können die Infrarot-Dioden auch auf die Schwellen gelegt werden.
Dann muss nicht mehr gebohrt werden.

---

**Sind Infrarotsender für eine Steuerung unverzichtbar?**

CTC unterstützt auch die klassischen Sensor-Konzepte wie Schaltgleise und Reed-Relais.
Dazu hat das [CTC-Multi-I/O-Board]({% link _data_sheet/CTC-Multi-IO-Board.md %}) vier Anschlüsse.

Sie verlieren dann aber auch die erweiterten Möglichkeiten der Infrarotsender:
* Automatisches Halten vor einem Signal
* Zuverlässige Positionsanzeige im Gleisbild
* Anzeige der Durchschnittsgeschwindigkeit der Lok in der App
* Automatisches Einmessen des Motorsensors der Lok

---

**Werden die Platinen der Elemente Weiche, Infrarot, IO Board etc. in Gehäusen geliefert?**

Die Platinen haben derzeit kein Gehäuse.
Für [CTC-Weichenmodule]({% link _data_sheet/CTC-Weichenmodul-H0.md %}) gibt es einen Halter zur einfachen Montage an der Platte, falls sie nicht ins C-Gleis eingebaut werden.

---

**Ist der Empfang des WLAN-Signals in den Metall-Loks von Märklin wirklich sicher?**

Wir haben bereits diverse Loks mit Metallgehäuse umgebaut.
Dabei waren wir selbst verblüfft, wie problemlos die Funkverbindung bei diesen Loks ist.
Hier einige konkrete Märklin-Beispiele:
 - [Alte V200 (Nr. 3021)](http://www.modellbau-wiki.de/wiki/Datei:V200006_1.jpg)
 - [Schweizer Re 4/4 II aus Digital-Starter-Set (Nr. 29859)](https://www.maerklin.de/de/produkte/details/article/29859)
 - [Vectron BR 247 - 30 Jahre MHI (Nr. 36292)](https://www.maerklin.de/de/produkte/details/article/36292)
 
---

**Können auch Lokomotiven mit Glockenanker oder Sinus-Antrieb umgerüstet werden?**

Unsere aktuellen Lok-Module können das nicht direkt.
Mit Hilfe eines separaten Treiber-Moduls sollte dies grundsätzlich aber kein Problem sein.
Ein passendes Lokmodul ist bereits in Planung - bitte kontaktieren Sie uns bei Bedarf.

---

**Ist das Protokoll für die Lokdecoder erhältlich, also welche Befehle steuern die Loks?**

Ja - die gesamte Software ist unter OpenSource unter dem Namen [PI-Rail auf GitLab](https://gitlab.com/pi-rail) veröffentlicht.
Sie können also eine beliebige Mischung aus von CTC erworbenen Modulen, CTC-Software und Eigenentwicklungen machen. 

Die Dokumentation dazu befindet sich erst im Aufbau - kontaktieren Sie uns, wenn Sie Fragen haben.
 
Wir freuen uns übrigens über jeden der an PI-Rail (der technischen Basis für CTC) mitarbeitet.

---

**Die CTC-App für Android kann ich irgendwie nicht im App-Store finden. Heißt die App so oder heißt sie anders?**

Die App ist noch nicht über den App-Store verfügbar.
Sie können sie derzeit aber als APK-Datei direkt von der [Projektseite auf GitLab](https://gitlab.com/pi-rail/pi-rail-android) herunterladen.

--

**Wie schliesst man Beleuchtung und Lichtsignale an CTC-Module an?**

Sie können Beleuchtung und Lichtsignale an die Schaltausgänge des CTC-Multi-I/O-Boards (oder auch des CTC-Weichenmoduls) anschließen: ein Ausgang je Licht.
Bei LEDs sollten passende Vorwiderstände der Beleuchtung bzw. dem Lichtsignal beiliegen.

Bei Lichtsignalen von Viessmann müssen Sie allerdings darauf achten, dass es keine mit "Multiplex-Anschluss" (Art.Nr. 47xx) sind.
Diese können nicht direkt an ein CTC-Modul angeschlossen werden, sondern nur an den "Multiplexer" von Viessmann.
Dessen Tasteneingänge können Sie dann aber mit Schaltausgängen von CTC-Modulen verbinden.

Die neuen Märklin Lichtsignale können mit Hilfe eines CTC-Lokmoduls und der DCC-Firmware per DCC angesteuert werden.