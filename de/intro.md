---
layout: page
title:  "Einführung in CTC"
tags:
---
![Banner]({% link assets/images/top-banner.jpg %})

## Einleitung

CTC erfindet die digitale Modellbahn von Grund auf neu.
Damit sind wir in der glücklichen Lage, dass wir nur wenig Rücksicht auf Kompatibilität nehmen müssen und die meisten "alten Zöpfe" abschneiden dürfen.
Was das konkret für Sie bedeutet, wenn Sie bereits eine Modellbahn besitzen, können sie in separaten, auf unterschiedliche Szenarien zugeschnittenen Artikeln auf dieser Web-Site nachlesen.

In diesem Artikel wollen wir Ihnen das grundlegende Konzept hinter CTC erläutern und aufzeigen, wie sie mit CTC starten können.

## Warum die digitale Modellbahn neu erfinden?

Die klassische digitale Modellbahn basiert auf einer zentralen Elektronik (Digitalzentrale), die mit den Lokomotiven über das Gleis kommuniziert.

Das der digitalen Modellbahn zugrundeliegende Konzept lässt nur eine sehr geringe Bandbreite zu.
Das bedeutet dass nur wenige Informationen übertragen werden können und Rückmeldungen gar nicht oder nur sehr eingeschränkt möglich sind.

Jeder Funke, der durch die Lok bei der Stromabnahme ausgelöst wird, kann die Datenübertragung stören.
Durch regelmäßiges Wiederholen der Nachrichten auf dem Gleis wird das Risiko dass eine Nachricht nicht ankommt minimiert, aber eben nicht ausgeschlossen.
Das heißt auch: Je schlechter der Zustand der Gleise ist, desto höher die Wahrscheinlichkeit, dass das Digitalsystem unzuverlässig arbeitet.

Ausgehend von einer Modellbahn im Garten reifte so in uns der Wunsch, die Kommunikation vom Gleis weg zu nehmen und die Idee für CTC war geboren.

## Entstehung des Konzepts

Als Alternative zur Kommunikation über das Gleis kam eigentlich nur eine Funk-Technologie in Frage.
Außerdem wollten wir unsere Modellbahn ohne große Aufwände vom Tablet oder SmartPhone aus steuern können.

Somit standen für eine direkte Verbindung zwischen App und Lok/Weiche/Signal nur noch Bluetooth und WiFi zur Verfügung.
Da Bluetooth für eine Vielzahl gleichzeitiger Verbindungen eher ungeeignet ist und dessen geringer Energiebedarf für uns unwichtig ist, fiel die Wahl auf WiFi.

Nun galt es nur noch passende WiFi-Module für Lokomotiven, Weichen, Signale, ... zu entwickeln und vor allem die Software dazu zu erfinden.

Alle Daten zu Lokomotiven, Weichen und Signalen sollten im jeweiligen Modul abgelegt werden.
Eine Lok sollte also nicht nur alle technischen Parameter (CVs bei der digitalen Modellbahn), sondern z.B. auch ihren Namen, ihr Bild oder eine ausführliche Beschreibung speichern können.
Aus diesem Wunsch heraus entstand ein Software-Konzept, das eine zentrale Komponente komplett überflüssig machte.
Wir hatten also nur noch das WiFi, die Module für Loks, Weichen und Signale und die App.

Den Kern der Software finden Sie inzwischen unter einer OpenSource-Lizenz veröffentlicht auf [PI-Rail.org](https://pi-rail.org).
Damit ist insbesondere das Kommunikationsprotokoll offengelegt und es steht Ihnen die Tür für eigene Erweiterungen offen.

## Was brauche ich denn nun?

Um mit CTC starten zu können brauchen sie nur:
* Einen [WLAN-Access-Point]({% link de/docu/WLAN-Access-Point.md %}). Für den Anfang kann das auch Ihr heimisches WLAN sein.
* Ein [CTC-Lokmodul]({% link de/docu/CTC-Lokmodule.md %}) für jede zu steuernde Lokomotive.
  Das entspricht dem Lok-Dekoder bei der digitalen Modellbahn.
* [CTC-Module]({% link de/docu/CTC-Schaltmodule.md %}) für die zu steuernden Weichen und Signale.
  Das entspricht dem Weichen-Dekoder bei der digitalen Modellbahn.
  Wie von der digitalen Modellbahn gewohnt, können an ein Modul auch mehrere Weichen und Signale angeschlossen werden.
* Die [CTC-App]({% link de/download.md %}) für PC (Linux, Mac oder Windows), Tablet oder SmartPhone.
* Strom auf dem Gleis.
  Das darf Gleichstrom oder Wechselstrom sein oder von der Digital-Steuerung die Zentrale bzw. der Booster.
  Die Höhe der Spannung hängt davon ab, was Ihre Loks, Weichen und Signale vertragen.
  Die CTC-Module sind für bis zu 24V ausgelegt.

## Und wie starte ich am einfachsten?

Am einfachsten lassen Sie sich von uns ein [Starter-Set]({% link _data_sheet/CTC-Starter-Sets.md %}) kommen, um sich so erst mal mit der neuen Welt von CTC vertraut zu machen.
Über die angebotenen Starter-Sets hinaus stellen wir Ihnen gerne ein für Sie passendes Starter-Set zusammen.
Auf Wunsch übernehmen wir gerne auch den Einbau der CTC-Module in Ihre Loks und Weichen.

Darüber hinaus finden Sie hoffentlich unter den auf dieser Web-Site beschriebenen Szenarien eines, das zu Ihrer Situation passt.
Wenn nicht, dann schildern Sie uns Ihre Situation und wir ergänzen unsere Szenarien.

Die drei aus unserer Sicht grundlegendsten Szenarien sind:
* Neu einsteigen ins Modellbahn-Hobby
* [Umsteigen von einer analogen Modellbahn]({% link _posts/de/szenarien/2020-06-19-Umsteigen-von-Analog.md %})
* [Umsteigen von einer digitalen Modellbahn]({% link _posts/de/szenarien/2020-06-19-Umsteigen-von-Digital.md %})

Die beiden Artikel zum Umstieg gehen auch auf den (anfänglichen) Parallelbetrieb mit CTC ein.

Häufig gestellte Fragen finden Sie inklusive Antwort in unserem [FAQ Bereich]({% link de/faq.html %})