---
layout: page
title:  "Die Presse über CTC"
tags:
---
Inzwischen ist eine so stattliche Anzahl von Artikel rund um CTC erschienen, dass sich diese separate Seite dazu lohnt.

An dieser Stelle möchten wir auch unser herzliches Dankeschön an all die engagierten Redakteure aussprechen, ohne deren Arbeit wohl kaum jemand auf CTC aufmerksam geworden wäre.

## Januar 2025 - Digitale Modellbahn

In beiden Artikeln zur Marktübersicht von Apps für iOS (Apple) und Android in der [DiMo (Ausgabe 1-2025)](https://www.verlagshaus24.com/digitale_modellbahn_2025_01/) taucht auch die CTC-App auf.

## August 2024 - Eisenbahn Amateur

Im schweizer [Eisenbahn Amateur 08/24](https://eisenbahn-amateur.ch) berichtet Christian Heer über den Umbau seiner alten Märklin-Anlage auf CTC.

Sein Fazit beginnt mit den Worten: ***"Als Modellbahnsteuerung der nächsten Generation bietet CTC-System diverse Vorteile gegenüber herkömmlichen Digitalsteuerungen.
Dazu zählt sicher auch die geringe Investition beim Einstieg, da Zentrale und Handregler nicht notwendig sind.***
              
## Mai 2024 - Gartenbahnprofi

Im [Gartenbahnprofi (3/2024)](https://gartenbahnprofi.eshop.t-online.de/p/127-gartenbahn-profi-nr-3-2024) berichtet Hans-Jürgen Götz unter dem Titel **"Innovations-Schub"** über Neuigkeiten bzgl. CTC.

Er endet mit den Worten ***"CTC ist zwar einer der jüngeren und eher kleineren Anbieter im digitalen Modelleisenbahnsegment, aber auch einer der ersten, der so konsequent und erfolgreich die WLAN-Technologie durchgängig implementiert hat."***

## April 2024 - Digitale Modellbahn

In der [DiMo (Ausgabe 2-2024)](https://www.vgbahn.shop/licht-im-zug) berichtet Hans-Jürgen Götz unter dem Titel **"Clevere Neuheiten von CTC"** über SUSI, Waggon-Zähler und Z21-Schnittstelle.

Er endet mit den Worten ***"CTC hat mit den Neuerungen einen gewaltigen Schritt gemacht und bietet sich nun nicht nur als Stand-alone-Lösung, sondern auch als Ergänzung für eine beliebige Modellbahnsteuerung an."*** 

## Februar 2024 - Loki

Im Artikel "**SUSI, Z21, NFC und mehr ...**" in [Loki 2/2024](https://loki.ch/de/artikel/86loki0224ante/susi-z21-nfc-und-mehr) berichtet Hans-Jürgen Götz über die Neuerungen seit dem ersten Loki-Artikel über CTC vom Mai 2021.
       
## November 2022 - Gartenbahnprofi

Im zweiten Artikel im [Gartenbahnprofi (6/2022)](https://gartenbahnprofi.eshop.t-online.de/p/118-gartenbahn-profi-nr-6-2022) berichtet Hans-Jürgen Götz unter dem Titel **"Für das sichere Fahren"** über die mit CTC 4 erweiterte Zugsicherung und Automatisierung.

Er endet mit den Worten: ***"CTC hat also noch jede Menge Potenzial und bietet nach wie vor viele Alleinstellungsmerkmale, sei es die Möglichkeit, alles nur via WLAN zu steuern, Lokomotiven auch mit Akkus fahren zu können, ...".***

## Oktober 2022 - eisenbahn magazin

Im [eisenbahn magazin Oktober 2022](https://www.eisenbahnmagazin.de/aktuelles/eisenbahn-magazin-10/22-aufs-land-mit-dem-kurswagen) erklärt Heiko Herholz unter dem Titel **"Wandel von Analog nach WLAN"** CTC aus dem Blickwinkel eines Umsteigers von einer analogen Modellbahn. 

Er endet mit den Worten: ***"Im Fazit betrachtet bietet das CTC-System völlig neue Möglichkeiten, die mit bisherigen Steuerungssystemen kaum vergleichbar sind.
Das macht CTC interessant für all jene, die bisher einen Umstieg auf eine Digitalsteuerung scheuten."***

## Frühjahr 2022 - Modellbahn-Kurier

Im [Modellbahn-Kurier 55 – Digital 2022](https://www.eisenbahn-kurier.de/neue-zeitschriften/25-modellbahn-kurier/6203-modellbahn-kurier-55-digital-2022) schreibt Hands-Jürgen Götz unter dem Titel "CTC – Clever Train Control Digital – ganz ohne Draht" über CTC.

Im Fazit schreibt er: ***"...Vor allem geht alles ohne Verkabelung per Funk, sofort und ganz einfach.
Und wer Gefallen daran findet und ausbauen will, dem stehen kaum Grenzen im Weg. ..."***

## Januar 2022 - MIBA spezial

In der [MIBA spezial 133](https://www.vgbahn.shop/modellbahn-digital-2022-steuern-mit-dem-pc) mit dem Titel **"Modellbahn digital 2022 - Steuern mit dem PC"** berichtet Maik Möritz unter anderm über CTC.

Er endet mit den Worten: ***"Keine Frage: CTC geht auf jeden Fall einen interessanten Weg und bringt eine Menge Innovation mit. 
Ich nutze das System seit einigen Monaten zur Steuerung unserer heimischen Gartenbahn und bin damit sehr zufrieden.
Die einfache und unkomplizierte Bedienung mit meinem Smartphone, aber auch die praktischen Automatisierungsmöglichkeiten und die simple Stromversorgung der Gleise über ein zweiadriges Kabel möchte ich dabei nicht mehr missen."***

## November 2021 - Gartenbahnprofi

Mit dem Artikel **"Digital – ganz drahtlos"** in der [Gartenbahnprofi 6/2021](https://gartenbahnprofi.eshop.t-online.de/p/112-gartenbahn-profi-nr-6-2021) berichtet Hans-Jürgen Götz im Rahmen der Artikelserie "Funk im Garten" über CTC.

Der Untertitel des Artikels beginnt mit den Worten ***"Loks und Weichen einfach so mit dem Smartphone steuern zu können, mal ganz ohne Zentrale - das ist kein Wunschtraum mehr."***

## Juni 2021 - Digitale Modellbahn

Im inzwischen dritten Artikel in der [DiMo (Ausgabe 3-2021)](https://www.vgbahn.shop/digitale-modellbahn-03/21) berichtet Hans-Jürgen Götz unter dem Titel **"Clever im Garten"** über Neuigkeiten bzgl. CTC-System, v.a. unsere Module für die Gartenbahn.
                               
Das Fazit lautet: ***"Nachdem inzwischen selbst Märklin seine zukünftigen Funkhandregler auf WLAN umstellt, dürfte den letzten Zweiflern der Wind aus den Segeln genommen worden sein, was den Einsatz dieser Technologie auf der Modelleisenbahn angeht.
CTC hat hier bereits ein paar jahre Vorsprung und baut sein Portfolio beständig aus."***

## Mai 2021 - Loki

In der schweizer **Loki** erscheint der erste Artikel über CTC in **Ausgabe 5/2021** unter dem Titel "[Ohne Kabel, ohne Zentrale](https://loki.ch/de/artikel/38loki0521ante/ohne-kabel-ohne-zentrale)".

Im Fazit schreibt Heiko Herholz: ***"Das CleverTrainControl-System ist die Erfüllung vieler Träume: ein System bei dem zwei Kabel zum Gleis als Stromversorgung reichen."***

## September 2020 - Digitale Modellbahn

Mit dem Titel "[Viele Grüße von Lissy](https://shop.vgbahn.info/media/pdf/Blick%20in%20diese%20Ausgabe/652004.pdf)" setzt Hans-Jürgen Götz seinen Bericht vom Juni mit Fokus auf unseren Infrarot-Sensoren fort.
In der am Titel verlinkten Heftvorschau der DiMo 4-2020 ist er komplett abgedruckt.

Das Fazit beginnt mit den Worten: ***"Das System hat enorm viel Zukunftspotential. 
Durch den Einsatz von Standard-WLAN-Komponenten ist es relativ preiswert und weltweit ohne Probleme einsetzbar.***

## Juni 2020 - Digitale Modellbahn 

In der Zeitschrift **"Digitale Modellbahn" (DiMo) 3-2020** erscheint unter dem Titel "[Digital und Drahtlos starten](https://www.rail4you.ch/dokumente/DiMo_2020-03_CTC_i.pdf)" der erste Presse-Artikel über CTC.

**Hans-Jürgen Götz** schreibt im Fazit: ***"Technologisch rückt die WLAN-Anwendung die Modell-bahn in die Jetztzeit, so wie vor 40 Jahren die klassischen Digitalprotokolle die Modellbahn in die damalige elektronische Jetztzeit führten."***
