---
layout: page
title:  "CTC Downloads"
tags:
---
**Wichtiger Hinweis:** Wegen der besonderen Funktionsstruktur des CTC-Systems werden Sie nach Download und Installation einen beinahe leeren Bildschirm sehen.
Das "Leben" zieht erst ein, mit der Anmeldung bzw. dem Betrieb von CTC-Modulen (Decodern).
                
## CTC-App Downloads

### CTC-App für Desktop

**Mit [CTC-App Version 4]({% link _posts/de/szenarien/2022-05-30-CTC-Version-4.md %}) kommen unsere IR-Sender bzw. NFC-Tags so richtig in Fahrt!**

Die Installer für die CTC-App können Sie direkt über folgende Links herunterladen: 
* [Windows Installer v4.37 (105 MB)](https://pi-rail.com/CTC-App-4.37.msi)
* [Linux Debian Package v4.37 (89 MB)](https://pi-rail.com/ctc-app_4.37_amd64.deb)
* [Mac OS/X Intel dmg Package v4.37 (123 MB)](https://pi-rail.com/CTC-App-Intel-4.37.dmg)
* [Mac OS/X ARM dmg Package v4.37 (123 MB)](https://pi-rail.com/CTC-App-4.37_ARM.dmg)

**NEU**: Ab Version 4.22 wurde das Fensterlayout für den PC deutlich flexibler.
Über den Menüpunkt "Ansicht" können neue Fenster erzeugt werden.
Mit der Maus können die Inhalte an ihrer Titelzeile in andere Fenster hineingeschoben und herausgezogen werden. 

**Hinweise zu Installation:**
* Siehe dazu die [Installationsanleitung]({% link _app_doku/020-installation-der-app.md %}).

### CTC-App Android

Die CTC-App finden Sie ab sofort für 9,99 EUR im [Google Play-Store](https://play.google.com/store/apps/details?id=de.pidata.rail.android) oder weiterhin kostenlos [hier für manuelle Installation](https://pi-rail.com/CTC-Android-v4.37-release.apk).  

**Hinweise:**
* Die Konfiguration von CTC-Modulen ist derzeit unter Android nur teilweise verfügbar.
* Für die Gebühr im PlayStore haben wir uns entschieden, um unnötige Downloads und daraus resultierende Bewertungen gering zu halten.
* Aufgrund des Zertifizierungsprozesses des Play-Stores kommt die App dort immer etwas später als die Desktop-Version.

### CTC-App iOS (iPhone, iPad)

Die CTC-App für iOS ist ab sofort im [App-Store](https://apps.apple.com/de/app/ctc-app/id1576119270) für 9,99 EUR verfügbar.
Funktional ist die App derzeit nur zur Steuerung nutzbar.
Für die Konfiguration der CTC-Module muss noch auf den Mac oder PC zurückgegriffen werden.

**Hinweise:**
* Die Funktion zum Einbuchen von CTC-Modulen ins WLAN ist leider nicht auf iOS verfügbar, da Apple hierzu keine geeignete API bereitstellt.
* Die Konfiguration von CTC-Modulen ist derzeit unter iOS nur teilweise verfügbar.
* Für die Gebühr im App-Store haben wir uns entschieden, um unnötige Downloads und daraus resultierende Bewertungen gering zu halten.
* Aufgrund des Zertifizierungsprozesses des App-Stores kommt die App für iOS immer etwas später als die Desktop-Version.

## Firmware

Die Firmware für die CTC-Module ist jeweils Bestandteil der CTC-App für Desktop.
Die genaue Version können Sie der Änderungshistorie (s.u.) entnehmen.
Ein separater Download ist derzeit nicht geplant.

## Änderungshistorie

### App Version 4.37 (nur PC und Android), Firmware 20250222

NEU:
- WLAN-Einbuchen robuster gemacht: kontrolliert jetzt ob Wechsel in Config-WLAN funktioniert hat (glaube keinem Microsoft-Produkt).
- Firmware: Neustart des Config-WLANs erfolgt nur wenn schon ein WLAN konfiguriert ist. 

Fehlerkorrektur:
- SSID/Passwort über HTML-Seite ging nicht (erzeugte eine ungültige Konfiguration).
- Einbuchen über Windows ging nicht mehr aufgrund von nicht mehr funktionierendem Windows API.

### App Version 4.36, Firmware 20241227
      
NEU: 
- Falls System-Sprache in CTC-App nicht vorhanden wird auf Englisch umgestellt
- Die zuletzt gewählte Sprache wird gespeichert und beim nächsten Start verwendet
- Desktop: Scrollbars hinzugefügt bei Gleisbild und Loksteuerung
- Android Config-Dialog: Layout für SmartPhone (Portrait) angepasst
- Android Config-Dialog: Kontrast bei Selektion sowie Plus-/Minus und Edit-Buttons verbessert

Fehlerkorrektur:
- App hängte sich auf, wenn System-Sprache nicht Deutsch, English oder Französisch war
- Android: Port-/Pin-Darstellung im Config-Dialog enthielt teilweise doppelte Einträge

### App Version 4.35 (nur Desktop)

Fehlerkorrektur:
- Weitere Probleme behoben beim Dateizugriff in "Produkt auswählen", "Erweiterung auswählen", "Neue Firmware hochladen", "IO-Config ersetzen" und "Config ersetzen"

### App Version 4.34 (nur Desktop)

Fehlerkorrektur:
- "Neue Firmware hochladen", "IO-Config ersetzen" und "Config ersetzen" haben die mitgelieferten Dateien nicht mehr gefunden.

### App Version 4.33 (nur Desktop Mac OS/X)

Fehlerkorrektur:
- Aufgrund einer API-Änderung seitens Apple funktionierte das Einbuchen ins WLAN über die CTC-App nicht mehr

### App Version 4.32, Firmware 20241026
       
NEU:
- Internationalisierung: Sprache ist nun umstellbar zwischen Deutsch, Englisch und Französisch
- Produktkataloge enthalten ebenfalls alle drei Sprachen und werden passend zur App-Sprache angezeigt.
- SiFa-Funktionalität: Lok hält (optional) automatisch nach festgelegter Zeit ohne Verbindung zur App
- CTC-Module können optional ein Mini-WLAN aufspannen (max. 2 Teilnehmer) - vorerst nicht über die App einstellbar.
- Ansteuerung von Digital-Signalen, die an einer Z21-Zentrale angeschlossen sind.
- Katalog "universell-loks.xml": Fahrlicht-HB2 gibt es nun für LowSide (Schalten des Minuspols) und HighSide (Schalten des Pluspols).
- Icons für Mehrfach-Sounds hinzugefügt und "universell-loks.xml" angepasst.
- In mehreren Dialogen war die Anzeige der IP-Adresse(n) leer.

Android:
- Workaround um Google-Bug: Config-Dateien werden nun möglichst explizit aus dem WiFi geladen. 
  Android hat HTTP-Anfragen oft auf dem Mobilfunk geschickt, wenn das Modellbahn-WLAN keine Internetverbindung hatte.
  Das sah man daran, dass von den Loks nur die MAc-Adresse und nicht der Name angezeigt wurde und die Lok nicht gesteuert werden konnte. 

Fehlerkorrekturen:
- Diverse korrekturen um gelegentlichen Hänger der App entgegenzuwirken.
- Gleisbild hochladen verlor gelegentlich Daten
- Skripte: Auswahl des Fragezeichen-Parameters fehlte bei CallCmd.
  Die Anzeige von IfCmd und SetCmd war fehlerhaft. 

Firmware:
- CTC-Lokmodul-PluX22 kann nun SUSI3 alternativ über Zugbus-Pins des PluX-Steckers ansteuern
- Logging-Puffer vergrößert, sodass auch bei umfangreicher Konfiguration noch alle Nachrichten von Modulstart in der App ankommen. 

### App Version 4.27

Fehlerkorrektur:
- Abstürze und Hänger beim Messe-Betrieb (WLAN-Störungen)

### App Version 4.26, Firmware 20240304
           
NEU:
- Fahrstraßen können nun im Gleisbild geschaltet werden
- Waggons können nun manuell an- und abgekoppelt werden
- Lok-Steuerung zeigt angekoppelte Waggons an
- Waggon-Reader (NFC) sendet Liste von gelesenen Tags statt nur das letzten.
  Grund: Beim schnellen Überfahren wurden einzelne Waggons ausgelassen.
- Lok-Entkuppler koppelt nun die Waggons auch logisch ab 

Desktop:
- Tooltips für Dock-Panes hinzugefügt
           
Android:
- Fehlerkorrektur: Icons wurden an diversen Stellen nicht angezeigt
       
Firmware:
- Fehlerkorrektur: Servos funktionierten nicht mehr am IO-Board

### App Version 4.25 (nur Desktop), Firmware 20240120

Desktop:
* Update auf Java 21.02

Systemstart robuster gemacht:
* Config-Laden robuster gemacht: Intervall zwischen zwei Versuchen beim Laden der Config-Dateien vergrößert
* Kommandos bzgl. Zustandsänderung von Balisen wird nur an Loks gesendet, die deren Config bereits vollständig geladen ist.
* Suchen nach neunen Netzwerken (Broadcast-Scan) auf 5s reduziert.
  Nur noch bei langsamen PCs wird das Intervall automatisch auf 30s verlängert. 

Fehlerkorrektur:
* Config bzw. IO-Config ersetzen funktionierten nur auf dem Entwicklungs-PC, da der Datei-Pfad falsch zusammengebaut wurde.
* Auswahl aus Dateisystem funktionierte nicht bei Config bzw. IO-Config ersetzen.
    
Firmware:
* Logging bei HTTP-Zugriffen und Firmware-Update entfernt, das es zu Hängern führen konnte.  

### App Version 4.24
    
Allgemein:
* Lok-Steuerung zeigt erste Lok (sobald eine gefunden wurde).
  Bisher war das Fenster initial leer.
* Klick auf Schalt-Buttons (auch im Gleisbild) zeigt ein Wartesymbol bis die Durchführung vom CTC-Modul bestätigt wurde.

Desktop:
* Im Config-Dialog wird beim Hinzufügen von Parametern eine Auswahl präsentiert, v.a. hilfreich für DCC Adresse und DCC-Funktionszuordnung.
* Config-Dialog: Horizontaler Trenner zwischen Parametern und Details ist nun verschiebbar.
* Klick in Lok-Liste öffnet eine neue Lok-Steuerung für die angeklickte Lok.
* Diverse Optimierungen beim Dialog-Layout. 
* Darstellung war bei dunklen Farben teilweise schlecht lesbar.
* Pop-up-Fenster (z.B. Rückfragen) werden nun in der Nähe des Mauszeigers geöffnet.
* Dialoglayout wird nun in App-Dir (statt Temp-Dir) gespeichert.
* Alternative Config-Dateien hinzugefügt für "Config ersetzen" und "IO-Config ersetzen" sowie Standard-Config von alter Lok-G und Weiche-G.

Z21 Emulation:                                                       
* Implementierung jetzt vollumfänglich für Modellbahn-Steuerungssoftware (getestet mit iTrain).
* Neu: Balisen als Belegt-Melder.
* Fehlerkorrekturen für Schalten von Weichen und Signalen sowie Lok-Fahrtrichtung. 

### App Version 4.23 (nur Desktop), Firmware 20231218

Desktop-App:
* Initiales Layout zeigt Modul-Liste statt leerem Fenster

Fehlerkorrektur:
* Betriebswerk anlegen funktionierte nicht (konnte nicht speichern).
* Bei Betrieb als Z21 Zentrale waren bei Loks vorwärts und rückwärts vertauscht.

### App Version 4.22 (nur Desktop), Firmware 20231218

Desktop-App:
* Umstellung auf Java 21
* Docking-Framework eingebaut, d.h. Fensterinhalte können frei angeordnet und auch in neue oder andere Fenster verschoben werden.
     
Allgemein:
* NFC-Reader zur Waggon-Erkennung wird nun vollständig unterstützt.
* Anzeige erkannter Waggons im Fenster "Block-Liste".
* Verwaltung von Waggon-Daten in "Betriebswerk".
* Waggon-Bilder können auf Schaltmodulen gespeichert werden.
* Kleinere Optimierungen bei Lastregelung und Bremsverhalten.
* Lok fährt nach Halt am roten Signal auch dann wieder los, wenn sie die Balise überfahren hat. 
         
Firmware:
* Anschluss von NFC-Reader zusätzlich an CTC-Weichenmodul möglich (bisher nur an CTC-Multi-IO-Board).
* fileCfg.xml entsorgt - wird nun direkt aus dem Inhalt des Dateisystems erzeugt.
* HTML-Seite etwas überarbeitet

### App Version 4.20 (nur Desktop), Firmware 20230911

Verbesserung der Meldungen in den Config-Dialogen:
- Log-Ausgabe in den Config-Dialogen editierbar gemacht, damit kann mit Strg-A / Strg-C der Inhalt der Log-Ausgabe in die Zwischenablage kopiert werden (für Support-Anfragen).
- Die Log-Ausgabe der Firmware-Aktualisierung wird nach dem Neustart des CTC-Moduls nicht mehr überschrieben.

Drehscheibe in Produktkatalog "universell-sonstiges.xml" aufgenommen, siehe auch Artikel [Anschluss Fleischmann-Drehscheibe]({% link _posts/de/szenarien/2023-09-12-Anschluss-Drehscheibe.md %}). 
                                                                                                                                   
Fehlerkorrektur Firmware:
* Bei Loko-G ging der zweite Motor nicht.
  Fehler wurde behoben durch Ermöglichung von Mehrfachnutzung von Pins für denselben Zweck (der Dir-Pin bei Loko-G hängt an beiden Motor-Treibern (Ports)).

### App Version 4.19, Firmware 20230902
               
Allgemein:
* **Unterstützung von SUSI3-Soundmodulen**, siehe Bedienungsanleitung [Kapitel 9.1 - "Sound mit SUSI-Schnittstelle"]({% link _app_doku/091-SUSI-sound.md %}).
* CTC-Module geben Log-Meldungen via UDP weiter.
  Die Anzeige erfolgt im Config-Dialog der CTC-App, siehe "Log-Ausgabe" in der Bedienungsanleitung [Kapitel 4 - "Module konfigurieren"]({% link _app_doku/040-module-konfigurieren.md %}).
* In der IO-Config können nun Pins mehrfach verwendet werden und so z.B. entweder für einen Port oder einzeln genutzt werden.   
* Neuer Button zum Ersetzen der IO-Config. 
  Um dabei Fehler zu vermeiden, enthält die IO-Config nun Firmware-Name und -Version. 
* Der Button zum Löschen der Config wurde ersetzt durch einen Button zum Zurücksetzen der Config.
* Unterstützung von Motoren, die an zwei Halbbrücken angeshclossen werden
* Unterstützung des [Sinus-Motor-Treibers von rail4you.ch](https://rail4you.ch/remotorisierung)
* CTC-App liefert nun Original Config-Dateien der CTC-Module mit.
  
Produktkataloge ergänzt:
* universell-licht.xml ergänzt um "LED an Logic"
* universell-loks.xml ergänzt um Fahrlicht SBB mit 3 Anschlüssen und Lok-Sound-Gruppen
* Build-Skript holt Configs aus Sketch-Ordner

Firmware:
* MotorAction steuert einen zweiten Motor synchron an, wenn er als zweiter Port in ItemConn definiert ist.
  Dazu muss im Produktkatalog "Doppelmotor" ausgewählt werden.
* DCC-Funktionalität auf den Stand 6.1.0 der Bibliothek "DCCInterfaceMaster" von Philip Gahtow gebracht.

Fehlerkorrekturen:
* Darstellungsfehler (doppelt gezeichnete Symbole) in der Gleisbilddarstellung behoben
* Lok-Firmware stürzte ab, wenn Cfg.xml leer oder ungültig war (keinen Motor enthielt)

Fehlerkorrekturen für zweimotorige Loks:
* Bei der Kalibrierung waren die Sensoren beider Motoren teilweise vermischt.
* Die Distanzberechnung der Lok hat immer beide Motoren zusammengezählt.      
* Die Sensoren werden nun in der Lok alle gleichzeitig gemessen, statt nacheinander.
* Der zweite Motor war bei Geschwindigkeitsänderungen immer um einen Zyklus (20 ms) versetzt zum ersten.

### App Version 4.17 (nur Desktop)

- Motor einstellen bearbeitet nun jeweils die Parameter des eingestellten Motor-Mode (direct bzw PID)
- Lok-Firmware: Parameter "Reaktion" eingebaut

### App Version 4.16 (nur Desktop)

Fehlerkorrekturen:
- Firmware für CTC-Lokmodul-G enthielt versehentlich noch Debug-Code und war damit unnötig langsam

### App Version 4.15, Firmware 20230604

- NEU (Desktop-App): WiFi-Monitor zeigt Signalstärke einer Lok als Grafik (über die Zeit).
- Firmware: CTC-Module schicken im Status die MAC-Adresse des WLAN-Routers, mit dem sie verbunden sind (Anzeige im Wifi-Monitor).
- Desktop-App: Dialoge mit Diagrammen passen die Diagrammgröße beim Ändern der Fenstergröße mit an.
- Fehlermeldungen bei Eingabe von Namen weisen nun darauf hin, dass auch Leerzeichen nicht erlaubt sind.
- Waggon-Erkennung: An das CTC-Multi-Board kann nun ein NFC-Reader angeschlossen werden, der unter dem Gleis montiert wird und so Waggons anhand eines NFC-Tags erkennen kann.
  Im Moment wird die gelesene ID unter "Sensoren" angezeigt, mehr Funktionalität kommt später.

Fehlerkorrekturen:
- Desktop-App: Sensor kalibrieren funktionierte nicht mehr richtig (seit Version 4.12).
- Firmware Loks: Zielbremsen war zu Beginn des Bremsvorgangs zu schwach und deshalb gegen Ende zu stark.
- Firmware: Zulässige Längen an WiFi-Norm angepasst: SSID (32 Zeichen), WLAN-Passwort (63 Zeichen).  

### App Version 4.14, Firmware 20230329
          
- Konfiguration von CTC-Modulen: Auswahldialog für Lok-Kommando (statt Freitexteingabe)  
- Z21 LAN Protokoll erweitert, sodass der Handregeler [LoDi-Con von Lokstoredigital](https://lokstoredigital.jimdo.com/lodi-con/) funktioniert.

Fehlerkorrekturen:
- Android: Gelegentlicher Absturz beim Fingertip auf dem Gleisbild (während dieses aktualisiert wurde)
- Firmware: Kommando "Stop" (Halt, Pause, Weiterfahrt) funktionierte nicht

### App Version 4.13, Firmware 20230225 (nur CTC-Lokmodul-21mtc)

**Hinweis:** Nur für Desktop (Linux, Mac, Windows), wegen defekter Firmware (s.u.)

Fehlerkorrekturen:
- Firmware für CTC-Lokmodul-21mtc war defekt

### App Version 4.12, Firmware 20230115
                 
- Sensorkalibrierung geht nun auch im Pendelbetrieb.
- Große Konfigurationsdialoge um Scrollbars ergänzt, sodass sie auch auf Bildschirmen mit geringer Auflösung funktionieren.
- Löschen von PosID im Gleisbild ermöglicht.

Fehlerkorrekturen:
- Ein Fehler in der Basisbibliothek hatte dazu geführt, dass diverse Buttons (z.B. Config Hochladen) nicht funktionierten. 
- Automatisierung ändern: Fahrstraße hinzufügen ging nicht
- Neuer Produktkatalog "universell-sonstiges.xml" mit Entkuppler (Gleis)
- Für das Logging wird nun täglich eine neue Datei begonnen; alte Logdateien werden nach 30 Tagen gelöscht
- CPU-Last unter Windows nochmals reduziert 
- Motor einstellen: Darstellung von Motorzuständen und Empfang von Motor-Daten repariert
- Anzeige von Lok-Zustand war teilweise falsch

### App Version 4.11

**Hinweis:** Nur für Desktop (Linux, Mac, Windows); Firmware unverändert
      
- universell-loks.xml um diverse Varianten von Fahrlicht ergänzt
- Upgrade auf Java 17.0.5
- Anzeige von NFC-Balisen im Auswahldialog verbessert: 3-Zeichen-PosID anstatt Tag-ID
- Neuer Button zum Löschen des Auslösers in der Trigger-Konfiguration

Fehlerkorrekturen:
- Konfiguration Messstrecke hat bei NFC die Tag-ID statt der 3-Zeichen-PosID übernommen
- Auswahl Trigger.sourceID hat bei NFC die Tag-ID statt der 3-Zeichen-PosID übernommen
- Auswahl Trigger.sourceID setzt "beliebiges Modul" (*) falls kein Auslöser angegeben
- Gelegentlich wurde Lok-Name im Gleisbild doppelt angezeigt 

### App Version 4.10, Firmware 20221022

- Neue Farbe violett bei Balisen für Kommando "Reverse" ('R').
- Dialoge "Gleisplan-Editor" und "Config-Editor" verkleinert, damit aie auch auf kleinen Bildschirmen mit 768 Zeilen (z.B. MS Surface-Tablet) bedienbar bleiben.
- In den Config-Dialogen wird bei Skripten nun das Script-Icon mit angezeigt.
- Beim Hinzufügen von Skripten und Skript-Name Ändern wird nun eine passende Icon-Auswahl angezeigt
- Broadcast Adressen werden nur noch alle Minute auslesen, da das unter Windows erhebliche CPU-Last erzeugt.

Fehlerkorrekturen:
- Korrektur für 20 oder mehr NFC-Balisen ging immer noch nicht.
- Bei "Automatik ein" wurde die in der App eingestellte Geschwindigkeit nicht übertragen, wenn Lok gestoppt war.
- In der Installation mitgelieferte Config-Dateien wurden aktualisiert.

**Hinweis:** Die Versionsnummern 4.08 und 4.09 wurden für interne Tests verbraucht.

### App Version 4.07

Verbesserungen:
- Nur Desktop: Menüpunkt "Config-Backup" speichert die Config-Dateien aller CTC-Module in einem Ordner.

Fehlerkorrekturen:
- Korrektur für 20 oder mehr NFC-Balisen funktionierte nicht immer.
- Im Config-Dialog für Sensoren fehlten Poll-Intervall und Auslöser-Typ.

### App Version 4.06

Verbesserungen:
- Z21 Protokoll: Mit der WLAN-Maus können nun auch Weichen geschaltet werden.
- Die Z21 Android App funktioniert jetzt ebenfalls.

Fehlerkorrekturen:
- Bei 20 oder mehr NFC-Balisen stürzten alle CTC-Module ab (zu große Sync-Nachricht).

### App Version 4.05, Firmware 20220828

Neu:
- Roco Z21 WLAN-Maus (bzw. dazu kompatible Handregler) kann nun zum Steuern von CTC-Loks verwendet werden
- An einer Z21 (bzw. kompatiblen Zentralen) angeschlossene Loks können von der CTC-App gesteuert werden 
    
Verbesserungen:
- Eingabeprüfung bei allen IDs/Namen
- Hinweis, wenn zu einem Script-Namen kein Icon existiert

Fehlerkorrekturen:
- Automatisierung: Pendelzugbetrieb ging nicht

### App Version 4.04, Firmware 20220814

**Hinweis:** Nur für Desktop (Linux, Mac, Windows).
                                                              
Verbesserungen:
- Beim Anlegen von Fahrstraßen wird eine Liste von Namen angezeigt, zu denen es Icons gibt

Fehlerkorrektur:
- Konfiguration der Automatisierung mit NFC-Balisen ging nicht

Fehlerkorrektur Firmware:
- Steuerung der Uhrzeiger-Richtung funktionierte nicht bei Loks mit zwei Motoren 

### App Version 4.03, Firmware 20220609

Siehe [CTC-App Version 4]({% link _posts/de/szenarien/2022-05-30-CTC-Version-4.md %}) 

### App Version 3.17, Firmware 20211110

**Hinweis:** Nur für Desktop (Linux, Mac, Windows).

Fehlerkorrektur Firmware:
* Die Firmware stürzte ab, wenn NFC-Balisen im Gleisbild platziert, aber die Positions-ID leer gelassen wurde.

### App Version 3.16

**Hinweis:** Nur für Desktop (Linux, Mac, Windows).

Fehlerkorrektur:
* Motor-Sensor kalibrieren funktionierte bei IR nicht mehr

### App Version 3.15, Firmware 20211031

**Hinweis:** Nur für Desktop (Linux, Mac, Windows).

Fehlerkorrektur Firmware:
* Die Firmware stürzte ab, wenn nicht alle Sensoren einem IO-Pin zugeordnet waren

### App Version 3.14

**Hinweis:** Nur für Desktop (Linux, Mac, Windows).

Fehlerkorrekturen:
* Ändern der Postion einer NFC-Balise im Gleisbild wurde nicht hochgeladen
* Im neuen Dialog "Automatisierung ändern" konnte ein Produkt nicht gelöscht werden

### App Version 3.13, Firmware 20210930

Neu:
* Produkt-Katalog wurden ergänzt
* Angezeigte Weichenstellung kann auf Sensor-Rückmeldung (Zungensensor) basieren (ab Firmware 20210930).
* Für NFC tags können nun auch Trigger angelegt werden, z.B. zum Halt vor einem roten Signal (ab Firmware 20210930).
* Es können Aktionen zum Schalten von Fahrstraßen angelegt werden.
* WiFi-Icon der Module zeigt nun auch die Signalstärke an (ab Firmware 20210930).

Fehlerkorrekturen:
* Bei CTC-Modulen mit sehr vielen Aktionen wurde Status-Nachrichten abgeschnitten.
* Bei Config-Upload nach Bild-Upload wurde die config an die falsche Stelle im Modul geschrieben.
* Namen düren nun auch Umlaute enthalten.

Desktop
* Java auf Version 17 aktualisiert. 
      
Android:
* Config-Dialoge sind nun vollständig vorhanden.
  
Firmware:
* Sensoren unterstützen Betrieb per Polling oder Interrupt 
* Der Motor-Sensor arbeitet nun präziser (Werte werden geglättet) 

### App Version 3.12, Firmware 20210707
 
Neu:
* Die Motor-Kalibrierung basiert jetzt auf mm/s.
  Damit ist nun ein Zielbremsen z.B. vor einem roten Signal möglich.
* In der Lok-Steuerung wird nun angezeigt, wie weit (mm) die Lok von der letzten gelesenen ID entfernt ist.
* Die Kommunikation (Synchronisierung-Signal, Broadcast) wurde robuster gemacht.
* Beim Löschen einer Config wird gleich der neue Name für das CTC-Modul erfragt.
* Für Licht und Effekte gibt es nun auch ein Steuerungs-Panel in der App.
* Der Produkt-Katalog wurde in mehrerer thematisch sortierte Kataloge aufgeteilt.

Android:
* Die Konfiguration von CTC-Modulen ist nun vollständig unter Android möglich, gut bedienbar aber nur auf dem Tablet.

iOS:
* Mit der ersten Version der App ist nur die Steuerung möglich.
  Für die Konfiguration der CTC-Module muss noch auf den Mac oder PC zurückgegriffen werden.
   
Firmware:
* Motor-Steuerung überarbeitet für Berechnungen basierend auf mm/s. 

### App Version 3.11

Gab es nur für Android wegen technischer Panne beim Einstellen im Play-Store.

### App Version 3.10, Firmware 20210514

Neu:
* Unterstützung von DCC-Dekodern, angeschlossen am Motor-Ausgang
* Desktop-App: Verschiedene Farb-Schematas für bessere Kontraste
* Überarbeitung der Weichen-Icons für bessere Kontraste

Hinweis: Bevor Sie DCC-Dekoder nutzen können, muss das CTC-Lokmodul mit der aktuellen Firmware versorgt werden.

### App Version 3.09, Firmware 20210416

Fehlerkorrekturen:
* Definition von Messstrecken: Auswahl IR-Balise funktionierte nicht
* Windows: Beide Dialoge zu Motor-Konfiguration gingen nicht auf
* Windows: Auswahl eines neuen Lok-Icons ging nicht

Android ist von den mit Version 3.09 behobenen Fehlern nicht betroffen.

### App Version 3.08, Firmware 20210416

Verbesserungen:
* Bedienung der Lok-Steuerung (Fahrtrichtung und Lok buchen) optimiert und Darstellung des Zustands verbessert
* Config-Dialog vorbereitet für DCC-Unterstützung (Umstellung von zwei Tabellen auf einen Baum)
               
Android:
* Darstellung v.a. für kleinere Bildschirme verbessert 

Firmware:
* Umschalten des Lichts bei Fahrtrichtungswechsel funktionierte nicht

### App Version 3.07, Firmware 20210410

Neu:  
* Unterstützung für Mehrfachtraktion
* Unterstützung für NFC-Balisen und -Reader als Alternative zu IR-Balise und -Empfänger
* Unterstützung für Servos im Lokodul-G, z.B. für Pantograph heben/senken

Fehlerkorrekturen:
* Trigger innerhalb des CTC-Moduls funktionierten nicht.
* Seltsames Verhalten des Buttons in der Lok-Steuerung für Richtung und Pause.

### App Version 3.06, Firmware 20210309

Unterstützung für Weichen mit Motor und magnetische Weichen mit Umpolung. 
* Für beide wird das in neue CTC-Weichenmodul-G benötigt
* Hinweis: Das bisherige CTC-Weichenmodul bleibt im Programm.

Für die kommenden Gartenbahnmodule eurde die basis gelegt: 
Unterstützung für 2-Motorige Lokomotiven und NFC-Reader.

### App Version 3.05, Firmware 20210220

Config bearbeiten
* Beim Wert-Regler fehlende Auswahl von Pin/Parameter hinzugefügt
* Beim neu anlegen eines Wert-Reglers wird automatisch der erste Pin ausgewählt 

Neue Module suchen:
* SSID und Password werden nun vom ersten gefundenen CTC-Modul übernommen

Android:
* "Neue Module suchen" ist nun auch mit Android möglich

Firmware allgemein:
* Neue HTML-Seite "/" mit Redirect auf "/index.html", d.h. es muss nur noch die IP-Adresse eingegeben werden.
* Neue Seite "Edit Wifi SSID and password" erlaubt komfortableres Bearbeiten von SSID und WLAN-Passwort.

### App Version 3.04
  
Android:
* Darstellung der Buttons in Stellwerk und Loksteuerung verbessert 
