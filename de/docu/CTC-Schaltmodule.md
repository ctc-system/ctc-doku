---
layout: page
title:  "CTC-Schaltmodule"
tags: Weiche Signal Licht Schalten
---
![Banner Schaltmodule]({% link assets/images/top-schaltmodule.jpg %})                  

## Gemeinsame Eigenschaften aller CTC Schaltmodule

| Eigenschaft         | Beschreibung                                                          |
|---------------------|-----------------------------------------------------------------------|
| Eingangsspannung    | ca. 9 V bis 24 V DC, Digitalstrom - (AC nur mit Stützkondensator)     |
| Schnittstellen      | CTC-IR-Balise                                                         |

Ab Februar 2025 legen wir allen CTC Schaltmodulen eine TVS-Diode bei.
Siehe dazu Abschnitt "Überspannungsschutz" auf der Seite ["Stromversorgung und Router"]({% link de/docu/Stromversorgung-und-Router.md %}).   

## Übersicht CTC Schaltmodule (ab 40,- EUR)

Zum Schalten von Weichen, Signalen, Licht, etc. gibt es folgende CTC-Module:

| CTC-Weichenmodul                                                    | Optimiert für                  | Maße       | Schaltausgänge    | Servos | Sensor-Eingänge | IR-Balisen | Summe Strom max. |
|---------------------------------------------------------------------|--------------------------------|------------|-------------------|--------|-----------------|------------|------------------|
| [CTC-Weichenmodul]({% link _data_sheet/CTC-Weichenmodul-H0.md %})   | Magnetartikel, Märklin C-Gleis | 68 x 26 mm | 4x 0,5 A Lowside  | -      | -               | 2          | 2 A              |
| [CTC-Weichenmodul-G]({% link _data_sheet/CTC-Weichenmodul-G.md %})  | Gartenbahn, Motorantrieb       | 53 x 31 mm | 4x 2 A Halbbrücke | 2      | 4               | 2          | 3 A              |
| [CTC-Multi-I/O-Board]({% link _data_sheet/CTC-Multi-IO-Board.md %}) | Magnetartikel, Servos          | 74 x 46 mm | 8x 0,5 A Lowside  | 2      | 4               | 2          | 2 A              |

<br/>

**Hinweise:**
* **Der Betrieb der CTC-Module an Wechselstrom (AC) oder nicht geglättetem Gleichstrom ohne zusätzlichen Kondensator führt zum irreparablen Schäden am CTC-Modul.**
* Schaltausgänge werden danach unterschieden, ob sie Masse bzw. Minus (LowSide) oder den Pluspol (HighSide) schalten.
  Vor allem bei LED-Platinen existiert oft für einen Pol eine gemeinsame Leitung.
  Dann ist es wichtig, ob die Schaltausgänge des Moduls als High- oder LowSide ausgeführt sind.
* Eine Halbbrücke kann zwischen Plus- und Minuspol umgeschaltet werden, ist aber nie ausgeschaltet.
  Vor allem sogenannte Magnetartikeln (z.B. klassischer Weichenantrieb) können bei falscher Konfiguration der Halbbrücke dauerhaft Schaden nehmen und sollten deshalb nicht an eine Halbbrücke angeschlossen werden.
* Für Servos wird zusätzlich ein Stromversorgungs-Modul benötigt.
       

## Einbauanleitungen

### Weichen

{% for conn in site.anschliessen %}
* [{{ conn.title }}]({{ site.baseurl }}{{ conn.url }})
{% endfor %}

Für das CTC-Weichenmodul gibt es folgende Einbauanleitungen:

* [Umbau Märklin C-Gleis Weiche]({% link _posts/de/szenarien/2021-05-05-Umbau-C-Weiche.md %})
* [Umbau Märklin M-Gleis Weiche]({% link _posts/de/szenarien/2021-04-22-Umbau-M-Gleis.md %})

Anleitungen für das CTC-Weichenmodul-G:
* [Umbau LGB Weiche]({% link _posts/de/szenarien/2021-04-04-Umbau-LGB-Weiche.md %})
* [Umbau Piko-G Weiche]({% link _posts/de/szenarien/2021-04-04-Umbau-Piko-G-Weiche.md %})

### Servos

* überblick zum Betrieb von [Servos mit CTC]({% link _posts/de/szenarien/2021-02-06-Servos.md %}) 

### Drehscheibe

An das CTC-Weichenmodul-G kann auch eine Drehscheibe angeschlossen werden, siehe [Anschluss Fleischmann-Drehscheibe]({% link _posts/de/szenarien/2023-09-12-Anschluss-Drehscheibe.md %})
