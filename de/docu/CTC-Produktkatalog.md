---
layout: page
title:  "CTC-Produktkatalog"
---
![Banner]({% link assets/images/top-banner.jpg %})

## Bestellvorgang

Ab sofort gibt es für Deutschland und die EU einen [Online-Shop]({% link shop.html %}), den Sie über den orangenen Enkaufswagen links erreichen.

Zur Bestellung aus der **Schweiz** schnreiben Sie nach wie vor bitte eine [E-Mail an info@rail4you.ch](mailto:info@rail4you.ch?subject=CTC Bestellung).

Preise erfahren Sie im [Online-Shop]({% link shop.html %}).

## Alte und neue Artikelnummern

Im Zuge des Online-Shops haben wir unsere **Artikelnummern und teilweise auch den Lieferunfang neu gestaltet**.

Mit der folgenden Tabelle finden sie von der alten Artikelnummer zum passenden aktuellen Produkt.
Sollte eine alte Nummer (P2-...) fehlen, dann teilen Sie uns das bitte mit, inkl. dem Hinweis aus welcher Preisliste Sie die Nummer haben.

### Starter-Sets      

| **Alte Nr.** | **Bezeichnung**                    | **Anmerkung**                                | **Neue Nr.** |
| P2-01-02   | CTC-App                              | Wie immer zum Download bzw. App-Store        | siehe [Download-Seite]({% link de/download.md %}) |
| P2-01-100  | Starter-Set Weichen                  | Alternative: CTC-Router + 2 Weichenmodule    | [ST-4040]({{site.baseurl}}/shop.html#!/CTC-Router-FritzBox-4040-indoor/p/725596015/category=176901546), [WI-M-4L-U]({{site.baseurl}}/shop.html#!/CTC-Weichenmodul-universal/p/725604076/category=176888458) |
| P2-01-110  | Starter-Set Lok und Weichen          | Ersetzt durch P2-01-115 und P2-01-116        | s.u. |
| P2-01-112  | Aufpreis Starter-Set: 1x CTC-Lok-Modul 21mtc  | Starter-Sets umstrukturiert         | [ST-4040-1LM-1WC]({{site.baseurl}}/shop.html#!/CTC-Starter-Set-C-Gleis/p/725587420/category=176901546) |
| P2-01-113  | Aufpreis Starter-Set: 1x CTC-Lok-Modul PluX22 | Starter-Sets umstrukturiert         | [ST-4040-1LP-2WUL]({{site.baseurl}}/shop.html#!/CTC-Starter-Set-Universal/p/725577988/category=176901546) |
| P2-01-114  | Aufpreis für Starter-Set G outdoor   | Neu: Separates Starter-Set Outdoor           | [ST-225O-1LG35-1WO]({{site.baseurl}}/shop.html#!/CTC-Starter-Set-G-3-5A-outdoor/p/726396019/category=176901546) |
| P2-01-115  | Starter-Set C-Gleis                  | aufgeteilt in Starter-Set und Ergänzungs-Set | [ST-4040-1LM-1WC]({{site.baseurl}}/shop.html#!/CTC-Starter-Set-C-Gleis/p/725587420/category=176901546) | 
| P2-01-116  | Sterter-Set Universal                | aufgeteilt in Starter-Set und Ergänzungs-Set | [ST-4040-1LP-2WUL]({{site.baseurl}}/shop.html#!/CTC-Starter-Set-Universal/p/725577988/category=176901546) |
| P2-01-120  | Starter-Set G2 3,5A                  | aufgeteilt in Starter-Set und Ergänzungs-Set | [ST-4040-1LG35-1WG]({{site.baseurl}}/shop.html#!/CTC-Starter-Set-G-3-5A-indoor/p/725579244/category=176901546) |
| P2-01-130  | Aufpreis Outdoor                     | Neu: Separates Starter-Set Outdoor           | [ST-225O-1LG35-1WO]({{site.baseurl}}/shop.html#!/CTC-Starter-Set-G-3-5A-outdoor/p/726396019/category=176901546) |

### Router und Stromversorgung

| **Alte Nr.** | **Bezeichnung**                    | **Anmerkung**                                | **Neue Nr.** |
| P2-02-01   | CTC-Router FB-4020                   | FritzBox 4020 gitb es nicht mehr             | - |
| P2-02-02   | CTC-Router FB-4040                   | -                                            | [ST-4040]({{site.baseurl}}/shop.html#!/CTC-Router-FritzBox-4040-indoor/p/725596015/category=176901546) |
| P2-03-01   | CTC-Netzteil 24V                     | Wir liefern keine Netzteile mehr             | siehe [Stromversorgung und Router]({% link de/docu/Stromversorgung-und-Router.md %}) | 
| P2-03-01.1 | Netzkabel 1,5m                       | Wir liefern keine Netzteile mehr             | siehe [Stromversorgung und Router]({% link de/docu/Stromversorgung-und-Router.md %}) | 
| P2-03-02   | CTC-Netzteil 15V                     | Wir liefern keine Netzteile mehr             | siehe [Stromversorgung und Router]({% link de/docu/Stromversorgung-und-Router.md %}) | 
| P2-03-02.1 | Netzkabel 1,5m                       | Wir liefern keine Netzteile mehr             | siehe [Stromversorgung und Router]({% link de/docu/Stromversorgung-und-Router.md %}) | 

### Lok-Module

| **Alte Nr.** | **Bezeichnung**                    | **Anmerkung**                             | **Neue Nr.** |
| P2-05-00   | CTC-Lok-Modul H0a                    | nicht mehr lieferbar                      | siehe [Datenblatt H0a]({% link _data_sheet/CTC-Lokmodul-H0a.md %}) |
| P2-05-01   | CTC-Lok-Modul H0d                    | nicht mehr lieferbar                      | siehe [Datenblatt H0d]({% link _data_sheet/CTC-Lokmodul-H0d.md %}) |
| P2-05-02   | CTC-Lok-Modul 21mtc, 1000µF          | Kondensatorgröße auswählbar               | [L-M1-21M]({{site.baseurl}}/shop.html#!/CTC-Lokmodul-21mtc/p/725533783/category=176866649)  |
| P2-05-02-1 | CTC-Lok-Modul 21mtc, 470 µF          | Kondensatorgröße auswählbar               | [L-M1-21M]({{site.baseurl}}/shop.html#!/CTC-Lokmodul-21mtc/p/725533783/category=176866649)  |
| P2-05-02-2 | CTC-Lok-Modul 21mtc, 4x100 µF        | Kondensatorgröße auswählbar               | [L-M1-21M]({{site.baseurl}}/shop.html#!/CTC-Lokmodul-21mtc/p/725533783/category=176866649)  |
| P2-05-03   | CTC-Lok-Modul PluX22, 1000µF         | Kondensatorgröße auswählbar               | [L-M1-P22]({{site.baseurl}}/shop.html#!/CTC-Lokmodul-PluX22/p/725292894/category=176866649)  |
| P2-05-03-1 | CTC-Lok-Modul PluX22, 470 µF         | Kondensatorgröße auswählbar               | [L-M1-P22]({{site.baseurl}}/shop.html#!/CTC-Lokmodul-PluX22/p/725292894/category=176866649)  |
| P2-05-03-1 | 2021: CTC-Lok-Modul NEM651 (6-pol.)  | lange Zeit in Vorbereitung                | kommt ab Mai 2025 |
| P2-05-03-2 | CTC-Lok-Modul PluX22, 4x100 µF       | Kondensatorgröße auswählbar               | [L-M1-P22]({{site.baseurl}}/shop.html#!/CTC-Lokmodul-PluX22/p/725292894/category=176866649)  |
| P2-05-03-2 | 2021: CTC-Lok-Modul NEM652 (8-pol.)  | lange Zeit in Vorbereitung                | kommt ab Mai 2025 |
| P2-05-04   | CTC-Lokmodul-G 3,5A                  | ersetzt durch Nachfolgeprodukt mit SUSI3  | [L-G2-35]({{site.baseurl}}/shop.html#!/Lokmodul-G2-mit-3-5-A-Peak/p/725538297/category=176866649) |
| P2-05-05   | CTC-Lokmodul-G 6A                    | -                                         | [L-G1-60]({{site.baseurl}}/shop.html#!/Lokmodul-G1-mit-6-A-Peak-ohne-SUSI/p/725587473/category=176866649)  |
| P2-05-06   | CTC-Lokmodul-G2_3.5A                 | Steckervariante auswählbar                | [L-G2-35]({{site.baseurl}}/shop.html#!/Lokmodul-G2-mit-3-5-A-Peak/p/725538297/category=176866649)  |
| P2-05-07   | CTC-Lokmodul-G2_6A                   | Steckervariante auswählbar                | [L-G2-60]({{site.baseurl}}/shop.html#!/Lokmodul-G2-mit-6-A-Peak/p/725541407/category=176866649)  |
| P2-05-08   | CTC-Lokmodul-G2_3.5A LGB             | Steckervariante auswählbar                | [L-G2-35]({{site.baseurl}}/shop.html#!/Lokmodul-G2-mit-3-5-A-Peak/p/725538297/category=176866649)  |
| P2-05-09   | CTC-Lokmodul-G2_6A_LGB               | Steckervariante auswählbar                | [L-G2-60]({{site.baseurl}}/shop.html#!/Lokmodul-G2-mit-6-A-Peak/p/725541407/category=176866649) |
| P2-05-20   | CTC-Lokmodul-21mtc_470μF             | Kondensatorgröße auswählbar               | [L-M1-21M]({{site.baseurl}}/shop.html#!/CTC-Lokmodul-21mtc/p/725533783/category=176866649)  |
| P2-05-21   | CTC-Lokmodul-21mtc_1000μF            | Kondensatorgröße auswählbar               | [L-M1-21M]({{site.baseurl}}/shop.html#!/CTC-Lokmodul-21mtc/p/725533783/category=176866649)  |
| P2-05-22   | CTC-Lokmodul-21mtc_4x100μF           | Kondensatorgröße auswählbar               | [L-M1-21M]({{site.baseurl}}/shop.html#!/CTC-Lokmodul-21mtc/p/725533783/category=176866649)  |
| P2-05-30   | CTC-Lokmodul-PluX22_470μF            | Kondensatorgröße auswählbar               | [L-M1-P22]({{site.baseurl}}/shop.html#!/CTC-Lokmodul-PluX22/p/725292894/category=176866649)  |
| P2-05-31   | CTC-Lokmodul-PluX22_1000μF           | Kondensatorgröße auswählbar               | [L-M1-P22]({{site.baseurl}}/shop.html#!/CTC-Lokmodul-PluX22/p/725292894/category=176866649)  |
| P2-05-32   | CTC-Lokmodul-PluX22_4x100μF          | Kondensatorgröße auswählbar               | [L-M1-P22]({{site.baseurl}}/shop.html#!/CTC-Lokmodul-PluX22/p/725292894/category=176866649)  |

### Weichen-Module

| **Alte Nr.** | **Bezeichnung**                    | **Anmerkung**                             | **Neue Nr.** |
| P2-10-00   | CTC-Weichen-Modul für C-Gleis        | ersetzt durch Nachfolgeprodukt            | [WI-M-4L-C1]({{site.baseurl}}/shop.html#!/CTC-Weichenmodul-C-Gleis/p/725604063/category=176888458)  |
| P2-10-00-1 | CTC-Weichen-Modul HO, 1x Märklinstecker + 1x 3er-Buchse | ersetzt durch Nachfolgeprodukt | [WI-M-4L-C1]({{site.baseurl}}/shop.html#!/CTC-Weichenmodul-C-Gleis/p/725604063/category=176888458)  |
| P2-10-00-2 | CTC-Weichen-Modul HO inkl. Halter, 2x 3er-Buchse        | ersetzt durch Nachfolgeprodukt | [WI-M-4L-U]({{site.baseurl}}/shop.html#!/CTC-Weichenmodul-universal/p/725604076/category=176888458)  |
| P2-10-00-3 | CTC-Weichenmodul_C-Gleis_2x          | -                                         | [WI-M-4L-C2]({{site.baseurl}}/shop.html#!/CTC-Weichenmodul-C-Gleis-2-Antriebe/p/725596051/category=176888458)  |
| P2-10-01   | CTC-Weichen-Modul G Klemmen, Halter  | Weiche G2 mit Klemmen auf Anfrage lieferbar | Nachfolger mit Gehäuse: [WI-G2-4S]({{site.baseurl}}/shop.html#!/CTC-Weichenmodul-G2-indoor/p/725797325/category=176888458)  |
| P2-10-02   | CTC-Weichen-Modul G Klemmen, Gehäuse | ersetzt durch Nachfolgeprodukt            | [WI-G2-4S]({{site.baseurl}}/shop.html#!/CTC-Weichenmodul-G2-indoor/p/725797325/category=176888458)  |
| P2-10-03   | CTC-Weichen-Modul HB inkl. Halter    | G1 Variante ohne Klemme nicht mehr lieferbar | G2 mit Gehäuse: [WI-G2-4S]({{site.baseurl}}/shop.html#!/CTC-Weichenmodul-G2-indoor/p/725797325/category=176888458)  |
| P2-10-04   | CTC-Weichenmodul-G2_Gehäuse (indoor) | -                                         | [WI-G2-4S]({{site.baseurl}}/shop.html#!/CTC-Weichenmodul-G2-indoor/p/725797325/category=176888458)  |
| P2-10-05   | CTC-Multi-IO-Board (indoor)          | -                                         | [WI-IO2-8S]({{site.baseurl}}/shop.html#!/CTC-Weichenmodul-Multi-IO-indoor/p/725521983/category=176888458)  |
| P2-10-06   | CTC-Multi-IO-Board (outdoor)         | -                                         | [WO-IO2-8S]({{site.baseurl}}/shop.html#!/CTC-Weichenmodul-Multi-IO-outdoor/p/725531354/category=176888458)  |
| P2-10-08   | CTC-Weichen-Modul G Outdoor          | ersetzt durch Nachfolgeprodukt            | [WO-G2-4S]({{site.baseurl}}/shop.html#!/CTC-Weichenmodul-G2-outdoor/p/725810507/category=176888458)  |
| P2-10-09   | CTC-Weichenmodul-G2_Outdoor          | -                                         | [WO-G2-4S]({{site.baseurl}}/shop.html#!/CTC-Weichenmodul-G2-outdoor/p/725810507/category=176888458)  |
| P2-10-10   | Signal-Modul                         | ersetzt durch Alternativprodukte          | siehe [Schaltmodule]({% link de/docu/CTC-Schaltmodule.md %})  |
| P2-10-20   | CTC-Weichenmodul_C-Gleis_Kabel       | -                                         | [WI-M-4L-C1]({{site.baseurl}}/shop.html#!/CTC-Weichenmodul-C-Gleis/p/725604063/category=176888458)  |
| P2-10-21   | CTC-Weichenmodul_Universal,_Kabel    | -                                         | [WI-M-4L-U]({{site.baseurl}}/shop.html#!/CTC-Weichenmodul-universal/p/725604076/category=176888458)  |

### Zubehör

| **Alte Nr.** | **Bezeichnung**                     | **Anmerkung**                             | **Neue Nr.** |
| P2-11-00   | CTC-IR-Balise Bausatz                 | -                                         | [ZW-IRB]({{site.baseurl}}/shop.html#!/CTC-IR-Balise-Bausatz/p/725728705/category=176900083)  |
| P2-11-10   | CTC-IR-Strip                          | immer noch nur Prototyp                   | Alternative: [ZW-IRB]({{site.baseurl}}/shop.html#!/CTC-IR-Balise-Bausatz/p/725728705/category=176900083)  |
| P2-11-20   | CTC-IR-Empfangsdiode 8-pol            | Variante für nicht mehr lieferbare Lokmodule H0a und H0d | auf Anfrage lieferbar | 
| P2-11-21   | CTC-IR-Empfangsdiode 10-Pol           | -                                         | [ZL-IR-10]({{site.baseurl}}/shop.html#!/CTC-IR-Empfanger-fur-Lok-10pol/p/725577870/category=176902513)  |
| P2-12-00   | Montagehalter_CTC-Weichen-Modul       | -                                         | [ZW-H-M]({{site.baseurl}}/shop.html#!/Montagehalter-fur-CTC-Weichenmodul/p/726427913/category=176900083)  |
| P2-12-01   | Zubehör Lok-Modul H0a                 | ersetzt durch Nachfolgeprodukt            | [ZL-UA]({{site.baseurl}}/shop.html#!/Lok-Umbausatz-Analog/p/725782778/category=176902513)  |
| P2-12-02   | Spannungswandler-Servo                | -                                         | [Z-SP-5V]({{site.baseurl}}/shop.html#!/Spannungswandler-fur-Servo-5-V/p/725728708/category=176900083)  |
| P2-12-03   | Adapterplatine 21mtc Tams             | -                                         | [LA-T-21M]({{site.baseurl}}/shop.html#!/Lokadapter-21mtc/p/725766481/category=176902513)  |
| P2-12-04   | CTC-Stecker_NFC_PluX22                | -                                         | [ZL-SN-P22]({{site.baseurl}}/shop.html#!/NFC-Stecker-fur-PluX22/p/725782762/category=176902513)  |
| P2-12-05   | Umbausatz-Analog                      | -                                         | [ZL-UA]({{site.baseurl}}/shop.html#!/Lok-Umbausatz-Analog/p/725782778/category=176902513)  |
| P2-12-06   | Adapterplatine PluX22                 | -                                         | [LA-P22]({{site.baseurl}}/shop.html#!/Lokadapter-PluX22/p/725577930/category=176902513) |
| P2-12-07   | Adapterplatine PluX22, SUSI3          | -                                         | [LA-P22-S]({{site.baseurl}}/shop.html#!/Lokadapter-PluX22-mit-SUSI/p/725577921/category=176902513) |
| P2-12-08   | Adapterplatine PluX22, SUSI3, Klemmen | -                                         | [LA-P22-GS]({{site.baseurl}}/shop.html#!/Lokadapter-PluX22-mit-Klemmen-und-SUSI/p/725788637/category=176902513)  |
| P2-13-00   | NFC-Reader                            | -                                         | [Z-NR4]({{site.baseurl}}/shop.html#!/NFC-Reader/p/725782807/category=176902513)  |
| P2-13-01   | NFC-Balise_LGB                        | Größe (Schwelle) auswählbar               | [ZW-NB]({{site.baseurl}}/shop.html#!/NFC-Balise/p/725782970/category=176900083)  |
| P2-13-02   | NFC-Nagel                             | -                                         | [ZW-NN]({{site.baseurl}}/shop.html#!/NFC-Nagel/p/726427951/category=176900083)  |
| P2-13-03   | NFC-Balise_Spur-1                     | Größe (Schwelle) auswählbar               | [ZW-NB]({{site.baseurl}}/shop.html#!/NFC-Balise/p/725782970/category=176900083)  |
| P2-13-04   | NFC-Tag selbstklebend 5er-Pack        | -                                         | [ZW-NT-4-10]({{site.baseurl}}/shop.html#!/NFC-Tag-5-Stuck-selbsklebend/p/725788647/category=176900083)  |