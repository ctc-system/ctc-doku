---
layout: page
title:  "WLAN Access-Point"
tags: WLAN
---

Beim WLAN-Access-Point (bzw. Router) bedienen wir uns des existierenden Standards.

Die CTC-Module arbeiten alle nur im 2,4 GHz Band.
Trotzdem lohnt sich ein Router mit zusätzlich 5 GHz.
Denn wenn Sie Ihr Tablet oder Smartphone per 5 GHz mit dem Router verbinden, stellen Sie sicher, dass dei Kommunikation über den Router erzwungen wird und der hat oft die bessere Funkverbindung zum CTC-Modul.

Gute Erfahrungen haben wir mit der **FritzBoxe 4040** gemacht, die wir aktuell für den **[CTC-Router]({% link _data_sheet/CTC-Router.md %})** verwenden.
Genauso gut tut es aber auch eine ältere (gebrauchte) Fritzbox.
Allerdings bootet diese meist deutlich behäbiger.

Experimente mit **RaspberryPI mit OpenWRT** haben uns nicht zufriedengestellt und kommen kaum günstiger als die FritzBox 4040.

Experimente mit **FritzBox 7170** haben gezeigt, dass sich diese nach dem Hochladen von Config-Dateien bei der UDP-Kommunikation verheddert.
Der Status der Module ging auf rot.
Erst das Trennen des PCs vom Modellbahn-WLAN und erneutes Verbinden brachte das System wieder ans Laufen.
Mit einer (neueren) **FritzBox 3272** trat das Problem hingegen nicht auf.
    