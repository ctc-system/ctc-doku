---
layout: page
title:  "Einbauanleitungen"
---
![Werkstatt]({% link assets/images/anschliessen/werkstatt.jpg %})

## Weichen mit Doppelspulen-Antrieb

Der Doppelspulen-Antrieb ist der **Standard-Antrieb in der Spur H0**, manchmal auch liebevoll "Klack-Klack" genannt.
Er besteht aus zwei Elektromagneten, jeweils einer für die beiden Schaltrichtungen.
Angeschlossen wird er mit drei Kabeln: Eine gemeinsame Stromversorgung und je eine Schaltleitung pro Elektromagnet.

**Hinweis:** Manche Doppelspulen-Antriebe schalten nicht oder unzuverlässig ab, wenn die Endlage erreicht ist.
Wenn diese mit einem Dauerstrom geschaltet werden, brennt schon nach wenigen Sekunden der Elektromagnet durch oder verklebt mit Kunststoffführungen.
Solche Schäden sind in der Regel irreparabel.

| CTC-Modul    | Hersteller: Produkte | Artikel                                    |
|--------------|----------------------|--------------------------------------------|
 {% for conn in site.anschliessen %}{%if conn.umbauart == "Doppelspule" %} | {{conn.modul}}  | **{{conn.hersteller}}**: {{conn.produkte}} | [{{conn.title}}]({{ site.baseurl }}{{ conn.url }}) |
{%endif%}{% endfor %}

## Motorische Weichen (2-adrig)
                 
Antriebe mit einem Motor, der je nach Schaltrichtung mit invertierter Polarität angesteuert wird.
Eine Besonderheit hierbei ist der **LGB-Weichenantrieb**, der klingt wie ein Doppelspulen-Antrieb (Klack-Klack), tatsächlich aber einen Motor enthält.  

| CTC-Modul    | Hersteller: Produkte | Artikel                                    |
|--------------|----------------------|--------------------------------------------|
 {% for conn in site.anschliessen %}{%if conn.umbauart == "Motor-2pol" %} | {{conn.modul}} | **{{conn.hersteller}}**: {{conn.produkte}} | [{{conn.title}}]({{ site.baseurl }}{{ conn.url }}) |
{%endif%}{% endfor %}

## Motorische Weichen (3-adrig)

Motorgetriebene Weichen ermöglichen ein vorbildgerecht langsames und sanftes Umschalten der Weiche.
Um ein Ersetzen der Doppelspulen-Antrieb möglichst einfach zu machen, bieten diverse Hersteller solche Weichen mit dreiadrigem Anschluss an.  

| CTC-Modul    | Hersteller: Produkte | Artikel                                    |
|--------------|----------------------|--------------------------------------------|
 {% for conn in site.anschliessen %}{%if conn.umbauart == "Motor-3pol" %} | {{conn.modul}}  | **{{conn.hersteller}}**: {{conn.produkte}} | [{{conn.title}}]({{ site.baseurl }}{{ conn.url }}) |
{%endif%}{% endfor %}

