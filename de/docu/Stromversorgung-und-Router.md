---
layout: page
title:  "Stromversorgung und Router"
---
Dieses Dokument widmet sich der Infrastruktur, die Sie ja bei einer Modellbahnanlage nur einmalig aufbauen müssen.
Bei CTC sind das die Stromversorgung und der WLAN-Router.

## Stromversorgung

**Achtung, eine ungeeignete Spannungsversorgung kann die CTC-Module irreparabel zerstören!**

Für eine Modellbahn, die ausschließlich mit CTC-Modulen betrieben wird, empfehlen wir die Anschaffung eines kurzschlussfesten Schaltnetzteils für Gleichstrom.
Bei Reichelt erhalten Sie z. B. folgende empfehlenswerte Tischnetzteile:  
* [MeanWell Tischnetzteile 90W](https://cdn-reichelt.de/documents/datenblatt/D500/GST90A_DS-EN.PDF)
* [MeanWell Tischnetzteile 280W](https://cdn-reichelt.de/documents/datenblatt/D500/GST280A_DS-EN.PDF)

Wer es gerne flexibler hat, kann auch zu einem Labornetzteil greifen, z. B. [PeakTech 6226](https://www.reichelt.de/labornetzgeraet-0-30-v-0-10-a-stabilisiert-peaktech-6226-p214837.html).
Die haben den Vorteil, dass eine Anzeige für Spannung und Strom eingebaut ist, aber auch folgende Nachteile:
* Man kann die Spannung versehentlich verstellen und dann beim nächsten Einschalten auf einen Rutsch alle CTC-Module zerstören.
* Ein Labornetzteil schaltet bei Kurzschluss in der Regel nicht ab, sondern beschränkt nur den Strom auf einen einstellbaren Wert.
             
Wenn Sie einen Mischbetrieb mit analoger oder alter digitaler Welt vorhaben, müssen Sie folgendes bedenken:
* Die CTC-Module sind im Auslieferungszustand weder für Wechselstrom (AC) noch für ungeglätteten Gleichstrom (DC) geeignet.
  Vor allem alte Modellbahntrafos liefern gerne ungeglätteten Gleichstrom.
* An sogenanntem Digitalstrom (Wechselstrom mit ca. 10 kHz) können CTC-Module ohne Bedenken betrieben werden.
* Wenn Sie eine separate Spannungsversorgung für Ihre CTC-Module nutzen, ist darauf zu Achten, dass es wirklich keine Querverbindungen zw. CTC-Modul und Gleisstrom gibt.
  Ein Klassiker ist hier z. B. die alte Märklin-Weiche, bei der über die Weichenlaterne eine Querverbindung zwischen Gleis und Weichenantrieb besteht.
  Bei Wechselstrom auf dem Gleis, wird ohne eine schützende Diode über diese Birne, das CTC-Modul zerstört.
                  
### Strombedarf

Bei der Auswahl eines neuen Netzteils stellt sich natürlich auch die Frage, wieviel Strom dieses liefern muss?
Dazu ein paar Anhaltspunkte:
* Den Strombedarf der CTC-Module können Sie den jeweiligen Datenblättern entnehmen.
* Hinzu kommt der Strombedarf der gleichzeitig fahrenden Lokomotiven.
* Bei eingeschalteter Beleuchtung spielen vor allem die guten alten Glühbirnen eine wichtige Rolle.
* Die Weichenantriebe brauchen zwar viel Strom, aber eben nur so lange sie schalten.
  Hier ist am ehesten der Einschaltstrom relevant, da die CTC-Module Weichen in der Regel zu Beginn auf ihre Default-Position stellen.
         
Am einfachsten kommt man hier zu einem vernünftigen Wert, indem man misst, v. a. den Einschaltstrom und vom gemessenen Wert (sehr) großzügig aufrundet.

Zum Reduzieren des Einschaltstroms gibt es folgende Möglichkeiten:
* Sie legen in der Config als Default ihrer Weichen einen Wert fest, den es nicht gibt.
  Dann ist die Weiche allerdings aus Sicht der CTC-App in einem undefinierten Zustand.
* Zusätzlich zum vorherigen Punkt können Sie mithilfe von Triggern oder auf Knopfdruck die Weichen nacheinander initialisieren.
  Bei Gelegenheit werden wir diesem Thema bestimmt noch einen Artikel spendieren.
  Bis dahin können Sie uns bei Bedarf gerne kontaktieren. 

### Wechselstrom und ungeglätteter Gleichstrom

Grundsätzlich möglich ist die Nutzung von CTC-Modulen, auch bei Wechselstrom und ungeglättetem Gleichstrom.
Allerdings müssen hier – vor Inbetriebnahme – Maßnahmen zum Schutz der CTC-Module ergriffen werden:
* Bei allen CTC-Modulen muss ein geeigneter Stützkondensator zum Glätten der Eingangsspannung vorhanden sein.
  Aktuelle Weichenmodule und IO-Boards sind bereits mit einem solchen Stützkondensator ausgestattet. 
* Es darf keine ungeschützten Querverbindungen zw. CTC-Modul und Gleisstrom geben (s. u.)
* Alte Trafos neigen dazu bei Kurzschlüssen mit heftigen Überspannungen zu reagieren.
  Siehe dazu unten den Abschnitt "Überspannungsschutz".

### Querverbindungen schützen
                   
Hier einige Beispiele von Querverbindungen und wie diese geschützt werden können:
* Alte Märklin-Weiche (M-Gleis): Zwischen die Birne der Weichenlaterne und dem Weichenantrieb eine schützende Diode einbauen.
* Alte Märklin-Lokomotiven, bei denen ein Pol der Beleuchtung fest mit dem Gehäuse verbunden ist: Zwischen die Birnen der Front-/Heckbeleuchtung und dem Anschluss am CTC-Lokmodul-H0a eine schützende Diode einbauen. 

### Überspannungsschutz

Alle Leistungsausgänge (Motor, Schaltausgänge, Halbbrücken) der CTC-Module sind gegen Kurschluss und Überlast gesichert, aber am Eingang darf die Maximalspannung von 24 Volt nicht überschritten werden.
Bei Trafos ohne elektronische Regelung kann es nun passieren, dass im Fall eines Kurzschlusses eine kurzzeitige **hohe Induktionsspannung am Gleis-Eingang** der CTC-Module entsteht, die diese beschädigt oder zerstört.
Bei Verwendung von alten Trafos ist diese Gefahr besonders groß.

Aber auch ein Schaltnetzteil versucht einen Stromanstieg, wie er durch Kurzschluss entsteht, auszugleichen.
Dabei sind Überschwingungen der Regelung nicht zu verhindern, sollten aber eigentlich so kurz ausfallen, dass die daran hängende Elektronik keinen Schaden nimmt.
Es wird aber kaum Netzeile geben, die speziell für den Anwendungsfall Gartenbahn entworfen wurden und schon gar nicht für CTC.
Somit kann es also durchaus sein, dass auch bei Verwendung enes modernen Schaltnetzteils für CTC-Module tödliche Überspannungen entstehen.

Auch das Zusammenspiel der restlichen in der Modellbahn verbauten Elektrik/Elektronik kann Überspannungen verstärken oder die Regelung eines Schaltnetzteils austricksen.
So können viele ungünstige Faktoren in Summe gerade den Tick zu viel für CTC-Module oder auch andere Elektronik sein.

Da dieses Wissen auf konkreten Kundenerfahrungen basiert, haben wir uns nach einem Schutz umgesehen.
TVS-Dioden sind die elektronischen Bauteile für diesen Zweck, z.B. die [SA24CA von Littlefuse](https://www.littelfuse.com/products/overvoltage-protection/tvs-diodes/leaded/sa/sa24ca).
Ab Februar 2025 legen wir allen [CTC-Schaltmodulen]({% link de/docu/CTC-Schaltmodule.md %}) und [CTC-Lokmodulen]({% link de/docu/CTC-Lokmodule.md %}) für Spur-G eine solche TVS-Diode bei.
Bei den kleinen Spuren sollte es reichen, wenn TVS-Dioden im Gleis verbaut sind.
Für bereits gelieferte CTC-Module legen wir die TVS-Dioden der nächsten Bestellung bei.

Die Dimensionierung der Dioden ist dabei ein Kompromiss zwischen Baugröße und Schutz.
Ob sie dem Spannungssprung eines Trafos standhalten muss die Praxis zeigen - Garantie können wir dafür keine geben. 
Insofern können wir nur empfehlen den alten Trafo durch ein modernes elektronisch geregeltes Netzteil zu ersetzen, damit bei Kurzschlüssen keine Überspannung mehr entsteht.

Die TVS-Diode wird einfach zwischen den beiden Gleisanschlüssen (Gleis-A und Gleis-B) platziert.
Da sie verpolungssicher ist, kann dabei nichts schiefgehen.
Ein Einbau in die Stromzuführung zum Gleis oder einfach an einer beliebigen Stelle im Gleis ist ebenfalls denkbar.
Wenn TVS-Dioden an mehreren Stellen der Anlage verbaut sind, schützen sie die CTC-Module im Falle eines Kurzschlusses.
Aber auch elektrostatischen Aufladungen (ESD) werden durch die TVS-Dioden vermieden.
                                   
Hier zwei Beispiele für den Einbau einer TVS-Diode, links im Gleis und rechts am Gleis-Eingang des CTC-Weichenmodul-G2:

![TVS-Dioden eingebaut]({% link assets/images/TVS-Diode.jpg %})

[TVS-Dioden kaufen]({{site.baseurl}}/shop.html#!/5-TVS-Dioden-Uberspannungsschutz/p/726293209/category=176900083){: .button}

## WLAN-Router

Grundsätzlich funktionieren sollte alles, was 2,4 GHz und WPA2-Verschlüsselung unterstützt.
Die CTC-Module und die CTC-App müssen uneingeschränkt per UDP und HTTP miteinander kommunizieren dürfen.
Wenn der Router auch über 5 GHz verfügt kann diese Frequenz für Tablet, SmartPhone und PC genutzt werden und so die Zuverlässigkeit noch mal erhöht werden, da dann alle Datenpakete den Weg über den Router nehmen müssen. 
Aufgrund guter Erfahrungen und der weiten Verbreitung haben wir uns für die FRITZ!Box 4040 als [CTC-Router]({% link _data_sheet/CTC-Router.md %}) entschieden.

Wenn Sie einen CTC-Router bestellen, egal ob im Starter-Set oder separat, erhalten Sie diesen komplett fertig konfiguriert.
Da uns dann der Netzwerkname (SSID) und die Netzwerkkennung (Passwort) bekannt sind, erhalten Sie Ihre CTC-Module von uns immer bereits für Ihr Modellbahn-WLAN eingebucht.
Dieser Service ist der Grund für den Preisaufschlag des CTC-Routers gegenüber einer serienmäßigen FRITZ!Box.
  
Grundsätzlich spricht nichts dagegen, dass Sie Ihr Modellbahn-WLAN selbst konfigurieren.
Das Einbuchen der CTC-Module ist denkbar einfach, wie Sie im [Kapitel 3 - Module ins WLAN einbuchen]({% link _app_doku/031-module-einbuchen.md %}) nachlesen können.
Die Konfiguration des WLANs kann aber durchaus eine Herausforderung darstellen.
**Wenn Sie sich darin nicht sicher fühlen, empfehlen wir Ihnen den CTC-Router bzw. das Starter-Set.**

Bitte haben Sie Verständnis dafür, dass wir Ihnen bei der Installation uns unbekannter Router in der Regel nicht oder nur wenig helfen können.
                
### WLAN-Router im Garten

Beim Betrieb im Garten ist natürlich auf Outdoor-Tauglichkeit zu achten.

Für unsere Gartenbahn setzen wir im Haus (z. B. in der Lok-Garage) einen CTC-Router (FRITZ!Box) ein und verlängern das WLAN mit einem Outdoor-Accesspoint (TP-Link AC1200).

