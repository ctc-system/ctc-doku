---
layout: page
title:  "Technische Dokumentation"
---
## Grundkonzept

CTC ist ein System aus Hard- und Software zur Steuerung von Modellbahnen. Es hat die folgenden Merkmale:
* **CTC-Module**: Elektronische Baugruppen zur Ansteuerung von Lokomotiven, Weichen, Signalen, Beleuchtungen, Aktions-Bausteine, etc. Auf jedem CTC-Modul übernimmt lokale Firmware und Konfiguration die lokalen Steuerungsaufgaben, wie Motoransteuerung, Weichenschaltung, Lichter an/aus, etc.
  Die jeweilige Konfiguration ist dezentral im zugehörigen CTC-Modul gespeichert.
* **CTC-App**: die Software dient als Anwenderschnittstelle (Graphische Anwenderschnittstelle) und kann sowohl auf einem Smart-Phone, einem Tablet mit Android oder iOS oder auch auf einem PC mit Windows, MAC oder Linux eingesetzt werden.
  Eine dedizierte und kostenaufwendige spezielle zentral Steuereinheit ist nicht erforderlich.
* CTC Module werden über die CTC-App konfiguriert.
* **Kommunikation über WLAN**: über einen dedizierten WLAN Access Punkt (z.B. eine FritzBox) findet die Kommunikation zwischen CTC-App und CTC-Modulen sowie der CTC-Module untereinander statt.
  Die Übertragungsgeschwindigkeit beträgt bis zu 54 Mbit/s.
  Außer der Spannungsversorgung ist keinerlei Verkabelung notwendig.
* Über die CTC-App werden die CTC-Module angesprochen.
  Die graphische Anwenderschnittstelle ermöglicht es beispielsweise Fahrgeschwindigkeit,  Weichenstellung,  Signallichter etc zu steuern.
* **Exakte Positionsermittlung**: mit Hilfe dedizierter Infrarot-Sender im Gleis und Infrarot-Empfänger in der Lokomotive kann die CTC-App die exakte Position bestimmen.
* Ein **Parallelbetrieb** mit anderen „marktgängigen“ Digital- und Analog-Systemen ist möglich.

## Die CTC-App

![Screenshot CTC-App Desktop]({% link assets/images/Screenshot_PC_Test-Anlage.png %})

![Screenshot CTC-App Android]({% link assets/images/Screenshot-Android-App.png %})

Die CTC-App erlaubt die Steuerung von Lokomotiven, Weichen, Beleuchtung, Aktions-Bausteinen, ... vom PC mit Linux, Mac oder Windows, sowie vom SmartPhone und Tablet mit Android und bald auch iOS.
Oben dargestellt ist die PC-Variante.

Die App speichert keinerlei Daten über die Modellbahn.
Beim Start liest sie alle notwendigen Informationen aus den jeweiligen CTC-Modulen und baut daraus ihre Oberfläche auf.
Auch die Bilder der Lokomotiven und das am unteren Rand der App angezeigte Gleisbild werden aus den entsprechenden CTC-Modulen ausgelesen.

Die Konfiguration der CTC-Module, das Aufspielen von Firmware und das (einmalige) Einbuchen von CTC-Modulen in das Modellbahn-WLAN erfolgt komfortabel über die CTC-App.
Ein ganz besonderes Feature ist das automatische Einmessen des Motor-Sensors von Lok-Modulen und das grafisch unterstützte Tuning der Motor-Parameter (PID-Regler).

Die App koordiniert die Kommunikation mit den Modulen, um auch bei großen Modellbahnen mit sehr vielen CTC-Modulen eine reibungslose Kommunikation zu gewährleisten.

## Installation

Software:
* Die App für Android wird im Moment noch als apk-Datei geliefert.
  Zukünftig werden Apps für Android und dann auch iOS über den jeweiligen App-Store installiert.
* Die PC-Applikation wird als zip-Datei geliefert und kann nach dem Auspacken direkt gestartet werden.
* Der [Access-Point (Router)]({% link de/docu/WLAN-Access-Point.md %}) wird durch uns komplett installiert geliefert.
  Folglich liegen uns alle Angaben wie die SSID und das Passwort des WLANs vor.
  Diese Angaben werden Ihnen in einem Protokoll mitgeliefert.
* Sie können natürlich auch selber einen WLAN-Router konfigurieren. 
* In beiden Varianten können wir die Device-Module komplett konfiguriert liefern.
  Die App enthält Menüpunkte, um Device-Module ins WLAN ein-/umzubuchen und zu konfigurieren.

Hardware:
* Der Einbau des [CTC-Lokmoduls]({% link de/docu/CTC-Lokmodule.md %}) in eine Lokomotive ist vergleichbar mit dem Einbau eines Decoders für klassische Digitalsteuerungen:
  Grundsätzlich sind 4 Drähte anzuschließen, nämlich 2x zu den Schienen und 2x zum DC-Motor mit Permanentmagneten im Stator.
  Auch gewickelte Statoren sind möglich.
  Zusatzfunktionen wie Licht und Entkuppler wären dann auch noch zu verdrahten.
* Der Einbau der CTC-Module in Weichen ist ebenfalls vergleichbar mit dem Einbau eines Decoders für klassische Digitalsteuerungen.
  Für das Märklin C-Gleis gibt es das [CTC-Weichenmodul]({% link _data_sheet/CTC-Weichenmodul-H0.md %}) zum Einbau in die Weiche mit derselben Bauform wie die Märklin-Dekoder.
  Für alle anderen Gleissysteme kann entweder das [CTC-Weichenmodul]({% link _data_sheet/CTC-Weichenmodul-H0.md %}) oder ein [CTC-Multi-I/O-Board]({% link _data_sheet/CTC-Multi-IO-Board.md %}) verwendet werden.
  Zum Anschluss der Kabel bieten wir verschiedene gängige Steck- und Schraubsysteme an.
* Jeweils zwei IR-Balisen zur Positionsbestimmung können an ein [CTC-Weichenmodul]({% link _data_sheet/CTC-Weichenmodul-H0.md %}) oder ein [CTC-Multi-I/O-Board]({{ site.baseurl }}/data_sheet/CTC-Multi-IO-Board.html) angeschlossen werden    
  
Device-Modul ins WLAN einbuchen:
* Konfigurationsmodus: Ein neues Device-Modul (ohne WLAN-Konfiguration) spannt ein eigenes WLAN auf und wartet darauf, konfiguriert zu werden.
* Findet ein bereits konfiguriertes Device-Modul sein WLAN nicht, so schaltet es nach einer Minute in den Konfigurationsmodus, wartet eine Minute auf eine Verbindung und setzt sich dann automatisch zurück.
* Die CTC-App kann auf Knopfdruck im Konfigurationsmodus befindliche Module erkennen und in die WLAN-Konfiguration des Device-Moduls schreiben.
  Nach dem Schreiben der WLAN-Konfiguration setzt sich das Device-Modul automatisch zurück und bucht sich dann ins Modellbahn-WLAN ein.

Steuerungs-Gerät ins WLAN einbuchen:
* Das Gerät (PC, Tablet, SmartPhone) auf dem sich die CTC-App befindet, wird wie für das Gerät üblich ins LAN oder WLAN eingebucht.

## Inbetriebnahme (Boot-Vorgang)

Sobald nach Start des Access-Points das WLAN zur Verfügung steht, können die Steuerungs-Geräte mit dem LAN / WLAN verbunden werden, so wie es für das jeweilige Gerät üblich ist.

Die fertig installierten Device-Module verbinden sich selbständig mit dem WLAN, sobald sie Strom bekommen.

Die CTC-App erkennt selbständig alle im selben WLAN vorhandenen Device-Module.
Später startende Device-Module, werden in der App sichtbar, sobald sie im WLAN eingebucht sind.
Das Einbuchen eines Device-Moduls dauert eine bis wenige Sekunden, sofern die Empfangsqualität gut ist.
Ist ein Funkloch vorhanden, gibt es die üblichen Möglichkeiten zur Verbesserung der Funkabdeckung, z.B. Repeater.

Für die, an Device-Modulen angeschlossenen Aktoren (z.B. Weiche, Signal), kann in der Device-Konfiguration eine Grundstellung eingetragen werden.
Diese wird direkt nach dem Start des Moduls eingenommen, also z. B. Weiche geradeaus oder Signal auf Halt.
Alternativ kann der initiale Stand über einen Sensoreingang gelesen werden.
Die Änderung der Konfiguration erfolgt menügeführt in der CTC-App.

## Gleisbild

Mithilfe der CTC-App kann ein einfaches Gleisbild, in Form eines Rasters erstellt werden, so wie man es inzwischen von vielen Modellbahn-Zentralen oder -Steuerungsprogrammen kennt.
Wie bei CTC üblich, wird dieses in den Modellbahnmodulen gespeichert, und zwar wie folgt:
* Für jedes zusammenhängende Segment der Modellbahn (z. B. eine Platte oder ein Norm-Modul) sucht man sich, eine auf diesem Segment fest eingebaute Weiche oder IO-Modul, aus. In diesem Modul wird der Gleisplan des Segments abgelegt. 
* Für jede einzelne Weiche, Signal, Schaltfunktion, Leuchte, ... wird im zugehörigen -Modul, dessen Koordinate innerhalb des Segment-Gleisplans liegt, gespeichert.
* Für die gesamte Modellbahn wird ein weiteres Modul ausgesucht, in dem die Anordnung der einzelnen Segmente, zu einem Gesamt-Gleisplan gespeichert werden. Dabei kann die Anlage auch in mehrere Gleisplanabschnitte bzw. Ebenen unterteilt werden.

Die Bearbeitung des Gleisplans erfolgt komfortabel über die CTC-App.

Beim Start der App findet diese den Gesamt-Gleisplan und bringt ihn zur Anzeige.
Nun können Weichen, Signale, ... über die Gleisbild-Anzeige geschaltet werden.
Die jeweilige Schaltposition wird selbstverständlich angezeigt. 

## Positionsermittlung

![Lokmodul]({% link assets/images/tech-doc/Lok-und-IR-Gleis.jpg %})

Mithilfe von Infrarot-Signalen kann eine CTC-Lokomotive ihre Position auf wenige Zentimeter genau bestimmen.
Dazu können an jedem Weichen- und IO-Modul zwei Infrarot-Sender angeschlossen werden.
Diese werden dann ins Gleisbett eingebaut.
Außerdem muss an das Lok-Modul ein Infrarot-Empfänger angeschlossen und an der Unterseite der Lok montiert werden.
    
Über die App wird nun jedem Infrarot-Sender eine ID zugeteilt und der Abstand in Zentimeter zur IR-Balise davor eingetragen.
Diese besteht aus der ID des zugehörigen Gleisabschnitts (2 Zeichen) und einer laufenden Nummer.
Die IR-Balisen eines Gleisabschnitts werden im Uhrzeigersinn aufsteigend nummeriert.
Außerdem wird die Position der IR-Balise im Gleisplan vermerkt.

Die IR-Balise sendet nun zyklisch ihre ID, den Abstand zur vorhergehenden IR-Balise und optional ein Kommando.
Überfährt die Lok die IR-Balise, so kann sie mithilfe der gelesenen ID und dem Abstand ihre Fahrtrichtung und Durchschnittsgeschwindigkeit seit der letzten gelesenen IR-Balise ermitteln.
Die Lok überträgt die gelesene ID und die ermittelte Geschwindigkeit an die CTC-App, woraufhin diese die Position der Lok inklusive Fahrtrichtung anzeigen kann.

Streckenabschnitte, die zwischen zwei IR-Balisen eine konstante Steigung (bzw. Gefälle) haben und komplett gerade oder konstant gebogen sind, können auch zum Einmessen des Motor-Sensors des Lok-Moduls verwendet werden.
In einem passenden Rundparcours kann die CTC-App dieses Einmessen automatisch durchführen.

Mithilfe eines gut eingemessenen Motor-Sensors, ist die Lok nun in der Lage, ihre Geschwindigkeit (in mm/s) und auch ihren Abstand zur letzten IR-Balise zu schätzen.
Verfügt die Modellbahnanlage nun ca. alle 60 bis 100 cm über eine IR-Balise, so wird die Positionsermittlung auf wenige Zentimeter genau.  