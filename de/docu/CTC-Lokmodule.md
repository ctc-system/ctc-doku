---
layout: page
title:  "CTC-Lokmodule"
tags: Lokmodul Lok
---

![Lok und CTC-Module]({% link assets/images/Loks-umbauen-Bild1.jpg %})

Wie bei den Digital-Dekodern gibt es auch bei CTC kein ganz universelles Lokmodul.
Zu verschieden sind die inneren Anschlüsse und Platzverhältnisse der Lokomotiven.

## Gemeinsame Eigenschaften aller CTC-Lokmodule                                                
                                                
| Eigenschaft         | Beschreibung                                                          |
|---------------------|-----------------------------------------------------------------------|
| Eingangsspannung    | ca. 9 V bis 24 V DC, Digitalstrom oder AC (mit Stützkondensator)      |
| Schnittstellen      | [CTC-IR-Empfänger]({% link _data_sheet/CTC-IR-Sender.md %}), I2C oder [CTC-NFC-Reader]({% link _data_sheet/CTC-NFC-Reader.md %})  |

Ab Februar 2025 legen wir allen Lokmodulen für Spur-G eine TVS-Diode bei.
Bei den kleinen Spuren sollte es reichen, wenn TVS-Dioden im Gleis verbaut sind.
Siehe dazu Abschnitt "Überspannungsschutz" auf der Seite ["Stromversorgung und Router"]({% link de/docu/Stromversorgung-und-Router.md %}).

## Übersicht CTC-Lokmodule (ab 62,- EUR)

| CTC-Lokmodul                                                         | Optimiert für      | Maße         | Motor-Ausgang          | Schalt-Ausgänge                         | Servos | Summe Strom max. |
|----------------------------------------------------------------------|--------------------|--------------|------------------------|-----------------------------------------|--------|------------------|
| [CTC-Lokmodul-21mtc]({% link _data_sheet/CTC-Lokmodul-21mtc.md %})   | Märklin 21mtc      | 30 x 15,5 mm | 1x 1,0 A               | 6 LowSide + 2 digital                   | 2      | 2 A              |
| [CTC-Lokmodul-PluX22]({% link _data_sheet/CTC-Lokmodul-PluX22.md %}) | PluX-22            | 30 x 16 mm   | 1x 1,0 A               | 8 LowSide bzw. 8x Halbbrücke            | 2      | 2 A              |
| [CTC-Lokmodul-G2]({% link _data_sheet/CTC-Lokmodul-G.md %})          | LGB / große Spuren | 78 x 30 mm   | 2x 1,0 A oder 2x 2,0 A | 6x 1,2 A LowSide<br>4x 2,5 A Halbbrücke | 2      | 15 A             |

<br/>

Nicht mehr angebotene Lokmodule:
* [CTC-Lokmodul-H0d]({% link _data_sheet/CTC-Lokmodul-H0d.md %})
* [CTC-Lokmodul-H0a]({% link _data_sheet/CTC-Lokmodul-H0a.md %})

**Hinweise:**
* **Der Betrieb der CTC-Lokmodule an Wechselstrom (AC) oder nicht geglättetem Gleichstrom ohne Stützkondensator führt zum irreparablen Schäden am CTC-Modul.**
* Schaltausgänge werden danach unterschieden, ob sie Masse bzw. Minus- (LowSide) oder den Pluspol (HighSide) schalten.
  Vor allem bei LED-Platinen an der Front, existiert oft für einen Pol eine gemeinsame Leitung.
  Dann ist es wichtig, ob die Schaltausgänge des Lokmoduls als High- oder LowSide ausgeführt sind.
* Eine Halbbrücke kann zwischen Plus- und Minuspol umgeschaltet werden, ist aber nie ausgeschaltet.
  Vor allem sogenannte Magnetartikel (z.B. Entkuppler) können bei falscher Konfiguration der Halbbrücke, dauerhaft Schaden nehmen und sollten deshalb nicht an eine Halbbrücke angeschlossen werden.  
* Für Servos wird zusätzlich ein Stromversorgungs-Modul benötigt.
* **Wenn Verbraucher (Lampen, Rauchgeneratoren, ...) in der Lok eine Verbindung zum Gehäuse haben, dürfen diese nur über eine Schutzdiode an das CTC-Modul angeschlossen werden.** 

## Lokumbau

Auf der **[Seite Lokumbau]({% link de/docu/Lokumbau.md %})** finden Sie allgemeines zum Lokumbau sowie diverse Lok-Umbauten in Text und Bild dokumentiert.
Diese werden regelmäßig ergänzt.
