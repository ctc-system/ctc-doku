---
layout: page
title:  "Digital-Dekoder"
---
Die digitale Modellbahn mit DCC, Motorola-Format und mfx gehören eigentlich der Vergangenheit an, wenn Sie CTC nutzen.

Leider werden neue Loks aber eben meist mit Digital-Dekodern ausgeliefert.
Und die Hersteller geben sich viel Mühe viele schöne Sound-Effekte auf diese Digital-Dekoder zu packen.
Es ist also naheliegend, dass der eine oder andere CTC-Kunde gerne diese Sounds auch beim Betrieb mit CTC zur Verfügung haben möchte.

Deshalb gibt es die Möglichkeit ein CTC-Modul zwischen Gleis und den klassischen Digital-Dekoder anzuschließen.
Voraussetzung dafür ist natürlich genügend Platz in der Lokomotive, denn nun müssen ja sowohl der alte Digital-Dekoder als auch ein CTC-Modul untergebracht werden. 

Derzeit wird von den CTC-Modulen nur das DCC-Protokoll unterstützt.
Da alle aktuellen Märklin mfx-Dekoder sogenannte Multiprotokoll-Dekoder sind, können diese ebenfalls per DCC angesteuert werden.
Eine Unterstützung vom alten Motorola-Format bzw. mfx werden wir bei Bedarf nachreichen.  

## "Mini-Zentrale" zum Testen

Für einen ersten Test können Sie das CTC-Lokmodul auch als Mini-Zentrale betreiben.
Dazu schließen Sie ein Gleis (oder Rollenprüfstand) an den Motorausgang des CTC-Lokdmoduls an und belassen den DCC-Dekoder in der Lok.
An den Gleiseingang des CTC-Lokmoduls schließen Sie dann eine Gleichstromquelle an.
Einen Stützkondensator lohnt es sich ebenfalls schon an das CTC-Lokmodul anzuschließen.

Mit der Lok auf dem Gleis bzw. Rollenprüfstand können Sie nun schon vor dem Lokeinbau die Konfiguration vornehmen und testen.

## Lokeinbau
 
Beim Lokeinbau wird das CTC-Modul wie sonst auch mit den beiden Gleisanschlüssen verbunden.
Für den Anschloss des Digital-Dekoders gibt es nun zwei Möglichkeiten:
* Im einfachsten Fall wird der Digital-Dekoder dann statt des Motors an den Motorausgang des CTC-Moduls angeschlossen.
* Alternativ kann der Digital-Dekoder auch an zwei Halbbrücken angeschlossen werden.
  Dann besteht sogar die Möglichkeit, den Motor über CTC zu steuern und parallel dazu Sounds von Digital-Dekoder abzurufen.
 
Für Licht und sonstige Schaltfunktionen haben Sie in beiden Fällen die freie Wahl, ob Sie diese am Digital-Dekoder belassen oder ans CTC-Modul anschließen. 

**Hinweis:** Das CTC-Lokmodul-H0a kann derzeit nicht für DCC genutzt werden.
Alle anderen CTC-Lokmodule sind für DCC geeignet.   

Bebilderte Umbau-Anleitungen folgen demnächst.

## Konfiguration

Wie man einen Digital-Dekoder konfiguriert, steht in der Bedienungsanleitung [Kapitel 4.6 - Config - DCC-Dekoder anschließen]({% link _app_doku/046-DCC-dekoder-anschliessen.md %}).











