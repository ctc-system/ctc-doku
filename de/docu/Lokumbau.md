---
layout: page
title:  "Lokumbau"
tags: Lokmodul Lok
---
![Umgebaute Loks]({% link assets/images/Loks-umbauen-Bild2.jpg %})

Für den Umbau von Loks sind folgende Szenarien zu unterscheiden:

* analoge Lokomotiven
* Digital-Lokomotiven ohne Steckverbindung
* Lokomotiven mit genormter Steckverbindung

Weiter unten auf dieser Seite haben wir diverse Lok-Umbauten für Sie in Text und Bild dokumentiert und ergänzen diese regelmäßig.
 
### Analog-Lokomotiven

Für eine analoge Märklin-Lokomotive finden Sie im Artikel
[Umbau BR53 Märklin 3102]({% link _lokumbau/BR53_Ma-3102.md %}) eine ausführliche Dokumentation des Umbaus.

Dem LGB-Klassiker "Stainz" haben wir uns im Artikel
[Umbau LGB Stainz #2020 und #2021]({% link _posts/de/szenarien/2021-03-30-Umbau-LGB-Stainz.md %}) gewidmet.
    
Für PluX22 haben wir einen eigenen [Einbauadapter]({% link _data_sheet/Adapter-PluX22.md %}) mit großen Lötkontakten bzw. Schraubklemmen im Programm.  

### Digital-Lokomotiven ohne Steckverbindung

Der Umbau gestaltet sich grundsätzlich ähnlich, wie bei analogen Lokomotiven.
Allerdings ist darauf zu achten, wie die Beschaltung der Funktionen, v. a. der Beleuchtung gestaltet ist.
Hier gibt es oft schon Licht-Platinen mit einem gemeinsamen Versorgungsanschluss und mehreren Schaltanschlüssen.

**Hinweis:** Manchmal sind die für LEDs wichtigen Vorwiderstände nicht auf der Licht-Platine, sondern auf der Hauptplatine oder gar dem Dekoder.
Wenn Sie eine LED direkt an einen Schaltausgang des CTC-Lokmoduls anschließen, brennt die LED sofort durch.

### Lokomotiven mit genormter Steckverbindung

Für die folgenden genormten Steckverbindungen haben wir bereits passende Lokmodule:
* [CTC-Lokmodul-21mtc]({% link _data_sheet/CTC-Lokmodul-21mtc.md %})
* [CTC-Lokmodul-PluX22]({% link _data_sheet/CTC-Lokmodul-PluX22.md %})

Sollten Sie Module für andere Standards benötigen, teilen Sie uns das bitte mit – dann können wir unsere Entwicklungspläne an Ihre Bedürfnisse anpassen.
                
### Lokomotiven mit DCC-Sound-Dekoder

Anstatt einen bestehenden DCC-Dekoder durch ein CTC-Modul zu ersetzen, können Sie auch das CTC-Modul zwischen Gleis und DCC-Dekoder einbauen.
Dies bietet sich vor allem an, wenn Sie den ausgefeilten Sound Ihrer Lokomotive auch mit CTC nutzen möchten.

Wie das geht erklärt der Artikel [Umbau PIKO BR 50 mit DCC Sound]({% link _posts/de/szenarien/2021-07-25-Umbau-DCC-BR50.md %}).
                   
## Umbaudokumentation H0

{% assign date_format = site.date_format.text %}

### Märklin H0 Analog

Diese Umbauten wurden noch mit dem nicht mehr lieferbaren CTC-Lomodul-H0a durchgeführt

| Baureihe   | Artikel | Lokmodul | Besonderheiten | Stand   |
|------------|---------|----------|----------------|---------|
{% for lokumbau in site.lokumbau %}{%if lokumbau.umbauart == "H0-Ma-Analog" %} | {{lokumbau.baureihe}} | [{{lokumbau.hersteller}} {{lokumbau.artikelnr}}]({{ site.baseurl }}{{ lokumbau.url }}) | {{lokumbau.lokmodul}} | {{lokumbau.speziell}} | {{ lokumbau.date | date: date_format }} |
{%endif%}{% endfor %}

### Märklin H0 Analog PluX22

Dies ist die neuere

| Baureihe   | Artikel | Lokmodul | Besonderheiten | Stand   |
|------------|---------|----------|----------------|---------|
{% for lokumbau in site.lokumbau %}{%if lokumbau.umbauart == "H0-Ma-Analog-PluX22" %} | {{lokumbau.baureihe}} | [{{lokumbau.hersteller}} {{lokumbau.artikelnr}}]({{ site.baseurl }}{{ lokumbau.url }}) | {{lokumbau.lokmodul}} | {{lokumbau.speziell}} | {{ lokumbau.date | date: date_format }} |
{%endif%}{% endfor %}

### H0 Digital

| Baureihe   | Artikel | Lokmodul | Besonderheiten | Stand   |
|------------|---------|----------|----------------|---------|
{% for lokumbau in site.lokumbau %}{%if lokumbau.umbauart == "H0-Digital" %} | {{lokumbau.baureihe}} | [{{lokumbau.hersteller}} {{lokumbau.artikelnr}}]({{ site.baseurl }}{{ lokumbau.url }}) | {{lokumbau.lokmodul}} | {{lokumbau.speziell}} | {{ lokumbau.date | date: date_format }} |
{%endif%}{% endfor %}

### H0 21mtc

| Baureihe   | Artikel | Lokmodul | Besonderheiten | Stand   |
|------------|---------|----------|----------------|---------|
{% for lokumbau in site.lokumbau %}{%if lokumbau.umbauart == "H0-21mtc" %} | {{lokumbau.baureihe}} | [{{lokumbau.hersteller}} {{lokumbau.artikelnr}}]({{ site.baseurl }}{{ lokumbau.url }}) | {{lokumbau.lokmodul}} | {{lokumbau.speziell}} | {{ lokumbau.date | date: date_format }} |
{%endif%}{% endfor %}

### H0 21mtc mit SUSI3-Sound

| Baureihe   | Artikel | Lokmodul | Besonderheiten | Stand   |
|------------|---------|----------|----------------|---------|
{% for lokumbau in site.lokumbau %}{%if lokumbau.umbauart == "H0-21mtc-SUSI" %} | {{lokumbau.baureihe}} | [{{lokumbau.hersteller}} {{lokumbau.artikelnr}}]({{ site.baseurl }}{{ lokumbau.url }}) | {{lokumbau.lokmodul}} | {{lokumbau.speziell}} | {{ lokumbau.date | date: date_format }} |
{%endif%}{% endfor %}

### H0 PluX22

| Baureihe   | Artikel | Lokmodul | Besonderheiten | Stand   |
|------------|---------|----------|----------------|---------|
{% for lokumbau in site.lokumbau %}{%if lokumbau.umbauart == "H0-PluX22" %} | {{lokumbau.baureihe}} | [{{lokumbau.hersteller}} {{lokumbau.artikelnr}}]({{ site.baseurl }}{{ lokumbau.url }}) | {{lokumbau.lokmodul}} | {{lokumbau.speziell}} | {{ lokumbau.date | date: date_format }} |
{%endif%}{% endfor %}

### H0 NEM-652

| Baureihe   | Artikel | Lokmodul | Besonderheiten | Stand   |
|------------|---------|----------|----------------|---------|
{% for lokumbau in site.lokumbau %}{%if lokumbau.umbauart == "H0-NEM-652" %} | {{lokumbau.baureihe}} | [{{lokumbau.hersteller}} {{lokumbau.artikelnr}}]({{ site.baseurl }}{{ lokumbau.url }}) | {{lokumbau.lokmodul}} | {{lokumbau.speziell}} | {{ lokumbau.date | date: date_format }} |
{%endif%}{% endfor %}

### H0 mit Sinus-Motor

| Baureihe   | Artikel | Lokmodul | Besonderheiten | Stand   |
|------------|---------|----------|----------------|---------|
{% for lokumbau in site.lokumbau %}{%if lokumbau.umbauart == "H0-Sinus" %} | {{lokumbau.baureihe}} | [{{lokumbau.hersteller}} {{lokumbau.artikelnr}}]({{ site.baseurl }}{{ lokumbau.url }}) | {{lokumbau.lokmodul}} | {{lokumbau.speziell}} | {{ lokumbau.date | date: date_format }} |
{%endif%}{% endfor %}

## Umbaudokumentation Gartenbahn

### Gartenbahn Analog

| Baureihe   | Artikel | Lokmodul | Besonderheiten | Stand   |
|------------|---------|----------|----------------|---------|
{% for lokumbau in site.lokumbau %}{%if lokumbau.umbauart == "G-Analog" %} | {{lokumbau.baureihe}} | [{{lokumbau.hersteller}} {{lokumbau.artikelnr}}]({{ site.baseurl }}{{ lokumbau.url }}) | {{lokumbau.lokmodul}} | {{lokumbau.speziell}} | {{ lokumbau.date | date: date_format }} |
{%endif%}{% endfor %}

### Gartenbahn DCC

| Baureihe   | Artikel | Lokmodul | Besonderheiten | Stand   |
|------------|---------|----------|----------------|---------|
{% for lokumbau in site.lokumbau %}{%if lokumbau.umbauart == "G-DCC" %} | {{lokumbau.baureihe}} | [{{lokumbau.hersteller}} {{lokumbau.artikelnr}}]({{ site.baseurl }}{{ lokumbau.url }}) | {{lokumbau.lokmodul}} | {{lokumbau.speziell}} | {{ lokumbau.date | date: date_format }} |
{%endif%}{% endfor %}

### Gartenbahn mit DCC-Sound

| Baureihe   | Artikel | Lokmodul | Besonderheiten | Stand   |
|------------|---------|----------|----------------|---------|
{% for lokumbau in site.lokumbau %}{%if lokumbau.umbauart == "G-DCC-Sound" %} | {{lokumbau.baureihe}} | [{{lokumbau.hersteller}} {{lokumbau.artikelnr}}]({{ site.baseurl }}{{ lokumbau.url }}) | {{lokumbau.lokmodul}} | {{lokumbau.speziell}} | {{ lokumbau.date | date: date_format }} |
{%endif%}{% endfor %}

### Gartenbahn mit SUSI3-Sound

| Baureihe   | Artikel | Lokmodul | Besonderheiten | Stand   |
|------------|---------|----------|----------------|---------|
{% for lokumbau in site.lokumbau %}{%if lokumbau.umbauart == "G-SUSI-Sound" %} | {{lokumbau.baureihe}} | [{{lokumbau.hersteller}} {{lokumbau.artikelnr}}]({{ site.baseurl }}{{ lokumbau.url }}) | {{lokumbau.lokmodul}} | {{lokumbau.speziell}} | {{ lokumbau.date | date: date_format }} |
{%endif%}{% endfor %}

## Umbaudokumentation Spur I

| Baureihe   | Artikel | Lokmodul | Besonderheiten | Stand   |
|------------|---------|----------|----------------|---------|
{% for lokumbau in site.lokumbau %}{%if lokumbau.umbauart == "Spur-I" %} | {{lokumbau.baureihe}} | [{{lokumbau.hersteller}} {{lokumbau.artikelnr}}]({{ site.baseurl }}{{ lokumbau.url }}) | {{lokumbau.lokmodul}} | {{lokumbau.speziell}} | {{ lokumbau.date | date: date_format }} |
{%endif%}{% endfor %}

## Umbaudokumentation H0m

### H0m Analog

| Baureihe   | Artikel | Lokmodul | Besonderheiten | Stand   |
|------------|---------|----------|----------------|---------|
{% for lokumbau in site.lokumbau %}{%if lokumbau.umbauart == "H0m-Analog" %} | {{lokumbau.baureihe}} | [{{lokumbau.hersteller}} {{lokumbau.artikelnr}}]({{ site.baseurl }}{{ lokumbau.url }}) | {{lokumbau.lokmodul}} | {{lokumbau.speziell}} | {{ lokumbau.date | date: date_format }} |
{%endif%}{% endfor %}
